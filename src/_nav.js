/*import odontologia from '../../assets/img/icon/odontologia.png'*/

export default {
  items: [
    {
      name: 'CONSULTA',
      url: '/base',
      icon: 'fa fa-medkit',
      children: [
        { /* mi codigo*/
          name: 'Consulta Nueva',
          url: '/PrimeraConsulta',
          icon: 'fa fa-file-archive-o',
        },
        {
          name: 'Tratamiento',
          url: '/Tratamiento',
          icon: 'fa fa-stethoscope',
        },
      ],
    },
    {
      name: 'INSUMOS MEDICOS',
      url: '/insumos',
      icon: 'fa fa-thermometer-empty',
      children: [
        {
          name: 'Nuevo Bind Card',
          url: '/Insumos/BindCard',          
          icon: 'fa fa-folder-open',
        },       
        {
          name: 'Registrar Entrada',
          url: '/Insumos/RegistroEntrada',
          icon: 'fa fa-mail-forward (alias)',
        },
        {
          name: 'Registrar Salida',
          url: '/Insumos/RegistroSalida',
          icon: 'fa fa-mail-reply (alias)',
        },
        {
          name: 'Lista General',
          url: '/Insumos/ListaGeneral',
          icon: 'fa fa-file-text',
        },
        {
          name: 'Lista Detalle',
          url: '/Insumos/ListaDetalle',
          icon: 'fa fa-file-text-o',
        },        
        
      ],
    },
    {
      name: 'PEDIDOS',
      url: '/Pedido',
      icon: 'fa fa-file-text',
      children: [      
        {
          name: 'Pedido de medicamentos y/o insumos',
          url: '/Pedido/PedidoMedicamentoInsumo',
          icon: 'fa fa-file-text-o',
        },
        {
          name: 'Ver todo',
          url: '/Pedido/VerTodo',
          icon: 'fa fa-paste (alias)',
        },
      ]
    },
    {
      name: 'CAMPAÑAS DE PREVENCIÓN',
      url: '/CampPrevencion',
      icon: 'fa fa-calendar',
      children: [
        {
          name: 'Agregar Campaña y/o Tareas',
          url: '/CampPrevencion/CampTareas',
          icon: 'fa fa-calendar-plus-o',
        },
        {
          name: 'Detalle de Cronograma',
          url: '/CampPrevencion/VerAsignacionTareas',
          icon: 'fa fa-calendar-minus-o',
        },
        {
          name: 'Lista de Campañas Finalizadas',
          url: '/CampPrevencion/Cronograma/ListaCronogramaFin',
          icon: 'fa fa-calendar-times-o',
        },
        {
          name: 'Registra Nuevo Cronograma',
          url: '/CampPrevencion/Cronograma/Registrar',
          icon: 'fa fa-calendar-plus-o',
        },
        {
          name: 'Cronograma de Campañas',
          url: '/CampPrevencion/Calendario',
          icon: 'fa fa-calendar-check-o',
        },
        /*{
          name: 'Registrar Atención Extramural',
          url: '/CampPrevencion/Extramural/Lista',
          icon: 'icon-bell',
        },
        {
          name: 'Registrar Atención Intramural',
          url: '/CampPrevencion/Intramural/Lista',
          icon: 'icon-bell',
        },*/
        
      ],
    },
    /*{
      name: 'CRONOGRAMA DE ATENCIÓN',
      url: '/icons',
      icon: 'icon-star',
      children: [
        {
          name: 'Agregar/Eliminar',
          url: '/icons/coreui-icons',
          icon: 'icon-star',
          badge: {
            variant: 'info',
            text: 'NEW',
          },
        },
        {
          name: 'Agenda',
          url: '/icons/flags',
          icon: 'icon-star',
        },
        {
          name: 'Font Awesome',
          url: '/icons/font-awesome',
          icon: 'icon-star',
          badge: {
            variant: 'secondary',
            text: '4.7',
          },
        },
        {
          name: 'Simple Line Icons',
          url: '/icons/simple-line-icons',
          icon: 'icon-star',
        },
      ],
    },*/
    /*{
      name: 'Pages',
      url: '/pages',
      icon: 'icon-star',
      children: [
        {
          name: 'Login',
          url: '/login',
          icon: 'icon-star',
        },
        {
          name: 'Register',
          url: '/register',
          icon: 'icon-star',
        },
        {
          name: 'Error 404',
          url: '/404',
          icon: 'icon-star',
        },
        {
          name: 'Error 500',
          url: '/500',
          icon: 'icon-star',
        },
      ],
    },*/
    /*  {
        name: 'Download CoreUI',
        url: 'http://coreui.io/react/',
        icon: 'icon-cloud-download',
        class: 'mt-auto',
        variant: 'success',
      },
      {
        name: 'Try CoreUI PRO',
        url: 'http://coreui.io/pro/react/',
        icon: 'icon-layers',
        variant: 'danger',
        
      },
      
      */
    {
      name: 'MANUAL DE USUARIO',
      url: 'https://drive.google.com/file/d/1go0LAbsihM36Wz_ic9v80Sxww_5KG3SY/view?usp=sharing',
      icon: 'icon-cloud-download',
      class: 'mt-auto',
    },
  ],
};
