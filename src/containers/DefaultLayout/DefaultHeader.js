import React, { Component } from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem, NavLink } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppHeaderDropdown, AppSidebarToggler } from '@coreui/react';
//import logo from '../../assets/img/brand/logo.svg'
//import sygnet from '../../assets/img/brand/sygnet.svg';

/*import odontologia from '../../assets/img/icon/odontologia.png'*/


const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props) {
    super(props);

    this.state = {
      idMedico:localStorage.getItem("idMedico"),
      nomAp:localStorage.getItem("nombre"),
      cargo:localStorage.getItem("cargo"),
    };
  }

  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
         {/* <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: 'CoreUI Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo' }}
        /> */}
        {/* START CODE */}
        <NavLink href="#/dashboard"><h4><strong>ODONTOLOGIA</strong></h4></NavLink>
        {/* <h2>ENFERMERIA</h2> */}
        {/* FINISH CODE */}
        <AppSidebarToggler className="d-md-down-none" display="lg" />




        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink href="#/dashboard">SI-GESHO-PROMES</NavLink>
          </NavItem>

        </Nav>




        <Nav className="ml-auto" navbar>

          <AppHeaderDropdown  direction="down">
            <DropdownToggle nav>
              <img src={'assets/img/icon/docN.png'} className="img-avatar" alt="admin@bootstrapmaster.com" />
              
              <strong style={{'fontSize':'10px'}}>{this.state.nomAp}</strong>
              <strong style={{'fontSize':'10px', marginRight: '20%'}}>{' - ' + this.state.cargo}</strong>
            </DropdownToggle>


            <DropdownMenu right style={{ right: 'auto' }}>
              
    
              <DropdownItem href="#/login"><i className="fa fa-lock"></i> Salir</DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
        {/*<AppAsideToggler className="d-md-down-none" />
        <AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
