import React from 'react';
import Loadable from 'react-loadable'

import DefaultLayout from './containers/DefaultLayout';

function Loading() {
  return <div>Loading...</div>;
}
const Users = Loadable({
  loader: () => import('./views/Users/Users'),
  loading: Loading,
});

const User = Loadable({
  loader: () => import('./views/Users/User'),
  loading: Loading,
});
//*********************************** Para el index ********************************** */

const Dashboard = Loadable({
  loader: () => import('./views/Dashboard'),
  loading: Loading,
});

//*********************************** Para el Proceso Primera consulta ********************************** */

const PrimeraConsulta = Loadable({
  loader: () => import('./views/PrimeraConsulta/PrimeraConsulta'),
  loading: Loading,
});
const EditarAntecedente = Loadable({
  loader: () => import('./views/PrimeraConsulta/Antecedentes/Editar'),
  loading: Loading,
});

const AntecedentesG = Loadable({
  loader: () => import('./views/PrimeraConsulta/Antecedentes/AntecedentesG'),
  loading: Loading,
});

//*********************************** Para el Proceso Tratamiento ********************************** */

const Tratamiento = Loadable({
  loader: () => import('./views/Tratamiento/Tratamiento'),
  loading: Loading,
});


const RegistraTratamiento = Loadable({
  loader: () => import('./views/Tratamiento/RegistraTratamiento/RegistraTratamiento'),
  loading: Loading,
});



//*********************************** Para la ficha odontoloogica ********************************** */

//Para mostrar la ficha de atencion odontologica de un paciente tal como se ve en el formulario 
const FichaOdontologica = Loadable({
  loader: () => import('./views/FichaAtencionOdontologica/FichaOdontologica'),
  loading: Loading,
});

//*********************************** Para el Campañas de prevencion********************************** */

const RegistraCamp = Loadable({
  loader: () => import('./views/CampPrevencion/Cronograma/Registrar/'),
  loading: Loading,
});
//Para agregar Campaña y Tareas
const CampTareas = Loadable({
  loader: () => import('./views/CampPrevencion/CampTareas/'),
  loading: Loading,
});

//Para agregar una nueva campaña
const Campaign = Loadable({
  loader: () => import('./views/CampPrevencion/CampTareas/Campaign'),
  loading: Loading,
});

//Para el calendario
const Calendario = Loadable({
  loader: () => import('./views/CampPrevencion/Calendario'),
  loading: Loading,
});

//Para el VerAsignacionTareas
const VerAsignacionTareas = Loadable({
  loader: () => import('./views/CampPrevencion/VerAsignacionTareas'),
  loading: Loading,
});


//Para el ver la lista de campañas finalizadas
const ListaCronogramaFin = Loadable({
  loader: () => import('./views/CampPrevencion/Cronograma/ListaCronogramaFin'),
  loading: Loading,
});

//Para LISTAR atencion en campañas EXtramurales
const ListaExtramural = Loadable({
  loader: () => import('./views/CampPrevencion/Extramural/Lista'),
  loading: Loading,
});

//Para LISTAR atencion en campañas Intramurales
const ListaIntramural = Loadable({
  loader: () => import('./views/CampPrevencion/Intramural/Lista'),
  loading: Loading,
});

//Para Buscar pacientes afiliados para  Registrar su atencion en campañas Intramurales
const BuscaPacienteIntramural = Loadable({
  loader: () => import('./views/CampPrevencion/Intramural/Buscar'),
  loading: Loading,
});

//*********************************** Para el Proceso Insumos medicos ********************************** */
//Para crear un nuevo BIND CARD 
const NuevoBC = Loadable({
  loader: () => import('./views/Insumos/BindCard'),
  loading: Loading,
});
//Para reagistrar la Entrada de insumos / medicamentos
const RegistroEntrada = Loadable({
  loader: () => import('./views/Insumos/RegistroEntrada'),
  loading: Loading,
});
//Para reagistrar la Salida de insumos / medicamentos
const RegistroSalida = Loadable({
  loader: () => import('./views/Insumos/RegistroSalida'),
  loading: Loading,
});
//Para la lista General
const ListaGeneral = Loadable({
  loader: () => import('./views/Insumos/ListaGeneral'),
  loading: Loading,
});
//Para la laista Detalle
const ListaDetalle = Loadable({
  loader: () => import('./views/Insumos/ListaDetalle'),
  loading: Loading,
});

//****************************************para el pedido de insumos****************************** */
const PedidoMedicamentoInsumo = Loadable({
  loader: () => import('./views/Pedido/PedidoMedicamentoInsumo'),
  loading: Loading,
});

const VerTodoMI = Loadable({
  loader: () => import('./views/Pedido/VerTodo'),
  loading: Loading,
});

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Inicio', component: DefaultLayout },
  //*********************************** Para el index ********************************** */
  { path: '/dashboard', name: 'Pagina Principal', component: Dashboard },
  { path: '/users', exact: true, name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },



  /*mi codigo*/
  //*********************************** Para el Proceso Primera consulta ********************************** */
  { path: '/PrimeraConsulta', exact: true, name: 'Primera Consulta', component: PrimeraConsulta },
  { path: '/PrimeraConsulta/Antecedentes/AntecedentesG/:id/:apellido_pat/:apellido_mat/:nombres', name: 'Antecedentes Generales', component: AntecedentesG },

  //{ path: '/PrimeraConsulta/Antecedentes/', exact: true, name: 'Antecedentes Generales', component: AntecedentesG },
  { path: '/PrimeraConsulta/Antecedentes/Editar/:id', name: 'Editar Antecedentes', component: EditarAntecedente },
  //Editar Alergias
  //{ path: '/PrimeraConsulta/Alergia/Editar/:id/:descripcion/:idAlergia', name: 'Editar Alergias', component: EditarAlergia },
  //Editar Toleramcia
  //{ path: '/PrimeraConsulta/Tolerancia/Editar/:id', name: 'Editar Tolerancia', component: EditarTolerancia},
  //Editar Examen Clinico
  //{ path: '/PrimeraConsulta/ExamenClinico/Editar/:id', name: 'Editar Examen Clinico', component: EditarExamen},


  //*********************************** Para la ficha odontoloogica ********************************** */
  // para la FichaOdontologica de un paciente
  { path: '/FichaAtencionOdontologica/FichaOdontologica/:idAf', exact: true, name: 'Ficha Odontologica', component: FichaOdontologica },

  //*********************************** Para el Proceso Tratamiento ********************************** */
  //tratamiento
  { path: '/Tratamiento', exact: true, name: 'Tratamiento', component: Tratamiento },
  { path: '/Tratamiento/RegistraTratamiento/:id/:apellido_pat/:apellido_mat/:nombres/', name: 'Registra Tratamiento', component: RegistraTratamiento },


  //*********************************** Para el Campañas de prevencion********************************** */
  //Pra las campañas y/o Tareas
  { path: '/CampPrevencion/CampTareas/', exact: true, name: 'Registro', component: CampTareas },
  //Pra el VerAsignacionTareas
  { path: '/CampPrevencion/VerAsignacionTareas/', name: 'Ver Asignación de Tareas a Medicos', component: VerAsignacionTareas },
  //Pra las campañas
  { path: '/CampPrevencion/CampTareas/Campaign/', name: 'Registrar Campaña Nueva', component: Campaign },
  //Pra el cronograma
  { path: '/CampPrevencion/Cronograma/Registrar/', name: 'Registrar Campaña en Cronograma', component: RegistraCamp },
  //Pra el Calendario
  { path: '/CampPrevencion/Calendario/', name: 'Campañas en curso', component: Calendario },
  //Pra el la lista decampañas finalizadas
  { path: '/CampPrevencion/Cronograma/ListaCronogramaFin/', name: 'Lista de Campañas Finalizadas', component: ListaCronogramaFin },
  //Para el lista de Campaña Extramural
  { path: '/CampPrevencion/Extramural/Lista/', exact: true, name: 'Lista de Campaña Extramural', component: ListaExtramural },
  //Para el Lista de Campaña Intramural
  { path: '/CampPrevencion/Intramural/Lista/', exact: true, name: 'Lista de Campaña Intramural', component: ListaIntramural },
  //Para Buscar pacientes afiliados para  Registrar su atencion en campañas Intramurales
  { path: '/CampPrevencion/Intramural/Buscar/:idCro/:idMedico/:nomCamp/:v', name: 'Registra atención Intramural', component: BuscaPacienteIntramural },




  //*********************************** Para el Proceso Insumos medicos ********************************** */
  //Para un nuevo bind card
  { path: '/Insumos/BindCard/', name: 'Nuevo Bind Card', component: NuevoBC },

  //Para la entrada de insumos medicos
  { path: '/Insumos/RegistroEntrada/', name: "Registro de entrada Insumos y/o Medicamentos", component: RegistroEntrada },

  //Para las salida de insumos medicos
  { path: '/Insumos/RegistroSalida/', name: "Registro de salida Insumos y/o Medicamentos", component: RegistroSalida },

  //Para la lidta General
  { path: '/Insumos/ListaGeneral/', name: "Lista General", component: ListaGeneral },

  //Para la lista Detalle
  { path: '/Insumos/ListaDetalle/', name: "Lista a Detalle", component: ListaDetalle },

  //Para el pedido de insumos medicos
  { path: '/pedido', exact: true, name: 'Pedidos', component: PedidoMedicamentoInsumo },
  { path: '/Pedido/pedidoMedicamentoInsumo', name: 'Pedido de medicamentos y/o insumos', component: PedidoMedicamentoInsumo },
  { path: '/Pedido/verTodo', name: 'Ver todo', component: VerTodoMI },
];




/**
 
//Para editar las alergias
/*const EditarAlergia = Loadable({
  loader: () => import('./views/PrimeraConsulta/Alergia/Editar'),
  loading: Loading,
});*/

//Para editar las tolerancias
/*const EditarTolerancia = Loadable({
  loader: () => import('./views/PrimeraConsulta/Tolerancia/Editar'),
  loading: Loading,
});

//eDITAR eXAMEN cLINICO
const EditarExamen= Loadable({
  loader: () => import('./views/PrimeraConsulta/ExamenClinico/Editar'),
  loading: Loading,
});*/



/*
 { path: '/theme', exact: true, name: 'Theme', component: Colors },
 { path: '/theme/colors', name: 'Colors', component: Colors },
 { path: '/theme/typography', name: 'Typography', component: Typography }
 { path: '/base', exact: true, name: 'Base', component: Cards },
 { path: '/base/cards', name: 'Cards', component: Cards },
 { path: '/base/forms', name: 'Forms', component: Forms },
 { path: '/base/switches', name: 'Switches', component: Switches },
 { path: '/base/tables', name: 'Tables', component: Tables },
 { path: '/base/tabs', name: 'Tabs', component: Tabs },
 { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
 { path: '/base/carousels', name: 'Carousel', component: Carousels },
 { path: '/base/collapses', name: 'Collapse', component: Collapses },
 { path: '/base/dropdowns', name: 'Dropdowns', component: Dropdowns },
 { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
 { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
 { path: '/base/navbars', name: 'Navbars', component: Navbars },
 { path: '/base/navs', name: 'Navs', component: Navs },
 { path: '/base/paginations', name: 'Paginations', component: Paginations },
 { path: '/base/popovers', name: 'Popovers', component: Popovers },
 { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
 { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },

 { path: '/buttons', exact: true, name: 'Buttons', component: Buttons },
 { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
 { path: '/buttons/button-dropdowns', name: 'Button Dropdowns', component: ButtonDropdowns },
 { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
 { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
 { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
 { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
 { path: '/icons/flags', name: 'Flags', component: Flags },
 { path: '/icons/font-awesome', name: 'Font Awesome', component: FontAwesome },
 { path: '/icons/simple-line-icons', name: 'Simple Line Icons', component: SimpleLineIcons },
 { path: '/notifications', exact: true, name: 'Notifications', component: Alerts },
 { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
 { path: '/notifications/badges', name: 'Badges', component: Badges },
 { path: '/notifications/modals', name: 'Modals', component: Modals },
 { path: '/widgets', name: 'Widgets', component: Widgets },
 { path: '/charts', name: 'Charts', component: Charts },
 

*/
export default routes;
