import React, { Component } from 'react';
import {Col,Row,Alert} from 'reactstrap';

class Alerta extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: 1
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    return (
      <Row>
      <Col xs="12" md="3">
      </Col>
                <Col xs="12" md="6">
                  <Alert color="danger">
                    Error !!! Usuario no registrado, verificar.
                  </Alert>
                </Col>
      <Col xs="12" md="3">
      </Col>
      </Row>
             
    );
  }
}

export default Alerta;
