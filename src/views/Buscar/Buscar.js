import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Button, Alert } from 'reactstrap';
import Alerta from './Alerta';
import Search from 'react-search-box';
import DatosPersonales from '../PrimeraConsulta/DatosPersonales';
//import Boton from "./Boton.js";

class Buscar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      data: [],
      list: [],
      pr_c: '',
      show: false,
      show2: false,
      loading: false,
      muestrBoton: false,
      muestraDatos: false,
      muestraAlerta: false,
      muestraBotonPrimeraConsulta: false,
      muestraBotonTratamiento: false,
      muestraBotonHistorial: false,
      id: '',
      prueba: '',

    };
    this.handleChange = this.handleChange.bind(this);
    this.handleDatosPers = this.handleDatosPers.bind(this);

  }



  async componentDidMount() { //ciclo de vida de react de forma asincrona

    this.setState({
      loading: true
    });

    try {
      const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/persona/v1/afiliado/');
      const data = await respuesta.json();
      // console.log('listAfiliado=', respuesta)

      this.setState({
        data,
        loading: false,

      });

    } catch (e) {
      console.log(e);
      console.log('listAfiliado=', this.state.data)
    }
  }

  handleChange(value) {
    //console.log(value);
    //console.log("data=",this.state.data)
    this.state.data.map(item => {
      if (value === item.cod_promes) {
        this.setState({
          id: item.id,
          muestraDatos: true,
        })
      } else {
        //console.log('no hay ese id');
        // this.setState({ muestraAlerta: true })
      }
      return 0;
    });
  }

  handleDatosPers() {
    fetch(process.env.REACT_APP_API_URL+'odontologia/persona/v1/busca/afiliado/' + this.state.id + '/')
      .then((Response) => Response.json())
      .then((findresponse) => {
        //console.log("FINDRESPONSE[]=",findresponse[0].pr_consulta)
        //console.log("FINDRESPONSE=", findresponse)

        this.setState({
          list: findresponse,
          pr_c: findresponse[0].pr_consulta
        })


        if (this.state.list.length === 0) {
          this.setState({
            show: false,
            show2: true
          })
        }
        else if (this.state.list.length === 1) {
          this.setState({
            show: true,
            show2: false
          })
        }
        // Para los botones 
        if (this.state.pr_c === '0') {
          this.setState({
            muestraBotonPrimeraConsulta: true,
            muestraBotonTratamiento: false,
            muestraBotonHistorial: false,
            muestraDatos:false,
          });
        } else if (this.state.pr_c === '1') {
          this.setState({
            muestraBotonTratamiento: true,
            muestraBotonHistorial: true,
            muestraBotonPrimeraConsulta: false,
            muestraDatos:false,
          });
        }else{
          console.log('es nada')
        }




      });



  }
  render() {

    if (this.state.loading) {
      return (
        <div className="">Esperando conexion...</div>
      );
    }

    
    return (

      <div className="animated fadeIn">

        <Row>
          <Col xs="12">
            <Card>

              <CardHeader>
                <i className="fa fa-search"></i><strong>Buscar Paciente</strong>
              </CardHeader>

              <CardBody>
                <form>
                  <div className="form-group">

                    <Search
                      data={this.state.data}
                      onChange={this.handleChange.bind(this)}
                      placeholder="Matricula PROMES"
                      class="search-class"
                      searchKey="cod_promes"


                    />
                  </div>
                </form>

                {(this.state.muestraDatos === true) ? <Button color="primary"  className="btn btn-outline-primary"  block onClick={this.handleDatosPers}>Ver datos personales</Button> : (this.state.muestraAlerta === true) ? <Alert color="danger">Usuario no afiliado</Alert> : null}
              </CardBody>

            </Card>
          </Col>

        </Row>




        {(this.state.show === true) ? <DatosPersonales
                                        list={this.state.list}
                                        muestraBotonPrimeraConsulta={this.state.muestraBotonPrimeraConsulta}
                                        muestraBotonTratamiento={this.state.muestraBotonTratamiento}
                                        muestraBotonHistorial={this.state.muestraBotonHistorial}
                                      /> : (this.state.show2 === true) ? <Alerta /> : null
        }


        {this.state.show2 && <Alerta />}
      </div>

    );
  }

}




/*
<svg width="200" height="250" version="1.1" xmlns="http://www.w3.org/2000/svg">
<g >
  <polygon points="0 0 , 20 0, 15 5, 5 5" fill="white" stroke="navy" onMouseLeave={this.handleMouseLeave.bind(this)} onClick={this.handleClick.bind(this)} onContextMenu={this.handleOnContextMenu.bind(this)}/>
  <polygon points="5 15 , 15 15, 20 20, 0 20" fill="white" stroke="navy" onMouseLeave={this.handleMouseLeave.bind(this)} onClick={this.handleClick.bind(this)} onContextMenu={this.handleOnContextMenu.bind(this)}/>
  <polygon points="15 5 , 20 0, 20 20, 15 15" fill="white" stroke="navy" onMouseLeave={this.handleMouseLeave.bind(this)} onClick={this.handleClick.bind(this)} onContextMenu={this.handleOnContextMenu.bind(this)}/>
  <polygon points="0 0 , 5 5, 5 15, 0 20" fill="white" stroke="navy" onMouseLeave={this.handleMouseLeave.bind(this)} onClick={this.handleClick.bind(this)} onContextMenu={this.handleOnContextMenu.bind(this)}/>
  <polygon points="5 5, 15 5, 15 15, 5 15" fill="white" stroke="navy" onMouseLeave={this.handleMouseLeave.bind(this)} onClick={this.handleClick.bind(this)} onContextMenu={this.handleOnContextMenu.bind(this)}/>
  
</g>
</svg>


constructor(props) {
    super(props);

    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,

      list: [],
      dato:'',
      swA:false,
      
      show: false,
      show2: false
    };
    
    this.toggleTable = this.toggleTable.bind(this)
  }

  
  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }


  toggleTable = () => {
    fetch(process.env.REACT_APP_API_URL+'odontologia/persona/v1/busca/afiliado/'+this.state.dato.value+'/')
    .then((Response)=>Response.json())
    .then((findresponse) => {
        //console.log(findresponse)
        this.setState({
          list: findresponse
        })
        //console.log(this.state.list.length)   

        if (this.state.list.length==0) {
          this.setState({
            show: false,
            show2: true
          })
          
         // document.write("No hay " + this.state.list.length);
        }
        else{
          this.setState({
            show: true,
            show2: false
          })
          //document.write("SI hay "+ this.state.list.length);
        } 
    });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <Card>
              
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Buscar Paciente</strong>
              </CardHeader>
    
              <CardBody>
                <form>
                <div className="form-group">
                    <label>Para buscar un paciente, introducir Codido PROMES, Cedula de Iidentidad o Registro Universitario</label>
                    <input type="text" className="form-control" ref={dato => this.state.dato = dato}/>
                </div>
                  <Button  onClick={ this.toggleTable } type="submit" className="btn btn-primary">Buscar</Button>

                </form>
                
              </CardBody>
            </Card>
          </Col>
        </Row>

        {this.state.show && <DatosPersonales list={this.state.list}  />}
        {this.state.show2 && <Alerta tipo={this.state.dato.value} />}


    </div>
    );
  }*/
export default Buscar;
