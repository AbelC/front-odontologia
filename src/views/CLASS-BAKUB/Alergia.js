import React, { Component } from 'react';
import { Button, DropdownItem, Card, CardBody, CardHeader, Col, Row,Form, FormGroup, Label,Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import Search from 'react-search-box';
import ListaAlergia from './ListaAlergia'; 

var url= process.env.REACT_APP_API_URL+'odontologia/alergia/v1/regalergia/';
class Alergias extends Component {
    /* Para listar*/
/*---------------Para registrar*/
constructor(props) {
    super(props)
    
    this.state = {
      medico: '',
      afiliado: '',
      fecha_medicacion:'',
      alergia :'',
      alergias: [],
      data: [],
      primary: false,
      warning: false,
    }
    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.toggleWarning = this.toggleWarning.bind(this);
}


handleOnSubmit(e) {
  if(this.state.alergia.value !== '' ){
    e.preventDefault()  
    let data = {
      medico: 1,
      afiliado:this.props.idA,
      fecha_atencion:'2018-07-23',
      alergia:this.state.alergia.value
    }
    //console.log(this.state.data);
    try {
  
      fetch(url, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be string or {object}!
        headers:{
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));
      window.location.reload();
    } catch (e) {
      console.log(e);
    }
    this.setState({
      primary: !this.state.primary,
    });



  }




  }

  togglePrimary() {

      console.log('this.state.alergia.value== ', this.state.alergia.value )
    if(this.state.alergia.value !== '' ){
      console.log('verdaad')
      this.setState({primary: !this.state.primary,});
    }else {
      console.log('falso')
      this.setState({warning: !this.state.warning,});   
  
    }
  }
  
  toggleWarning() {
    this.setState({
      warning: !this.state.warning,
    });
  }

    /*,,,,*/
    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
          const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/alergia/v1/alergias/');
          const alergias = await respuesta.json();
          this.setState({
            alergias
          });
        } catch (e) {
            console.log(e);
        }
      }



    render() {
      console.log('propss alergia ',this.props)
        return (
            <Col xs="12" sm="6">
            <Card>
              <CardHeader>
              <i className="fa fa-align-justify"></i> <strong>Alergias</strong>    
              <div className="card-header-actions">                  
                    <strong className="text-muted">Paciente: {this.props.nombres + ' ' + this.props.apellido_pat + ' ' + this.props.apellido_mat}</strong>                  
                </div>           
              </CardHeader>



              <CardBody>
                <Form inline method="POST">
                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                      <Label for="fecha" className="mr-sm-2">Alergia</Label>
                      <select type="select" name="tolerancia" id="tolerancia" className="form-control" ref={alergia => this.state.alergia = alergia}>
                        <option value="">---</option>

                            {this.state.alergias.map(item => (    
                                <option key={item.id} value={item.id}>{item.descripcion}</option>                        
                            ))}

                      </select>
                    </FormGroup>
                  



                  
                    <Button onClick={this.togglePrimary}>Submit</Button>
                    <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-primary ' + this.props.className}>
                          <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
                          <ModalBody>
                          ¿Está seguro de guardar, los datos introducidos?
                          </ModalBody>
                          <ModalFooter>
                            <Button onClick={this.handleOnSubmit} type="submit" color="primary">Aceptar</Button>{' '}
                            <Button color="secondary" onClick={this.togglePrimary}>Cancelar</Button>
                          </ModalFooter>
                </Modal>
                </Form>


                  <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
                       className={'modal-warning ' + this.props.className}>
                  <ModalHeader toggle={this.toggleWarning}>Campos vacios</ModalHeader>
                  <ModalBody>
                    Llene los campos faltantes, para guardar el registro de alergias del paciente.
                  </ModalBody>
                  <ModalFooter>
                    <Button color="warning" onClick={this.toggleWarning}>Volver</Button>{' '}
                    
                  </ModalFooter>
                  </Modal>



<br/>
 <DropdownItem divider />

                <ListaAlergia idA={this.props.idA}/>

              </CardBody>
            </Card>
          </Col>



    
);
}
}
export default Alergias;
