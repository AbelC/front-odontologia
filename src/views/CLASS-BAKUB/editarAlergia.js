import React, { Component } from 'react';
import { Table, Button,  Card, CardBody, CardHeader, Col, Row,Form, FormGroup, Label,Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import Search from 'react-search-box';



class Editar extends Component {
  
  constructor(props) {
    super(props)
    this.state = {
      id:'',

      medico: '',
      afiliado: '',
      fecha_medicacion:'',
      alergia :'',
      alergias: [],
      list_reg_alergia:[],
      primary: false,
      muestraBotonAlergia: false,
      muestraAlerta:false,
      idAlergia:''
   }
  this.handleOnSubmit = this.handleOnSubmit.bind(this)
   this.togglePrimary = this.togglePrimary.bind(this)


  // console.log('myID==', this.props.match.params.id)
   //console.log('descripcion==', this.props.match.params.descripcion)
   console.log('blabla=>>',this.props)

  }



 /* async componentDidMount() { //Lista la alergia de un pacinete dandole el id del registro de su alergia en la tbla regalergia
    try {
      const respuesta_reg = await fetch(process.env.REACT_APP_API_URL+'odontologia/alergia/v1/regalergia/'+this.props.match.params.id+'/');
      const list_reg_alergia = await respuesta_reg.json();
      
      console.log('list_reg_alergia=',list_reg_alergia)
      this.setState({
        list_reg_alergia
      });
    } catch (e) {
        console.log(e);
    }
    
  }*/
  async componentDidMount() { //Lisata las alergias de la tbla alergias
    try {
      const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/alergia/v1/alergias/');
      const alergias = await respuesta.json();
      //console.log('list_alergias=',alergias)

      const respuesta_reg = await fetch(process.env.REACT_APP_API_URL+'odontologia/alergia/v1/regalergia/'+this.props.id+'/');
      const list_reg_alergia = await respuesta_reg.json();      
      //console.log('list_reg_alergia=',list_reg_alergia)

      this.setState({
        alergias,
        list_reg_alergia
      });
    } catch (e) {
        console.log(e);
    }
  }



  togglePrimary() {
    this.setState({
      primary: !this.state.primary,
    });
  }


        
handleChange(value) {
  console.log('alergia: ',value); 
  //console.log("alergias=",this.state.alergias)
this.state.alergias.map((item, indice)=>{
    if (value === item.descripcion){
      console.log('item:',item.id); 
      

fetch(process.env.REACT_APP_API_URL+'odontologia/alergia/v1/busca/alergiaid/'+item.id+'/')
    .then((Response)=>Response.json())
    .then((findresponse) => {
        //console.log("FINDRESPONSE[]=",findresponse[0].pr_consulta)
        //console.log("FINDRESPONSE=",findresponse)

        this.setState({
          list: findresponse, 
          idAlergia: item.id

        })
        console.log('id de alergia: ' , this.state.idAlergia); 
       // console.log('LIST=',this.state.list.length)   
      //console.log("pr_c=",this.state.pr_c)
        if (this.state.list.length===0) {
          this.setState({
            muestraAlerta:true
          })
          
         // document.write("No hay " + this.state.list.length);
        }
        else{
          this.setState({
            muestraBotonAlergia:true
          })
          //document.write("SI hay "+ this.state.list.length);
        }


        console.log('muestraAlerta:' , this.state.muestraAlerta, ' muestraBotonAlergia: ', this.state.muestraBotonAlergia)
 });

    }
}


)

}
























  handleOnSubmit(e) {//Realiza el cambio d la alergia en la tbla reg alergia
    e.preventDefault()
    let url = ''
    let data = {}
    url = process.env.REACT_APP_API_URL+'odontologia/alergia/v1/regalergia/'+this.props.id+'/'
    data = {
      
      medico: this.state.list_reg_alergia.medico,    
      fecha_atencion: this.state.list_reg_alergia.fecha_atencion,
      afiliado:this.state.list_reg_alergia.afiliado,

      alergia:this.state.alergia.value
    }
    try {
      fetch(url, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers:{
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response), console.log('PUT echo:'));
    } catch (e) {
        console.log(e);
    }

    this.setState({
      primary: !this.state.primary,
    });

  }




  togglePrimary() {
    this.setState({
      primary: !this.state.primary,
    });
  }



render() {
/*document.write(this.props.match.params.id)*/
    return (
<div className="animated fadeIn">
<Row>
        <Col>
            <Card>
              
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Editar Alergia</strong>
              </CardHeader>


              <CardBody>
              <Table responsive hover>
                <tbody>
                  <tr>
                    <td><strong>AlergiAAAAAa</strong></td>
                    <td>



                      
                    <select type="select" name="tolerancia" id="tolerancia" className="form-control"  ref={alergia => this.state.alergia = alergia}>
                        <option value={this.props.idAlergia}>{this.props.descripcion}</option>

                            {this.state.alergias.map(item => (    
                                <option key={item.id} value={item.id}>{item.descripcion}</option>                        
                            ))}

                      </select>
                    </td>
                  </tr>
                </tbody>
              </Table>
            



            
                  
                    <Button color="primary"onClick={this.togglePrimary} className="mr-1">Editar</Button>

                    <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-primary ' + this.props.className}>
                          <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
                          <ModalBody>
                            ¿Está seguro que quiere editar?
                          </ModalBody>
                          <ModalFooter>
                            <Button onClick={this.handleOnSubmit} type="submit" color="primary">Aceptar</Button>{' '}
                            <Button color="secondary" onClick={this.togglePrimary}>Cancelar</Button>
                          </ModalFooter>
                    </Modal>
              </CardBody>
            </Card>
        </Col>
  </Row>
















</div>
    );
  }
}

export default Editar;
