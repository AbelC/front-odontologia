import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button, Modal, ModalBody, ModalFooter, ModalHeader, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import Tareas from './Tareas'
class AsignaTareas extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: '',
            descripcion: '',
            visible: false,
            custom: [false, false, false],
            listaCamp: [],
            primary: false,
            idC: 0,
            nomC: '',
            page: 1,
            eachPage: 12,
        }
        this.onDismiss = this.onDismiss.bind(this);
        this.toggleCustom = this.toggleCustom.bind(this);
        this.togglePrimary = this.togglePrimary.bind(this);
        this.getIDC = this.getIDC.bind(this);
        this.handleClick = this.handleClick.bind(this)
    }

    onDismiss() {
        this.setState({ visible: false });
    }

    toggleCustom(tab) {
        const prevState = this.state.custom;
        const state = prevState.map((x, index) => tab === index ? !x : false);
        this.setState({
            custom: state,
        });
    }

    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/listaCamp/');
            const listaCamp = await respuesta.json();
            this.setState({
                listaCamp
            });
        } catch (e) {
            console.log(e);
        }
    }

    togglePrimary() {
        this.setState({
            primary: !this.state.primary,
        });
    }

    getIDC(valor, nom) {
        console.log('Valor.', valor, ' NomC: ', nom)
        this.setState({
            idC: valor,
            nomC: nom
        });
    }
    handleClick(event) {
        this.setState({
            page: Number(event.target.id)
        });
    }
    render() {
        //console.log('campaign:', this.state.campaign)


        

        /**************BEGIN PAGINACION (Asignacion de tareas)***********/
        const { page, eachPage } = this.state;

        const last = page * eachPage;
        const first = last - eachPage;
        const listaAsigTarea = this.state.listaCamp.slice(first, last);

        //lista
        const listC = listaAsigTarea.map(item => {
            return (
                <tr key={item.id} >
                    <td>{item.fechaGuardado}</td>
                    <td>{item.nombre}</td>
                    <td>{item.descripcion}</td>
                    <td><Button onClick={() => { this.togglePrimary(); this.getIDC(item.id, item.nombre); }} >Asignar</Button></td>
                </tr>
            )
        });
        //para los numeros
        const pages = [];
        for (let i = 1; i <= Math.ceil(this.state.listaCamp.length / eachPage); i++) {
            pages.push(i);
        }

        const renderpages = pages.map(number => {
            if (number % 2 === 1) {
                return (
                    <PaginationItem key={number} active>
                        <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                    </PaginationItem>
                );
            }
            else {
                return (
                    <PaginationItem key={number}>
                        <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                    </PaginationItem>
                );
            }
        });
        /************END PAGINACION (Asignacion de tareas)***********/


        return (
            <Row>
                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i> <strong>Asignar Tareas a una campaña</strong>
                        </CardHeader>
                        <CardBody>
                            <Table responsive hover bordered size="sm">
                                <thead>
                                    <tr>
                                        <th scope="col">Fecha de Guardado</th>
                                        <th scope="col">Nombre de la campaña de prevención</th>
                                        <th scope="col">Detalle</th>
                                        <th scope="col">Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {listC}
                                </tbody>
                            </Table>
                            <Row>
                                <Col md="10">
                                    <Pagination>
                                        {renderpages}
                                    </Pagination>
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>
                </Col>

                <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-primary ' + this.props.className}>
                    <ModalHeader toggle={this.togglePrimary}>Tareas</ModalHeader>
                    <ModalBody>
                        <Tareas idCamp={this.state.idC} nomC={this.state.nomC} />
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.togglePrimary}>Cerrar</Button>
                    </ModalFooter>
                </Modal>

            </Row>
        );
    }
}
export default AsignaTareas;
