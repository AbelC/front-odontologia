import React from 'react';
import ReactDOM from 'react-dom';
import CampTareas from './CampTareas';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<CampTareas />, div);
    ReactDOM.unmountComponentAtNode(div);
});
