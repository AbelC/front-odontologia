import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Button, Label, FormGroup, Alert } from 'reactstrap';
//LISTA LAS TAREAS PARA GUARDARALAS
var url = process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/tieneTarea/';
class Tareas extends Component {
    constructor(props) {
        super(props);
        this.tareasRefs = [];
        this.state = {
            listaTareas: [],
            idCamp: 0,
            idTarea: 0,
            vecTareas: [],
            muestraAlert: false,
            muestraAlert2: null,
            muestraAlertVacio: false,
            visible2: true,
        }
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.onDismiss = this.onDismiss.bind(this)
        this.onDismiss2 = this.onDismiss2.bind(this)
    }
    onDismiss() {
        this.setState({
            visible: false
        })
    }
    onDismiss2() {
        this.setState({
            visible2: false
        })
    }

    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/tarea/');
            const listaTareas = await respuesta.json();
            this.setState({
                listaTareas
            });
        } catch (e) {
            console.log(e);
        }
    }

    handleOnSubmit(e) {
        e.preventDefault()
        var tareasTrue = []
        //console.log('tareasRefs.length', this.tareasRefs.length)

        for (let i = 0; i < this.tareasRefs.length; i++) {
            var check = this.tareasRefs[i].checked;
            //console.log('check: ', check)            
            if (check) tareasTrue = tareasTrue.concat([this.tareasRefs[i].value])
            //console.log('tareasRefs', this.tareasRefs[i].value)

        }
        //console.log('tareasTrue.lenght', tareasTrue.length)
        if (tareasTrue.length !== 0) {
            for (let i = 0; i < tareasTrue.length; i++) {
                console.log('tareasTrue', tareasTrue[i])
                //console.log('DATOS NO VACIOS')
                let data = {
                    idCamp: this.props.idCamp,
                    idTarea: tareasTrue[i],
                }
                try {
                    fetch(url, {
                        method: 'POST', // or 'PUT'
                        body: JSON.stringify(data), // data can be string or {object}!
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(res => res.json())
                        .catch(error => console.error('Error:', error))
                        .then(response => console.log('Success:', response), this.setState({ muestraAlert: true }));
                    //window.location.reload();
                } catch (e) {
                    console.log(e);
                }
            }
            if (this.state.muestraAlert === true) {
                this.setState({ muestraAlert2: true })
            } else {
                this.setState({ muestraAlert2: false })
            }

        } else {
            this.setState({
                muestraAlertVacio: true
            });
            //alert('Rellene campos faltantes')

        }



    }


    render() {
        //console.log('campaign:', this.state.campaign)
        // console.log('this props: ', this.props)


        
        const listT = this.state.listaTareas.map((item, index) => {
            return (
                <FormGroup check key={index}>
                    <input className="form-check-input" type="checkbox" value={item.id} ref={ref => this.tareasRefs[index] = ref} />
                    <Label className="form-check-label" check>{item.nombreTarea}</Label>
                </FormGroup>

            )

        })

        //console.log('muestraAlert2: ', this.state.muestraAlert2)

        return (
            <Row>
                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i> <strong>Asignar Tareas a una campaña</strong>
                        </CardHeader>
                        <CardBody>
                            Elige las tareas que tendra <strong>{this.props.nomC} </strong>
                            <br />
                            {listT}
                            <br />
                            <Button onClick={this.handleOnSubmit}>Guardar</Button>
                            <br />
                            {(this.state.muestraAlertVacio === true) ? <Alert color="danger" isOpen={this.state.visible} toggle={this.onDismiss} >No a seleccionado niguna opción</Alert> : null}

                            {(this.state.muestraAlert === true) ? <Alert color="success" isOpen={this.state.visible2} toggle={this.onDismiss2}>Tareas registradas con exito</Alert> : null}

                        </CardBody>
                    </Card>
                </Col>
            </Row>
        );
    }
}

/**



<tr key={item.id} >
                    <td><CustomInput type='checkbox' id={item.id} label={item.nombreTarea} ref={ref => this.tareasRefs[index] = ref}  /></td>
                </tr>
        
 

 */
export default Tareas;
