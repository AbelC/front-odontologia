import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Button, Collapse } from 'reactstrap';
import Campaign from './Campaign/Campaign';
import Tareas from './Tareas/Tareas';
import AsignaTareas from './AsignaTareasCamp/AsignaTareas'
class CampTareas extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: '',
            descripcion: '',         
            visible: false,         
            custom: [false, false, false],
        }
        this.onDismiss = this.onDismiss.bind(this);
        this.toggleCustom = this.toggleCustom.bind(this);
    }


    onDismiss() {
        this.setState({ visible: false });
    }

    toggleCustom(tab) {
        const prevState = this.state.custom;
        const state = prevState.map((x, index) => tab === index ? !x : false);
        this.setState({
            custom: state,
        });
    }



    render() {
        //console.log('campaign:', this.state.campaign)
        return (
            <Row>
                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i> <strong>Registrar Campaña y/o Tareas</strong>
                        </CardHeader>
                        <CardBody>
                            <div id="exampleAccordion" data-children=".item">
                                <div className="item">
                                    <Button  color="primary"  block onClick={() => this.toggleCustom(0)} aria-expanded={this.state.custom[0]} aria-controls="exampleAccordion1">Agregar Campaña Nueva</Button>
                                    <Collapse isOpen={this.state.custom[0]} data-parent="#exampleAccordion" id="exampleAccordion1">
                                    <br/>
                                        <Campaign />
                                    </Collapse>
                                </div>
                                <br/>
                                <div className="item">
                                    <Button color="secondary"  block onClick={() => this.toggleCustom(1)} aria-expanded={this.state.custom[1]} aria-controls="exampleAccordion2">Agregar Tareas nuevas</Button>
                                    <Collapse isOpen={this.state.custom[1]} data-parent="#exampleAccordion" id="exampleAccordion2">
                                        <br/>
                                        <Tareas />
                                    </Collapse>
                                </div>
                                <br/>
                                <div className="item">
                                    <Button color="secondary"  block onClick={() => this.toggleCustom(2)} aria-expanded={this.state.custom[2]} aria-controls="exampleAccordion2">Asignar tareas a una campaña</Button>
                                    <Collapse isOpen={this.state.custom[2]} data-parent="#exampleAccordion" id="exampleAccordion2">
                                        <br/>
                                        <AsignaTareas />
                                    </Collapse>
                                </div>
                            </div>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        );
    }
}
export default CampTareas;
