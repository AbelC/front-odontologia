import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Form, FormGroup, Label, Button, Alert, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
var url = process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/listaCamp/';
class Campaign extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: '',
            descripcion: '',
            muestraAlert: false,
            visible: true,
            success: false,
            warning: false,
        }
        this.onDismiss = this.onDismiss.bind(this);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.toggleSuccess = this.toggleSuccess.bind(this);
        this.toggleWarning = this.toggleWarning.bind(this);
    }

    toggleWarning() {
        this.setState({
            warning: !this.state.warning,
        });
    }
    toggleSuccess() {
        if (this.nombre.value !== '' && this.descripcion.value !== '') {
            this.setState({
                success: !this.state.success,
            });
        } else {
            this.setState({ warning: !this.state.warning, });
        }
    }

    onDismiss() {
        this.setState({ visible: false });
    }

    handleOnSubmit(e) {
        e.preventDefault()
        if (this.nombre.value !== '' && this.descripcion.value !== '') {
            var options = {
                day: '2-digit',
                month: '2-digit',
                year: 'numeric',
                hour: '2-digit',
                minute: '2-digit',
                second: '2-digit'
            };
            var d = new Date();
            var time = d.toLocaleString('es-bo', options);
            var date = time.split(' ')[0].split('/');
            time = time.split(' ')[1].split(':');
            var dateS = date[2] + '-' + date[1] + '-' + date[0];
            //var dateFull = new Date(date[2], date[1], date[0], time[0], time[1], time[2]);
            //var dateS = dateFull.getFullYear() + '-' + dateFull.getMonth() + '-' + dateFull.getDate();  /*Date to send */
            //var timeS = dateFull.getHours() + ':' + dateFull.getMinutes() + ':' + dateFull.getSeconds();  /**Time to send */

            let data = {
                nombre: this.nombre.value,
                descripcion: this.descripcion.value,
                fechaGuardado:dateS,
            }
            //console.log(this.state.data);
            try {

                fetch(url, {
                    method: 'POST', // or 'PUT'
                    body: JSON.stringify(data), // data can be string or {object}!
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(response => console.log('Success:', response), this.setState({ muestraAlert: true }));
                //e.target.reset();
                window.location.reload();
                document.getElementById("create-course-form").reset();
            } catch (e) {
                console.log(e);
            }

            this.setState({
                nombre: '',
                descripcion: '',
            });
            this.setState({
                success: !this.state.success,
            });

        }
    }
    render() {
        //console.log('campaign:', this.state.campaign)
        return (
            <Row>
                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i> <strong>Registrar Campaña</strong>
                        </CardHeader>
                        <CardBody>
                            <Form id="create-course-form">
                                <Row>
                                    <Col xs="12" sm="6">
                                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                            <Label for="NombreCampaña" className="mr-sm-2">Titulo de Campaña</Label>
                                            <input type="text" className="form-control" placeholder="Titulo Campaña" ref={nombre => this.nombre = nombre} />
                                        </FormGroup>
                                    </Col>
                                    <Col xs="12" sm="6">

                                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                            <Label for="descripcion" className="mr-sm-2">Descripción</Label>
                                            <textarea className="form-control" placeholder="Descripción de la Campaña" style={{ 'height': '150px', 'maxHeight': '151px' }} ref={descripcion => this.descripcion = descripcion} />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <br />
                                {(this.state.muestraAlert === true) ? <Alert color="success" isOpen={this.state.visible} toggle={this.onDismiss} fade={false}>¡¡¡ Guardado correctamente !!! <br /> Ahora lo puede agregar al cronograma</Alert> : null}

                                <Button color="success" onClick={this.toggleSuccess} >Guardar</Button>
                            </Form>
                        </CardBody>
                    </Card>
                </Col>


                <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
                    className={'modal-warning ' + this.props.className}>
                    <ModalHeader toggle={this.toggleWarning}>Campos vacios</ModalHeader>
                    <ModalBody>
                        Llene los campos faltantes, por favor.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="warning" onClick={this.toggleWarning}>Volver</Button>{' '}

                    </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                    className={'modal-success ' + this.props.className}>
                    <ModalHeader toggle={this.toggleSuccess}>Registrar Campaña</ModalHeader>
                    <ModalBody>
                        ¿Esta seguro de guardar el registro?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.handleOnSubmit}>Aceptar</Button>
                        <Button color="secondary" onClick={this.toggleSuccess}>Cerrar</Button>
                    </ModalFooter>
                </Modal>



            </Row>
        );
    }
}
export default Campaign;
