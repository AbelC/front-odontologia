import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Form, FormGroup, Label, Alert, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
var url = process.env.REACT_APP_API_URL + 'odontologia/campPrevencion/v1/tarea/'; //guarda tareas
class Tareas extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nombreTarea: '',
            muestraAlert: false,
            visible: true,
            success: false,
            warning: false,
        }
        this.onDismiss = this.onDismiss.bind(this);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.toggleSuccess = this.toggleSuccess.bind(this);
        this.toggleWarning = this.toggleWarning.bind(this);
    }



    toggleWarning() {
        this.setState({
            warning: !this.state.warning,
        });
    }
    toggleSuccess() {
        if (this.nombreTarea.value !== '') {
            this.setState({
                success: !this.state.success,
            });
        } else {
            this.setState({ warning: !this.state.warning, });
        }
    }

    onDismiss() {
        this.setState({ visible: false });
    }


    handleOnSubmit(e) {
        e.preventDefault()
        if (this.nombreTarea.value !== '') {

            let data = {
                nombreTarea: this.nombreTarea.value,
            }
            //console.log(this.state.data);
            try {

                fetch(url, {
                    method: 'POST', // or 'PUT'
                    body: JSON.stringify(data), // data can be string or {object}!
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(response => console.log('Success:', response), this.setState({ muestraAlert: true }));
                //e.target.reset();
                window.location.reload();

            } catch (e) {
                console.log(e);
            }

            this.setState({
                nombreTarea: '',
            });
            this.setState({
                success: !this.state.success,
            });

        }
    }

    render() {
        //console.log('muestraAlert:', this.state.muestraAlert)
        return (
            <Row>
                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i> <strong>Registrar Campaña</strong>
                        </CardHeader>
                        <CardBody>
                            <Form id="create-course-form">

                                <Row>
                                    <Col xs="8">
                                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                            <Label for="NombreCampaña" className="mr-sm-2">Titulo de la Tarea</Label>
                                            <input type="text" className="form-control" placeholder="Tarea" ref={nombreTarea => this.nombreTarea = nombreTarea} />
                                        </FormGroup>
                                        <br />
                                        {(this.state.muestraAlert === true) ? <Alert color="primary" isOpen={this.state.visible} toggle={this.onDismiss} fade={false}>¡¡¡ Guardado correctamente !!! <br /> Ahora lo puede asignar esta tarea a un cronograma</Alert> : null}

                                    </Col>
                                    <Col xs="4">
                                        <br />
                                        <Button color="success" onClick={this.toggleSuccess} >Guardar</Button>
                                    </Col>
                                </Row>



                            </Form>
                        </CardBody>
                    </Card>
                </Col>


                <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
                    className={'modal-warning ' + this.props.className}>
                    <ModalHeader toggle={this.toggleWarning}>Campos vacios</ModalHeader>
                    <ModalBody>
                        Llene los campos faltantes, por favor.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="warning" onClick={this.toggleWarning}>Volver</Button>{' '}

                    </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                    className={'modal-success ' + this.props.className}>
                    <ModalHeader toggle={this.toggleSuccess}>Registrar Campaña</ModalHeader>
                    <ModalBody>
                        ¿Esta seguro de guardar el registro?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={this.handleOnSubmit}>Aceptar</Button>
                        <Button color="secondary" onClick={this.toggleSuccess}>Cerrar</Button>
                    </ModalFooter>
                </Modal>
            </Row>
        );
    }
}
export default Tareas;
