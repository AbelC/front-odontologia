import React from 'react';
import ReactDOM from 'react-dom';
import Tareas from './Tareas';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Tareas />, div);
    ReactDOM.unmountComponentAtNode(div);
});
