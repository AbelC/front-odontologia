import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button, ModalBody, ModalHeader, ModalFooter, Modal, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import TareasOfCamp from './TareasOfCamp'
class ListaCronograma extends Component {
    constructor(props) {
        super(props);

        this.state = {
            listaCronograma: [],
            listaAsignaCarrera: [],
            primary: false,
            warning: false,
            success: false,
            swButton: false,
            idC: 0,
            nomC: '',
            page: 1,
            eachPage: 20,
        }

        //this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.togglePrimary = this.togglePrimary.bind(this)
        this.toggleWarning = this.toggleWarning.bind(this);
        this.toggleSuccess = this.toggleSuccess.bind(this);
        this.toggleCerrar = this.toggleCerrar.bind(this);
        this.getIdNom = this.getIdNom.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }



    toggleWarning() {
        this.setState({
            warning: !this.state.warning,
        });
    }
    toggleSuccess() {
        this.setState({
            success: !this.state.success,
        });
    }

    toggleCerrar() {
        window.location.reload();
    }

    togglePrimary() {
        this.setState({ primary: !this.state.primary, });
    }


    getIdNom(valor, nom) {
        //console.log('Valor.', valor, ' NomC: ', nom)
        this.setState({
            idC: valor,
            nomC: nom
        });
    }


    async componentDidMount() { //ciclo de vida de react de forma asincrona
        var options = {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit'
        };
        var d = new Date();
        var time = d.toLocaleString('es-bo', options);
        var date = time.split(' ')[0].split('/');
        time = time.split(' ')[1].split(':');
        var dateS = date[2] + '-' + date[1] + '-' + date[0];
    
        //var dateFull = new Date(date[2], date[1], date[0], time[0], time[1], time[2]);
        //var dateS = dateFull.getFullYear() + '-' + dateFull.getMonth() + '-' + dateFull.getDate();  /*Date to send */
        //var timeS = dateFull.getHours() + ':' + dateFull.getMinutes() + ':' + dateFull.getSeconds();  /**Time to send */

        try {
            /*const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/listaCronogramaCamp/');
            const listaCronograma = await respuesta.json();*/

            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/listaCampNoEnd/' + dateS + '/');
            const listaCronograma = await respuesta.json();

            const respuestaC = await fetch(process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/asignaCarreraJoin/');
            const listaAsignaCarrera = await respuestaC.json();
            this.setState({
                listaCronograma,
                listaAsignaCarrera
            });
        } catch (e) {
            console.log(e);
        }
    }

    handleClick(event) {
        this.setState({
            page: Number(event.target.id)
        });
    }
    render() {
        //console.log('listaAsignaCarrera:', this.state.listaAsignaCarrera)



        /*console.log('fecha_server:', fecha_server);
        console.log('dia:', dia)
        console.log('mes:', mes)
        console.log('año:', year)
        console.log('fecha enviar:', fecha_en)*/

        /**************BEGIN PAGINACION (Lista de cronograma que no termino aun )***********/
        const { page, eachPage } = this.state;

        const last = page * eachPage;
        const first = last - eachPage;
        const listaCronogramaPag = this.state.listaCronograma.slice(first, last);

        //lista
        const listCro = listaCronogramaPag.map(item => {
            return (
                <tr key={item.id} >
                    <td style={{ 'textAlign': 'center' }}>{item.fechaIni}</td>
                    <td style={{ 'textAlign': 'center' }}>{item.fechaFin}</td>
                    <td>
                        {(item.lugar !== '') ? item.lugar : (item.lugar === '') ?
                            this.state.listaAsignaCarrera.map(item2 => {
                                if (item.id === item2.idCronograma) {
                                    return (
                                        <span key={item2.id} >
                                            {' - ' + item2.idCarrera.nombre}
                                        </span>
                                    )
                                }
                                return '';
                            }

                            )
                            : null}
                    </td>
                    <td style={{ 'textAlign': 'center' }}>{item.tipo}</td>
                    <td style={{ 'textAlign': 'center' }}>{item.campaign.nombre}</td>
                    <td style={{ 'textAlign': 'center' }}>{item.version}</td>
                    <td><Button onClick={() => { this.togglePrimary(); this.getIdNom(item.campaign.id, item.campaign.nombre); }} >Asignar</Button></td>
                </tr>
            )

        });
        //para los numeros
        const pages = [];
        for (let i = 1; i <= Math.ceil(this.state.listaCronograma.length / eachPage); i++) {
            pages.push(i);
        }

        const renderpages = pages.map(number => {
            if (number % 2 === 1) {
                return (
                    <PaginationItem key={number} active>
                        <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                    </PaginationItem>
                );
            }
            else {
                return (
                    <PaginationItem key={number}>
                        <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                    </PaginationItem>
                );
            }
        });
        /************END PAGINACION (MEDICAMENTOS)***********/



        return (
            <Row>
                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i> <strong>Lista Cronograma - Asignar Tareas</strong>
                        </CardHeader>
                        <CardBody>
                            <Table responsive hover bordered size="sm" >
                                <thead>
                                    <tr>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Fecha de Inicio</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Fecha de conclusión</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Lugar / Facultad</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>tipo</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>campaña</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Versión</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Acción</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    {listCro}
                                </tbody>
                            </Table>
                            <Row>
                                <Col md="12">
                                    <Pagination>
                                        {renderpages}
                                    </Pagination>
                                </Col>
                                
                            </Row>
                        </CardBody>
                    </Card>
                </Col>
                <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className="modal-dialog modal-lg">
                    <ModalHeader toggle={this.togglePrimary}>Tareas</ModalHeader>
                    <ModalBody>
                        <TareasOfCamp idCamp={this.state.idC} nomC={this.state.nomC} />
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.togglePrimary}>Cerrar</Button>
                    </ModalFooter>
                </Modal>
                <br />

            </Row>

        );
    }
}
/*
<td><Button onClick={() => { this.togglePrimary(); this.getIdNom(item.id, item.nombre); }} >Asignar</Button></td>
*/
export default ListaCronograma;

