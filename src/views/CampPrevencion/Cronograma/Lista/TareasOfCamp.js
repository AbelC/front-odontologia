import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button, Alert } from 'reactstrap';
//LISTA LAS TAREAS PARA GUARDARALAS
var url = process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/asignaTarea/';
class TareasOfCamp extends Component {
    constructor(props) {
        super(props);
        this.asignaTareaDocRefs = [];
        this.asignaTareaEnfRefs = [];
        this.state = {
            listaTieneTareas: [],
            listaMedico: [],
            muestraOp: null,
            idTieneTarea: 0,
            idMedico: 0,
        }
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
    }

    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/tieneTarea/tareasOfCamp/' + this.props.idCamp + '/');
            const listaTieneTareas = await respuesta.json();

            const respuestaM = await fetch(process.env.REACT_APP_API_URL+'odontologia/persona/v1/medico');
            const listaMedico = await respuestaM.json();

            this.setState({
                listaTieneTareas,
                listaMedico
            });

        } catch (e) {
            console.log(e);
        }

        if (this.state.listaTieneTareas.length === 0) {
            //console.log('VACIOO: ', this.state.listaTieneTareas.length)
            this.setState({ muestraOp: false })
        } else {
            //console.log('LLENOOO: ', this.state.listaTieneTareas.length)
            this.setState({ muestraOp: true })
        }
    }

    handleOnSubmit(e) {
        e.preventDefault()

        /*console.log('asignaTareaDocRefs.length: ', this.asignaTareaDocRefs.length )
        console.log('asignaTareaDocRefs', this.asignaTareaDocRefs)
        console.log('*****************************************')
        console.log('asignaTareaEnfRefs.lengh: ', this.asignaTareaEnfRefs.length)
        console.log('asignaTareaEnfRefs: ', this.asignaTareaEnfRefs)*/
        //console.log('asignaTareaDocRefs.length', this.asignaTareaDocRefs.length )
        for (let i = 0; i < this.asignaTareaDocRefs.length; i++) {

            var check = this.asignaTareaDocRefs[i].value;
            var checkEnfer = this.asignaTareaEnfRefs[i].value;
            //console.log('checkMedico: ', check);
            //console.log('checkEnfer: ', checkEnfer);

            if (check !== "0") {
                let data;
                var strArray;
                if (checkEnfer === '') {
                    strArray = check.split(',')
                    data = {
                        idTieneTarea: strArray[0],
                        idMedico: strArray[1],
                        nombreEnfermera: '-'
                    }
                } else {
                    strArray = check.split(',')
                    data = {
                        idTieneTarea: strArray[0],
                        idMedico: strArray[1],
                        nombreEnfermera: checkEnfer
                    }
                }

                //console.log('data: ', data)
                try {
                    fetch(url, {
                        method: 'POST', // or 'PUT'
                        body: JSON.stringify(data), // data can be string or {object}!
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(res => res.json())
                        .catch(error => console.error('Error:', error))
                        .then(response => console.log('Success:', response), console.log('Datos enviados'));

                    window.location.reload();
                } catch (e) {
                    console.log(e);
                }
            }
            // medicoSelct = medicoSelct.concat([this.tareasRefs[i].value])

            //if (check) medicoSelct = medicoSelct.concat([this.asignaTareaDocRefs[i].value])
            //console.log('tareasRefs', this.tareasRefs[i].value)
        }

    }



    render() {
        const listT = this.state.listaTieneTareas.map((item, index) => {
            return (
                <tr key={item.id}>
                    <td>{item.idTarea.nombreTarea}</td>
                    <td>
                        <select type="select" name="doctores" id="doctores" className="form-control" ref={ref => this.asignaTareaDocRefs[index] = ref}>
                            <option value="0">------</option>
                            {
                                this.state.listaMedico.map(item2 => {
                                    return (
                                        <option key={item2.id} value={item.id + ',' + item2.id} >{item2.nombres + ' ' + item2.apellido_paterno}</option>
                                    )
                                })
                            }
                        </select>
                    </td>
                    <td><input type="text" className="form-control" ref={ref => this.asignaTareaEnfRefs[index] = ref} /></td>
                </tr>
            )
        })
        return (
            <Row>
                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i><strong>Asignación de tareas al medico odontologo para la campaña:</strong>  <strong>{this.props.nomC}</strong>
                        </CardHeader>
                        <CardBody>
                            {(this.state.muestraOp === true) ?
                                <Table responsive hover size="sm">
                                    <thead>
                                        <tr>                                
                                            <th scope="col">Tarea</th>
                                            <th scope="col">Asignar a:</th>
                                            <th scope="col">Enfermera</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {listT}
                                    </tbody>
                                </Table> : <Alert color="danger">La campaña <strong>{this.props.nomC}</strong> no tiene asignada ninguna tarea.    <Button color="link" href="#/CampPrevencion/CampTareas">asignar tareas</Button>     </Alert>}
                            {(this.state.muestraOp === true) ? <Button onClick={this.handleOnSubmit}>Guardar</Button> : null}
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        );
    }
}
export default TareasOfCamp;
/**
 
for (var j = 0; j < strArray.length-1; j++) {
                    //console.log('** ', j, '-', strArray[j])

                    let data = {
                        idTieneTarea: strArray[j],
                        idMedico: strArray[j+1],
                    }
                    console.log('data: ', data)
                }

 */