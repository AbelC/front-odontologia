import React from 'react';
import ReactDOM from 'react-dom';
import ListaCronogramaFin from './ListaCronogramaFin';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ListaCronogramaFin />, div);
    ReactDOM.unmountComponentAtNode(div);
});
