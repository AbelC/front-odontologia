import React, { Component } from 'react';
import Search from 'react-search-box';
import { Button, Card, CardBody, CardHeader, Col, Form, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import ListaCronograma from '../Lista/ListaCronograma';
//import Calendario from '../../Calendario/Calendario'
var url = process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/listaCronograma/';
var url2 = process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/asignaCarrera/';
class RegistraCamp extends Component {
    constructor(props) {
        super(props);
        this.carrerasRefs = [];
        this.state = {
            fechaIni: '',
            fechaFin: '',
            lugar: '',
            tipo: '',
            campaign: '',
            version: '',
            medico: '',
            camp: [],
            list: [],
            primary: false,
            warning: false,
            success: false,
            swButton: false,
            muestraOpIntra: false,
            muestraOpExtra: false,
            muestraOpFeria: false,
            listaFacultad: [],
            listaCarrera: [],
            selectCarrera: [],
            danger: false,
        }

        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.togglePrimary = this.togglePrimary.bind(this)
        this.toggleWarning = this.toggleWarning.bind(this);
        this.toggleSuccess = this.toggleSuccess.bind(this);
        this.toggleCerrar = this.toggleCerrar.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handleMuestraCarrera = this.handleMuestraCarrera.bind(this);
        this.toggleDanger = this.toggleDanger.bind(this);
    }
    toggleDanger() {
        this.setState({
            danger: !this.state.danger,
        });
    }
    handleMuestraCarrera(event) {
        console.log('event: ', event)
        var idF = parseInt(event, 10)
        var carrera = []
        carrera = this.state.listaCarrera
        var carreraTrue = []
        // this.setState({ selectCarrera: [] })

        for (let i = 0; i < carrera.length; i++) {
            if (carrera[i].idFacultad === idF) {
                //console.log('carrera:', carrera[i].nombre, ' - ', carrera[i].id);
                let data = {
                    id: carrera[i].id,
                    nombre: carrera[i].nombre
                }
                carreraTrue = carreraTrue.concat([data])
                //console.log('data: ', data)
            }

        }

        this.setState({
            selectCarrera: carreraTrue
        })
    }

    handleSelect(event) {
        //console.log('event: ', event)

        if (event === 'INTRAMURAL') {
            this.setState({
                muestraOpIntra: true,
                muestraOpExtra: false,
                muestraOpFeria: false
            })
        } else if (event === 'EXTRAMURAL') {
            this.setState({
                muestraOpExtra: true,
                muestraOpIntra: false,
                muestraOpFeria: false
            })
        } else if (event === 'FERIAS DE SALUD') {
            this.setState({
                muestraOpFeria: true,
                muestraOpExtra: false,
                muestraOpIntra: false
            })
        }


    }

    toggleWarning() {
        this.setState({
            warning: !this.state.warning,
        });
    }
    toggleSuccess() {
        this.setState({
            success: !this.state.success,
        });
    }

    toggleCerrar() {
        window.location.reload();
    }


    togglePrimary() {
        if (this.fechaIni.value !== '' && this.fechaFin.value !== '' && this.tipo.value !== '' && this.state.campaign !== '') {
            console.log('verdaad')
            this.setState({ primary: !this.state.primary, });
        } else {
            console.log('falso')
            this.setState({ warning: !this.state.warning, });

        }
    }
    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/listaCamp/');
            const camp = await respuesta.json();

            const respuestaF = await fetch(process.env.REACT_APP_API_URL+'umsa/v1/facultad/');
            const listaFacultad = await respuestaF.json();

            const respuestaC = await fetch(process.env.REACT_APP_API_URL+'umsa/v1/carrera/');
            const listaCarrera = await respuestaC.json();

            this.setState({
                camp,
                listaFacultad,
                listaCarrera
            });
        } catch (e) {
            console.log(e);
        }
    }



    handleOnSubmit(e) {
        if (this.fechaFin.value >= this.fechaIni.value) {

            e.preventDefault()
            let data = {
                fechaIni: this.fechaIni.value,
                fechaFin: this.fechaFin.value,
                lugar: this.lugar.value,
                tipo: this.tipo.value,
                version: this.version.value,
                campaign: this.state.campaign,
                medico: 1,

            }

            try {
                fetch(url, {
                    method: 'POST', // or 'PUT'
                    body: JSON.stringify(data), // data can be string or {object}!
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(response => {
                        console.log('Success:', response, 'Datos enviados')
                        if (this.lugar.value === '' && this.carrerasRefs.length !== 0) {
                            var carrerasTrue = []
                            for (let i = 0; i < this.carrerasRefs.length; i++) {
                                var check = this.carrerasRefs[i].checked;
                                if (check) carrerasTrue = carrerasTrue.concat([this.carrerasRefs[i].value])
                            }

                            for (let i = 0; i < carrerasTrue.length; i++) {
                                let data2 = {
                                    idCarrera: carrerasTrue[i],
                                    idCronograma: response.id,
                                }

                                fetch(url2, {
                                    method: 'POST', // or 'PUT'
                                    body: JSON.stringify(data2), // data can be string or {object}!
                                    headers: {
                                        'Content-Type': 'application/json'
                                    }
                                }).then(res => res.json())
                                    .catch(error => console.error('Error:', error))
                                    //  .then(response => console.log('Success:', response))
                                    .then(respx => console.log('Success Asigna:', respx, 'Datos enviados ASIGNA'));
                            }
                        }
                    }

                    );

                window.location.reload();
            } catch (e) {
                console.log(e);
            }

            this.setState({
                fechaIni: '',
                fechaFin: '',
                lugar: '',
                tipo: '',
                campaign: '',
                medico: '',
            });

            this.setState({
                primary: !this.state.primary,
            });

            this.setState({ now: new Date() });

        } else {
            this.setState({
                danger: !this.state.danger,
                primary: !this.state.primary,
            });
        }

    }


    handleChange(value) {
        console.log('Campaña: ', value);
        this.state.camp.map(item => {
            if (value === item.nombre) {
                //console.log('item:',item.id); 
                fetch(process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/busca/camp/' + item.id + '/')
                    .then((Response) => Response.json())
                    .then((findresponse) => {
                        //console.log("FINDRESPONSE=",findresponse)
                        this.setState({
                            list: findresponse,
                            swButton: true,
                        })
                        //console.log('LIST=',this.state.list.length)   

                        if (this.state.list.length === 1) {
                            this.setState({
                                campaign: this.state.list[0].id
                            })
                        }
                    });

            }
            return 0;
        }
        )
    }


    render() {
        const listaFac = this.state.listaFacultad.map(item2 => {
            return (
                <option key={item2.id} value={item2.id} >{item2.nombre}</option>
            )
        });

        const listC = this.state.selectCarrera.map((item, index) => {
            return (
                <FormGroup check key={index}>
                    <input className="form-check-input" type="checkbox" value={item.id} ref={ref => this.carrerasRefs[index] = ref} />
                    <Label className="form-check-label" check>{item.id + '.- ' + item.nombre}</Label>
                </FormGroup>

            )

        })


        //console.log('lenght: ', this.state.selectCarrera.length, 'this.state.selectCarrera: ', this.state.selectCarrera)


        return (
            <div>
                <Row>
                    <Col xs="12">
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify"></i> <strong>Registrar en Cronograma</strong>
                            </CardHeader>
                            <CardBody>
                                <Form>

                                    <Row>
                                        <Col xs="12" sm="6">
                                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                                <Label for="Buscar" className="mr-sm-2">Buscar Campaña</Label>
                                                <Search
                                                    data={this.state.camp}
                                                    onChange={this.handleChange.bind(this)}
                                                    placeholder="Buscar Campaña"
                                                    class="search-class"
                                                    searchKey="nombre"
                                                />
                                                <Button color="link" onClick={this.toggleSuccess} href={'#/CampPrevencion/CampTareas/Campaign'} className="mr-1">Añadir nueva campaña</Button>
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" sm="6">
                                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                                <Label for="fechaIni" className="mr-sm-2">Versión de camapaña</Label>
                                                <input type="text" placeholder="Ej. V1" className="form-control" style={{ width: '5em' }} ref={version => this.version = version} />
                                            </FormGroup>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col xs="12" sm="6">
                                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                                <Label for="fechaIni" className="mr-sm-2">Fecha Inicio</Label>
                                                <input type="date" className="form-control" ref={fechaIni => this.fechaIni = fechaIni} />
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" sm="6">
                                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                                <Label for="fechaFin" className="mr-sm-2">Fecha Conclusión</Label>
                                                <input type="date" ref={fechaFin => this.fechaFin = fechaFin} className="form-control" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs="12" sm="6">
                                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                                <Label for="Buscar" className="mr-sm-2">Tipo</Label>
                                                <select type="select" ref={tipo => this.tipo = tipo} className="form-control" onChange={e => this.handleSelect(e.target.value)}>
                                                    <option value="">Seleccionar tipo de campaña</option>
                                                    <option value="INTRAMURAL">Intramural</option>
                                                    <option value="EXTRAMURAL">Extramural</option>
                                                    <option value="FERIAS DE SALUD">Ferias de Salud</option>
                                                </select>
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" sm="6">
                                            {
                                                (this.state.muestraOpIntra === true) ?
                                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                                        <Label for="lugar" className="mr-sm-2">Lugar</Label>
                                                        <input type="text" className="form-control" value="PROMES" disabled ref={lugar => this.lugar = lugar} />
                                                    </FormGroup> : null
                                            }

                                            {
                                                (this.state.muestraOpExtra === true) ?
                                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                                        <Label for="lugar" className="mr-sm-2">Lugar</Label>
                                                        <input type="hidden" value="" ref={lugar => this.lugar = lugar} />
                                                        <select type="select" className="form-control" onChange={e => this.handleMuestraCarrera(e.target.value)}>
                                                            <option value="">Seleccionar Facultad</option>
                                                            {listaFac}
                                                        </select>

                                                        {listC}
                                                    </FormGroup> : null


                                            }

                                            {
                                                (this.state.muestraOpFeria === true) ?
                                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                                        <Label for="lugar" className="mr-sm-2">Lugar</Label>
                                                        <input type="text" className="form-control" ref={lugar => this.lugar = lugar} placeholder="Lugar donde se realizara la campaña" />
                                                    </FormGroup> : null
                                            }




                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs="6" sm="4"></Col>
                                        <Col xs="6" sm="4"></Col>
                                        <Col sm="4">
                                            <br />
                                            {(this.state.swButton === true) ? <Button color="success" onClick={this.togglePrimary} >Guardar</Button> : null}
                                        </Col>
                                    </Row>
                                </Form>
                            </CardBody>
                        </Card>
                    </Col>

                    <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-primary ' + this.props.className}>
                        <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
                        <ModalBody>
                            ¿Desea agregar al cronograma de camapañas de prevencion?
                    </ModalBody>
                        <ModalFooter>
                            <Button onClick={this.handleOnSubmit} type="submit" color="primary">Aceptar</Button>{' '}
                            <Button color="secondary" onClick={this.togglePrimary}>Cancelar</Button>
                        </ModalFooter>
                    </Modal>



                    <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
                        className={'modal-warning ' + this.props.className}>
                        <ModalHeader toggle={this.toggleWarning}>Campos vacios</ModalHeader>
                        <ModalBody>
                            Llene los campos faltantes, para agregar al cronograma de camapañas de prevencion.
                    </ModalBody>
                        <ModalFooter>
                            <Button color="warning" onClick={this.toggleWarning}>Volver</Button>{' '}

                        </ModalFooter>
                    </Modal>
                </Row>
                <Modal isOpen={this.state.danger} toggle={this.toggleDanger}
                    className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDanger}>Alerta</ModalHeader>
                    <ModalBody>
                        Error en la fecha de inicio o en la fecha de conclusión.
                        La fecha de conclusión debe ser mayor a la fecha de inicio.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.toggleDanger}>Cerrar</Button>{' '}                        
                    </ModalFooter>
                </Modal>

                <ListaCronograma />

            </div>
        );
    }
}
/*

*/
export default RegistraCamp;