import React from 'react';
import ReactDOM from 'react-dom';
import RegistraCamp from './RegistraCamp';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<RegistraCamp />, div);
    ReactDOM.unmountComponentAtNode(div);
});
