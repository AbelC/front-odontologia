import React from 'react';
import ReactDOM from 'react-dom';
import ListaExtramural from './ListaExtramural';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ListaExtramural />, div);
    ReactDOM.unmountComponentAtNode(div);
});
