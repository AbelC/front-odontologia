import React from 'react';
import ReactDOM from 'react-dom';
import RegistraExtramural from './RegistraExtramural';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<RegistraExtramural />, div);
    ReactDOM.unmountComponentAtNode(div);
});
