import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Button, Alert, Table, Modal, ModalBody, ModalFooter, ModalHeader, DropdownItem } from 'reactstrap';

import Search from 'react-search-box';
import ListaPacienteAtendido from '../ListaPacienteAtendido/ListaPacienteAtendido'

var fecha_server = new Date();
var dia = fecha_server.getDate();
var mes = null;
//var mes = fecha_server.getMonth();

switch (fecha_server.getMonth()) {
  case 0:
    mes = 1;
    break;
  case 1:
    mes = 2;
    break;
  case 2:
    mes = 3;
    break;
  case 3:
    mes = 4;
    break;
  case 4:
    mes = 5;
    break;
  case 5:
    mes = 6;
    break;
  case 6:
    mes = 7;
    break;
  case 7:
    mes = 8;
    break;
  case 8:
    mes = 9;
    break;
  case 9:
    mes = 10;
    break;
  case 10:
    mes = 11;
    break;
  case 11:
    mes = 12;
    break;
  default:
    mes = '';


}
var year = fecha_server.getFullYear();

var fecha_en = '';
var fecha_show = '';
fecha_en = year + '-' + mes + '-' + dia;
fecha_show = dia + '-' + mes + '-' + year;

var url = process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/listaCampIntraMural/';
class Buscar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      data: [],
      listMedicoTarea: [],
      loading: false,
      muestraDatos: false,
      muestraTabla: false,

      id: '',
      matricula: '',
      nombres: '',
      apellidoPat: '',
      apellidoMat: '',
      edad: null,
      sexo: '',
      observaciones:'',
      success: false,

      idCro: this.props.match.params.idCro,
      idMedico: this.props.match.params.idMedico,
      nomCamp: this.props.match.params.nomCamp,
      version: this.props.match.params.v,

      nombreMedico: '',
      apPatMedico: '',
      apMatMedico: '',
      idMedicoP:0,




    };
    this.handleChange = this.handleChange.bind(this);
    this.handleDatosPers = this.handleDatosPers.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.toggleSuccess = this.toggleSuccess.bind(this);

  }



  async componentDidMount() { //ciclo de vida de react de forma asincrona

    this.setState({
      loading: true
    });

    try {
      const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/persona/v1/afiliado/');
      const data = await respuesta.json();

      const respuesta2 = await fetch(process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/asignaTareaJoin/' + this.state.idMedico + '/');
      const listMedicoTarea = await respuesta2.json();
      console.log('listMedicoTarea=', listMedicoTarea[0].idMedico.nombres)
      /**
        nombreMedico:listMedicoTarea.idMedico.nombres,
              apPatMedico:listMedicoTarea.idMedico.apellido_paterno,
              apMatMedico:listMedicoTarea.idMedico.apellido_Materno,
       */
      this.setState({
        data,
        idMedicoP:listMedicoTarea[0].idMedico.id,
        nombreMedico:listMedicoTarea[0].idMedico.nombres,
        apPatMedico:listMedicoTarea[0].idMedico.apellido_paterno,
        apMatMedico:listMedicoTarea[0].idMedico.apellido_materno,
        listMedicoTarea,        
        loading: false,
      });

    } catch (e) {
      console.log(e);
      console.log('listAfiliado=', this.state.data)
    }
  }

  handleChange(value) {
    //console.log(value);
    //console.log("data=",this.state.data)
    this.state.data.map(item => {
      if (value === item.cod_promes) {
        this.setState({
          id: item.id,
          muestraDatos: true,
        })
      } else {
        //console.log('no hay ese id');
        // this.setState({ muestraAlerta: true })
      }
      return 0;
    });
  }

  handleDatosPers() {
    fetch(process.env.REACT_APP_API_URL+'odontologia/persona/v1/busca/afiliado/' + this.state.id + '/')
      .then((Response) => Response.json())
      .then((findresponse) => {
        //console.log("FINDRESPONSE[]=",findresponse[0].pr_consulta)
        //console.log("FINDRESPONSE=", findresponse)
        var y = findresponse[0].fecha_nacimiento.split('-');
        //console.log('year: ', year - y[0])
        this.setState({
          id: findresponse[0].id,
          matricula: findresponse[0].cod_promes,
          nombres: findresponse[0].nombres,
          apellidoPat: findresponse[0].apellido_pat,
          apellidoMat: findresponse[0].apellido_mat,
          edad: year - y[0],
          sexo: findresponse[0].sexo,
          muestraTabla: true,
          muestraDatos: false
        })
      });



  }


  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });
  }
  handleOnSubmit(e) {
    console.log('DATOS NO VACIOS')
    e.preventDefault()
    let data = {
      fecha_atencion: fecha_en,
      cronograma: this.state.idCro,
      afiliado: this.state.id, // this.props.match.params.id,  
      medico: this.state.idMedico,
      observaciones:this.observaciones.value,
    }

    try {
      fetch(url, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be string or {object}!
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response));
      window.location.reload();
    } catch (e) {
      console.log(e);
    }

    this.setState({
      id: '',
      matricula: '',
      nombres: '',
      apellidoPat: '',
      apellidoMat: '',
      edad: '',
      sexo: '',
      success: !this.state.success,
    });
  }



  render() {
    //console.log('***  ', this.props)
    if (this.state.loading) {
      return (
        <div className="">Esperando conexion...</div>
      );
    }
    const listMed = this.state.listMedicoTarea.map(item => {
      return (
        <ul key={item.id} >
          <li>{item.idTieneTarea.idTarea.nombreTarea}</li>
        </ul>
      )

    })

    return (
      <div className="animated fadeIn">
        <Alert style={{ 'width': '50%' }} color="dark">
          <strong><h4><center>Panel de información</center></h4></strong>
          <DropdownItem divider />
          <strong>Nombre de la campaña:</strong>{' ' + this.state.nomCamp + ' '} <strong>Versión:</strong>{this.state.version}<br />
          <strong>Dr(a):</strong> {this.state.idMedicoP+ ' ' + this.state.nombreMedico + ' ' + this.state.apPatMedico + ' ' + this.state.apMatMedico}<br />
          La(s) Tarea(s) que usted tiene para esta campaña son:
          {listMed}
        </Alert>
        <Row>
          <Col xs="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Buscar Paciente</strong>
                <div className="card-header-actions">
                  <strong className="text-muted">FECHA: {fecha_show}</strong>
                </div>

              </CardHeader>
              <CardBody>
                <form>
                  <div className="form-group">
                    <Search
                      data={this.state.data}
                      onChange={this.handleChange.bind(this)}
                      placeholder="Matricula PROMES"
                      class="search-class"
                      searchKey="cod_promes"
                    />
                  </div>
                </form>
                {(this.state.muestraDatos === true) ? <Button color="primary" className="btn btn-outline-primary" block onClick={this.handleDatosPers}>Ver datos personales</Button> : (this.state.muestraAlerta === true) ? <Alert color="danger">Usuario no afiliado</Alert> : null}

                <br />
                {
                  (this.state.muestraTabla === true) ?
                    <Table responsive bordered size="sm" >
                      <thead>
                        <tr>
                          <td>ID</td>
                          <th scope="col" style={{ 'textAlign': 'center' }}>Matricula de Seguro</th>
                          <th scope="col" style={{ 'textAlign': 'center' }}>Nombres</th>
                          <th scope="col" style={{ 'textAlign': 'center' }}>Apellidos</th>
                          <th scope="col" style={{ 'textAlign': 'center' }}> Edad</th>
                          <th scope="col" style={{ 'textAlign': 'center' }}>Sexo</th>
                          <th scope="col" style={{ 'textAlign': 'center' }}>Observación</th>
                          <th scope="col" style={{ 'textAlign': 'center' }}>Acción</th>
                        </tr>
                      </thead>
                      <tbody>

                        <tr>
                          <td> {this.state.id}</td>
                          <td style={{ 'textAlign': 'center' }}>{this.state.matricula}</td>
                          <td style={{ 'textAlign': 'center' }}>{this.state.nombres}</td>
                          <td style={{ 'textAlign': 'center' }}> {this.state.apellidoPat + ' ' + this.state.apellidoMat}</td>
                          <td style={{ 'textAlign': 'center' }}>{this.state.edad + ' años'}</td>
                          <td style={{ 'textAlign': 'center' }}>{this.state.sexo}</td>
                          <td><textarea style={{ 'width': '100%', 'height': '100px' }} ref={observaciones => this.observaciones = observaciones} placeholder="Observaciones"></textarea> </td>
                          <td style={{ 'textAlign': 'center' }}><Button color="link" onClick={this.toggleSuccess} className="mr-1">Registrar atención</Button></td>
                        </tr>
                      </tbody>
                    </Table> : null
                }

              </CardBody>
            </Card>
          </Col>
        </Row>


        <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
          className={'modal-success ' + this.props.className}>
          <ModalHeader toggle={this.toggleSuccess}>Confirmación</ModalHeader>
          <ModalBody>
            ¿Esta seguro de registrar la atención?
                  </ModalBody>
          <ModalFooter>
            <Button color="success" onClick={this.handleOnSubmit}>Aceptar</Button>{' '}
            <Button color="secondary" onClick={this.toggleSuccess}>Cancelar</Button>
          </ModalFooter>
        </Modal>


        <ListaPacienteAtendido idCro={this.state.idCro} idMedico={this.state.idMedico} />
      </div>

    );
  }

}
export default Buscar;
