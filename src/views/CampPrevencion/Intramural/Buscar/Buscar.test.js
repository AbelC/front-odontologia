import React from 'react';
import ReactDOM from 'react-dom';
import Buscar from './Buscar';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Buscar />, div);
    ReactDOM.unmountComponentAtNode(div);
});