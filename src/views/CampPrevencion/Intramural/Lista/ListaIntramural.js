import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Pagination, PaginationItem, PaginationLink,Button  } from 'reactstrap';
var fecha_server = new Date();
var dia = fecha_server.getDate();
var mes = null;
//var mes = fecha_server.getMonth();

switch (fecha_server.getMonth()) {
    case 0:
        mes = 1;
        break;
    case 1:
        mes = 2;
        break;
    case 2:
        mes = 3;
        break;
    case 3:
        mes = 4;
        break;
    case 4:
        mes = 5;
        break;
    case 5:
        mes = 6;
        break;
    case 6:
        mes = 7;
        break;
    case 7:
        mes = 8;
        break;
    case 8:
        mes = 9;
        break;
    case 9:
        mes = 10;
        break;
    case 10:
        mes = 11;
        break;
    case 11:
        mes = 12;
        break;
    default:
        mes = '';


}


var year = fecha_server.getFullYear();

var fecha_en = '';
//var fecha_show='';
fecha_en = year + '-' + mes + '-' + dia;
//fecha_show = dia + '-' + mes + '-' + year ;

class ListaIntramural extends Component {
    constructor(props) {
        super(props);

        this.state = {
            listaCronograma: [],
        }


    }


    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/listaNoEndIntramural/' + fecha_en + '/');
            const listaCronograma = await respuesta.json();

            this.setState({
                listaCronograma,
                
            });
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        const listCroFin = this.state.listaCronograma.map(item => {
            return (
                <tr key={item.id} >
                    <td style={{ 'textAlign': 'center' }}>{item.campaign.nombre}</td>
                    <td style={{ 'textAlign': 'center' }}>{item.version}</td>
                    <td style={{ 'textAlign': 'center' }}>{item.lugar}</td>
                    <td style={{ 'textAlign': 'center' }}>{item.tipo}</td>
                    <td style={{ 'textAlign': 'center' }}>{item.fechaFin}</td>
                    <td><Button color="primary" block href={'#/CampPrevencion/Intramural/Buscar/' + item.id +'/' + 2 + '/'+item.campaign.nombre +'/'+item.version }>Registrar</Button></td>
                </tr>
            )

        })
        return (
            <Row>
                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i> <strong>Lista de Campañas Intramurales</strong>
                        </CardHeader>
                        <CardBody>
                            <Table responsive hover bordered size="sm" >
                                <thead>
                                    <tr>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Campaña</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Versión</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Lugar / Facultad</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>tipo</th>                                
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Fecha de conclusión</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Acción</th>                                      
                                    </tr>
                                </thead>
                                <tbody>
                                    {listCroFin}
                                </tbody>
                            </Table>
                            <Pagination aria-label="Page navigation example">
                                <PaginationItem disabled>
                                    <PaginationLink previous href="#" />
                                </PaginationItem>
                                <PaginationItem active>
                                    <PaginationLink href="#">
                                        1
                                    </PaginationLink>
                                </PaginationItem>
                                <PaginationItem>
                                    <PaginationLink href="#">
                                        2
                                    </PaginationLink>
                                </PaginationItem>
                                <PaginationItem>
                                    <PaginationLink href="#">
                                        3
                                    </PaginationLink>
                                </PaginationItem>
                                <PaginationItem>
                                    <PaginationLink href="#">
                                        4
                                    </PaginationLink>
                                </PaginationItem>
                                <PaginationItem>
                                    <PaginationLink href="#">
                                        5
                                    </PaginationLink>
                                </PaginationItem>
                                <PaginationItem>
                                    <PaginationLink next href="#" />
                                </PaginationItem>
                            </Pagination>
                        </CardBody>
                    </Card>
                </Col>
            </Row>

        );
    }
}
export default ListaIntramural;

