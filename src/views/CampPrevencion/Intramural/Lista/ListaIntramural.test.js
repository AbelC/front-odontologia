import React from 'react';
import ReactDOM from 'react-dom';
import ListaIntramural from './ListaIntramural';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ListaIntramural />, div);
    ReactDOM.unmountComponentAtNode(div);
});
