import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Pagination, PaginationItem, PaginationLink, Table } from 'reactstrap';
var fecha_server = new Date();
var year = fecha_server.getFullYear();


class ListaPacienteAtendido extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listaRegIntraMedico: []
        }


    }


    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/campPrevencion/v1/listaRegIntraMedico/' + this.props.idCro + '/' + this.props.idMedico + '/');
            const listaRegIntraMedico = await respuesta.json();

            this.setState({
                listaRegIntraMedico
            });
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        const listIntraMed = this.state.listaRegIntraMedico.map((item, i) => {
            var y = item.afiliado.fecha_nacimiento.split('-');
            var edad = year - y[0];
            return (

                <tr key={item.id} >
                    <td style={{ 'textAlign': 'center' }}>{item.id}</td>
                    <td style={{ 'textAlign': 'center' }}>{i + 1}</td>
                    <td style={{ 'textAlign': 'center' }}>{item.fecha_atencion}</td>
                    <td style={{ 'textAlign': 'center' }}>{item.afiliado.cod_promes}</td>
                    <td style={{ 'textAlign': 'center' }}>{item.afiliado.nombres + ' ' + item.afiliado.apellido_pat + ' ' + item.afiliado.apellido_mat}</td>
                    <td style={{ 'textAlign': 'center' }}>{edad}</td>
                    <td style={{ 'textAlign': 'center' }}>{item.afiliado.sexo}</td>
                    <td style={{ 'textAlign': 'center' }}>{item.observaciones}</td>
                </tr>
            )
        })
        return (
            <Row>
                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i> <strong>Registro de estudiante atendido en campaña</strong>
                        </CardHeader>
                        <CardBody>
                            <Table responsive hover bordered size="sm" >
                                <thead>
                                    <tr>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>id</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Nro</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Fecha</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Matricula del Seguro</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Nombres y apellidos</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Edad</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Sexo</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Observación</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    {listIntraMed}
                                </tbody>
                            </Table>




                            <Pagination aria-label="Page navigation example">
                                <PaginationItem disabled>
                                    <PaginationLink previous href="#" />
                                </PaginationItem>
                                <PaginationItem active>
                                    <PaginationLink href="#">
                                        1
                                    </PaginationLink>
                                </PaginationItem>
                                <PaginationItem>
                                    <PaginationLink href="#">
                                        2
                                    </PaginationLink>
                                </PaginationItem>
                                <PaginationItem>
                                    <PaginationLink href="#">
                                        3
                                    </PaginationLink>
                                </PaginationItem>
                                <PaginationItem>
                                    <PaginationLink href="#">
                                        4
                                    </PaginationLink>
                                </PaginationItem>
                                <PaginationItem>
                                    <PaginationLink href="#">
                                        5
                                    </PaginationLink>
                                </PaginationItem>
                                <PaginationItem>
                                    <PaginationLink next href="#" />
                                </PaginationItem>
                            </Pagination>
                        </CardBody>
                    </Card>
                </Col>
            </Row>

        );
    }
}
export default ListaPacienteAtendido;

