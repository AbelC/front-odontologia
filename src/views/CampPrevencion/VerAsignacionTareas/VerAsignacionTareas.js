import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
// TabContent, TabPane, ListGroup, ListGroupItem,
class VerAsignacionTareas extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // activeTab: 0,
            listaCronograma: [],
            modal: false
        }

        //this.toggle = this.toggle.bind(this);
        this.toggle = this.toggle.bind(this);
        this.get = this.get.bind(this);
    }

    get(idCronograma, idCamp, nomCamp) {
        console.log('idCronograma: ', idCronograma, ' idCamp.', idCamp, ' NomC: ', nomCamp)
        /*  this.setState({
              idC: valor,
              nomC: nom
          });*/
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    /*toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }*/



    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL + 'odontologia/campPrevencion/v1/listaCronogramaCamp/');
            const listaCronograma = await respuesta.json();
            this.setState({
                listaCronograma
            });
        } catch (e) {
            console.log(e);
        }
    }





    render() {
        const listCro = this.state.listaCronograma.map((item, i) => {
            return (
                <tr key={item.id}>

                    <td>{item.campaign.nombre}</td>

                    <td>{item.version}</td>
                    <td>{item.tipo}</td>
                    <td>{item.lugar}</td>
                    <td>{item.fechaIni}</td>
                    <td>{item.fechaFin}</td>
                </tr>
            )
        });
        /*  const listCro = this.state.listaCronograma.map((item, i) => {
              return (
                  <ListGroupItem key={item.id} onClick={() => this.toggle(i)} action active={this.state.activeTab === i} >
                      <table>
                          <tbody>
                              <tr><td>{item.campaign.nombre + ' - ' + item.version + ' - ' + item.tipo}</td></tr>
                              <tr><td>{item.lugar}</td></tr>
                              <tr><td>{item.fechaIni + ' / ' + item.fechaFin}</td></tr>
                          </tbody>
                      </table>
  
  
                  </ListGroupItem>
  
              )
          });
  
          const listOp = this.state.listaCronograma.map((item, i) => {
              return (
                  <TabPane key={item.id} tabId={i} >
                      <p>
                          {item.campaign.descripcion}
                      </p>
                  </TabPane>
              )
          });*/
        return (
            <Row>
                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i> <strong>Ver Asiganción de Tareas</strong>
                        </CardHeader>
                        <CardBody>
                            <Table responsive hover bordered size="sm" >
                                <thead>
                                    <tr>

                                        <th scope="col" style={{ 'textAlign': 'center' }}>Nombre</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Versión</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>tipo</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Lugar / Facultad</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Fecha de Inicio</th>
                                        <th scope="col" style={{ 'textAlign': 'center' }}>Fecha de conclusión</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    {listCro}
                                </tbody>
                            </Table>

                        </CardBody>
                    </Card>
                </Col>


                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Tareas Asignadas</ModalHeader>
                    <ModalBody>

                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggle}>Cerrar</Button>{' '}
                    </ModalFooter>
                </Modal>
            </Row>
        );
    }
}

/*
<Row>
                                <Col xs="5">
                                    <ListGroup id="list-tab" role="tablist" >
                                        {listCro}
                                    </ListGroup>
                                </Col>
                                <Col xs="7">
                                    <TabContent activeTab={this.state.activeTab}>
                                        {listOp}
                                    </TabContent>
                                </Col>
                            </Row>


                             <td><Button color="link" onClick={() => { this.toggle(); this.get(item.id,item.campaign.id, item.campaign.nombre); }}>{item.campaign.nombre + ' - ' + item.campaign.id}</Button></td>
*/
export default VerAsignacionTareas;
