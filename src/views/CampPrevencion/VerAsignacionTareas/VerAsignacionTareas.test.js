import React from 'react';
import ReactDOM from 'react-dom';
import VerAsignacionTareas from './VerAsignacionTareas';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<VerAsignacionTareas />, div);
    ReactDOM.unmountComponentAtNode(div);
});
