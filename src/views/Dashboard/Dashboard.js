import React, { Component } from 'react';
//import { Bar, Line } from 'react-chartjs-2';
import { Row, Col, CardImg, Card } from 'reactstrap';
//import Widget03 from '../../views/Widgets/Widget03'
//import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
//import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'


class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
      idMedico:localStorage.getItem("idMedico"),
      nomAp:localStorage.getItem("nombre"),
      cargo:localStorage.getItem("cargo"),
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }

  render() {

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="0" md="2"></Col>
          <Col xs="6" md="2">
            <a href="#/dashboard">
              <Card>
                <CardImg top width="100%" src="assets/promes.jpg" alt="Index" />
              </Card>
            </a>
          </Col>
          <Col xs="6" md="2">
            <a href="#/PrimeraConsulta">
              <Card>
                <CardImg top width="100%" src="assets/img/menu/primeraconsulta.png" alt="Primera consulta odontologica" />
              </Card>
            </a>
          </Col>
          <Col xs="6" md="2">
            <a href="#/Tratamiento">
              <Card>
                <CardImg top width="100%" src="assets/img/menu/tratamiento.png" alt="Tratamiento odontologico" />
              </Card>
            </a>
          </Col>
        </Row>
        <br />
        <Row>
          <Col xs="0" md="2"></Col>
          <Col xs="6" md="2">
            <a href="#/Insumos/BindCard">
              <Card>
                <CardImg top width="100%" src="assets/img/menu/insumosmedicos.png" alt="Insumos medicos" />
              </Card>
            </a>
          </Col>
          <Col xs="6" md="2">
            <a href="#/Pedido/PedidoMedicamentoInsumo">
              <Card>
                <CardImg top width="100%" src="assets/img/menu/pedidoInsumos.png" alt="Pedido de insumos" />
              </Card>
            </a>
          </Col>
          <Col xs="6" md="2">
            <a href="#/CampPrevencion/CampTareas">
              <Card>
                <CardImg top width="100%" src="assets/img/menu/campaing.png" alt="Campañas de prevencion bucal" />
              </Card>
            </a>
          </Col>
        </Row>



      </div>
    );
  }
}
export default Dashboard;
