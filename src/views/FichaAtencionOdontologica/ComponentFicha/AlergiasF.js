import React, { Component } from 'react';
import { Table, Col } from 'reactstrap';
//import { AppSwitch } from '@coreui/react'
//import Tolerancia_F from './Tolerancia_F';


class Alergias_F extends Component {
    constructor(props) {
        super(props);
        this.state = {
            alergias: [],
            i:0,
        }
    }


    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/alergia/v1/lista/alergias/afiliado/' + this.props.idAf + '/');
            const alergias = await respuesta.json();
            this.setState({
                alergias
            });
        } catch (e) {
            console.log(e);
        }


    }

increment(){
    this.setState({i: this.state.i + 1})
}



    render() {
        
        return (





            
                <Col>
                    Alergia a: 
                    <Table responsive hover bordered size="sm">
                    <tbody>
                        {this.state.alergias.map((item, i) => (
                            <tr key={item.id}>
                                <td>{i+1}</td><td>{item.alergia.descripcion}</td>
                            </tr>

                            
                        ) ) }
                    </tbody>
                    </Table>
                </Col>
              

        );
    }
}

export default Alergias_F;
