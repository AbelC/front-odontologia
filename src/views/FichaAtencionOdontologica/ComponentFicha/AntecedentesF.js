import React, { Component } from 'react';
import { Table, CardBody } from 'reactstrap';
//import { AppSwitch } from '@coreui/react'


class Antecedentes_F extends Component {

    constructor(props) {
        super(props);
        this.state = {
            antecedentes: []
        }
    }




    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/lista/antecedentes/afiliado/' + this.props.idAf + '/');
            const antecedentes = await respuesta.json();
            this.setState({
                antecedentes
            });
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        return (
            <div>
                <CardBody>
                    Antecedentes de una enfermedad de importancia y medicaccion constante
                    <Table responsive hover bordered size="sm">
                        <tbody>
                            {this.state.antecedentes.map((item, i) => (
                                <tr key={item.id}>
                                    <td>{i + 1}</td><td><strong>Fecha:</strong></td><td>{item.fecha}</td><td><strong>Enfermedad:</strong></td><td>{item.enfermedad.descripcion}</td><td><strong>Medicacion:</strong></td><td>{item.medicacion}</td><td><strong>Tipo:</strong></td><td>{item.tipo}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </CardBody>
            </div>


        );
    }
}

export default Antecedentes_F;
