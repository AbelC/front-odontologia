import React, { Component } from 'react';
import { Table, CardBody } from 'reactstrap';
//import { AppSwitch } from '@coreui/react'


class DatosPersonales_F extends Component {

    constructor(props) {
        super(props);

        this.state = {
            afiliado_list: [],
            apellido_pat: '',
            apellido_mat: '',
            apellido_esposo: '',
            nombres: '',
            edad: '',
            sexo: '',
            estado_civil: '',
            ocupacion: '',
            domicilio: '',
            telefono: '',
            odontologo: '',
            fecha: '',

        }

    }

    async componentDidMount() {//ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/persona/v1/busca/afiliado/' + this.props.idAf + '/');
            const afiliado_list = await respuesta.json();

            //console.log('afiliado=', afiliado_list)
            //console.log('nombre==', afiliado_list[0].nombres)
            this.setState({
                afiliado_list,
                apellido_pat: afiliado_list[0].apellido_pat,
                apellido_mat: afiliado_list[0].apellido_mat,
                apellido_esposo: '',
                nombres: afiliado_list[0].nombres,
                edad: '',
                sexo: '',
                estado_civil: '',
                ocupacion: '',
                domicilio: '',
                telefono: '',
                odontologo: '',
                fecha: '',

            });

            //  console.log('thisEStae00 == ', this.state.afiliado_list)
            //console.log('id== ', afiliado_list[0].id)


        } catch (e) {
            console.log(e);
            console.log('entro al cath')
        }
    }


    render() {

        /*  const lista_afiliado = this.state.afiliado_list.map((item, indice) => {
              return (
                  <tr key={item.id}>
                      <td>{item.ci}</td>
                      <td>{item.registro_universitario}</td>
                      <td>{item.cod_promes}</td>
                      <td>{item.apellido_pat}</td>
                      <td>{item.apellido_mat}</td>
                      <td>{item.nombres}</td>
                      <td>{item.fecha_nacimiento}</td>
                  </tr>
  
              )
  
          }
          )*/

        return (


            <div>
                <CardBody>



                    <Table responsive hover bordered size="sm">

                        <tbody>
                            <tr>
                                <td>{this.state.apellido_pat}</td>
                                <td>{this.state.apellido_mat}</td>
                                <td>-</td>
                                <td>{this.state.nombres}</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr>
                                <td><strong>Apellido Paterno</strong></td>
                                <td><strong>Apellido Materno</strong></td>
                                <td><strong>Apellido del esposo</strong></td>
                                <td><strong>Nombres</strong></td>
                                <td><strong>Edad</strong></td>
                                <td><strong>Sexo</strong></td>
                            </tr>

                        </thead>
                        <tbody>
                            <tr>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr>
                                <td><strong>Estado Civil</strong></td>
                                <td><strong>Ocupacion</strong></td>
                                <td><strong>Domicilio</strong></td>
                                <td><strong>Telefono</strong></td>
                                <td><strong>Odontologo</strong></td>
                                <td><strong>Fecha de Atencion</strong></td>
                            </tr>
                        </thead>
                    </Table>
                </CardBody>
            </div >


        );
    }
}

export default DatosPersonales_F;
