import React, { Component } from 'react';
import { Table, CardBody } from 'reactstrap';
//import { AppSwitch } from '@coreui/react'
import OdontogramaF2 from './OdontogramaF2';

class ExamenClinico_F extends Component {
    constructor(props) {
        super(props)
        this.state = {
            examen: [],
            idExGet:0,
            swRender:false,
        }
    }
    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/tolerancia/v1/lista/examenclinico/afiliado/' + this.props.idAf + '/');
            const examen = await respuesta.json();
            //console.log('idEX: F-> ', examen[0].id)
            this.setState({
                idExGet:examen[0].id,
                swRender:true,
                examen
            });
        } catch (e) {
            console.log(e);
        }
    }
    render() {
        return (
            <div>
                <CardBody>
                    <Table responsive hover bordered size="sm">
                        <thead>
                            <tr>
                                <th scope="col">Higiene Bucal</th>
                                <th scope="col">Oclusión</th>
                                <th scope="col">Mucosa Gingival</th>
                                <th scope="col">Enfermedad Periodontal</th>
                                <th scope="col">Mucosa Bucal</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.examen.map(item => (
                                <tr key={item.id}>
                                    <td>{item.higieneBucal}</td>
                                    <td>{item.oclusion}</td>
                                    <td>{item.mucosa_gingival}</td>
                                    <td>{item.enfermedad_periodontal}</td>
                                    <td>{item.mucosa_bucal}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </CardBody>


                <OdontogramaF2 idExGet={this.state.idExGet} swRender={this.state.swRender}/>

            </div>


        );
    }
}

export default ExamenClinico_F;
