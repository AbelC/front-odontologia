import React, { Component } from 'react';
import { Table, Row, Col } from 'reactstrap';

class ListaMotivoConsultaF extends Component {
    //, Modal, ModalBody, ModalHeader, ModalFooter
    constructor(props) {
        super(props);
        this.state = {
            list_tieneMotivoConsulta: [],
        }
    }

    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/tieneMotivoConsultJoin/' + this.props.idAf + '/');
            const list_tieneMotivoConsulta = await respuesta.json();
            this.setState({
                list_tieneMotivoConsulta
            });
        } catch (e) {
            console.log(e);
        }
    }
    render() {
        const motivoConsultaList = this.state.list_tieneMotivoConsulta.map(item => {
            return (
                <tr key={item.id}>
                    <td>{item.motivoConsulta_id.detalle}</td>
                    <td>{item.fecha_motivoConsulta}</td>
                </tr>
            )
        })

        return (
            <div className="animated fadeIn">
                <Row>
                    <Col xs="12">                        
                            <Row>
                                <Col sm="12" md={{ size: 8, offset: 2 }}>
                                    <Table responsive hover bordered size="sm" style={{ 'textAlign': 'center' }}>
                                        <thead>
                                            <tr>
                                                <th scope="col">Motivo de Consulta</th>
                                                <th scope="col">Fechas de Motivos de consulta</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {motivoConsultaList}
                                        </tbody>
                                    </Table>
                                </Col>
                            </Row>                        
                    </Col>
                </Row>
            </div>
        );
    }
}
/* <td> <Button outline color="warning" href={'#/PrimeraConsulta/Alergia/Editar/' + item.id + '/' + item.alergia.descripcion + '/' + item.alergia.id} >Editar</Button></td> */
export default ListaMotivoConsultaF;
