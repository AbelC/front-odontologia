import React, { Component } from 'react';
import { CardBody, Col, Row, Table } from 'reactstrap';
//import { AppSwitch } from '@coreui/react'
//import Odontogram from '../../PrimeraConsulta/Odontograma/components/Odontogram'
var optionesSVG = {
    stroke: 'navy',
    strokeWidth: '0.5',
    opacity: '1',

};
const points0 = { points: [[0, 0], [20, 0], [15, 5], [5, 5]], }
const points1 = { points: [[5, 15], [15, 15], [20, 20], [0, 20]], }
const points2 = { points: [[15, 5], [20, 0], [20, 20], [15, 15]], }
const points3 = { points: [[0, 0], [5, 5], [5, 15], [0, 20]], }
const points4 = { points: [[5, 5], [15, 5], [15, 15], [5, 15]] }

//pieza 1.
const pieza11 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza12 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza13 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza14 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza15 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza16 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza17 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza18 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
//pieza 2.
const pieza21 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza22 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza23 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza24 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza25 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza26 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza27 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza28 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
//piezas 3.
const pieza31 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza32 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza33 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza34 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza35 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza36 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza37 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza38 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
//piezas 4.
const pieza41 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza42 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza43 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza44 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza45 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza46 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza47 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};
const pieza48 = {
    vestubular: '',
    distal: '',
    palatina: '',
    mesial: '',
    oclusal: '',
    pieza_dental: '',
    patologia: 0,
    idExamenClinico: 0,
};


class Odontograma_F extends Component {
    constructor(props) {
        super(props);
        this.state = {
            diente: [],
            stroke: 'navy',
            strokeWidth: '0.5',
            opacity: '1',
            swCut: false,
        };
        //this.print2 = this.print2.bind(this)
    }



    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            //console.log('this.props.idExGet: ', this.props.idExGet)
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/examenclinico/v1/listaOdontogramaPorEx/' + this.props.idExGet + '/');
            const diente = await respuesta.json();
            //console.log('diente: ', diente)
            this.setState({
                diente
            });
            if (this.props.idExGet === 0) {
                this.setState({ swCut: false })
            } else if (this.props.idExGet !== 0) {
                this.setState({ swCut: true })
            }
        } catch (e) {
            console.log(e);
        }

    }

    render() {
        //  (this.props.swRender === true && this.state.swCut === false) ? this.componentDidMount() : console.log();
        //console.log('props: ', this.props);
        //  console.log('state: >> ', this.state.swCut);

        for (let i = 0; i < this.state.diente.length; i++) {
            //console.log('================================');
            //console.log('piezaDental: ', this.state.diente[i].pieza_dental);
            switch (this.state.diente[i].pieza_dental) {
                //PIEZA 1.
                case '1.1':
                    pieza11.vestubular = this.state.diente[i].vestibular;
                    pieza11.distal = this.state.diente[i].distal;
                    pieza11.palatina = this.state.diente[i].palatina;
                    pieza11.mesial = this.state.diente[i].mesial;
                    pieza11.oclusal = this.state.diente[i].oclusal;
                    pieza11.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza11.patologia = this.state.diente[i].patologia;
                    pieza11.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '1.2':
                    pieza12.vestubular = this.state.diente[i].vestibular;
                    pieza12.distal = this.state.diente[i].distal;
                    pieza12.palatina = this.state.diente[i].palatina;
                    pieza12.mesial = this.state.diente[i].mesial;
                    pieza12.oclusal = this.state.diente[i].oclusal;
                    pieza12.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza12.patologia = this.state.diente[i].patologia;
                    pieza12.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '1.3':
                    pieza13.vestubular = this.state.diente[i].vestibular;
                    pieza13.distal = this.state.diente[i].distal;
                    pieza13.palatina = this.state.diente[i].palatina;
                    pieza13.mesial = this.state.diente[i].mesial;
                    pieza13.oclusal = this.state.diente[i].oclusal;
                    pieza13.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza13.patologia = this.state.diente[i].patologia;
                    pieza13.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '1.4':
                    pieza14.vestubular = this.state.diente[i].vestibular;
                    pieza14.distal = this.state.diente[i].distal;
                    pieza14.palatina = this.state.diente[i].palatina;
                    pieza14.mesial = this.state.diente[i].mesial;
                    pieza14.oclusal = this.state.diente[i].oclusal;
                    pieza14.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza14.patologia = this.state.diente[i].patologia;
                    pieza14.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '1.5':
                    pieza15.vestubular = this.state.diente[i].vestibular;
                    pieza15.distal = this.state.diente[i].distal;
                    pieza15.palatina = this.state.diente[i].palatina;
                    pieza15.mesial = this.state.diente[i].mesial;
                    pieza15.oclusal = this.state.diente[i].oclusal;
                    pieza15.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza15.patologia = this.state.diente[i].patologia;
                    pieza15.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '1.6':
                    pieza16.vestubular = this.state.diente[i].vestibular;
                    pieza16.distal = this.state.diente[i].distal;
                    pieza16.palatina = this.state.diente[i].palatina;
                    pieza16.mesial = this.state.diente[i].mesial;
                    pieza16.oclusal = this.state.diente[i].oclusal;
                    pieza16.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza16.patologia = this.state.diente[i].patologia;
                    pieza16.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '1.7':
                    pieza17.vestubular = this.state.diente[i].vestibular;
                    pieza17.distal = this.state.diente[i].distal;
                    pieza17.palatina = this.state.diente[i].palatina;
                    pieza17.mesial = this.state.diente[i].mesial;
                    pieza17.oclusal = this.state.diente[i].oclusal;
                    pieza17.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza17.patologia = this.state.diente[i].patologia;
                    pieza17.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '1.8':
                    pieza18.vestubular = this.state.diente[i].vestibular;
                    pieza18.distal = this.state.diente[i].distal;
                    pieza18.palatina = this.state.diente[i].palatina;
                    pieza18.mesial = this.state.diente[i].mesial;
                    pieza18.oclusal = this.state.diente[i].oclusal;
                    pieza18.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza18.patologia = this.state.diente[i].patologia;
                    pieza18.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;





                //PIEZA 2.
                case '2.1':
                    pieza21.vestubular = this.state.diente[i].vestibular;
                    pieza21.distal = this.state.diente[i].distal;
                    pieza21.palatina = this.state.diente[i].palatina;
                    pieza21.mesial = this.state.diente[i].mesial;
                    pieza21.oclusal = this.state.diente[i].oclusal;
                    pieza21.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza21.patologia = this.state.diente[i].patologia;
                    pieza21.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '2.2':
                    pieza22.vestubular = this.state.diente[i].vestibular;
                    pieza22.distal = this.state.diente[i].distal;
                    pieza22.palatina = this.state.diente[i].palatina;
                    pieza22.mesial = this.state.diente[i].mesial;
                    pieza22.oclusal = this.state.diente[i].oclusal;
                    pieza22.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza22.patologia = this.state.diente[i].patologia;
                    pieza22.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '2.3':
                    pieza23.vestubular = this.state.diente[i].vestibular;
                    pieza23.distal = this.state.diente[i].distal;
                    pieza23.palatina = this.state.diente[i].palatina;
                    pieza23.mesial = this.state.diente[i].mesial;
                    pieza23.oclusal = this.state.diente[i].oclusal;
                    pieza23.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza23.patologia = this.state.diente[i].patologia;
                    pieza23.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '2.4':
                    pieza24.vestubular = this.state.diente[i].vestibular;
                    pieza24.distal = this.state.diente[i].distal;
                    pieza24.palatina = this.state.diente[i].palatina;
                    pieza24.mesial = this.state.diente[i].mesial;
                    pieza24.oclusal = this.state.diente[i].oclusal;
                    pieza24.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza24.patologia = this.state.diente[i].patologia;
                    pieza24.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '2.5':
                    pieza25.vestubular = this.state.diente[i].vestibular;
                    pieza25.distal = this.state.diente[i].distal;
                    pieza25.palatina = this.state.diente[i].palatina;
                    pieza25.mesial = this.state.diente[i].mesial;
                    pieza25.oclusal = this.state.diente[i].oclusal;
                    pieza25.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza25.patologia = this.state.diente[i].patologia;
                    pieza25.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '2.6':
                    pieza26.vestubular = this.state.diente[i].vestibular;
                    pieza26.distal = this.state.diente[i].distal;
                    pieza26.palatina = this.state.diente[i].palatina;
                    pieza26.mesial = this.state.diente[i].mesial;
                    pieza26.oclusal = this.state.diente[i].oclusal;
                    pieza26.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza26.patologia = this.state.diente[i].patologia;
                    pieza26.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '2.7':
                    pieza27.vestubular = this.state.diente[i].vestibular;
                    pieza27.distal = this.state.diente[i].distal;
                    pieza27.palatina = this.state.diente[i].palatina;
                    pieza27.mesial = this.state.diente[i].mesial;
                    pieza27.oclusal = this.state.diente[i].oclusal;
                    pieza27.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza27.patologia = this.state.diente[i].patologia;
                    pieza27.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '2.8':
                    pieza28.vestubular = this.state.diente[i].vestibular;
                    pieza28.distal = this.state.diente[i].distal;
                    pieza28.palatina = this.state.diente[i].palatina;
                    pieza28.mesial = this.state.diente[i].mesial;
                    pieza28.oclusal = this.state.diente[i].oclusal;
                    pieza28.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza28.patologia = this.state.diente[i].patologia;
                    pieza28.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;




                //PIEZA 3.
                case '3.1':
                    pieza31.vestubular = this.state.diente[i].vestibular;
                    pieza31.distal = this.state.diente[i].distal;
                    pieza31.palatina = this.state.diente[i].palatina;
                    pieza31.mesial = this.state.diente[i].mesial;
                    pieza31.oclusal = this.state.diente[i].oclusal;
                    pieza31.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza31.patologia = this.state.diente[i].patologia;
                    pieza31.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '3.2':
                    pieza32.vestubular = this.state.diente[i].vestibular;
                    pieza32.distal = this.state.diente[i].distal;
                    pieza32.palatina = this.state.diente[i].palatina;
                    pieza32.mesial = this.state.diente[i].mesial;
                    pieza32.oclusal = this.state.diente[i].oclusal;
                    pieza32.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza32.patologia = this.state.diente[i].patologia;
                    pieza32.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '3.3':
                    pieza33.vestubular = this.state.diente[i].vestibular;
                    pieza33.distal = this.state.diente[i].distal;
                    pieza33.palatina = this.state.diente[i].palatina;
                    pieza33.mesial = this.state.diente[i].mesial;
                    pieza33.oclusal = this.state.diente[i].oclusal;
                    pieza33.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza33.patologia = this.state.diente[i].patologia;
                    pieza33.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '3.4':
                    pieza34.vestubular = this.state.diente[i].vestibular;
                    pieza34.distal = this.state.diente[i].distal;
                    pieza34.palatina = this.state.diente[i].palatina;
                    pieza34.mesial = this.state.diente[i].mesial;
                    pieza34.oclusal = this.state.diente[i].oclusal;
                    pieza34.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza34.patologia = this.state.diente[i].patologia;
                    pieza34.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '3.5':
                    pieza35.vestubular = this.state.diente[i].vestibular;
                    pieza35.distal = this.state.diente[i].distal;
                    pieza35.palatina = this.state.diente[i].palatina;
                    pieza35.mesial = this.state.diente[i].mesial;
                    pieza35.oclusal = this.state.diente[i].oclusal;
                    pieza35.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza35.patologia = this.state.diente[i].patologia;
                    pieza35.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '3.6':
                    pieza36.vestubular = this.state.diente[i].vestibular;
                    pieza36.distal = this.state.diente[i].distal;
                    pieza36.palatina = this.state.diente[i].palatina;
                    pieza36.mesial = this.state.diente[i].mesial;
                    pieza36.oclusal = this.state.diente[i].oclusal;
                    pieza36.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza36.patologia = this.state.diente[i].patologia;
                    pieza36.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '3.7':
                    pieza37.vestubular = this.state.diente[i].vestibular;
                    pieza37.distal = this.state.diente[i].distal;
                    pieza37.palatina = this.state.diente[i].palatina;
                    pieza37.mesial = this.state.diente[i].mesial;
                    pieza37.oclusal = this.state.diente[i].oclusal;
                    pieza37.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza37.patologia = this.state.diente[i].patologia;
                    pieza37.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '3.8':
                    pieza38.vestubular = this.state.diente[i].vestibular;
                    pieza38.distal = this.state.diente[i].distal;
                    pieza38.palatina = this.state.diente[i].palatina;
                    pieza38.mesial = this.state.diente[i].mesial;
                    pieza38.oclusal = this.state.diente[i].oclusal;
                    pieza38.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza38.patologia = this.state.diente[i].patologia;
                    pieza18.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;




                //PIEZA 4.
                case '4.1':
                    pieza41.vestubular = this.state.diente[i].vestibular;
                    pieza41.distal = this.state.diente[i].distal;
                    pieza41.palatina = this.state.diente[i].palatina;
                    pieza41.mesial = this.state.diente[i].mesial;
                    pieza41.oclusal = this.state.diente[i].oclusal;
                    pieza41.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza41.patologia = this.state.diente[i].patologia;
                    pieza41.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '4.2':
                    pieza42.vestubular = this.state.diente[i].vestibular;
                    pieza42.distal = this.state.diente[i].distal;
                    pieza42.palatina = this.state.diente[i].palatina;
                    pieza42.mesial = this.state.diente[i].mesial;
                    pieza42.oclusal = this.state.diente[i].oclusal;
                    pieza42.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza42.patologia = this.state.diente[i].patologia;
                    pieza42.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '4.3':
                    pieza43.vestubular = this.state.diente[i].vestibular;
                    pieza43.distal = this.state.diente[i].distal;
                    pieza43.palatina = this.state.diente[i].palatina;
                    pieza43.mesial = this.state.diente[i].mesial;
                    pieza43.oclusal = this.state.diente[i].oclusal;
                    pieza43.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza43.patologia = this.state.diente[i].patologia;
                    pieza43.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '4.4':
                    pieza44.vestubular = this.state.diente[i].vestibular;
                    pieza44.distal = this.state.diente[i].distal;
                    pieza44.palatina = this.state.diente[i].palatina;
                    pieza44.mesial = this.state.diente[i].mesial;
                    pieza44.oclusal = this.state.diente[i].oclusal;
                    pieza44.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza44.patologia = this.state.diente[i].patologia;
                    pieza44.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '4.5':
                    pieza45.vestubular = this.state.diente[i].vestibular;
                    pieza45.distal = this.state.diente[i].distal;
                    pieza45.palatina = this.state.diente[i].palatina;
                    pieza45.mesial = this.state.diente[i].mesial;
                    pieza45.oclusal = this.state.diente[i].oclusal;
                    pieza45.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza45.patologia = this.state.diente[i].patologia;
                    pieza45.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '4.6':
                    pieza46.vestubular = this.state.diente[i].vestibular;
                    pieza46.distal = this.state.diente[i].distal;
                    pieza46.palatina = this.state.diente[i].palatina;
                    pieza46.mesial = this.state.diente[i].mesial;
                    pieza46.oclusal = this.state.diente[i].oclusal;
                    pieza46.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza46.patologia = this.state.diente[i].patologia;
                    pieza46.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '4.7':
                    pieza47.vestubular = this.state.diente[i].vestibular;
                    pieza47.distal = this.state.diente[i].distal;
                    pieza47.palatina = this.state.diente[i].palatina;
                    pieza47.mesial = this.state.diente[i].mesial;
                    pieza47.oclusal = this.state.diente[i].oclusal;
                    pieza47.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza47.patologia = this.state.diente[i].patologia;
                    pieza47.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;
                case '4.8':
                    pieza48.vestubular = this.state.diente[i].vestibular;
                    pieza48.distal = this.state.diente[i].distal;
                    pieza48.palatina = this.state.diente[i].palatina;
                    pieza48.mesial = this.state.diente[i].mesial;
                    pieza48.oclusal = this.state.diente[i].oclusal;
                    pieza48.pieza_dental = this.state.diente[i].pieza_dental;
                    pieza48.patologia = this.state.diente[i].patologia;
                    pieza48.idExamenClinico = this.state.diente[i].idExamenClinico;
                    break;

                default:
                    console.log('error en la busqueda'); alert('error en la busqueda de diente')
            }

        }
        const lis1 = this.state.diente.map(item => {
            if (item.pieza_dental === '1.1' || item.pieza_dental === '1.2' || item.pieza_dental === '1.3' || item.pieza_dental === '1.4' ||
                item.pieza_dental === '1.5' || item.pieza_dental === '1.6' || item.pieza_dental === '1.7' || item.pieza_dental === '1.8') {
                return (
                    <tr key={item.pieza_dental}>
                        <td>{item.pieza_dental}</td>
                        <td>{(item.patologia === '0') ? 'SANO' :
                            (item.patologia === '1') ? 'CARIES' :
                                (item.patologia === '2') ? 'OBTURADO' :
                                    (item.patologia === '3') ? 'AUSENTE' :
                                        (item.patologia === '4') ? 'RESTO RADICULAR' :
                                            (item.patologia === '5') ? 'FRACTURA' :
                                                (item.patologia === '6') ? 'CORONA' :
                                                    (item.patologia === '7') ? 'NO CLASIFICABLE' : null}</td>
                    </tr>
                )
            } else {
                return console.log();
            }
        });
        const lis2 = this.state.diente.map(item => {
            if (item.pieza_dental === '2.1' || item.pieza_dental === '2.2' || item.pieza_dental === '2.3' || item.pieza_dental === '2.4' ||
                item.pieza_dental === '2.5' || item.pieza_dental === '2.6' || item.pieza_dental === '2.7' || item.pieza_dental === '2.8') {
                return (
                    <tr key={item.pieza_dental}>
                        <td>{item.pieza_dental}</td>
                        <td>{(item.patologia === '0') ? 'SANO' :
                            (item.patologia === '1') ? 'CARIES' :
                                (item.patologia === '2') ? 'OBTURADO' :
                                    (item.patologia === '3') ? 'AUSENTE' :
                                        (item.patologia === '4') ? 'RESTO RADICULAR' :
                                            (item.patologia === '5') ? 'FRACTURA' :
                                                (item.patologia === '6') ? 'CORONA' :
                                                    (item.patologia === '7') ? 'NO CLASIFICABLE' : null}</td>
                    </tr>
                )
            } else {
                return console.log();

            }
        });

        const lis3 = this.state.diente.map(item => {
            if (item.pieza_dental === '3.1' || item.pieza_dental === '3.2' || item.pieza_dental === '3.3' || item.pieza_dental === '3.4' ||
                item.pieza_dental === '3.5' || item.pieza_dental === '3.6' || item.pieza_dental === '3.7' || item.pieza_dental === '3.8') {
                return (
                    <tr key={item.pieza_dental}>
                        <td>{item.pieza_dental}</td>
                        <td>{(item.patologia === '0') ? 'SANO' :
                            (item.patologia === '1') ? 'CARIES' :
                                (item.patologia === '2') ? 'OBTURADO' :
                                    (item.patologia === '3') ? 'AUSENTE' :
                                        (item.patologia === '4') ? 'RESTO RADICULAR' :
                                            (item.patologia === '5') ? 'FRACTURA' :
                                                (item.patologia === '6') ? 'CORONA' :
                                                    (item.patologia === '7') ? 'NO CLASIFICABLE' : null}</td>
                    </tr>
                )
            } else {
                return console.log();
            }
        });
        const lis4 = this.state.diente.map(item => {
            if (item.pieza_dental === '4.1' || item.pieza_dental === '4.2' || item.pieza_dental === '4.3' || item.pieza_dental === '4.4' ||
                item.pieza_dental === '4.5' || item.pieza_dental === '4.6' || item.pieza_dental === '4.7' || item.pieza_dental === '4.8') {
                return (
                    <tr key={item.pieza_dental}>
                        <td>{item.pieza_dental}</td>
                        <td>{(item.patologia === '0') ? 'SANO' :
                            (item.patologia === '1') ? 'CARIES' :
                                (item.patologia === '2') ? 'OBTURADO' :
                                    (item.patologia === '3') ? 'AUSENTE' :
                                        (item.patologia === '4') ? 'RESTO RADICULAR' :
                                            (item.patologia === '5') ? 'FRACTURA' :
                                                (item.patologia === '6') ? 'CORONA' :
                                                    (item.patologia === '7') ? 'NO CLASIFICABLE' : null}</td>
                    </tr>
                )
            } else {
                return console.log();
            }
        });

        return (

            <CardBody>
                <Row>
                    <Col xs="8">
                        <svg style={{ width: '101%', height: '250px' }}>
                            <g>
                                {'Dientes de adulto'}
                                <g transform="scale(1.5)">
                                    <g transform="translate(0,0)">
                                        <polygon {...points0} fill={pieza18.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza18.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza18.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza18.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza18.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>18</text>
                                    </g>
                                    <g transform="translate(25,0)">
                                        <polygon {...points0} fill={pieza17.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza17.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza17.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza17.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza17.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>17</text>
                                    </g>
                                    <g transform="translate(50,0)">
                                        <polygon {...points0} fill={pieza16.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza16.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza16.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza16.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza16.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>16</text>
                                    </g>
                                    <g transform="translate(75,0)">
                                        <polygon {...points0} fill={pieza15.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza15.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza15.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza15.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza15.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>15</text>
                                    </g>
                                    <g transform="translate(100,0)">
                                        <polygon {...points0} fill={pieza14.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza14.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza14.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza14.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza14.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>14</text>
                                    </g>
                                    <g transform="translate(125,0)">
                                        <polygon {...points0} fill={pieza13.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza13.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza13.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza13.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza13.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>13</text>
                                    </g>
                                    <g transform="translate(150,0)">
                                        <polygon {...points0} fill={pieza12.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza12.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza12.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza12.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza12.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>12</text>
                                    </g>
                                    <g transform="translate(175,0)">
                                        <polygon {...points0} fill={pieza11.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza11.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza11.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza11.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza11.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>11</text>
                                    </g>
                                    <g transform="scale(1)">
                                        <g transform="translate(210,0)">
                                            <polygon {...points0} fill={pieza21.vestubular} {...optionesSVG} />
                                            <polygon {...points1} fill={pieza21.palatina} {...optionesSVG} />
                                            <polygon {...points2} fill={pieza21.mesial} {...optionesSVG} />
                                            <polygon {...points3} fill={pieza21.distal}  {...optionesSVG} />
                                            <polygon {...points4} fill={pieza21.oclusal}{...optionesSVG} />
                                            <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>21</text>
                                        </g>
                                        <g transform="translate(235,0)">
                                            <polygon {...points0} fill={pieza22.vestubular} {...optionesSVG} />
                                            <polygon {...points1} fill={pieza22.palatina} {...optionesSVG} />
                                            <polygon {...points2} fill={pieza22.mesial} {...optionesSVG} />
                                            <polygon {...points3} fill={pieza22.distal}  {...optionesSVG} />
                                            <polygon {...points4} fill={pieza22.oclusal}{...optionesSVG} />
                                            <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>22</text>
                                        </g>
                                        <g transform="translate(260,0)">
                                            <polygon {...points0} fill={pieza23.vestubular} {...optionesSVG} />
                                            <polygon {...points1} fill={pieza23.palatina} {...optionesSVG} />
                                            <polygon {...points2} fill={pieza23.mesial} {...optionesSVG} />
                                            <polygon {...points3} fill={pieza23.distal}  {...optionesSVG} />
                                            <polygon {...points4} fill={pieza23.oclusal}{...optionesSVG} />
                                            <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>23</text>
                                        </g>
                                        <g transform="translate(285,0)">
                                            <polygon {...points0} fill={pieza24.vestubular} {...optionesSVG} />
                                            <polygon {...points1} fill={pieza24.palatina} {...optionesSVG} />
                                            <polygon {...points2} fill={pieza24.mesial} {...optionesSVG} />
                                            <polygon {...points3} fill={pieza24.distal}  {...optionesSVG} />
                                            <polygon {...points4} fill={pieza24.oclusal}{...optionesSVG} />
                                            <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>24</text>
                                        </g>
                                        <g transform="translate(310,0)">
                                            <polygon {...points0} fill={pieza25.vestubular} {...optionesSVG} />
                                            <polygon {...points1} fill={pieza25.palatina} {...optionesSVG} />
                                            <polygon {...points2} fill={pieza25.mesial} {...optionesSVG} />
                                            <polygon {...points3} fill={pieza25.distal}  {...optionesSVG} />
                                            <polygon {...points4} fill={pieza25.oclusal}{...optionesSVG} />
                                            <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>25</text>
                                        </g>
                                        <g transform="translate(335,0)">
                                            <polygon {...points0} fill={pieza26.vestubular} {...optionesSVG} />
                                            <polygon {...points1} fill={pieza26.palatina} {...optionesSVG} />
                                            <polygon {...points2} fill={pieza26.mesial} {...optionesSVG} />
                                            <polygon {...points3} fill={pieza26.distal}  {...optionesSVG} />
                                            <polygon {...points4} fill={pieza26.oclusal}{...optionesSVG} />
                                            <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>26</text>
                                        </g>
                                        <g transform="translate(360,0)">
                                            <polygon {...points0} fill={pieza27.vestubular} {...optionesSVG} />
                                            <polygon {...points1} fill={pieza27.palatina} {...optionesSVG} />
                                            <polygon {...points2} fill={pieza27.mesial} {...optionesSVG} />
                                            <polygon {...points3} fill={pieza27.distal}  {...optionesSVG} />
                                            <polygon {...points4} fill={pieza27.oclusal}{...optionesSVG} />
                                            <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>27</text>
                                        </g>
                                        <g transform="translate(385,0)">
                                            <polygon {...points0} fill={pieza28.vestubular} {...optionesSVG} />
                                            <polygon {...points1} fill={pieza28.palatina} {...optionesSVG} />
                                            <polygon {...points2} fill={pieza28.mesial} {...optionesSVG} />
                                            <polygon {...points3} fill={pieza28.distal}  {...optionesSVG} />
                                            <polygon {...points4} fill={pieza28.oclusal}{...optionesSVG} />
                                            <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>28</text>
                                        </g>
                                    </g>
                                </g>
                                <g transform="scale(1.5)">
                                    <g transform="translate(210,40)">
                                        <polygon {...points0} fill={pieza31.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza31.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza31.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza31.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza31.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>31</text>
                                    </g>
                                    <g transform="translate(235,40)">
                                        <polygon {...points0} fill={pieza32.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza32.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza32.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza32.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza32.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>32</text>
                                    </g>
                                    <g transform="translate(260,40)">
                                        <polygon {...points0} fill={pieza33.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza33.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza33.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza33.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza33.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>33</text>
                                    </g>
                                    <g transform="translate(285,40)">
                                        <polygon {...points0} fill={pieza34.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza34.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza34.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza34.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza34.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>34</text>
                                    </g>
                                    <g transform="translate(310,40)">
                                        <polygon {...points0} fill={pieza35.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza35.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza35.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza35.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza35.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>35</text>
                                    </g>
                                    <g transform="translate(335,40)">
                                        <polygon {...points0} fill={pieza36.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza36.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza36.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza36.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza36.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>36</text>
                                    </g>
                                    <g transform="translate(360,40)">
                                        <polygon {...points0} fill={pieza37.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza37.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza37.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza37.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza37.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>37</text>
                                    </g>
                                    <g transform="translate(385,40)">
                                        <polygon {...points0} fill={pieza38.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza38.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza38.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza38.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza38.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>38</text>
                                    </g>
                                </g>
                                <g transform="scale(1.5)">
                                    <g transform="translate(0,40)">
                                        <polygon {...points0} fill={pieza48.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza48.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza48.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza48.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza48.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>48</text>
                                    </g>
                                    <g transform="translate(25,40)">
                                        <polygon {...points0} fill={pieza47.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza47.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza47.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza47.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza47.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>47</text>
                                    </g>
                                    <g transform="translate(50,40)">
                                        <polygon {...points0} fill={pieza46.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza46.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza46.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza46.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza46.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>46</text>
                                    </g>
                                    <g transform="translate(75,40)">
                                        <polygon {...points0} fill={pieza45.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza45.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza45.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza45.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza45.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>45</text>
                                    </g>
                                    <g transform="translate(100,40)">
                                        <polygon {...points0} fill={pieza44.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza44.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza44.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza44.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza44.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>44</text>
                                    </g>
                                    <g transform="translate(125,40)">
                                        <polygon {...points0} fill={pieza43.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza43.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza43.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza43.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza43.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>43</text>
                                    </g>
                                    <g transform="translate(150,40)">
                                        <polygon {...points0} fill={pieza42.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza42.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza42.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza42.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza42.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>42</text>
                                    </g>
                                    <g transform="translate(175,40)">
                                        <polygon {...points0} fill={pieza41.vestubular} {...optionesSVG} />
                                        <polygon {...points1} fill={pieza41.palatina} {...optionesSVG} />
                                        <polygon {...points2} fill={pieza41.mesial} {...optionesSVG} />
                                        <polygon {...points3} fill={pieza41.distal}  {...optionesSVG} />
                                        <polygon {...points4} fill={pieza41.oclusal}{...optionesSVG} />
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>41</text>
                                    </g>
                                </g>
                                {'Dientes de niño'}
                                <g transform="scale(1.5)">
                                    <g transform="translate(210,80)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>61</text>
                                    </g>
                                    <g transform="translate(235,80)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>62</text>
                                    </g>
                                    <g transform="translate(260,80)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>63</text>
                                    </g>
                                    <g transform="translate(285,80)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>64</text>
                                    </g>
                                    <g transform="translate(310,80)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>65</text>
                                    </g>
                                </g>
                                <g transform="scale(1.5)">
                                    <g transform="translate(75,80)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>55</text>
                                    </g>
                                    <g transform="translate(100,80)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>54</text>
                                    </g>
                                    <g transform="translate(125,80)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>53</text>
                                    </g>
                                    <g transform="translate(150,80)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>52</text>
                                    </g>
                                    <g transform="translate(175,80)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>51</text>
                                    </g>
                                </g>
                                <g transform="scale(1.5)">
                                    <g transform="translate(210,120)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>71</text>
                                    </g>
                                    <g transform="translate(235,120)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>72</text>
                                    </g>
                                    <g transform="translate(260,120)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>73</text>
                                    </g>
                                    <g transform="translate(285,120)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>74</text>
                                    </g>
                                    <g transform="translate(310,120)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>75</text>
                                    </g>
                                </g>
                                <g transform="scale(1.5)">
                                    <g transform="translate(75,120)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>85</text>
                                    </g>
                                    <g transform="translate(100,120)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>84</text>
                                    </g>
                                    <g transform="translate(125,120)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>83</text>
                                    </g>
                                    <g transform="translate(150,120)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>82</text>
                                    </g>
                                    <g transform="translate(175,120)">
                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="15,5 20,0 20,20 15,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                        <text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>81</text>
                                    </g>
                                </g>
                            </g>
                        </svg>

                    </Col>
                    <Col xs="4">
                        <Table responsive hover bordered size="sm" style={{ width: '100%' }}>
                            <tbody>
                                <tr><td>0</td><td>SANO</td><td>4</td><td>RESTO RADICULAR</td></tr>
                                <tr><td>1</td><td>CARIES</td><td>5</td><td>FRACTURA</td></tr>
                                <tr><td>2</td><td>OBTURADO</td><td>6</td><td>CORONA</td></tr>
                                <tr><td>3</td><td>AUSENTE</td><td>7</td><td>NO CLASIFICABLE</td></tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <p>Pieza 1.1 - 1.8</p>
                        <Table responsive hover bordered size="sm" style={{ width: '10em', textAlign: 'center' }}>
                            <thead>
                                <tr>
                                    <th>Pieza</th>
                                    <th>Patologia</th>
                                </tr>
                            </thead>
                            <tbody>
                                {lis1}
                            </tbody>
                        </Table>
                    </Col>
                    <Col>
                        <p>Pieza 2.1 - 2.8</p>
                        <Table responsive hover bordered size="sm" style={{ width: '10em', textAlign: 'center' }}>
                            <thead>
                                <tr>
                                    <th>Pieza</th>
                                    <th>Patologia</th>
                                </tr>
                            </thead>
                            <tbody>
                                {lis2}
                            </tbody>
                        </Table>
                    </Col>
                    <Col>
                        <p>Pieza 3.1 - 3.8</p>
                        <Table responsive hover bordered size="sm" style={{ width: '10em', textAlign: 'center' }}>
                            <thead>
                                <tr>
                                    <th>Pieza</th>
                                    <th>Patologia</th>
                                </tr>
                            </thead>
                            <tbody>
                                {lis3}
                            </tbody>
                        </Table>
                    </Col>
                    <Col>
                        <p>Pieza 4.1 - 4.8</p>
                        <Table responsive hover bordered size="sm" style={{ width: '10em', textAlign: 'center' }}>
                            <thead>
                                <tr>
                                    <th>Pieza</th>
                                    <th>Patologia</th>
                                </tr>
                            </thead>
                            <tbody>
                                {lis4}
                            </tbody>
                        </Table>
                    </Col>


                </Row>

            </CardBody>

        );
    }
}
/*

   print2() {
        /*Actual time and date 
        var options2 = {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric'
        }
        var options3 = {
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit'
        }
        var d = new Date();
        var showDate = d.toLocaleString('es-bo', options2)
        var showTime = d.toLocaleString('es-bo', options3)
        /*Actual time and date 
        const input = document.getElementById('tablaP');
        html2canvas(input)
            .then((canvas) => {
                const imgData = canvas.toDataURL('image/png');
                const pdf = new jsPDF('l', 'mm', 'legal');
                pdf.setFontStyle("bold");
                pdf.text(170, 15, "ODONTOGRAMA", null, null, 'center');
                pdf.text(170, 22, "PROGRAMA MEDICO ESTUDIANTIL (PROMES)", null, null, 'center');
                pdf.setFontType("normal");
                pdf.setFontSize(10);
                pdf.text(170, 27, showDate + ' ' + showTime, null, null, 'center');
                pdf.setFontSize(12);
                pdf.addImage(imgData, 'JPEG', 10, 32, 150, 100);
                // pdf.output('dataurlnewwindow');
                // pdf.save("download.pdf");
                window.open(pdf.output('bloburl'), '_blank');
            });
    }

   <div id="tablaP">
                    <strong>ODONTOGRAMAAA</strong>
                </div>


                <Button block color="success" onClick={this.print2}>Imprimir ODONTOGRAMA</Button>
*/
export default Odontograma_F;
