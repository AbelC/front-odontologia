import React, { Component } from 'react';
import { Table, Col } from 'reactstrap';
//import { AppSwitch } from '@coreui/react'


class Tolerancia_F extends Component {

    constructor(props) {
        super(props);
        this.state = {
            listTolerancia: []
        }
    }
    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/tolerancia/v1/lista/tolerancia/afiliado/' + this.props.idAf + '/');
            const listTolerancia = await respuesta.json();
            this.setState({
                listTolerancia
            });
        } catch (e) {
            console.log(e);
        }
    }


    render() {
        return (
                <Col>
                    <Table responsive hover bordered size="sm">



                        {this.state.listTolerancia.map(item => (

                            <tbody key={item.id}>
                                <tr>
                                    <td>Tolerancia Antibioticos:</td><td>{item.tolerancia_antibioticos}</td>
                                </tr>
                                <tr>
                                    <td>Sufre hemorragia despues de una exodoncia:</td><td>{item.hemorragia}</td>
                                </tr>
                            </tbody>

                        ))}



                    </Table>
                </Col>
           
        );
    }
}

export default Tolerancia_F;
