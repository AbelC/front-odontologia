import React, { Component } from 'react';
import { Table, CardBody } from 'reactstrap';


class Tratamiento_F extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listTratamiento: [],
            modal: false,
        }

    }


    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/tratamiento/v1/listado/afiliado/' + this.props.idAf + '/');
            const listTratamiento = await respuesta.json();
            this.setState({
                listTratamiento
            });
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        return (
            <CardBody>

                <Table responsive hover bordered size="sm">
                    <thead>
                        <tr>
                            <th scope="col" style={{ textAlign: 'center', width: '100px' }}>Fecha de atención</th>
                            <th scope="col" style={{ width: '10px' }}>Pieza Dentaria</th>
                            <th scope="col" style={{ textAlign: 'center', width: '300px', maxWidth: '301px' }}>Diagnostico</th>
                            <th scope="col" style={{ textAlign: 'center', width: '200px', maxWidth: '301px' }}>Tratamiento realizado</th>
                            <th scope="col" style={{ textAlign: 'center' }}>Observaciones</th>
                            <th scope="col">Doctor</th>

                        </tr>
                    </thead>
                    <tbody>
                        {this.state.listTratamiento.map(item => (
                            <tr key={item.id}>
                                <td style={{ textAlign: 'center', width: '100px' }}>{item.fecha_atencion}</td>
                                <td style={{ width: '10px' }}>{item.pieza_tratada}</td>
                                <td style={{ width: '300px', height: '50px', maxWidth: '301px', maxHeight: '100px' }}>{
                                    item.diagnostico.map(d => (
                                        <ul key={d.codigo}>
                                            <li>{d.codigo + '-' + d.descripcion}</li>
                                        </ul>
                                    ))
                                }</td>
                                <td style={{ width: '200px', height: '50px', maxWidth: '301px', maxHeight: '100px' }}>{item.tratamiento_realizado}</td>
                                <td>{item.observaciones}</td>
                                <td>{item.medico.nombres + ' ' + item.medico.apellido_paterno}</td>


                            </tr>

                        ))
                        }
                    </tbody>
                </Table>


            </CardBody>
        );
    }
}
/**
 *   <td> <Button outline color="warning" >Editar</Button></td>
 * <th scope="col">Acción</th>
 */
export default Tratamiento_F;