import React, { Component } from 'react';
import { Card, CardBody, DropdownItem, CardHeader, Col, Row, Button } from 'reactstrap';
//import { AppSwitch } from '@coreui/react'
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'
import html2canvas from 'html2canvas'

import DatosPersonalesF from './ComponentFicha/DatosPersonalesF';
import AntecedentesF from './ComponentFicha/AntecedentesF';
import AlergiasF from './ComponentFicha/AlergiasF';
import ToleranciaF from './ComponentFicha/ToleranciaF';
import ExamenClinicoF from './ComponentFicha/ExamenClinicoF';
import TratamientoF from './ComponentFicha/TratamientoF';
//import Prueba from './ComponentFicha/prueba'


class FichaOdontologica extends Component {

    constructor(props) {
        super(props);
        this.state = {
            idAf: this.props.match.params.idAf,
        }
        this.print = this.print.bind(this)
    }
    print() {
        /*Actual time and date */
        var options2 = {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric'
        }
        var options3 = {
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit'
        }
        var d = new Date();
        var showDate = d.toLocaleString('es-bo', options2)
        var showTime = d.toLocaleString('es-bo', options3)
        /*Actual time and date */
        const input = document.getElementById('tabla');
        html2canvas(input)
            .then((canvas) => {
                const imgData = canvas.toDataURL('image/png');
                const pdf = new jsPDF('p', 'mm', 'letter');
                pdf.setFontStyle("bold");
                pdf.setFontSize(11);
                pdf.text(110, 15, "FICHA DE ATENCIÓN ODONTOLÓGICA", null, null, 'center');
                pdf.text(110, 22, "PROGRAMA MEDICO ESTUDIANTIL (PROMES)", null, null, 'center');
                pdf.setFontType("normal");
                pdf.setFontSize(10);
                pdf.text(105, 27, showDate + ' ' + showTime, null, null, 'center');

                pdf.setLineWidth(0.1);
                pdf.line(15, 33, 200, 33);

                pdf.setFontSize(12);
                pdf.text(110, 39, "DATOS PERSONALES", null, null, 'center');



                pdf.addImage(imgData, 'JPEG', 4, 50);
                // pdf.output('dataurlnewwindow');
                // pdf.save("download.pdf");
                window.open(pdf.output('bloburl'), '_blank');
            });

    }


    render() {
        return (

            <div className="animated fadeIn">
                {/*console.log('idAFF FICHA _ MATCH= ', this.state.idAf)*/}
                <Row>
                    <Col xs="12">
                        <Card>

                            <CardHeader>
                                <i className="fa fa-align-justify"></i><strong>FICHA DE ATENCIÓN ODONTOLÓGICA</strong>
                                <div className="card-header-actions">
                                    <Button block color="link" onClick={this.print}>Imprimir</Button>
                                </div>
                            </CardHeader>

                            <CardBody>
                                <div id="tabla">
                                    lll
                </div>
                                <DropdownItem divider />
                                <strong> <p style={{ textAlign: 'center' }} >DATOS PERSONALES DEL PACIENTE</p></strong>
                                <DropdownItem divider />

                                <DatosPersonalesF idAf={this.state.idAf} />

                                <DropdownItem divider />
                                <strong><p style={{ textAlign: 'center' }} >ANTECEDENTES GENERALES</p></strong>
                                <DropdownItem divider />
                                <AntecedentesF idAf={this.state.idAf} />
                                <Row>
                                    <Col xs="6"><AlergiasF idAf={this.state.idAf} /></Col>
                                    <Col xs="6"><ToleranciaF idAf={this.state.idAf} /></Col>
                                </Row>
                                <DropdownItem divider />
                                <strong><p style={{ textAlign: 'center' }} >EXAMEN CLINICO INTRAORAL</p></strong>
                                <DropdownItem divider />
                                <ExamenClinicoF idAf={this.state.idAf} />
                                <DropdownItem divider />
                                <strong><p style={{ textAlign: 'center' }} >TRATAMIENTO ODONTOLÓGICO</p></strong>
                                <DropdownItem divider />

                                <TratamientoF idAf={this.state.idAf} />

                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>





        );
    }
}

export default FichaOdontologica;
