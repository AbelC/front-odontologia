import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button } from 'reactstrap';
//import { AppSwitch } from '@coreui/react'
import OdontogramaF from './ComponentFicha/OdontogramaF'
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'
import html2canvas from 'html2canvas'
class FichaOdontologica extends Component {
  constructor(props) {
    super(props);

    this.state = {
      idAf: this.props.match.params.idAf,
      //Datospersonales
      afiliado_list: [],
      apellido_pat: '',
      apellido_mat: '',
      apellido_esposo: '',
      nombres: '',
      edad: '',
      sexo: '',
      estado_civil: '',
      ocupacion: '',
      domicilio: '',
      telefono: '',
      odontologo: '',
      fecha: '',
      //antecedentes generales
      antecedentes: [],
      //alergias
      alergias: [],
      //tolerancias
      listTolerancia: [],
      //examen clinico
      examen: [],
      idExGet: 0,
      //tratamiento
      listTratamiento: [],
      //Motivo Consulta
      list_tieneMotivoConsulta: [],

    }
    this.print = this.print.bind(this)
  }

  async componentDidMount() {//ciclo de vida de react de forma asincrona
    try {
      const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/persona/v1/busca/afiliado/' + this.state.idAf + '/');
      const afiliado_list = await respuesta.json();
      const respuesta2 = await fetch(process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/lista/antecedentes/afiliado/' + this.state.idAf + '/');
      const antecedentes = await respuesta2.json();
      const respuesta3 = await fetch(process.env.REACT_APP_API_URL+'odontologia/alergia/v1/lista/alergias/afiliado/' + this.state.idAf + '/');
      const alergias = await respuesta3.json();
      const respuesta4 = await fetch(process.env.REACT_APP_API_URL+'odontologia/tolerancia/v1/lista/tolerancia/afiliado/' + this.state.idAf + '/');
      const listTolerancia = await respuesta4.json();
      const respuesta5 = await fetch(process.env.REACT_APP_API_URL+'odontologia/tolerancia/v1/lista/examenclinico/afiliado/' + this.state.idAf + '/');
      const examen = await respuesta5.json();
      console.log('idExamenCli: ', examen[0].id)
      const respuesta6 = await fetch(process.env.REACT_APP_API_URL+'odontologia/tratamiento/v1/listado/afiliado/' + this.state.idAf + '/');
      const listTratamiento = await respuesta6.json();
      const respuestaMotivoC = await fetch(process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/tieneMotivoConsultJoin/' + this.state.idAf + '/');
      const list_tieneMotivoConsulta = await respuestaMotivoC.json();
      this.setState({
        afiliado_list,
        apellido_pat: afiliado_list[0].apellido_pat,
        apellido_mat: afiliado_list[0].apellido_mat,
        apellido_esposo: '',
        nombres: afiliado_list[0].nombres,
        edad: '',
        sexo: '',
        estado_civil: '',
        ocupacion: '',
        domicilio: '',
        telefono: '',
        odontologo: '',
        fecha: '',

        antecedentes,
        alergias,
        listTolerancia,
        examen,
        listTratamiento,
        idExGet: examen[0].id,
        list_tieneMotivoConsulta
      });

    } catch (e) {
      console.log(e);
      console.log('entro al cath')
    }
  }

  print() {
    /*Actual time and date */
    var options2 = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
    }
    var options3 = {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    }
    var d = new Date();
    var showDate = d.toLocaleString('es-bo', options2)
    var showTime = d.toLocaleString('es-bo', options3)
    /*Actual time and date */
    const input = document.getElementById('odontograma');
    html2canvas(input)
      .then((canvas) => {
        const imgData = canvas.toDataURL('image/png');
        const pdf = new jsPDF('p', 'mm', 'letter');
        pdf.setFontStyle("bold");
        pdf.setFontSize(11);
        pdf.text(110, 15, "FICHA DE ATENCIÓN ODONTOLÓGICA", null, null, 'center');
        pdf.text(110, 22, "PROGRAMA MEDICO ESTUDIANTIL (PROMES)", null, null, 'center');
        pdf.setFontType("normal");
        pdf.setFontSize(10);
        pdf.text(105, 27, showDate + ' ' + showTime, null, null, 'center');

        pdf.setLineWidth(0.1);
        pdf.line(15, 33, 200, 33);

        pdf.setFontSize(12);
        pdf.text(110, 39, "DATOS PERSONALES", null, null, 'center');

        pdf.addImage(imgData, 'PNG', 12, 200, 191, 153);

        var res = pdf.autoTableHtmlToJson(document.getElementById("datosGral"));
        pdf.autoTable(res.columns, res.data, {
          theme: 'grid',
          styles: {},
          headerStyles: { fontSize: 8 },
          bodyStyles: { fontSize: 8 },
          alternateRowStyles: {},
          columnStyles: {},
          //Properties
          startY: false,
          margin: { top: 45, left: 10 },
          pageBreak: 'auto',
          tableWidth: 'auto',
          showHeader: 'firstPage',
          tableLineColor: 200,
          tableLineWidth: 0,
        });

        var res2 = pdf.autoTableHtmlToJson(document.getElementById("antecedentes"));
        pdf.autoTable(res2.columns, res2.data, {
          theme: 'striped',
          styles: {},
          headerStyles: { fontSize: 8 },
          bodyStyles: { fontSize: 8 },
          alternateRowStyles: {},
          columnStyles: {},
          //Properties
          startY: false,
          margin: { top: 80, left: 10 },
          pageBreak: 'auto',
          tableWidth: 'auto',
          showHeader: 'firstPage',
          tableLineColor: 200,
          tableLineWidth: 0,
        });

        var res3 = pdf.autoTableHtmlToJson(document.getElementById("alergias"));
        pdf.autoTable(res3.columns, res3.data, {
          theme: 'striped',
          styles: {},
          headerStyles: { fontSize: 8 },
          bodyStyles: { fontSize: 8 },
          alternateRowStyles: {},
          columnStyles: {},
          //Properties
          startY: false,
          margin: { top: 115, left: 10 },
          pageBreak: 'auto',
          tableWidth: 'auto',
          showHeader: 'firstPage',
          tableLineColor: 200,
          tableLineWidth: 0,
        });

        var res4 = pdf.autoTableHtmlToJson(document.getElementById("toleranciaHemorragia"));
        pdf.autoTable(res4.columns, res4.data, {
          theme: 'striped',
          styles: {},
          headerStyles: { fontSize: 8 },
          bodyStyles: { fontSize: 8 },
          alternateRowStyles: {},
          columnStyles: {},
          //Properties
          startY: false,
          margin: { top: 140, left: 10 },
          pageBreak: 'auto',
          tableWidth: 'auto',
          showHeader: 'firstPage',
          tableLineColor: 200,
          tableLineWidth: 0,
        });
        pdf.setFontSize(12);
        pdf.text(1100, 39, "EXAMEN CLINICO INTRAORAL", null, null, 'center');

        var res5 = pdf.autoTableHtmlToJson(document.getElementById("examenClinico"));
        pdf.autoTable(res5.columns, res5.data, {
          theme: 'striped',
          styles: {},
          headerStyles: { fontSize: 8 },
          bodyStyles: { fontSize: 8 },
          alternateRowStyles: {},
          columnStyles: {},
          //Properties
          startY: false,
          margin: { top: 170, left: 10 },
          pageBreak: 'auto',
          tableWidth: 'auto',
          showHeader: 'firstPage',
          tableLineColor: 200,
          tableLineWidth: 0,
        });

        /*const columns = [
         { title: "Fecha de atención", dataKey: "fecha_atencion" },
         { title: "Pieza Dentaria", dataKey: "pieza_tratada" },
         { title: "Diagnostico", dataKey: "diagnostico" },
         { title: "Observaciones", dataKey: "observaciones" },
         { title: "Doctor", dataKey: "fecha_atencion" },
       ];
      pdf.autoTable(columns, this.state.listTratamiento, {
         createdCell: function (cell, data) {
           if (data.colum.dataKey === 'diagnostico') {
             cell.text = cell.raw.codigo + ' ' + cell.raw.descripcion;
           }
         },
         theme: 'striped',
         styles: {},
         headerStyles: { fontSize: 8 },
         bodyStyles: { fontSize: 8 },
         alternateRowStyles: {},
         columnStyles: {},
         //Properties
         startY: false,
         margin: { top: 350, left: 10 },
         pageBreak: 'auto',
         tableWidth: 'auto',
         //showHeader: 'firstPage',
         tableLineColor: 200,
         tableLineWidth: 0,
       });*/

        /*
        
          if (data.colum.dataKey === 'medico') {
            cell.text = cell.raw.nombres + ' ' + cell.raw.apellido_paterno;
          }
        var res6 = pdf.autoTableHtmlToJson(document.getElementById("tratamiento"));
        pdf.autoTable(res6.columns, res6.data, {
          theme: 'striped',
          styles: {},
          headerStyles: { fontSize: 8 },
          bodyStyles: { fontSize: 8 },
          alternateRowStyles: {},
          columnStyles: {},
          //Properties
          startY: false,
          margin: { top: 350, left: 10 },
          pageBreak: 'auto',
          tableWidth: 'auto',
          //showHeader: 'firstPage',
          tableLineColor: 200,
          tableLineWidth: 0,
        });*/


        // pdf.output('dataurlnewwindow');
        // pdf.save("download.pdf");
        window.open(pdf.output('bloburl'), '_blank');
      });

  }
  render() {
    return (
      <div className="animated fadeIn">
        {console.log('idAFF FICHA_PROPS= ', this.state.idAf)}
        <Row>
          <Col xs="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Ficha de Atención Odontológica</strong>
              </CardHeader>
              <CardBody>
                <div className="card-header-actions">
                  <Button block color="link" onClick={this.print}>Imprimir</Button>
                </div>
                <strong> <p style={{ textAlign: 'center' }} >DATOS PERSONALES DEL PACIENTE</p></strong>
                <hr />
                <div>
                  <Table responsive hover bordered size="sm" id="datosGral">
                    <tbody>
                      <tr>
                        <td>{this.state.apellido_pat}</td>
                        <td>{this.state.apellido_mat}</td>
                        <td>-</td>
                        <td>{this.state.nombres}</td>
                        <td>25</td>
                        <td>MASCULINO</td>
                      </tr>
                    </tbody>
                    <thead>
                      <tr>
                        <td><strong>Apellido Paterno</strong></td>
                        <td><strong>Apellido Materno</strong></td>
                        <td><strong>Apellido del esposo</strong></td>
                        <td><strong>Nombres</strong></td>
                        <td><strong>Edad</strong></td>
                        <td><strong>Sexo</strong></td>
                      </tr>

                    </thead>
                    <tbody>
                      <tr>
                        <td>SOLTERO</td>
                        <td>ESTUDIANTE</td>
                        <td>EL TEJAR</td>
                        <td>73071254</td>
                        <td>FERNANDO CASTRO</td>
                        <td>04-11-2018</td>
                      </tr>
                    </tbody>
                    <thead>
                      <tr>
                        <td><strong>Estado Civil</strong></td>
                        <td><strong>Ocupacion</strong></td>
                        <td><strong>Domicilio</strong></td>
                        <td><strong>Telefono</strong></td>
                        <td><strong>Odontologo</strong></td>
                        <td><strong>Fecha de Atencion</strong></td>
                      </tr>
                    </thead>
                  </Table>
                </div >

                <hr />
                <strong><p style={{ textAlign: 'center' }} >MOTIVOS DE CONSULTA</p></strong>
                <hr />
                <div>
                  <Table responsive hover bordered size="sm" style={{ 'textAlign': 'center' }}>
                    <thead>
                      <tr>
                        <th scope="col">Motivo de Consulta</th>
                        <th scope="col">Fechas de Motivos de consulta</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        this.state.list_tieneMotivoConsulta.map(item => {
                          return (
                            <tr key={item.id}>
                              <td>{item.motivoConsulta_id.detalle}</td>
                              <td>{item.fecha_motivoConsulta}</td>
                            </tr>
                          )
                        })
                      }
                    </tbody>
                  </Table>
                </div>

                <hr />
                <strong><p style={{ textAlign: 'center' }} >ANTECEDENTES GENERALES</p></strong>
                <hr />

                <div>
                  Antecedentes de una enfermedad de importancia y medicaccion constante
                  <Table responsive hover bordered size="sm" id="antecedentes">
                    <tbody>
                      {this.state.antecedentes.map((item, i) => (
                        <tr key={item.id}>
                          <td>{i + 1}</td><td><strong>Fecha:</strong></td><td>{item.fecha}</td><td><strong>Enfermedad:</strong></td><td>{item.enfermedad.descripcion}</td><td><strong>Medicacion:</strong></td><td>{item.medicacion}</td><td><strong>Tipo:</strong></td><td>{item.tipo}</td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </div>

                <Row>
                  <Col xs="6">
                    Alergia a:
                    <Table responsive hover bordered size="sm" id="alergias">
                      <tbody>
                        {this.state.alergias.map((item, i) => (
                          <tr key={item.id}>
                            <td>{i + 1}</td><td>{item.alergia.descripcion}</td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </Col>
                  <Col xs="6">
                    <Table responsive hover bordered size="sm" id="toleranciaHemorragia">
                      {this.state.listTolerancia.map(item => (
                        <tbody key={item.id}>
                          <tr>
                            <td>Tolerancia Antibioticos:</td><td>{item.tolerancia_antibioticos}</td>
                          </tr>
                          <tr>
                            <td>Sufre hemorragia despues de una exodoncia:</td><td>{item.hemorragia}</td>
                          </tr>
                        </tbody>
                      ))}
                    </Table>
                  </Col>
                </Row>


                <hr />
                <strong><p style={{ textAlign: 'center' }} >EXAMEN CLINICO INTRAORAL</p></strong>
                <hr />
                <div>
                  <Table responsive hover bordered size="sm" id="examenClinico">
                    <thead>
                      <tr>
                        <th scope="col">Higiene Bucal</th>
                        <th scope="col">Oclusión</th>
                        <th scope="col">Mucosa Gingival</th>
                        <th scope="col">Enfermedad Periodontal</th>
                        <th scope="col">Mucosa Bucal</th>
                      </tr>
                    </thead>
                    <tbody>

                      {this.state.examen.map(item => (
                        <tr key={item.id}>
                          <td>{item.higieneBucal}</td>
                          <td>{item.oclusion}</td>
                          <td>{item.mucosa_gingival}</td>
                          <td>{item.enfermedad_periodontal}</td>
                          <td>{item.mucosa_bucal}</td>
                        </tr>

                      ))}
                    </tbody>
                  </Table>
                </div>
                <div id="odontograma">
                  {(this.state.idExGet !== 0) ? <div><OdontogramaF idExGet={this.state.idExGet} /></div> : null}
                </div>




                <hr />
                <strong><p style={{ textAlign: 'center' }} >TRATAMIENTO ODONTOLÓGICO</p></strong>
                <hr />
                <div>
                  <Table responsive hover bordered size="sm" id="tratamiento">
                    <thead>
                      <tr>
                        <th scope="col" style={{ textAlign: 'center', width: '100px' }}>Fecha de atención</th>
                        <th scope="col" style={{ width: '10px' }}>Pieza Dentaria</th>
                        <th scope="col" style={{ textAlign: 'center', width: '300px', maxWidth: '301px' }}>Diagnostico</th>
                        <th scope="col" style={{ textAlign: 'center', width: '200px', maxWidth: '301px' }}>Tratamiento realizado</th>
                        <th scope="col" style={{ textAlign: 'center' }}>Observaciones</th>
                        <th scope="col">Doctor</th>

                      </tr>
                    </thead>
                    <tbody>
                      {this.state.listTratamiento.map(item => (
                        <tr key={item.id}>
                          <td style={{ textAlign: 'center', width: '100px' }}>{item.fecha_atencion}</td>
                          <td style={{ width: '10px' }}>{item.pieza_tratada}</td>
                          <td style={{ width: '300px', height: '50px', maxWidth: '301px', maxHeight: '100px' }}>{
                            item.diagnostico.map(d => (
                              <ul key={d.codigo}>
                                <li>{d.codigo + '-' + d.descripcion}</li>
                              </ul>
                            ))
                          }</td>
                          <td style={{ width: '200px', height: '50px', maxWidth: '301px', maxHeight: '100px' }}>{item.tratamiento_realizado}</td>
                          <td>{item.observaciones}</td>
                          <td>{item.medico.nombres + ' ' + item.medico.apellido_paterno}</td>
                        </tr>
                      ))
                      }
                    </tbody>
                  </Table>
                </div>



              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default FichaOdontologica;
/*
 */