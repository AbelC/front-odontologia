import React from 'react';
import ReactDOM from 'react-dom';
import FichaOdontologica from './FichaOdontologica';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<FichaOdontologica />, div);
  ReactDOM.unmountComponentAtNode(div);
});
