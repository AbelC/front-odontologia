import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row } from 'reactstrap';
//import { AppSwitch } from '@coreui/react'



import DatosPersonalesF from './ComponentFicha/DatosPersonalesF';
import AntecedentesF from './ComponentFicha/AntecedentesF';
import AlergiasF from './ComponentFicha/AlergiasF';
import ToleranciaF from './ComponentFicha/ToleranciaF';
import ExamenClinicoF from './ComponentFicha/ExamenClinicoF';
import ListaMotivoConsultaF from './ComponentFicha/ListarMotivoConsultaF'

//esta es la ficha odontologica para ver aqui

class FichaOdontologica2 extends Component {

  constructor(props) {
    super(props);

    this.state = {
      idAf: this.props.idAf,
    }
  }

  render() {
    return (
      <div className="animated fadeIn">
        {console.log('idAFF FICHA_PROPS 2= ', this.state.idAf)}
        <Row>
          <Col xs="12">
            <Card>

              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Ficha de Atencion Odontologica</strong>
              </CardHeader>

              <CardBody>
                <hr />
                <strong> <p style={{ textAlign: 'center' }} >DATOS PERSONALES DEL PACIENTE</p></strong>
                <hr />
                <DatosPersonalesF idAf={this.state.idAf} />

                <hr />
                <strong><p style={{ textAlign: 'center' }} >MOTIVOS DE CONSULTA</p></strong>
                <hr />
                <ListaMotivoConsultaF idAf={this.state.idAf} />

                <hr />
                <strong><p style={{ textAlign: 'center' }} >ANTECEDENTES GENERALES</p></strong>
                <hr />

                <AntecedentesF idAf={this.state.idAf} />

                <Row>
                  <Col xs="6"><AlergiasF idAf={this.state.idAf} /></Col>
                  <Col xs="6"><ToleranciaF idAf={this.state.idAf} /></Col>
                </Row>


                <hr />
                <strong><p style={{ textAlign: 'center' }} >EXAMEN CLINICO INTRAORAL</p></strong>
                <hr />

                <ExamenClinicoF idAf={this.state.idAf} />


              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>





    );
  }
}

export default FichaOdontologica2;
