import React, { Component } from 'react';
import { Row, Card, Col, CardBody, CardHeader, Form, FormGroup, Label, Button, Modal, ModalBody, ModalFooter, ModalHeader, } from 'reactstrap';
//import { AppSwitch } from '@coreui/react'
//import Tolerancia_F from './Tolerancia_F';

var url = process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/listaGuarda/';
class NuevoBC extends Component {
    constructor(props) {
        super(props);
        this.state = {
            warning: false,
            success: false,
            dangerNoti: false,
            infoNoti: false,
        }
        this.toggleSuccess = this.toggleSuccess.bind(this);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.toggleWarning = this.toggleWarning.bind(this);
        this.toggleDangerNoti = this.toggleDangerNoti.bind(this);
        this.toggleInfoNoti = this.toggleInfoNoti.bind(this);
    }
    toggleDangerNoti() {
        this.setState({
            danger: !this.state.danger,
        });
        window.location.reload()
    }

    toggleInfoNoti() {
        this.setState({
            info: !this.state.info,
        });
        window.location.reload();
        window.location='#/Insumos/ListaGeneral';
    }

    toggleSuccess() {
        if(this.codigo.value !=='' && this.localizacion.value !== '' && this.descripcion.value !== '' && this.unidad.value !== '' && this.fecha.value !== '' && this.tipo.value !== ''){
            this.setState({
                success: !this.state.success,
            });
        }else{
            this.setState({
                warning: !this.state.warning,
            });
        }
    }
    toggleWarning() {
        this.setState({
            warning: !this.state.warning,
        });
    }
    handleOnSubmit(e) {  
            console.log('DATOS NO VACIOS')
            e.preventDefault()
            let data = {
                codigo: this.codigo.value,
                localizacion: this.localizacion.value,
                descripcion: this.descripcion.value,
                unidad: this.unidad.value,
                fechaVencimiento: this.fecha.value,
                tipo: this.tipo.value,
            }

            //console.log('** ', data)
            try {
                fetch(url, {
                    method: 'POST', // or 'PUT'
                    body: JSON.stringify(data), // data can be string or {object}!
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(resp => {
                    if (resp.status === 201) {
                        this.setState({
                            infoNoti: true
                        })
                        //console.log('resp.status', resp.status)
                    } else {
                        //console.log('resp.status ERROR', resp.status)
                        this.setState({
                            dangerNoti: true
                        })
                    }
                })

                /*.then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(response => console.log('Success:', response));*/
                //window.location.reload();
                //window.location='#/Insumos/ListaGeneral';
            } catch (e) {
                console.log(e);
            }
            this.setState({
                success: !this.state.success,
            });

            this.setState({ now: new Date() });
        }
    render() {

        return (
            <Col xs="12">
                <Card>
                    <CardHeader>
                        <i className="fa fa-align-justify"></i> <strong>Nuevo Bind Card</strong>
                        <div className="card-header-actions">
                            <strong className="text-muted"></strong>
                        </div>
                    </CardHeader>
                    <CardBody>
                        <Form>
                            <Row>
                                <Col sm="3">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="fecha" className="mr-sm-2">Codigo N°</Label>
                                        <input type="text" className="form-control" ref={codigo => this.codigo = codigo} required />
                                    </FormGroup>
                                </Col>
                                <Col sm="3">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="fecha" className="mr-sm-2">Unidad</Label>
                                        <select className="form-control" ref={unidad => this.unidad = unidad}>
                                            <option value="">SELECCIONAR</option>
                                            <option value="CAJA">CAJA</option>
                                            <option value="COMPRIMIDO">COMPRIMIDO</option>
                                            <option value="ESTUCHE">ESTUCHE</option>
                                            <option value="FRASCO">FRASCO</option>
                                            <option value="JUEGO">JUEGO</option>
                                            <option value="PIEZA">PIEZA</option>
                                            <option value="SOBRE">SOBRE</option>                                          
                                        </select>
                                    </FormGroup>
                                </Col>
                                <Col sm="3">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="fecha" className="mr-sm-2">Descripción</Label>
                                        <input type="text" className="form-control" ref={descripcion => this.descripcion = descripcion} required />
                                    </FormGroup>
                                </Col>
                                <Col sm="3">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="fecha" className="mr-sm-2">Tipo</Label>
                                        <select className="form-control" ref={tipo => this.tipo = tipo}>
                                            <option value="">SELECCIONAR</option>
                                            <option value="INSUMO">INSUMO</option>
                                            <option value="MEDICAMENTO">MEDICAMENTO</option>
                                        </select>
                                    </FormGroup>
                                </Col>
                            </Row>



                            <br />
                            <Row>
                                <Col sm="6">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="fecha" className="mr-sm-2">Localización</Label>
                                        <input type="text" className="form-control" value="Odontologia"  ref={localizacion => this.localizacion = localizacion} disabled />
                                    </FormGroup>
                                </Col>
                                <Col sm="6">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="fecha" className="mr-sm-2" >Fecha Vencimiento</Label>
                                        <input type="date" className="form-control" ref={fecha => this.fecha = fecha} required />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <br />

                            <Button color="success" block onClick={this.toggleSuccess} className="mr-1">Guardar</Button>


                        </Form>
                    </CardBody>
                </Card>

                <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                    className={'modal-success ' + this.props.className}>
                    <ModalHeader toggle={this.toggleSuccess}>Confirmación</ModalHeader>
                    <ModalBody>
                        ¿Esta seguro de guardar?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={this.handleOnSubmit}>Aceptar</Button>{' '}
                        <Button color="secondary" onClick={this.toggleSuccess}>Cancelar</Button>
                    </ModalFooter>
                </Modal>
                <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
                    className={'modal-warning ' + this.props.className}>
                    <ModalHeader toggle={this.toggleWarning}>Campos vacios</ModalHeader>
                    <ModalBody>
                        Verifique que ningun campo este vacio.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleWarning}>Cerrar</Button>
                    </ModalFooter>
                </Modal>
                   {/*MODALES DE NOTIFICACION SI SE REGISTRO CORRECTAMENTE LOS DATOS*/}
                   <Modal isOpen={this.state.dangerNoti} toggle={this.toggleDangerNoti}
                    className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDangerNoti}><strong>Advertencia</strong></ModalHeader>
                    <ModalBody>
                        Los datos NO fueron registrados correctamente, verifique e intente nuevamente.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleDangerNoti}>Cerrar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.infoNoti} toggle={this.toggleInfoNoti}
                    className={'modal-info ' + this.props.className}>
                    <ModalHeader toggle={this.toggleInfoNoti}><strong>Correcto</strong></ModalHeader>
                    <ModalBody>
                        Los datos fueron registrados correctamente.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleInfoNoti}>Cerrar</Button>
                    </ModalFooter>
                </Modal>
            </Col>


        );
    }
}

export default NuevoBC;
