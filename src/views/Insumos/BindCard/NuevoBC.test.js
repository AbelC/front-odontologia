import React from 'react';
import ReactDOM from 'react-dom';
import NuevoBC from './NuevoBC';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<NuevoBC />, div);
  ReactDOM.unmountComponentAtNode(div);
});
