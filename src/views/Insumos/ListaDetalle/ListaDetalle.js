import React, { Component } from 'react';
import { Row, Card, Col, CardBody, CardHeader, Button, Modal, ModalBody, ModalFooter, ModalHeader, Label, Table } from 'reactstrap';
import Search from 'react-search-box';
import TableDetalle from './TableDetalle';
//import { AppSwitch } from '@coreui/react'
class ListaDetalle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            success: false,
            bindCard_v: [],
            medico_v: [],
            insumo: 0,
            sumSal: 0,            
            sumEnt: 0,
            //visible: false,
            //visible2: false,
            warning: false,
            //swShowSent: false,
            //swShowAdvance: false,
            swShowTable: false,
            saldo: 0,
            //cantidad: 0,
            descripcion: '',
            codigo: '',
            danger: false,

        }
        this.toggleSuccess = this.toggleSuccess.bind(this);
        this.handleObtieneSum = this.handleObtieneSum.bind(this);
 
        // this.handleVerificaSaldo = this.handleVerificaSaldo.bind(this);
    }
  

    async componentDidMount() { //Lista de diagnosticos
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/listaGuarda/');
            const bindCard_v = await respuesta.json();

            this.setState({
                bindCard_v,
            });

        } catch (e) {
            console.log(e);
        }
    }


    toggleSuccess() {
        this.setState({
            success: !this.state.success,
        });
    }
    handleObtieneSum() {
        //this.setState({ visible2: false, visible: false, swShowSent: false, swShowAdvance: false })

        var idIns = null;
        idIns = this.state.insumo;
        //var cant = this.cantidad.value;
        //this.setState({ cantidad: cant })

        fetch(process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/sumEntradas/' + idIns + '/')
            .then((ResponseE) => ResponseE.json())
            .then(findresponseEntradas => {
                if (findresponseEntradas.length !== 0) {
                    //console.log("findresponseEntradas: ", findresponseEntradas[0].sum);
                    this.setState({ sumEnt: findresponseEntradas[0].sum });

                    fetch(process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/sumSalidas/' + idIns + '/')
                        .then((ResponseS) => ResponseS.json())
                        .then(findresponseSalidas => {
                            if (findresponseSalidas.length !== 0) {
                                //console.log("findresponseSalidas: ", findresponseSalidas[0].sum);
                                //this.setState({ sumSal: findresponseSalidas[0].sum })

                                if (findresponseEntradas.length !== 0 && findresponseSalidas.length !== 0) {
                                    //this.handleVerificaSaldo();
                                    this.setState({ saldo: findresponseEntradas[0].sum - findresponseSalidas[0].sum })
                                }

                            } else {
                                //this.setState({ sumSal: 0 });
                                if (findresponseEntradas.length !== 0) {
                                    //this.handleVerificaSaldo();
                                    this.setState({ saldo: findresponseEntradas[0].sum - 0 })
                                }
                            }
                        });

                } else {
                    //this.setState({ sumEnt:0, sumSal: 0 });
                    //this.handleVerificaSaldo();
                    this.setState({ saldo: 0 })
                }
            });

        this.setState({ swShowTable: true })
    }

    handleChangeBindCard(value) {
        //console.log('value: ', value);   
        // this.setState({ visible2: false, visible: false, swShowSent: false, swShowAdvance: false, swShowTable: false })
        this.setState({ swShowTable: false, saldo: 0 })
        var idInsumo = 0;
        var desc;
        var cod;
        for (let i = 0; i < this.state.bindCard_v.length; i++) {
            if (this.state.bindCard_v[i].codigo === value) {
                //console.log('idBindCard: ', this.state.bindCard_v[i].id, ' codigo: ', this.state.bindCard_v[i].codigo);
                idInsumo = this.state.bindCard_v[i].id;
                desc = this.state.bindCard_v[i].descripcion;
                cod = this.state.bindCard_v[i].codigo;
                break;
            }
        }
        if (idInsumo !== 0) {
            this.setState({ insumo: idInsumo, descripcion: desc, codigo: cod })
        } else {
            console.log('no')
        }
    }
    render() {
        return (
            <Row>
                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i> <strong>Lista Detalle</strong>
                            <div className="card-header-actions">
                                <strong className="text-muted"></strong>
                            </div>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col sm="3"></Col>
                                <Col sm="4">
                                    <Label for="fecha" className="mr-sm-2">Insumo / Medicamento</Label>
                                    <Search
                                        data={this.state.bindCard_v}
                                        onChange={this.handleChangeBindCard.bind(this)}
                                        placeholder="Codigo"
                                        name="buscCodigoInsumo"
                                        class="search-class"
                                        searchKey="codigo"
                                    />
                                </Col>
                                <Col sm="2">
                                    <br />
                                    <Button color="link" block onClick={this.handleObtieneSum} className="mr-1">Buscar</Button>
                                </Col>
                            </Row>
                            <hr />
                            {(this.state.swShowTable === true) ?
                                <Row>
                                    <Col sm="12" md={{ size: 8, offset: 2 }}>
                                        <Table responsive bordered size="sm" style={{ 'textAlign': 'center' }}>
                                            <tbody>
                                                <tr><th style={{ 'width': '25%' }}>Código</th><th>Descripción</th><th style={{ 'width': '15%' }}>Saldo</th></tr>
                                                <tr><td>{this.state.codigo}</td><td>{this.state.descripcion}</td><td>{this.state.saldo}</td></tr>
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row> : null}
                            {(this.state.swShowTable === true) ?
                                <TableDetalle idBC={this.state.insumo}  codigo={this.state.codigo}  descripcion={this.state.descripcion} saldo={this.state.saldo}/> : null}

                        </CardBody>
                    </Card>

                    <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                        className={'modal-success ' + this.props.className}>
                        <ModalHeader toggle={this.toggleSuccess}>Confirmación</ModalHeader>
                        <ModalBody>
                            ¿Esta seguro de guardar?
                    </ModalBody>
                        <ModalFooter>
                            <Button color="success" onClick={this.handleOnSubmit}>Aceptar</Button>{' '}
                            <Button color="secondary" onClick={this.toggleSuccess}>Cancelar</Button>
                        </ModalFooter>
                    </Modal>
                </Col>
            </Row>

        );
    }
}

export default ListaDetalle;
