import React from 'react';
import ReactDOM from 'react-dom';
import ListaDetalle from './ListaDetalle';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ListaDetalle />, div);
  ReactDOM.unmountComponentAtNode(div);
});
