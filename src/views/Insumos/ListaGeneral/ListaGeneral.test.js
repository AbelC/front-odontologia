import React from 'react';
import ReactDOM from 'react-dom';
import ListaGeneral from './ListaGeneral';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ListaGeneral />, div);
  ReactDOM.unmountComponentAtNode(div);
});
