import React, { Component } from 'react';


class Saldo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sumEnt: 0,
            sumSal: 0,
        };
    }
    /*    async componentDidMount() { //ciclo de vida de react de forma asincrona
            try {
                fetch(process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/sumEntradas/' + this.props.idIns + '/')
                    .then((ResponseE) => ResponseE.json())
                    .then(findresponseEntradas => {
                        console.log('ent: ', this.props.idIns)
                        if (findresponseEntradas.length !== 0) {
                            //console.log("findresponseEntradas: ", findresponseEntradas[0].sum, ' findresponseEntradas[0].id: ', findresponseEntradas[0].insumo_id);
                            this.setState({ sumEnt: findresponseEntradas[0].sum })
                        }else{
                            //console.log("findresponseEntradas: ", 0, ' findresponseEntradas[0].id: ', this.props.idIns);
                        }
                    });
    
    
                fetch(process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/sumSalidas/' + this.props.idIns + '/')
                    .then((ResponseS) => ResponseS.json())
                    .then(findresponseSalidas => {
                        //console.log('sal: ', this.props.idIns)
                        if (findresponseSalidas.length !== 0) {                        
                                //console.log("findresponseSalidas: ", findresponseSalidas[0].sum, ' findresponseSalidas[0].id: ', findresponseSalidas[0].insumo_id);
                                this.setState({ sumSal: findresponseSalidas[0].sum })
                            
                        }else{
                            //console.log("findresponseSalidas: ", 0, ' findresponseSalidas[0].id: ', this.props.idIns);
                        }
                    });
            } catch (e) {
                console.log(e);
            }
        }*/
    async componentDidMount() {
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL + 'odontologia/InsumoMedicamento/v1/sumEntradas/' + this.props.idIns + '/');
            const data = await respuesta.json();
            if (data.length > 0) {
                this.setState({
                    sumEnt: data[0].sum
                });
            }
            const respuesta2 = await fetch(process.env.REACT_APP_API_URL + 'odontologia/InsumoMedicamento/v1/sumSalidas/' + this.props.idIns + '/');
            const data2 = await respuesta2.json();
            if (data2.length > 0) {
                this.setState({
                    sumSal: data2[0].sum
                });
            }
        } catch (e) {
            console.log(e);
        }
    }
    render() {

        return (
            <div className="animated fadeIn">
                <strong>{this.state.sumEnt - this.state.sumSal}</strong>
                {
                    //  console.log('E: ', this.state.sumEnt, ' S: ', this.state.sumSal,' ID: ', this.props.idIns)

                }
            </div>
        );
    }
}

export default Saldo;
