import React from 'react';
import ReactDOM from 'react-dom';
import RegistroEntrada from './RegistroEntrada';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RegistroEntrada />, div);
  ReactDOM.unmountComponentAtNode(div);
});
