import React, { Component } from 'react';
import Search from 'react-search-box';
import {
    Row, Card, Col, CardBody, CardHeader, Form, FormGroup, Table,
    Label, Button, Modal, ModalBody, ModalFooter, ModalHeader, Alert
} from 'reactstrap';
//import { AppSwitch } from '@coreui/react'

var option = {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
};
var dd = new Date();
var timeE = dd.toLocaleString('es-bo', option);
var dateE = timeE.split(' ')[0].split('/');
timeE = timeE.split(' ')[1].split(':');

var dateShow = dateE[0] + '/' + dateE[1] + '/' + dateE[2];

var url = process.env.REACT_APP_API_URL + 'odontologia/InsumoMedicamento/v1/registroSalida/';
class RegistroSalida extends Component {
    constructor(props) {
        super(props);
        this.state = {
            success: false,
            bindCard_v: [],
            medico_v: [],
            insumo: 0,
            sumSal: 0,
            sumEnt: 0,
            visible: false,
            visible2: false,
            warning: false,
            warning2: false,
            warning3: false,
            swShowSent: false,
            swShowAdvance: false,
            swShowTable: false,
            saldo: 0,
            cantidad: 0,
            descripcion: '',
            codigo: '',
            danger: false,
            dangerNoti: false,
            infoNoti: false,

        }
        this.toggleSuccess = this.toggleSuccess.bind(this);
        this.onDismiss = this.onDismiss.bind(this);
        this.onDismiss2 = this.onDismiss2.bind(this);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.handleObtieneSum = this.handleObtieneSum.bind(this);
        this.handleVerificaSaldo = this.handleVerificaSaldo.bind(this);
        this.toggleWarning = this.toggleWarning.bind(this);
        this.toggleWarning2 = this.toggleWarning2.bind(this);
        this.toggleWarning3 = this.toggleWarning3.bind(this);
        this.toggleDanger = this.toggleDanger.bind(this);
        this.toggleDangerNoti = this.toggleDangerNoti.bind(this);
        this.toggleInfoNoti = this.toggleInfoNoti.bind(this);
    }
    toggleDangerNoti() {
        this.setState({
            danger: !this.state.danger,
        });
        window.location.reload()
    }

    toggleInfoNoti() {
        this.setState({
            info: !this.state.info,
        });
        window.location.reload();
        window.location = '#/Insumos/ListaGeneral';
    }
    toggleDanger() {
        this.setState({
            danger: !this.state.danger,
        });
    }
    toggleWarning2() {
        this.setState({
            warning2: !this.state.warning2,
        });
    }
    toggleWarning3() {
        this.setState({
            warning3: !this.state.warning3,
        });
    }
    onDismiss() {
        this.setState({ visible: !this.state.visible });
    }
    onDismiss2() {
        this.setState({ visible2: !this.state.visible2 });
    }
    toggleWarning() {
        this.setState({
            warning: !this.state.warning,
        });
    }
    async componentDidMount() { //Lista de diagnosticos
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL + 'odontologia/InsumoMedicamento/v1/listaGuarda/');
            const bindCard_v = await respuesta.json();
            const respuesta2 = await fetch(process.env.REACT_APP_API_URL + 'odontologia/persona/v1/medico/');
            const medico_v = await respuesta2.json();
            //
            this.setState({
                bindCard_v,
                medico_v
            });

        } catch (e) {
            console.log(e);
        }
    }


    handleChangeBindCard(value) {
        //console.log('value: ', value);
        this.setState({ visible2: false, visible: false, swShowSent: false, swShowAdvance: false, swShowTable: false })
        var idInsumo = 0;
        var desc;
        var cod;
        for (let i = 0; i < this.state.bindCard_v.length; i++) {
            if (this.state.bindCard_v[i].codigo === value) {
                //console.log('idBindCard: ', this.state.bindCard_v[i].id, ' codigo: ', this.state.bindCard_v[i].codigo);
                idInsumo = this.state.bindCard_v[i].id;
                desc = this.state.bindCard_v[i].descripcion;
                cod = this.state.bindCard_v[i].codigo;
                break;
            }
        }
        if (idInsumo !== 0) {
            this.setState({ insumo: idInsumo, descripcion: desc, codigo: cod })
        } else {
            console.log('no')
        }
    }

    handleObtieneSum() {
        this.setState({ visible2: false, visible: false, swShowSent: false, swShowAdvance: false })


        var canti = parseInt(this.cantidad.value, 10);
        if (canti >= 1) {
            var idIns = null;
            idIns = this.state.insumo;
            var cant = this.cantidad.value;
            this.setState({ cantidad: cant })

            if (idIns !== null && this.cantidad.value !== '') {
                if (this.cantidad.value !== '0') {
                    this.setState({ swShowTable: true })
                    fetch(process.env.REACT_APP_API_URL + 'odontologia/InsumoMedicamento/v1/sumEntradas/' + idIns + '/')
                        .then((ResponseE) => ResponseE.json())
                        .then(findresponseEntradas => {
                            if (findresponseEntradas.length !== 0) {
                                //console.log("findresponseEntradas: ", findresponseEntradas[0].sum);
                                this.setState({ sumEnt: findresponseEntradas[0].sum });

                                fetch(process.env.REACT_APP_API_URL + 'odontologia/InsumoMedicamento/v1/sumSalidas/' + idIns + '/')
                                    .then((ResponseS) => ResponseS.json())
                                    .then(findresponseSalidas => {
                                        if (findresponseSalidas.length !== 0) {
                                            //console.log("findresponseSalidas: ", findresponseSalidas[0].sum);
                                            this.setState({ sumSal: findresponseSalidas[0].sum })

                                            if (findresponseEntradas.length !== 0 && findresponseSalidas.length !== 0) {
                                                this.handleVerificaSaldo();
                                            }

                                        } else {
                                            this.setState({ sumSal: 0 });
                                            if (findresponseEntradas.length !== 0) {
                                                this.handleVerificaSaldo();
                                            }
                                        }
                                    });

                            } else {
                                this.setState({ visible: true });
                            }
                        });
                } else {
                    this.setState({ danger: true })
                }

            } else {
                this.setState({ warning: !this.state.warning });
            }



        } else {
            this.setState({ warning2: !this.state.warning2 })
        }



    }


    handleVerificaSaldo() {
        //if(this.state.visible === true) this.setState({visible:false})

        //console.log('idInsumo: ', idInsumo, ' cant:', cant, ' this.state.sumEnt: ', this.state.sumEnt, ' this.state.sumSal: ', this.state.sumSal)
        var sal = this.state.sumEnt - this.state.sumSal;
        this.setState({ saldo: sal })
        console.log('this.state.cantidad : ', this.state.cantidad)
        if (this.state.cantidad > sal) {
            //console.log('cantidad insuficiente, el  es: ', sal);                
            this.setState({ swShowAdvance: true, swShowSent: false });
        } else {
            this.setState({ swShowSent: true, visible2: true, swShowAdvance: false });
        }


    }
    toggleSuccess() {
        if (this.state.insumo !== '' && this.medico.value !== '' && this.cantidad.value !== '' && this.enfermera.value !== '') {
            this.setState({
                success: !this.state.success,
            });
        } else {
            this.setState({
                warning3: !this.state.warning3,
            });
        }

    }
    handleOnSubmit(e) {
        console.log('DATOS NO VACIOS')
        var options = {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit'
        };
        var d = new Date();
        var time = d.toLocaleString('es-bo', options);
        var date = time.split(' ')[0].split('/');
        time = time.split(' ')[1].split(':');
        var dateS = date[2] + '-' + date[1] + '-' + date[0];
        var timeS = time[0] + ':' + time[1] + ':' + time[2];

        //var dateFull = new Date(date[2], date[1], date[0], time[0], time[1], time[2]);
        //var dateS = dateFull.getFullYear() + '-' + dateFull.getMonth() + '-' + dateFull.getDate();  /*Date to send */
        //var timeS = dateFull.getHours() + ':' + dateFull.getMinutes() + ':' + dateFull.getSeconds();  /**Time to send */


        e.preventDefault()
        let data = {
            fechaRegistro: dateS,
            hora: timeS,
            insumo: this.state.insumo,
            cantidad: this.state.cantidad,
            observaciones: this.observaciones.value,
            medico: this.medico.value,
            enfermera: this.enfermera.value,
        }

        //console.log('** ', data)
        try {
            fetch(url, {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(data), // data can be string or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(resp => {
                    if (resp.status === 201) {
                        this.setState({
                            infoNoti: true
                        })
                        //console.log('resp.status', resp.status)
                    } else {
                        //console.log('resp.status ERROR', resp.status)
                        this.setState({
                            dangerNoti: true
                        })
                    }
                })
            /*.then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => console.log('Success:', response));*/
            //window.location.reload();
            //window.location = '#/Insumos/ListaGeneral';

        } catch (e) {
            console.log(e);
        }
        this.setState({
            success: !this.state.success,
        });



    }
    render() {
        const listM = this.state.medico_v.map(item => {
            return (
                <option key={item.id} value={item.id}>{item.id + ' ' + item.nombres + ' ' + item.apellido_paterno}</option>
            )
        });
        //console.log('idInsumo: ', this.state.insumo, ' this.state.sumEnt: ', this.state.sumEnt, ' this.state.sumSal: ', this.state.sumSal)
        return (
            <Col xs="12">
                <Card>
                    <CardHeader>
                        <i className="fa fa-align-justify"></i> <strong>Registro de Salida de Insumos Medicos</strong>
                        <div className="card-header-actions">
                            <strong className="text-muted"></strong>
                        </div>
                    </CardHeader>
                    <CardBody>
                        {(this.state.visible2 === true) ?
                            <Row>
                                <Col sm="12" md={{ size: 8, offset: 2 }}>
                                    <Alert color="success" isOpen={this.state.visible2} toggle={this.onDismiss2}>
                                        Puede registrar la salida de un medicamento o insumo.
                                    </Alert>
                                    <hr />
                                </Col>
                            </Row> : null}
                        {(this.state.swShowTable === true) ?
                            <Row>
                                <Col sm="12" md={{ size: 8, offset: 2 }}>
                                    <Table responsive bordered size="sm" style={{ 'textAlign': 'center' }}>
                                        <tbody>
                                            <tr><th style={{ 'width': '25%' }}>Código</th><th>Descripción</th><th style={{ 'width': '15%' }}>Cantidad</th></tr>
                                            <tr><td>{this.state.codigo}</td><td>{this.state.descripcion}</td><td>{this.state.cantidad}</td></tr>
                                        </tbody>
                                    </Table>
                                </Col>
                            </Row> : null}
                        <Form>
                            <Row>
                                <Col sm="2"></Col>
                                <Col sm="4">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="fecha" className="mr-sm-2">Insumo / Medicamento</Label>
                                        <Search
                                            data={this.state.bindCard_v}
                                            onChange={this.handleChangeBindCard.bind(this)}
                                            placeholder="Codigo"
                                            name="buscCodigoInsumo"
                                            class="search-class"
                                            searchKey="codigo"
                                        />
                                    </FormGroup>
                                </Col>
                                <Col sm="2">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="fecha" className="mr-sm-2">Cantidad</Label>
                                        <input type="number" className="form-control" ref={cantidad => this.cantidad = cantidad} required />
                                    </FormGroup>
                                </Col>
                                <Col sm="2">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <br />
                                        <Button color="link" onClick={this.handleObtieneSum}>Verificar</Button>
                                    </FormGroup>
                                </Col>
                            </Row>
                            {(this.state.visible === true) ?
                                <Alert color="danger" isOpen={this.state.visible} toggle={this.onDismiss}>
                                    Debe registrar la entrada de este insumo o medicamento, antes de registrar la salida.
                            </Alert> : null}

                            {(this.state.swShowSent === true) ?
                                <Row>
                                    { /*(this.state.visible === true)  ? this.setState({visible:false}) : null*/}
                                    <Col sm="4">
                                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                            <Label for="fecha" className="mr-sm-2">Fecha Salida</Label>
                                            <input type="text" className="form-control" value={dateShow} disabled required />
                                        </FormGroup>
                                    </Col>
                                    <Col sm="4">
                                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                            <Label for="fecha" className="mr-sm-2">Para el médico</Label>
                                            <select className="form-control" ref={medico => this.medico = medico}>
                                                <option value="">SELECCIONAR</option>
                                                {listM}
                                            </select>
                                        </FormGroup>
                                    </Col>
                                    <Col sm="4">
                                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                            <Label for="fecha" className="mr-sm-2">Enfermera</Label>
                                            <input type="text" className="form-control" ref={enfermera => this.enfermera = enfermera} required />
                                        </FormGroup>
                                    </Col>
                                    <Col sm="4">
                                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                            <Label for="fecha" className="mr-sm-2">Observaciones</Label>
                                            <textarea type="textarea" className="form-control" ref={observaciones => this.observaciones = observaciones} required />
                                        </FormGroup>
                                    </Col>
                                </Row> : null}
                            <br />
                            {(this.state.swShowSent === true) ? <Button color="success" block onClick={this.toggleSuccess} className="mr-1">Guardar</Button> : null}

                            {(this.state.swShowAdvance === true) ?

                                <Row>
                                    <Col sm="12" md={{ size: 8, offset: 2 }}>
                                        <hr />
                                        <Alert color="danger">
                                            El saldo es insuficiente, solo cuenta con : <strong>{' ' + this.state.saldo + ' unidades '}</strong> de {' ' + this.state.descripcion}
                                        </Alert>
                                    </Col>
                                </Row> : null}
                        </Form>
                    </CardBody>
                </Card>

                <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                    className={'modal-success ' + this.props.className}>
                    <ModalHeader toggle={this.toggleSuccess}>Confirmación</ModalHeader>
                    <ModalBody>
                        ¿Esta seguro de guardar?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={this.handleOnSubmit}>Aceptar</Button>{' '}
                        <Button color="secondary" onClick={this.toggleSuccess}>Cancelar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
                    className={'modal-warning ' + this.props.className}>
                    <ModalHeader toggle={this.toggleWarning}>Campos vacios</ModalHeader>
                    <ModalBody>
                        No selecciono ningun código de insumo o medicamento, o no selecciono la cantidad a retirar.
                </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleWarning}>cerrar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.danger} toggle={this.toggleDanger}
                    className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDanger}>Advertencia</ModalHeader>
                    <ModalBody>
                        No puede Retirar 0 (cero) productos.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleDanger}>Cerrar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.warning2} toggle={this.toggleWarning2}
                    className={'modal-warning ' + this.props.className}>
                    <ModalHeader toggle={this.toggleWarning2}>Campos vacios</ModalHeader>
                    <ModalBody>
                        La cantidad a retirar es incorrecta.
                </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleWarning2}>cerrar</Button>
                    </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.warning3} toggle={this.toggleWarning3}
                    className={'modal-warning ' + this.props.className}>
                    <ModalHeader toggle={this.toggleWarning3}>Campos vacios</ModalHeader>
                    <ModalBody>
                        Verifique que no haya campos vacion.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleWarning3}>Cerrar</Button>
                    </ModalFooter>
                </Modal>
                {/*MODALES DE NOTIFICACION SI SE REGISTRO CORRECTAMENTE LOS DATOS*/}
                <Modal isOpen={this.state.dangerNoti} toggle={this.toggleDangerNoti}
                    className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDangerNoti}><strong>Advertencia</strong></ModalHeader>
                    <ModalBody>
                        Los datos NO fueron registrados correctamente, verifique e intente nuevamente.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleDangerNoti}>Cerrar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.infoNoti} toggle={this.toggleInfoNoti}
                    className={'modal-info ' + this.props.className}>
                    <ModalHeader toggle={this.toggleInfoNoti}><strong>Correcto</strong></ModalHeader>
                    <ModalBody>
                        Los datos fueron registrados correctamente.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleInfoNoti}>Cerrar</Button>
                    </ModalFooter>
                </Modal>


            </Col>


        );
    }
}

export default RegistroSalida;