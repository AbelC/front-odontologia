import React from 'react';
import ReactDOM from 'react-dom';
import RegistroSalida from './RegistroSalida';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RegistroSalida />, div);
  ReactDOM.unmountComponentAtNode(div);
});
