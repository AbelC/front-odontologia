import React, { Component } from 'react';
import { Alert } from 'reactstrap';

class Error extends Component {



    constructor(props) {
        super(props);

        this.state = {
            visible: true
        };

        this.onDismiss = this.onDismiss.bind(this);
    }

    onDismiss() {
        this.setState({ visible: false });
    }


    render() {
        return (
            <div className="animated fadeIn">
            <br/>
                <Alert color="danger" isOpen={this.state.visible} toggle={this.onDismiss}>
                    Algo salio mal.
                </Alert>
            </div>
        );
    }
}

export default Error;
