import React, { Component } from 'react';
import { Alert } from 'reactstrap';

class Exito extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: true
        };

        this.onDismiss = this.onDismiss.bind(this);
    }

    onDismiss() {
        this.setState({ visible: false });
    }


    render() {
        return (
            <div className="animated fadeIn">
              <br/>
                <Alert color="success" isOpen={this.state.visible} toggle={this.onDismiss}>
                    Acción realizada con EXITO !!!
                </Alert>
            </div>
        );
    }
}

export default Exito;
