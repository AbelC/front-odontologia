import React, { Component } from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, InputGroup, InputGroupAddon, InputGroupText, Row, Alert } from 'reactstrap';
const usersData = [
  { id: 0, name: 'Fernando Castro', email: 'fernando@promes.com', password: 'promes', role: 'Jefe de Unidad' },
  { id: 1, name: 'Luis Pereira', email: 'luis@promes.com', password: 'promes', role: 'Odontologo' },
  { id: 2, name: 'Vivian Aguilar', email: 'vivian@promes.com', password: 'promes', role: 'Odontologo' },
  { id: 3, name: 'Lilian Cusicanqui', email: 'lilian@promes.com', password: 'promes', role: 'Odontologo' },
  { id: 4, name: 'Zulma Peña', email: 'zulma@promes.com', password: 'promes', role: 'Odontologo' },

]
class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      idMedico: 0,
      nomAp: '',
      cargo: '',
      visible: false
    }
    this.iniciaSesion = this.iniciaSesion.bind(this);
    this.getUser = this.getUser.bind(this);
    this.guardaLocalStorage = this.guardaLocalStorage.bind(this);
    this.onDismiss = this.onDismiss.bind(this);

  }
  onDismiss() {
    this.setState({ visible: !this.state.visible });
  }
  getUser() {
    var usuario = this.user.value;
    var pass = this.pass.value;
    console.log(usuario, ' - ', pass, ' ')
    fetch(process.env.REACT_APP_API_URL + 'odontologia/persona/v1/user/' + usuario + '/')
      .then((ResponseS) => ResponseS.json())
      .then(findresponseUser => {
        console.log('findresponseUser: ', findresponseUser[0])
        if (findresponseUser.length !== 0) {
          this.setState({
            idMedico: findresponseUser[0].id,
            nomAp: findresponseUser[0].nombres + ' ' + findresponseUser[0].apellido_paterno,
            cargo: findresponseUser[0].cargo,
          })
          this.guardaLocalStorage();

        } else {
          this.setState({ visible: !this.state.visible });
          console.log('USUARIO O CONTRASEÑA INCORRECTA')
        }


      });
  }
  iniciaSesion() {
    var user = this.user.value;
    var pass = this.pass.value;
    console.log(user, ' - ', pass, ' ', usersData)
  }

  guardaLocalStorage() {
    /*console.log('idMedico: ', this.state.idMedico)
    console.log('nomAp: ', this.state.nomAp)
    console.log('cargo: ', this.state.cargo)*/

    localStorage.setItem("idMedico", this.state.idMedico)
    localStorage.setItem("nombre", this.state.nomAp)
    localStorage.setItem("cargo", this.state.cargo)
    window.location = '#/dashboard';
  }
  render() {



    return (
      <div className="app flex-row align-items-center">

        <Container>
          <Row>
            <Col>
              <strong><h3 style={{ textAlign: 'center' }} >SI-GESHO-PROMES</h3></strong>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col md="5">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <h4>INICIO SESIÓN</h4>
                    <p className="text-muted">Ingrese su usuario</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <input className="form-control" type="text" placeholder="Usuario" ref={user => this.user = user} />
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <input className="form-control" type="password" ref={pass => this.pass = pass} placeholder="Password" />
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" onClick={this.getUser} className="px-4">Login</Button>
                      </Col>
                    </Row>
                    <br/>
                    <Alert color="danger" isOpen={this.state.visible} toggle={this.onDismiss}>
                        USUARIO O CONTRASEÑA INCORRECTA
                    </Alert>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
/*
href="http://localhost:3000/#/dashboard" 
 <Button href="http://localhost:3000/#/dashboard" color="primary"  ref={pass => this.pass = pass} className="px-4">Login</Button>
*/
export default Login;
