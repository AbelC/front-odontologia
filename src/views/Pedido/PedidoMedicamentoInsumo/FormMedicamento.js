import React, { Component } from 'react';
import { Button, Col, Form, FormGroup, Label, Input, Row } from 'reactstrap';
import Search from 'react-search-box'

class FormMedicamento extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.toggleFade = this.toggleFade.bind(this);
        this.state = {
            collapse: true,
            fadeIn: true,
            timeout: 300,
            primary: false,
            show1: false,
            show2: false,

            list: [],
            medicamento: '',
            idInsumo:'',
            descripcion:'',
            unidad:'',
            codigo:'',

        }
        this.saveMaterial = this.saveMaterial.bind(this)
        this.togglePrimary = this.togglePrimary.bind(this)
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }

    toggleFade() {
        this.setState((prevState) => { return { fadeIn: !prevState } });
    }
    async componentDidMount() {
        try {
            /*Return all the medicines*/
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/listaGuarda/');
            const data = await respuesta.json();
            this.setState({
                data
            });
        } catch (e) {
            console.log(e);
        }
    }


    saveMaterial() {
        let data = {
            unidad: this.unidad.value,
            detalle: this.detalle.value
        }
        try {
            fetch('http://192.168.99.100:8000/enfermeria/pedido_materiales/v1/crearm/', {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(resp => {
                    if (resp.status === 201) {
                        this.setState({
                            show1: !this.state.show1
                        })
                        this.componentDidMount()
                    }
                    else {
                        this.setState({
                            show2: !this.state.show2
                        })
                    }
                })
        } catch (e) {
            console.log(e);
        }
    }
    togglePrimary() {
        this.setState({
            primary: !this.state.primary,
            show1: false,
            show2: false
        })
    }

    /*handleChange(value) {
        this.state.data.map((item, indice) => {
            if (value === item.nombre) {
                //Return the medicine with id x 
                fetch('http://192.168.99.100:8000/enfermeria/farmacia_auxiliar/v1/listam/' + item.id + '/')
                    .then((Response) => Response.json())
                    .then((findresponse) => {
                        this.setState({
                            list: findresponse,
                            medicamento: value
                        })
                    });
            }
            return 0;
        })
    }*/



    handleChangeBindCard(value) {
        //console.log('value: ', value);
        var idInsumo = 0;
        var descripcion = '';
        var unidad = '';
        var codigo = '';
        for (let i = 0; i < this.state.data.length; i++) {
            if (this.state.data[i].descripcion === value) {
                //console.log('idBindCard: ', this.state.data[i].id, ' codigo: ', this.state.data[i].codigo);
                idInsumo = this.state.data[i].id;
                descripcion = this.state.data[i].descripcion;
                unidad = this.state.data[i].unidad;
                codigo = this.state.data[i].codigo;
                break;
            }
        }
    
        if (idInsumo !== 0) {
            this.setState({ 
                idInsumo,
                descripcion,
                unidad,
                codigo,
            })
        } else {
            console.log('no')
        }

    }

    render() {

        return (
            <Form onSubmit={this.props.addMedicamento}>
                <Row>
                    <Col sm="5">
                        <FormGroup>
                            <Label htmlFor="detalle">Detalle</Label>
                            <Search
                                data={this.state.data}
                                onChange={this.handleChangeBindCard.bind(this)}
                                placeholder="BUSCAR"
                                class="search-class"
                                searchKey="descripcion"
                            />
                            <Input type="" id="detalle" hidden name="detalle" defaultValue={(this.state.idInsumo !== '') ? this.state.idInsumo+ '-' + this.state.descripcion : ""} />
                        </FormGroup>
                    </Col>
                    <Col sm="2">
                        <FormGroup >
                            <Label htmlFor="detalle">Código</Label>
                            <Input type="text" id="codigo" name="codigo" disabled defaultValue={(this.state.codigo !== '') ? this.state.codigo : ""} />
                        </FormGroup>
                    </Col>
                    <Col sm="3">
                        <FormGroup >
                            <Label htmlFor="unidad">Unidad</Label>
                            <Input  type="text" id="unidad" name="unidad" disabled defaultValue={(this.state.unidad !== '') ? this.state.unidad : ""} />
                        </FormGroup>
                    </Col>
                    <Col sm="2">
                        <FormGroup>
                            <Label htmlFor="cantidad">Cantidad</Label>
                            <Input valid type="number" id="cantidad" name="cantidad" />
                        </FormGroup>
                    </Col>
                </Row>

                <Row>
                    <Col></Col>
                    <Col><Button block type="submit" color="success">Adicionar</Button></Col>
                    <Col></Col>
                </Row>
                <hr />

            </Form>
        );
    }
}
export default FormMedicamento;


