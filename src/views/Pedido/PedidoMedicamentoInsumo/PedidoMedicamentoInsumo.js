import React, { Component } from 'react';
import {
  Button, Card, CardBody, CardHeader, Col, Table, FormGroup, Label, Row, Input, PaginationItem, PaginationLink, Pagination, Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap';
import FormMedicamento from './FormMedicamento';

class PedidoMedicamentoInsumo extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      medicamentos: [],
      page: 1,
      eachPage: 8,
      info: [],
      primary: false,
      primary2: false,
      primary3: false,
      primary4: false,
      primary5: false,
      message: '',
      flag: false,
      idMedico:localStorage.getItem("idMedico"),
    }

    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.handleMedicamento = this.handleMedicamento.bind(this)
    this.handleClick = this.handleClick.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.togglePrimary2 = this.togglePrimary2.bind(this)
    this.togglePrimary3 = this.togglePrimary3.bind(this)
    this.togglePrimary4 = this.togglePrimary4.bind(this)
    this.togglePrimary5 = this.togglePrimary5.bind(this)
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState } });
  }
  async componentDidMount() {

  }
  handleClick(event) {
    this.setState({
      page: Number(event.target.id)
    });
  }
  togglePrimary() {
    this.setState({
      primary: !this.state.primary,
    });
  }
  togglePrimary2() {
    this.setState({
      primary2: !this.state.primary2,
    });
  }
  togglePrimary3() {
    this.setState({
      primary3: !this.state.primary3,
    });
  }
  togglePrimary4() {
    this.setState({
      primary4: !this.state.primary4,
    });
    window.location.reload()
  }
  togglePrimary5() {
    this.setState({
      primary5: !this.state.primary5,
    });
    window.location.reload()
  }
  handleMedicamento(e) {
    e.preventDefault();
    /*console.log('e.target: ', e.target);
    console.log('e.target.detalle.value: ', e.target.detalle.value);
    console.log('e.target.codigo.value: ', e.target.codigo.value);
    console.log('e.target.unidad.value : ', e.target.unidad.value );
    console.log('e.target.cantidad.value: ', e.target.cantidad.value)*/


    if (e.target.detalle.value !== '' && e.target.unidad.value !== '' && e.target.cantidad.value !== '' && e.target.codigo.value !== '') {
      if (e.target.cantidad.value > 0) {
        let obj = {
          detalle: e.target.detalle.value,
          unidad: e.target.unidad.value,
          cantidad: e.target.cantidad.value,
          codigo: e.target.codigo.value,
        }
        this.setState({
          medicamentos: this.state.medicamentos.concat([obj]),
          flag: true
        })
        e.target.reset();
      }
      else {
        this.setState({
          primary: !this.state.primary,
          message: 'La cantidad debe ser mayor a cero.'
        });
      }
    }
    else {
      this.setState({
        primary: !this.state.primary,
        message: 'Complete los espacios en blanco.'
      });
    }
  }
  handleOnSubmit(e) {
    e.preventDefault()
    /*Start: Calculate the date and time currect of bolivia*/
    var options = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    };
    var d = new Date();
    var time = d.toLocaleString('es-bo', options)

    var date = time.split(' ')[0].split('/')
    time = time.split(' ')[1].split(':')
    var dateS = date[2] + '-' + date[1] + '-' + date[0];
    var timeS = time[0] + ':' + time[1] + ':' + time[2];

    //var dateFull = new Date(date[2], date[1], date[0], time[0], time[1], time[2]);
    //var dateS = dateFull.getFullYear() + '-' + dateFull.getMonth() + '-' + dateFull.getDate()  /*Date to send */
    //var timeS = dateFull.getHours() + ':' + dateFull.getMinutes() + ':' + dateFull.getSeconds()  /**Time to send */
    /*End: Calculate the date and time currect of bolivia */
    this.state.medicamentos.map(item => {
      let data = {
        fechaPedido: dateS,
        hora: timeS,
        cantidadEntregada: 0,
        cantidadSolicitada: item.cantidad,
        idMedicamentoInsumo: item.detalle.split('-')[0],
        medicoRegistro: this.state.idMedico,////arreglar con el id del login
        nroPedido:0
      }
      try {
        fetch(process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/NuevoPedido/', {
          method: 'POST',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          }
        })
          .then(resp => {
            if (resp.status === 201) {
              //console.log('Success', resp.json());
              this.setState({
                primary4: true
              })
            }
            else {
              this.setState({
                primary5: true
              })
            }
          })

      } catch (e) {
        console.log(e);
      }
      return 0;
    })


  }
  deleteItem(item) {
    const medicamentos2 = this.state.medicamentos.filter(medicamentos => {
      return medicamentos !== item
    })
    if (this.state.medicamentos.length !== 1) {
      this.setState({
        medicamentos: [...medicamentos2]
      })
    }
    else {
      this.setState({
        medicamentos: [...medicamentos2],
        flag: false,
      })
    }
  }

  render() {
    //console.log('vec_med: ', this.state.medicamentos)
    /**************BEGIN PAGINACION (medicamentos)***********/
    const { page, eachPage } = this.state;

    const last = page * eachPage;
    const first = last - eachPage;
    const listaMed = this.state.medicamentos.slice(first, last);

    //lista
    const medicamentos = listaMed.map((item, indice) => {
      return (
        <tr key={indice}>
          <td>{indice + 1}</td>
          <td>{item.codigo}</td>
          <td>{item.cantidad}</td>
          <td>{item.unidad}</td>
          <td>{item.detalle.split('-')[1]}</td>
          <td>
            <Button onClick={(e) => this.deleteItem(item)} color="danger">Descartar</Button>
          </td>
        </tr>
      )
    })

    //para los numeros
    const pages = [];
    for (let i = 1; i <= Math.ceil(this.state.medicamentos.length / eachPage); i++) {
      pages.push(i);
    }

    const renderpages = pages.map(number => {
      if (number % 2 === 1) {
        return (
          <PaginationItem key={number} active>
            <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
          </PaginationItem>
        );
      }
      else {
        return (
          <PaginationItem key={number}>
            <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
          </PaginationItem>
        );
      }
    });

    /************END PAGINACION (medicamentos)***********/
    var options2 = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
    }
    var options3 = {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    }
    var d = new Date();
    var showDate = d.toLocaleString('es-bo', options2)  /*Date to show in input */
    var showTime = d.toLocaleString('es-bo', options3)  /*Time to show in input*/
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> <strong>Registro de Pedido de insumos y/o medicamentos</strong>
              </CardHeader>
              <CardBody>
                {/* <Form method="post" action=""> */}
                {/* <FormGroup row>
                      <Col md="9"></Col>
                      <Col md="2">
                        <Label htmlFor="nroPedido">Número de pedido</Label>
                      </Col>
                      <Col xs="12" md="1">
                        <input type=" " id="nroPedido" name="nroPedido" className="form-control"/>
                      </Col>
                    </FormGroup> */}



                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="unidad">unidad solicitante</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input type="text" id="unidad" name="unidad" placeholder="Enfermería" value="ODONTOLOGIA - PROMES" disabled />
                  </Col>
                  <Col md="1">
                    <Label htmlFor="fecha">Fecha de pedido</Label>
                  </Col>
                  <Col xs="12" md="2">
                    <input type="text" id="fecha" name="fecha" className="form-control" defaultValue={showDate} disabled ref={fecha => this.fecha = fecha} />
                  </Col>
                  <Col md="1">
                    <Label htmlFor="hora">Hora</Label>
                  </Col>
                  <Col xs="12" md="2">
                    <input type="time" id="hora" name="hora" className="form-control" defaultValue={showTime} disabled ref={hora => this.hora = hora} />
                  </Col>
                </FormGroup>

                {/* </Form> */}
                <hr />

                <FormMedicamento addMedicamento={this.handleMedicamento.bind(this)} />




              </CardBody>
            </Card>

          </Col>
        </Row>
        <Row>
          <Col sm="12" md={{ size: 8, offset: 2 }}>
            <Card>
              <CardBody>
                
                <Table responsive bordered size="sm">
                  <thead>
                    <tr>
                      <th>Nro</th>
                      <th>codigo</th>
                      <th>cantidad</th>
                      <th>unidad</th>
                      <th>detalle</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                  <tbody>
                    {medicamentos}
                  </tbody>
                </Table>
                <Pagination>
                  {renderpages}
                </Pagination>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col></Col>
          <Col>
            <Button color="primary" block onClick={this.togglePrimary2} className="mr-1" disabled={!this.state.flag}>Registrar pedido</Button>
          </Col>
          <Col></Col>
        </Row>

        <FormGroup row>
          <Col md="5"></Col>
          <Col xs="12" md="4">
            <Modal isOpen={this.state.primary2} toggle={this.togglePrimary2} className={'modal-primary ' + this.props.className}>
              <ModalHeader toggle={this.togglePrimary2}><strong>Confirmación</strong></ModalHeader>
              <ModalBody>
                ¿Desea registrar los datos?
                          </ModalBody>
              <ModalFooter>
                <Button onClick={this.handleOnSubmit} type="submit" color="primary">Si</Button>{' '}
                <Button color="secondary" onClick={this.togglePrimary2}>No</Button>
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.primary3} toggle={this.togglePrimary3} className={'modal-warning ' + this.props.className}>
              <ModalHeader toggle={this.togglePrimary3}><strong>Advertencia</strong></ModalHeader>
              <ModalBody>
                Complete los espacios en blanco.
                          </ModalBody>
              <ModalFooter>
                <Button color="warning" onClick={this.togglePrimary3}>Aceptar</Button>
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.primary4} toggle={this.togglePrimary4} className={'modal-success ' + this.props.className}>
              <ModalHeader toggle={this.togglePrimary4}><strong>Correcto</strong></ModalHeader>
              <ModalBody>
                Los datos se registraron correctamente.
                          </ModalBody>
              <ModalFooter>
                <Button color="success" onClick={this.togglePrimary4}>Aceptar</Button>
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.primary5} toggle={this.togglePrimary5} className={'modal-danger ' + this.props.className}>
              <ModalHeader toggle={this.togglePrimary5}><strong>Correcto</strong></ModalHeader>
              <ModalBody>
                Los datos no se registraron. Por favor intente de nuevo.
                          </ModalBody>
              <ModalFooter>
                <Button color="danger" onClick={this.togglePrimary5}>Aceptar</Button>
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-warning ' + this.props.className}>
              <ModalHeader toggle={this.togglePrimary}><strong>Advertencia</strong></ModalHeader>
              <ModalBody>
                {this.state.message}
              </ModalBody>
              <ModalFooter>
                <Button color="warning" onClick={this.togglePrimary}>Aceptar</Button>
              </ModalFooter>
            </Modal>
          </Col>
        </FormGroup>
      </div>
    );
  }
}
export default PedidoMedicamentoInsumo;

