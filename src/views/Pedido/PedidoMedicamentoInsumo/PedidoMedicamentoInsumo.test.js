import React from 'react';
import ReactDOM from 'react-dom';
import PedidoMedicamentoInsumo from './PedidoMedicamentoInsumo';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PedidoMedicamentoInsumo />, div);
  ReactDOM.unmountComponentAtNode(div);
});
