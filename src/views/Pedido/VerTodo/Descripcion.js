import React, { Component } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row, Table, Modal, ModalBody, ModalFooter, ModalHeader, Alert
} from 'reactstrap';
import Print2 from './Print2'
import RegistroEntrada2 from './RegistroEntrada2'
class Descripcion extends Component {
  constructor(props) {
    super(props);

    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      details: [],
      editMode: false,
      editMode2: false,
      editModeNroPedido: null,
      primary: false,
      swSaveInsumo: false,
      danger: false,

      nroPedido: 0,
      idChange: 0,

      cantidadSolicitada: 0,
      cantidadEntregada: 0,
      cantidadEntregadaSent: 0,
      fechaPedido: '',
      hora: '',
      idMedicamentoInsumo: 0,
      nomInsumo: '',
      codigoInsumo: '',
      medicoRegistro: 0,

      visible: false,
      msj: '',
      colorMensaje: '',
      msj2: '',
      swEntrada: false,
    };
    this.registerNroPedido = this.registerNroPedido.bind(this);
    this.togglePrimary = this.togglePrimary.bind(this);
    this.register = this.register.bind(this);
    this.register2 = this.register2.bind(this);
    this.YesSaveInsumo = this.YesSaveInsumo.bind(this);
    this.toggleDanger = this.toggleDanger.bind(this);
    this.saveNroPedido = this.saveNroPedido.bind(this);
    this.saveCantEntregada = this.saveCantEntregada.bind(this);
    this.onDismiss = this.onDismiss.bind(this);
  }
  toggleDanger() {
    this.setState({
      danger: !this.state.danger,
      primary:false
    });
  }

  onDismiss() {
    this.setState({ visible: false });
  }

  YesSaveInsumo() {
    this.setState({ swSaveInsumo: true });
  }

  togglePrimary() {
    this.setState({
      primary: !this.state.primary,
    });
  }
  async componentDidMount() {
    const json = await fetch(process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/pedidoFH/' + this.props.fechaPedido + '/' + this.props.hora + '/')
    //console.log('**')
    const j = await json.json()
    this.setState({
      details: j
    })
  }

  register() {
    this.setState({
      editMode: !this.state.editMode
    })
  }
  register2() {
    this.setState({
      editModeNroPedido: null
    })
  }
  registerNroPedido() {
    this.setState({
      editModeNroPedido: !this.state.editModeNroPedido
    })
  }
  saveCantEntregada() {
    if (this.cant.value >= 0) {
      if (this.cant.value <= this.state.cantidadSolicitada) {
        try {
          let data = {
            //idChange: this.state.idChange,
            cantidadSolicitada: this.state.cantidadSolicitada,
            cantidadEntregada: this.cant.value,
            fechaPedido: this.state.fechaPedido,
            hora: this.state.hora,
            idMedicamentoInsumo: this.state.idMedicamentoInsumo,
            medicoRegistro: this.state.medicoRegistro,
            nroPedido: this.state.nroPedido,
          }
          //.then(() => this.componentDidMount())

          //console.log('dataCan: ', data)
          fetch(process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/editaPedido/' + this.state.idChange + '/', {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json'
            }
          }).then(resp => {
            if (resp.status === 200) {
              //console.log('Success 201', resp.json(), ' resp.status:', resp.status );
              this.componentDidMount();
              this.setState({
                editMode: false,
                swSaveInsumo: false,
                visible: true,
                msj: 'La cantidad entregada se guardo correctamente.',
                colorMensaje: 'success',
                cantidadEntregadaSent: this.cant.value,
                swEntrada: true
              })
            }
            else {
              this.setState({
                editMode: false,
                swSaveInsumo: false,
                visible: true,
                msj: 'Error, la cantidad entregada no se guardo.',
                colorMensaje: 'danger',
                swEntrada: false
                
              })
            }
          })

        } catch (e) {
          console.log(e);
        }
      } else {
        this.setState({
          msj2: 'Cantidad entregada debe ser menor o igual a la cantidad solicitada',
          primary: false,
          danger: true,
          swEntrada: false,
          visible: false,      

        })
      }
    } else {
      this.setState({
        msj2: 'La Cantidad entregada no es valida',
        primary: false,
        danger: true,    
        swEntrada: false,
        visible: false,  
      })
    }


  }

  saveNroPedido() {
    this.setState({
      editModeNroPedido: false,
      nroPedido: this.nroP.value,
    });
    //console.log('n:', this.state.details.length)
    var vec = [];
    vec = this.state.details;
    var n = vec.length;
    for (let i = 0; i < n; i++) {
      if (i === n - 1) {
        //console.log('this: ', vec[i].idMedicamentoInsumo.codigo, ' i: ', i, 'did mount')
        try {
          let data = {
            cantidadSolicitada: vec[i].cantidadSolicitada,
            cantidadEntregada: vec[i].cantidadEntregada,
            fechaPedido: vec[i].fechaPedido,
            hora: vec[i].hora,
            idMedicamentoInsumo: vec[i].idMedicamentoInsumo.id,
            medicoRegistro: vec[i].medicoRegistro,
            nroPedido: this.nroP.value,
          }
          //console.log('data: ', data,' vec[i]: ',vec[i].id,    ' i:', i, 'did mount', )
          fetch(process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/editaPedido/' + vec[i].id + '/', {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json'
            }
          }).then(() => this.componentDidMount())
        } catch (e) {
          console.log(e);
        }

      } else {
        //console.log('this: ', vec[i].idMedicamentoInsumo.codigo, ' i: ', i);
        try {
          let data = {
            cantidadSolicitada: vec[i].cantidadSolicitada,
            cantidadEntregada: vec[i].cantidadEntregada,
            fechaPedido: vec[i].fechaPedido,
            hora: vec[i].hora,
            idMedicamentoInsumo: vec[i].idMedicamentoInsumo.id,
            medicoRegistro: vec[i].medicoRegistro,
            nroPedido: this.nroP.value,
          }
          //console.log('data: ', data, ' vec[i]: ',vec[i].id , ' i: ', i)
          fetch(process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/editaPedido/' + vec[i].id + '/', {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json'
            }

          }).then(() => this.componentDidMount())
        } catch (e) {
          console.log(e);
        }

      }
    }
  }
  render() {
    //console.log(' primary: ', this.state.primary)
    console.log('props desc: ', this.props)
    var c = 0
    const description = this.state.details.map((item, index) => {
      c = c + 1
      if (c === 1) {
        return (
          <div key={index}>
            <FormGroup row>
              <Col md="2">
                <Label htmlFor="unidad">Unidad solicitante</Label>
              </Col>
              <Col xs="12" md="4">
                <Input type="text" id="unidad" name="unidad" value="ODONTOLOGIA PROMES" disabled />
              </Col>

              <Col md="2">
                <Label htmlFor="unidad">N° de Pedido*</Label>
              </Col>
              <Col xs="12" md="4">
                {
                  (this.state.editModeNroPedido === true) ?
                    <div>
                      <input type="number" id="nroPedido" name="nroPedido" className="form-control" defaultValue={item.nroPedido} ref={nroP => this.nroP = nroP} />
                      <Button color="primary" size="sm" onClick={this.saveNroPedido}>Guardar</Button>
                      <Button color="danger" size="sm" onClick={this.register2}>Cancelar</Button>
                    </div> : (this.state.editModeNroPedido === false || item.nroPedido !== 0) ? <Input type="text" id="nroPedido" valid name="nroPedido" value={item.nroPedido} disabled /> :
                      <div onDoubleClick={this.registerNroPedido}>
                        {item.nroPedido}
                      </div>
                }
              </Col>

            </FormGroup>

            <FormGroup row>
              <Col md="2">
                <Label htmlFor="fecha">Fecha</Label>
              </Col>
              <Col xs="12" md="4">
                <Input type="text" id="fecha" name="fecha" defaultValue={item.fechaPedido} disabled />
              </Col>
              <Col md="2">
                <Label htmlFor="hora">Hora</Label>
              </Col>
              <Col xs="12" md="4">
                <Input type="text" id="hora" name="hora" defaultValue={item.hora} disabled />
              </Col>
            </FormGroup>
          </div >
        )
      }
      else {
        return (
          <div key={index} hidden></div>
        )
      }
    });
    //this.setState({
    //idChange: item.id, cantidadSolicitada: item.cantidadSolicitada, cantidadEntregada: this.cant.value, fechaPedido: item.fechaPedido, hora: item.hora,
    //idMedicamentoInsumo: item.idMedicamentoInsumo.id, medicoRegistro: item.medicoRegistro, nroPedido: this.state.nroPedido
    //});
    const table = this.state.details.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.idMedicamentoInsumo.codigo}</td>
          <td>{item.idMedicamentoInsumo.id}</td>
          <td>{item.idMedicamentoInsumo.unidad}</td>
          <td>{item.idMedicamentoInsumo.descripcion}</td>
          <td>{item.cantidadSolicitada}</td>
          <td>
            {this.state.editMode && this.state.idChange === item.id  && item.cantidadEntregada === 0 ?
              <div>
                <input type="number" id="cantidad" name="cantidad" className="form-control" defaultValue={item.cantidadEntregada} ref={cant => this.cant = cant} />
                <Button size="sm" color="primary" onClick={() => { this.saveCantEntregada(); this.togglePrimary() }} >Guardar</Button>
                <Button size="sm" color="danger" onClick={this.register}>Cancelar</Button>
              </div> :
              <div onDoubleClick={() => {
                this.register(); this.setState({
                  idChange: item.id, cantidadSolicitada: item.cantidadSolicitada, cantidadEntregada: this.state.cantidadEntregada, fechaPedido: item.fechaPedido, hora: item.hora,
                  idMedicamentoInsumo: item.idMedicamentoInsumo.id, medicoRegistro: item.medicoRegistro, nroPedido: item.nroPedido, nomInsumo: item.idMedicamentoInsumo.descripcion,
                  codigoInsumo: item.idMedicamentoInsumo.codigo
                });
              }}>
                {item.cantidadEntregada}
              </div>
            }
          </td>
        </tr>
      )
    });
    /* console.log('*****************************************************')
     console.log('cantidadSolicitada: ', this.state.cantidadSolicitada)
     console.log('cantidadEntregada: ', this.state.cantidadEntregada)
     console.log('fechaPedido: ', this.state.fechaPedido)
     console.log('hora: ', this.state.hora)
     console.log('idMedicamentoInsumo: ', this.state.idMedicamentoInsumo)
     console.log('nomInsumo: ', this.state.nomInsumo)
     console.log('codigoInsumo: ', this.state.codigoInsumo)
     console.log('medicoRegistro: ', this.state.medicoRegistro)
     console.log('nroPedido: ', this.state.nroPedido)
     console.log('*****************************************************')*/

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> <strong>Completar pedido {' ' + this.props.fechaPedido + ' - ' + this.props.hora}</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  {description}
                  <hr />
                  <Row>
                    <Col xs="12" lg="12">
                      <Table responsive hover bordered size="sm">
                        <thead>
                          <tr>
                            <th>idPedido</th>
                            <th>Código</th>
                            <th>IDmedi</th>
                            <th>Unidad</th>
                            <th>Detalle</th>
                            <th>Cantidad solicitada</th>
                            <th>Cantidad entregada*</th>
                          </tr>
                        </thead>
                        <tbody>
                          {table}
                        </tbody>
                      </Table>
                    </Col>
                  </Row>
                </Form>
                <Row>
                  <Col md="4"></Col>
                  <Col md="4">        
                    <Print2 fechaPedido={this.props.fechaPedido} hora={this.props.hora} /> 
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal isOpen={this.state.primary} toggle={this.togglePrimary}
          className={'modal-primary ' + this.props.className}>
          <ModalHeader toggle={this.togglePrimary}>Modal title   {'idChange: ' + this.state.idChange}</ModalHeader>
          <ModalBody>
            <Alert color={this.state.colorMensaje} isOpen={this.state.visible} toggle={this.onDismiss}>
              {this.state.msj}
            </Alert>
            {
              (this.state.swEntrada === true) ? <div>{
                (this.state.swSaveInsumo === false) ?
                  <Alert color="warning">
                    ¿Desea registrar la entrada de este inusumo Ahora? <br />
                    <Button onClick={this.YesSaveInsumo} color="link">Si</Button>
                    <Button onClick={this.togglePrimary} color="link">No</Button>
                    <br /> Podra realizar este registro luego en Registro Entrada.
                  </Alert> : <RegistroEntrada2
                    idMedicamentoInsumo={this.state.idMedicamentoInsumo} nomInsumo={this.state.nomInsumo}
                    codigoInsumo={this.state.codigoInsumo}
                    nroPedido={this.state.nroPedido} cantidadEntregada={this.state.cantidadEntregadaSent}
                  />
              }</div> : null
            }



          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.togglePrimary}>Cerrar</Button>{' '}
          </ModalFooter>
        </Modal>
        <Modal isOpen={this.state.danger} toggle={this.toggleDanger}
          className={'modal-danger ' + this.props.className}>
          <ModalHeader toggle={this.toggleDanger}>Advertencia</ModalHeader>
          <ModalBody>
            {this.state.msj2}
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={this.toggleDanger}>cerrar</Button>{' '}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default Descripcion;
