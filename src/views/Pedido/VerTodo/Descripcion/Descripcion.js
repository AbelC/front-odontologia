import React, { Component } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row, Pagination, Table
} from 'reactstrap';
import Print from '../Print'
class Descripcion extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      details: [],
      list: [],
      editMode: false,
      idChange: 0,
      cantidad_solicitada: 0,
      cantidad_entregada: 0,
      fecha: '',
      hora: '',
      nombre: '',
      nro: 0,
      material: 0
    };
    this.register = this.register.bind(this)
    this.save = this.save.bind(this)
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }

  async componentDidMount (){
    const json = await fetch('http://192.168.99.100:8000/enfermeria/pedido_materiales/v1/pedidolista/'+this.props.nro+'/')
    const j = await json.json()
    this.setState({
      details: j
    })

    const json2 = await fetch('http://192.168.99.100:8000/enfermeria/pedido_materiales/v1/pedidolistat/'+this.props.nro+'/')
    const j2 = await json2.json()
    this.setState({
      list: j2
    })
  }

  register(){
    this.setState({
      editMode: !this.state.editMode
    })
  }
  save(){
    this.setState({
      editMode: false
    })
    try {
        let data = {
          cantidad_solicitada: this.state.cantidad_solicitada,
          cantidad_entregada: this.cant.value,
          fecha: this.state.fecha,
          hora: this.state.hora,
          nombre: this.state.nombre,
          nro: this.state.nro,
          material: this.state.material
        }
        fetch('http://192.168.99.100:8000/enfermeria/pedido_materiales/v1/actualizap/'+this.state.idChange+'/', {
          method: 'PUT',
          body: JSON.stringify(data),
          headers:{
            'Content-Type': 'application/json'
          }
        })
        .then(() => this.componentDidMount())
    }catch (e) {
      console.log(e);
    }
  }
  render() {
    const description = this.state.details.map((item, index) => {
      return (
        <div key={index}>
          <FormGroup row>
            <Col md="6"></Col>
            <Col md="3">
              <Label htmlFor="pedido">No. pedido</Label>
            </Col>
            <Col xs="12" md="3">
              <Input type="text" id="pedido" name="pedido" defaultValue={item.nro} disabled/>
            </Col>
          </FormGroup>
          <FormGroup row>
            <Col md="4">
              <Label htmlFor="unidad">Unidad solicitante</Label>
            </Col>
            <Col xs="12" md="8">
              <Input type="text" id="unidad" name="unidad" placeholder="Enfermería" value="Enfermería" disabled />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Col md="4">
              <Label htmlFor="nombre">Nombre</Label>
            </Col>
            <Col xs="12" md="8">
              <Input type="text" id="nombre" name="nombre" defaultValue={item.nombre.profesion + ' ' + item.nombre.nombres + ' ' + item.nombre.apellido_paterno + ' ' + item.nombre.apellido_materno} disabled/>
              
            </Col>
          </FormGroup>
          <FormGroup row>
            <Col md="2">
              <Label htmlFor="fecha">Fecha</Label>
            </Col>
            <Col xs="12" md="4">
              <Input type="text" id="fecha" name="fecha" defaultValue={item.fecha} disabled/>
            </Col>
            <Col md="2">
              <Label htmlFor="hora">Hora</Label>
            </Col>
            <Col xs="12" md="4">
              <Input type="text" id="hora" name="hora" defaultValue={item.hora} disabled/>
            </Col>
          </FormGroup>
        </div>
      )
    });
    const table = this.state.list.map((item, index) => {
      return(
        <tr key={index}>
          <td>{item.material.unidad}</td>
          <td>{item.material.detalle}</td>
          <td>{item.cantidad_solicitada}</td>
          <td>
            { this.state.editMode && this.state.idChange === item.id ? <div><input type="number" id="cantidad" name="cantidad" className="form-control" defaultValue={item.cantidad_entregada} ref={cant => this.cant = cant}/><Button color="primary" onClick={this.save}>Guardar</Button><Button color="danger" onClick={this.register}>Cancelar</Button></div>:<div onDoubleClick={ () => {this.register(); this.setState({idChange: item.id,cantidad_solicitada: item.cantidad_solicitada, cantidad_entregada: item.cantidad_entregada, fecha: item.fecha, hora: item.hora, nombre: item.nombre, nro: item.nro, material: item.material.id})}}>{item.cantidad_entregada}</div>}
          </td>
        </tr> 
      )
    });
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <h3><i className="fa fa-pencil"></i> <strong>Completar pedido</strong></h3>
              </CardHeader>
              <CardBody>
                <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  {description}    
                  <hr />
                  <Row>
                    <Col xs="12" lg="12">
                      <Table responsive striped>
                        <thead>
                        <tr>
                          <th>Unidad</th>
                          <th>Detalle</th>
                          <th>Cantidad solicitada</th>
                          <th>Cantidad entregada</th>
                        </tr>
                        </thead>
                        <tbody>
                          {table}
                        </tbody>
                      </Table>
                        
                      <Pagination>
                        
                      </Pagination>
                    </Col> 
                  </Row>
                </Form>
                <Row>
                  <Col md="4"></Col>
                  <Col md="4">
                    <Print id={this.props.nro}/>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Descripcion;
