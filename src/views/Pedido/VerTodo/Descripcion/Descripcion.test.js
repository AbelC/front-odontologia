import React from 'react';
import ReactDOM from 'react-dom';
import Descripcion from './Descripcion';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Descripcion />, div);
  ReactDOM.unmountComponentAtNode(div);
});
