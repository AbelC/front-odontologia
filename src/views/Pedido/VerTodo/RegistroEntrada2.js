import React, { Component } from 'react';
//import Search from 'react-search-box';
import { Row, Card, Col, CardBody, CardHeader, Form, FormGroup, Label, Button, Modal, ModalBody, ModalFooter, ModalHeader, Alert } from 'reactstrap';
//import { AppSwitch } from '@coreui/react'
//import Tolerancia_F from './Tolerancia_F';
var option = {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
};
var dd = new Date();
var timeE = dd.toLocaleString('es-bo', option);
var dateE = timeE.split(' ')[0].split('/');
timeE = timeE.split(' ')[1].split(':');

var dateShow = dateE[0] + '/' + dateE[1] + '/' + dateE[2];


var url = process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/registroEntrada/';
class RegistroEntrada2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            success: false,

            bindCard_v: [],
            insumo: 0,
            warning: false,
            warning2: false,
            colorM: '',
            mensaje: '',
            swSent: false,
        }
        this.toggleSuccess = this.toggleSuccess.bind(this);

        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.toggleWarning = this.toggleWarning.bind(this);
        this.toggleWarning2 = this.toggleWarning2.bind(this);
    }

    toggleWarning() {
        this.setState({
            warning: !this.state.warning,
        });
    }
    toggleWarning2() {
        this.setState({
            warning2: !this.state.warning2,
        });
    }

    toggleSuccess() {
        if (this.props.idMedicamentoInsumo !== 0 && this.props.nroPedido !== 0 && this.props.cantidadEntregada !== 0 && this.enfermera.value !== '') {
            this.setState({
                success: !this.state.success,
            });
        } else {
            this.setState({
                warning: !this.state.warning,
            });
        }


    }
    async componentDidMount() { //Lista de diagnosticos
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/listaGuarda/');
            const bindCard_v = await respuesta.json();
            this.setState({
                bindCard_v,
            });

        } catch (e) {
            console.log(e);
        }
    }

    /* handleChangeBindCard(value) {
         //console.log(value);
         this.setState({ insumo: value })
     }*/




    handleChangeBindCard(value) {
        //console.log('value: ', value);
        var idInsumo = 0;
        for (let i = 0; i < this.state.bindCard_v.length; i++) {
            if (this.state.bindCard_v[i].codigo === value) {
                //console.log('idBindCard: ', this.state.bindCard_v[i].id, ' codigo: ', this.state.bindCard_v[i].codigo);
                idInsumo = this.state.bindCard_v[i].id;
                break;
            }
        }
        if (idInsumo !== 0) {
            this.setState({ insumo: idInsumo })
        } else {
            console.log('no')
        }

    }



    handleOnSubmit(e) {
        if (this.props.cantidadEntregada >= 1 && this.props.nroPedido >= 1) {
            console.log('DATOS NO VACIOS')
            var options = {
                day: '2-digit',
                month: '2-digit',
                year: 'numeric',
                hour: '2-digit',
                minute: '2-digit',
                second: '2-digit'
            };
            var d = new Date();
            var time = d.toLocaleString('es-bo', options);
            var date = time.split(' ')[0].split('/');
            time = time.split(' ')[1].split(':');
            var dateS = date[2] + '-' + date[1] + '-' + date[0];
            var timeS = time[0] + ':' + time[1] + ':' + time[2];


            //var dateFull = new Date(date[2], date[1], date[0], time[0], time[1], time[2]);
            //var dateS = dateFull.getFullYear() + '-' + dateFull.getMonth() + '-' + dateFull.getDate();  /*Date to send */
            //var timeS = dateFull.getHours() + ':' + dateFull.getMinutes() + ':' + dateFull.getSeconds();  /**Time to send */


            e.preventDefault()
            let data = {
                fechaRegistro: dateS,
                hora: timeS,
                insumo: this.props.idMedicamentoInsumo,
                nroPedido: this.props.nroPedido,
                cantidad: this.props.cantidadEntregada,
                observaciones: this.observaciones.value,
                enfermera: this.enfermera.value,
            }
            /*.then(res => res.json())
                                .catch(error => console.error('Error:', error))
                                .then(response => console.log('Success:', response)); */
            //console.log('** ', data)
            try {
                fetch(url, {
                    method: 'POST', // or 'PUT'
                    body: JSON.stringify(data), // data can be string or {object}!
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(resp => {
                    if (resp.status === 201) {
                        //console.log('Success 201', resp.json(), ' resp.status:', resp.status );
                        this.setState({
                            swSent: true,
                            colorM: 'success',
                            mensaje: 'Los datos se guardaron correctamente, revisar el listado de insumos y medicamentos.',
                        })
                    } else {
                        this.setState({
                            colorM: 'danger',
                            mensaje: 'Error, los datos NO se guardaron, revice si los datos son correctos.',

                        })
                    }
                })
                //window.location.reload();
                //window.location = '#/Insumos/ListaGeneral';
            } catch (e) {
                console.log(e);
            }
            this.setState({
                success: !this.state.success,
            });

        } else {
            this.setState({
                warning2: !this.state.warning2,
            });
        }



    }
    render() {
        console.log('PROPS: ', this.props)
        return (
            <Col sm="12">
                <Card>
                    <CardHeader>
                        <i className="fa fa-align-justify"></i> <strong>Registro de Entrada de Insumos Medicos</strong>
                        <div className="card-header-actions">
                            <strong className="text-muted"></strong>
                        </div>
                    </CardHeader>
                    <CardBody>
                        <Form>
                            <Row>
                                <Col sm="12">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="fecha" className="mr-sm-2">Insumo / Medicamento</Label>
                                        <input type="text" className="form-control" value={this.props.codigoInsumo + ' - ' + this.props.nomInsumo} disabled required />
                                        <input type="text" hidden className="form-control" value={this.props.idMedicamentoInsumo} disabled required />
                                    </FormGroup>
                                </Col>

                                <Col sm="12">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="fecha" className="mr-sm-2">Fecha Entrada</Label>
                                        <input type="text" className="form-control" value={dateShow} disabled required />
                                    </FormGroup>
                                </Col>
                                <Col sm="12">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="fecha" className="mr-sm-2">N° de pedido</Label>
                                        <input type="number" className="form-control" value={this.props.nroPedido} disabled />
                                    </FormGroup>
                                </Col>

                            </Row>
                            <Row>
                                <Col sm="12">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="fecha" className="mr-sm-2">Cantidad</Label>
                                        <input type="number" className="form-control" value={this.props.cantidadEntregada} disabled />
                                    </FormGroup>
                                </Col>
                                <Col sm="12">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="fecha" className="mr-sm-2">Enfermera</Label>
                                        <input type="text" className="form-control" ref={enfermera => this.enfermera = enfermera} required />
                                    </FormGroup>
                                </Col>
                                <Col sm="12">
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="fecha" className="mr-sm-2">Observaciones</Label>
                                        <textarea type="textarea" className="form-control" defaultValue="AÑADIDO DESDE EL PEDIDO DIRECTAMENTE" ref={observaciones => this.observaciones = observaciones} required />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <br />
                            <Button color="success" block onClick={this.toggleSuccess} className="mr-1">Guardar</Button>
                            {
                                (this.state.colorM === 'success') ?
                                    <Alert color={this.state.colorM}>
                                        {this.state.mensaje}
                                    </Alert> :
                                    <Alert color={this.state.colorM}>
                                        {this.state.mensaje}
                                    </Alert>
                            }





                        </Form>
                    </CardBody>
                </Card>

                <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                    className={'modal-success ' + this.props.className}>
                    <ModalHeader toggle={this.toggleSuccess}>Confirmación</ModalHeader>
                    <ModalBody>
                        ¿Esta seguro de guardar?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={this.handleOnSubmit}>Aceptar</Button>{' '}
                        <Button color="secondary" onClick={this.toggleSuccess}>Cancelar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
                    className={'modal-warning ' + this.props.className}>
                    <ModalHeader toggle={this.toggleWarning}>Campos vacios</ModalHeader>
                    <ModalBody>
                        Verifique que ningun campo este vacio.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleWarning}>Cerrar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.warning2} toggle={this.toggleWarning2}
                    className={'modal-warning ' + this.props.className}>
                    <ModalHeader toggle={this.toggleWarning2}>Alerta</ModalHeader>
                    <ModalBody>
                        Cantidad incorrecta o número de pedido incorrecto, verifique los datos.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleWarning2}>Cerrar</Button>
                    </ModalFooter>
                </Modal>
            </Col>


        );
    }
}

export default RegistroEntrada2;
