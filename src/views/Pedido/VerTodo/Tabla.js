import React, { Component } from 'react';
import { Col, Pagination, PaginationItem, PaginationLink, Table, Button, Modal, ModalHeader, ModalBody, Row} from 'reactstrap';
import Descripcion from './Descripcion/Descripcion'


class Tabla extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.toggleFade = this.toggleFade.bind(this);
        this.state = {
            collapse: true,
            fadeIn: true,
            timeout: 300,
            page: 1,
            eachPage: 10,
            primary: false,
            id: 0
        }
        this.handleClick = this.handleClick.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
        this.changeID = this.changeID.bind(this)
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }

    toggleFade() {
        this.setState((prevState) => { return { fadeIn: !prevState }});
    }
    toggleModal() {
        this.setState({
            primary: !this.state.primary
        });
    }
    
    changeID(idF){
        this.setState({
            id:idF
        });
    }
    handleClick(event) {
        this.setState({
            page: Number(event.target.id)
        });
    }

    render() {
        
        /**************BEGIN PAGINACION (PEDIDO DE MATERIALES)***********/
        const { page, eachPage } = this.state;
            
        const last = page * eachPage;
        const first = last - eachPage;
        const listaPed = this.props.list.slice(first, last);

        //lista
        const ped_materiales = listaPed.map((item, i) => {
            
                return(
                    <tr key={i}>
                        <td># {item.nro}</td>
                        <td>Pedido de materiales</td>
                        <td>
                            <Button block color="info" onClick={() => { this.toggleModal(); this.changeID(item.nro);}  }>Ver</Button>
                            
                        </td>
                        
                    </tr>
                )
            
        });

        //para los numeros
        const pages = [];
        for (let i = 1; i <= Math.ceil(this.props.list.length / eachPage); i++) {
            pages.push(i);
        }

        const renderpages = pages.map(number => {
            if(number % 2 === 1){
                return (
                    <PaginationItem key={number} active>
                        <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                    </PaginationItem>
                );
            }
            else{
                return (
                    <PaginationItem key={number}>
                        <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                    </PaginationItem>
                );
            }
        });

        /************END PAGINACION (PEDIDO DE MATERIALES)***********/
        return (
            <div className="animated fadeIn">
            <Row>
                <Col md="2"></Col>
                <Col md="8">
                    <Table responsive striped>
                    <thead>
                    <tr>
                        <th>Número de pedido</th>
                        <th>Tipo</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                        {ped_materiales}
                    </tbody>
                    </Table>
                    <Pagination>
                        {renderpages}
                    </Pagination>
                    <Modal isOpen={this.state.primary} toggle={this.toggleModal} className={'modal-primary modal-lg'}>
                        <ModalHeader toggle={this.toggleModal}><strong>Pedido de materiales</strong></ModalHeader>
                        <ModalBody>
                            <Descripcion nro={this.state.id}/>
                        </ModalBody>
                    </Modal>
                </Col>
            </Row>
            </div>
        );
    }
}

export default Tabla;
