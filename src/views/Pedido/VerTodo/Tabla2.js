import React, { Component } from 'react';
import { Col, Pagination, PaginationItem, PaginationLink, Table, Button, Modal, ModalHeader, ModalBody, Row } from 'reactstrap';
import Descripcion from './Descripcion'

class Tabla2 extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.toggleFade = this.toggleFade.bind(this);
        this.state = {
            collapse: true,
            fadeIn: true,
            timeout: 300,
            page: 1,
            eachPage: 10,
            primary: false,
            fechaPedido: '',
            hora: ''
        }
        this.handleClick = this.handleClick.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
        this.changeID = this.changeID.bind(this)
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }

    toggleFade() {
        this.setState((prevState) => { return { fadeIn: !prevState } });
    }
    toggleModal() {
        this.setState({
            primary: !this.state.primary
        });
    }

    changeID(fechaPedido, hora) {
        this.setState({
            fechaPedido,
            hora
        });
    }
    handleClick(event) {
        this.setState({
            page: Number(event.target.id)
        });
    }

    render() {

        /**************BEGIN PAGINACION (PEDIDO DE MATERIALES)***********/
        const { page, eachPage } = this.state;

        const last = page * eachPage;
        const first = last - eachPage;
        const listaPed = this.props.list.slice(first, last);

        //lista
        const medicines_orders = listaPed.map((item, index) => {
            return (
                <tr key={index}>
                    <td style={{ 'width': '20px', 'textAlign': 'center' }}>{this.props.list.length - index}</td>                    
                    <td>{item.fechaPedido}</td>
                    <td>{item.hora}</td>
                    <td>Pedido de medicamentos e insumos</td>
                    <td style={{ 'width': '30%', 'textAlign': 'center' }}>                    
                        <Button block outline color="primary" size="sm" onClick={() => { this.toggleModal(); this.changeID(item.fechaPedido, item.hora); }}>Ver</Button>
                    </td>
                </tr>
            )
        });

        //para los numeros
        const pages = [];
        for (let i = 1; i <= Math.ceil(this.props.list.length / eachPage); i++) {
            pages.push(i);
        }

        const renderpages = pages.map(number => {
            if (number % 2 === 1) {
                return (
                    <PaginationItem key={number} active>
                        <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                    </PaginationItem>
                );
            }
            else {
                return (
                    <PaginationItem key={number}>
                        <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                    </PaginationItem>
                );
            }
        });

        /************END PAGINACION (PEDIDO DE MATERIALES)***********/

        return (
            <div className="animated fadeIn">
                <Row>
                    <Col sm="12" md={{ size: 8, offset: 2 }}>
                        <Table responsive hover bordered size="sm">
                            <thead>
                                <tr>
                                    <th>Nro</th>                                    
                                    <th style={{ 'textAlign': 'center' }}>Fecha</th>
                                    <th style={{ 'textAlign': 'center' }}>Hora</th>
                                    <th style={{ 'textAlign': 'center' }}>Tipo</th>
                                    <th style={{ 'textAlign': 'center' }}>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                {medicines_orders}
                            </tbody>
                        </Table>
                        <Pagination>
                            {renderpages}
                        </Pagination>
                    </Col>
                </Row>

                <Modal isOpen={this.state.primary} toggle={this.toggleModal} className={'modal-primary modal-lg'}>
                    <ModalHeader toggle={this.toggleModal}><strong>Pedido de medicamentos e insumos</strong></ModalHeader>
                    <ModalBody>
                        <Descripcion fechaPedido={this.state.fechaPedido} hora={this.state.hora} />
                    </ModalBody>
                </Modal>

            </div>
        );
    }
}

export default Tabla2;
