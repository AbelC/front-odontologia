import React, { Component } from 'react';
import { Alert, Card, CardBody, CardHeader, Col, Row } from 'reactstrap';
//import Tabla from './Tabla'
import Tabla2 from './Tabla2'

class VerTodo extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      show: false,
      show2: false,
      show3: false,
      list: [],
      list2: []
    }
    //this.search = this.search.bind(this)
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState } });
  }

  async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/InsumoMedicamento/v1/listaPedido/');
      const list2 = await respuesta.json();

      if (list2.length === 0) {
        this.setState({
          show3: false,
          show2: true,
          show: false
        })
      } else {
        this.setState({
          show3: true,
          show2: false,
          show: false
        })
      }
      this.setState({
        list2
      });


    } catch (e) {
      console.log(e);
    }
  }

  /*search() {
    if (this.option_ped.value === "0") {
      //Materials orders
      fetch('http://192.168.99.100:8000/enfermeria/pedido_materiales/v1/listap/')
        .then((Response) => Response.json())
        .then((findresponse) => {
          this.setState({
            list: findresponse
          })
          if (this.state.list.length === 0) {
            this.setState({
              show: false,
              show2: true,
              show3: false
            })
          }
          else {
            this.setState({
              show: true,
              show2: false,
              show3: false
            })
          }
        });
    }
    else {
      if (this.option_ped.value === "1") {
        //Medicins orders
        fetch('http://192.168.99.100:8000/enfermeria/farmacia_auxiliar/v1/pedidosmedicamentos/')
          .then(rsp => rsp.json())
          .then(answer => {
            this.setState({
              list2: answer
            })
            if (this.state.list2.length === 0) {
              this.setState({
                show3: false,
                show2: true,
                show: false
              })
            } else {
              this.setState({
                show3: true,
                show2: false,
                show: false
              })
            }
          })
      }
    }
  }*/
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Lista de Pedidos</strong>
              </CardHeader>
              <CardBody>
                <h3><strong><center>Lista de Pedidos</center></strong></h3>
                <hr />
                {/*this.state.show && <Tabla list={this.state.list} />*/}

                {this.state.show3 && <Tabla2 list={this.state.list2} />}
                {this.state.show2 &&
                  <Row>
                    <Col md="4"></Col>
                    <Col md="4">
                      <Alert color="danger">
                        <strong>No se realizo pedidos.</strong>
                      </Alert>
                    </Col>
                  </Row>
                }
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    );
  }
}

export default VerTodo;
