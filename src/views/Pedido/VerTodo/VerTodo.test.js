import React from 'react';
import ReactDOM from 'react-dom';
import VerTodo from './VerTodo';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<VerTodo />, div);
  ReactDOM.unmountComponentAtNode(div);
});
