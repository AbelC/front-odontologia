import React, { Component } from 'react';
import { Button, DropdownItem, Card, CardBody, CardHeader, Col, Alert, Form, FormGroup, Row, Label, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import Search from 'react-search-box';
import ListaAlergia from './ListaAlergia';
import NuevaAlergia from './NuevaAlergia/NuevaAlergia'

var url = process.env.REACT_APP_API_URL + 'odontologia/alergia/v1/regalergia/';
class Alergias extends Component {
  /* Para listar*/
  /*---------------Para registrar*/
  constructor(props) {
    super(props)

    this.state = {
      medico: '',
      afiliado: '',
      fecha_medicacion: '',
      alergia: '',
      alergias: [],
      data: [],
      list: [],
      primary: false,
      warning: false,
      success: false,
      muestraBotonAlergia: false,
      muestraAlerta: false,
      idAlergia: 0,
      sw: false,
      dangerNoti: false,
      infoNoti: false,
      idMedico:localStorage.getItem("idMedico"),
    }
    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.toggleWarning = this.toggleWarning.bind(this);
    this.toggleSuccess = this.toggleSuccess.bind(this);
    this.toggleDangerNoti = this.toggleDangerNoti.bind(this);
    this.toggleInfoNoti = this.toggleInfoNoti.bind(this);
  }

  toggleDangerNoti() {
    this.setState({
      danger: !this.state.danger,
    });
    window.location.reload()
  }

  toggleInfoNoti() {
    this.setState({
      info: !this.state.info,
    });
    window.location.reload()
  }


  toggleSuccess() {

    console.log('this.state.idAlergia == ', this.state.idAlergia)
    if (this.state.idAlergia !== '') {
      console.log('verdaad')
      this.setState({ success: !this.state.success, });

    } else {
      console.log('falso')
      this.setState({ warning: !this.state.warning, });

    }
  }

  togglePrimary() {
    this.setState({
      primary: !this.state.primary,
    });
  }
  toggleWarning() {
    this.setState({
      warning: !this.state.warning,
    });
  }

  /*,,,,*/
  async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      const respuesta = await fetch(process.env.REACT_APP_API_URL + 'odontologia/alergia/v1/alergias/');
      const alergias = await respuesta.json();
      this.setState({
        alergias
      });
    } catch (e) {
      console.log(e);
    }
  }


  /*
    handleChange(value) {
      //console.log('alergia: ', value);
      //console.log("alergias=",this.state.alergias);
  
      this.state.alergias.map(item => {
        if (value === item.descripcion) {
          this.setState({
            idAlergia: item.id,
            sw: true,
          });
          this.handleProcesa;
        } else {
          console.log('no es igual')
        }
        return 0;
      });
  
  
    }*/


  handleChange(value) {
    //console.log('alergia: ', value);
    //console.log("alergias=",this.state.alergias)
    this.state.alergias.map(item => {
      if (value === item.descripcion) {
        // console.log('item:',item.id); 
        fetch(process.env.REACT_APP_API_URL + 'odontologia/alergia/v1/busca/alergiaid/' + item.id + '/')
          .then((Response) => Response.json())
          .then((findresponse) => {
            //console.log("FINDRESPONSE[]=",findresponse[0].pr_consulta)
            //console.log("FINDRESPONSE=",findresponse)

            this.setState({
              list: findresponse,
              idAlergia: item.id

            })
            /// console.log('id de alergia: ' , this.state.idAlergia); 
            // console.log('LIST=',this.state.list.length)   
            //console.log("pr_c=",this.state.pr_c)
            if (this.state.list.length === 0) {

              this.setState({
                muestraAlerta: true
              })
              //console.log('verdad: list - ', this.state.list, ' length: ', this.state.list.length, ' muestraAlerta: ' + this.state.muestraAlerta)

            }
            else {

              this.setState({
                muestraBotonAlergia: true
              })
              //console.log('falso: list - ', this.state.list, ' length: ', this.state.list.length, ' muestraBotonAlergia', this.state.muestraBotonAlergia)
            }


            //console.log('muestraAlerta:', this.state.muestraAlerta, ' muestraBotonAlergia: ', this.state.muestraBotonAlergia)
          });

      } else {
        this.setState({
          muestraBotonAlergia: false
        })
        //console.log('***falso: list - ', this.state.list, ' length: ', this.state.list.length, ' muestraBotonAlergia', this.state.muestraBotonAlergia)
      }
      return 0;
    }
    )
  }


  handleOnSubmit(e) {
    if (this.state.idAlergia !== '') {
      e.preventDefault()
      var options = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
      };
      var d = new Date();
      var time = d.toLocaleString('es-bo', options);
      var date = time.split(' ')[0].split('/');
      time = time.split(' ')[1].split(':');
      var dateS = date[2] + '-' + date[1] + '-' + date[0];
      var timeS = time[0] + ':' + time[1] + ':' + time[2];
      // console.log('dateS: ', dateS)
      //console.log('timeS: ', timeS)
      //var dateFull = new Date(date[2], date[1], date[0], time[0], time[1], time[2]);
      //var dateS = dateFull.getFullYear() + '-' + dateFull.getMonth() + '-' + dateFull.getDate();  /*Date to send */
      //var timeS = dateFull.getHours() + ':' + dateFull.getMinutes() + ':' + dateFull.getSeconds();  /**Time to send */

      let data = {
        medico: this.state.idMedico,
        afiliado: this.props.idA,
        fecha_atencion: dateS,
        hora: timeS,
        alergia: this.state.idAlergia
      }
      //console.log(this.state.data);
      try {
        fetch(url, {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(data), // data can be string or {object}!
          headers: {
            'Content-Type': 'application/json'
          }
        })
          .then(resp => {
            if (resp.status === 201) {
              this.setState({
                infoNoti: true
              })
              //console.log('resp.status', resp.status)
            } else {
              //console.log('resp.status ERROR', resp.status)
              this.setState({
                dangerNoti: true
              })
            }
          })

        /*.then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response));*/
        //window.location.reload();
      } catch (e) {
        console.log(e);
      }
      this.setState({
        success: !this.state.success,
      });
    }
  }


  render() {
    //console.log('propss alergia ',this.props)
    // console.log('el idAlergia:', this.state.idAlergia, ' sw=', this.state.sw)
    return (
      <Col xs="12" sm="6">
        <Card>
          <CardHeader>
            <i className="fa fa-align-justify"></i> <strong>Alergias</strong>
            <div className="card-header-actions">
              <strong className="text-muted">Paciente: {this.props.nombres + ' ' + this.props.apellido_pat + ' ' + this.props.apellido_mat}</strong>
            </div>
          </CardHeader>



          <CardBody>
            <Form>
              <Row>
                <Col sm="9">
                  <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Label for="fecha" className="mr-sm-2">Alergia</Label>
                    <Search
                      data={this.state.alergias}
                      onChange={this.handleChange.bind(this)}
                      placeholder="Buscar Alergia"
                      class="search-class"
                      searchKey="descripcion"
                    />
                  </FormGroup>
                  <Button onClick={this.togglePrimary} color="link">¿Agregar Nuevo?</Button>
                </Col>
                <Col sm="2">
                  <br />
                  <br />
                  {(this.state.muestraBotonAlergia === true) ? <Button color="success" onClick={this.toggleSuccess}>Guardar</Button> : (this.state.muestraAlerta === true) ? <Alert color="danger">No se encuentra en el registro, debe agregar esta alergia a la lista</Alert> : null}
                </Col>
              </Row>

              <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                className={'modal-success ' + this.props.className}>
                <ModalHeader toggle={this.toggleSuccess}>Confirmación</ModalHeader>
                <ModalBody>
                  ¿Está seguro de guardar, los datos introducidos?
                  </ModalBody>
                <ModalFooter>
                  <Button onClick={this.handleOnSubmit} type="submit" color="success">Aceptar</Button>{' '}
                  <Button color="secondary" onClick={this.toggleSuccess}>Cancelar</Button>
                </ModalFooter>
              </Modal>


              <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-primary ' + this.props.className}>
                <ModalHeader toggle={this.togglePrimary}>Nueva alergia</ModalHeader>
                <ModalBody>
                  <NuevaAlergia />
                </ModalBody>
                <ModalFooter>

                  <Button color="secondary" onClick={this.togglePrimary}>Cancelar</Button>
                </ModalFooter>
              </Modal>
            </Form>


            <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
              className={'modal-warning ' + this.props.className}>
              <ModalHeader toggle={this.toggleWarning}>Campos vacios</ModalHeader>
              <ModalBody>
                Llene los campos faltantes, para guardar el registro de alergias del paciente.
                  </ModalBody>
              <ModalFooter>
                <Button color="warning" onClick={this.toggleWarning}>Volver</Button>{' '}

              </ModalFooter>
            </Modal>

            {/*MODALES DE NOTIFICACION SI SE REGISTRO CORRECTAMENTE LOS DATOS*/}
            <Modal isOpen={this.state.dangerNoti} toggle={this.toggleDangerNoti}
              className={'modal-danger ' + this.props.className}>
              <ModalHeader toggle={this.toggleDangerNoti}><strong>Advertencia</strong></ModalHeader>
              <ModalBody>
                Los datos NO fueron registrados correctamente, verifique e intente nuevamente.
                    </ModalBody>
              <ModalFooter>
                <Button color="secondary" onClick={this.toggleDangerNoti}>Cerrar</Button>
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.infoNoti} toggle={this.toggleInfoNoti}
              className={'modal-info ' + this.props.className}>
              <ModalHeader toggle={this.toggleInfoNoti}><strong>Correcto</strong></ModalHeader>
              <ModalBody>
                Los datos fueron registrados correctamente.
                    </ModalBody>
              <ModalFooter>
                <Button color="secondary" onClick={this.toggleInfoNoti}>Cerrar</Button>
              </ModalFooter>
            </Modal>


            <br />
            <DropdownItem divider />

            <ListaAlergia idA={this.props.idA} />

          </CardBody>
        </Card>
      </Col>




    );
  }
}
export default Alergias;
