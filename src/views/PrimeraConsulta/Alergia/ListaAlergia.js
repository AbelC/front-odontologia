
import React, { Component } from 'react';
import { Table, Button, CardBody, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';

import Editar from './Editar/Editar';

class ListaAlergia extends Component {

    constructor(props) {
        super(props);
        this.state = {
            alergias: [],
            id: '',
            idAlergia: '',
            descripcion: ''
        }
        this.toggle = this.toggle.bind(this);
        this.changeID = this.changeID.bind(this);
    }

    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/alergia/v1/lista/alergias/afiliado/' + this.props.idA + '/');
            const alergias = await respuesta.json();
            this.setState({
                alergias
            });
        } catch (e) {
            console.log(e);
        }


    }

    toggle() {

        this.setState({
            modal: !this.state.modal,

        });
    }
    changeID(a, b, c) {
        //console.log('a== ',a);
        //console.log('b== ',b);
        //console.log('c== ',c);
        this.setState({
            id: a,
            descripcion: b,
            idAlergia: c
        });
    }


    render() {
        const listAl = this.state.alergias.map(item => {
            return (
                <tr key={item.id}>
                    <td>{item.alergia.descripcion}</td>
                    <td>
                        <Button outline color="warning" onClick={() => { this.toggle(); this.changeID(item.id, item.alergia.descripcion, item.alergia.id); }}>Editar</Button>
                    </td>
                </tr>
            )
        })

        return (


            <CardBody>

                <Table responsive hover bordered size="sm">
                    <thead>
                        <tr>
                            <th scope="col">Alergia</th>
                            <th scope="col">Acción</th>
                        </tr>

                    </thead>
                    <tbody>
                        {listAl}
                    </tbody>
                </Table>


                <Modal isOpen={this.state.modal} toggle={this.toggle} className={'modal-info ' + this.props.className}>
                    <ModalHeader toggle={this.toggle}>Editar alergias</ModalHeader>
                    <ModalBody>
                        {/*console.log('id-Modal=',this.state.id)*/}
                        <Editar id={this.state.id} idAlergia={this.state.idAlergia} descripcion={this.state.descripcion} />

                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle}>Volver</Button>
                        <Button color="secondary" onClick={this.toggle}>Cerrar</Button>
                    </ModalFooter>
                </Modal>



            </CardBody>





        );
    }
}
/* <td> <Button outline color="warning" href={'#/PrimeraConsulta/Alergia/Editar/'+ item.id +'/'+ item.alergia.descripcion+ '/'+item.alergia.id} >Editar</Button></td> */
export default ListaAlergia;
