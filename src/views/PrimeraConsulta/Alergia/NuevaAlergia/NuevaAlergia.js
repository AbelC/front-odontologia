
import React, { Component } from 'react';
import { Button, Card, CardBody, CardHeader, Col, Form, FormGroup, Row, Label, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
var url = process.env.REACT_APP_API_URL+'odontologia/alergia/v1/alergias/';
class NuevaAlergia extends Component {

    constructor(props) {
        super(props);
        this.state = {
            success: false,
            warning: false,
        }
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.toggleSuccess = this.toggleSuccess.bind(this);
        this.toggleWarning = this.toggleWarning.bind(this);
    }
    toggleSuccess() {
        if (this.nuevaAlergia.value !== '') {
            this.setState({
                success: !this.state.success,
            });
        } else {
            this.setState({
                warning: !this.state.warning,
            });
        }

    }

    toggleWarning() {
        this.setState({
            warning: !this.state.warning,
        });
    }
    handleOnSubmit(e) {

        let data = {
            descripcion: this.nuevaAlergia.value,
        }
        //console.log(this.state.data);
        try {
            fetch(url, {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(data), // data can be string or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => console.log('Success:', response));
            window.location.reload();
        } catch (e) {
            console.log(e);
        }
        this.setState({
            success: !this.state.success,
        });

    }


    render() {
        return (
            <div>
                <Col xs="12">
                    <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i> <strong>Agregar Nueva Alergia</strong>
                        </CardHeader>
                        <CardBody>
                            <Form>
                                <Row>
                                    <Col sm="9">
                                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                            <Label for="fecha" className="mr-sm-2">Alergia</Label>
                                            <input type="text" className="form-control" placeholder="Nueva alergia" ref={nuevaAlergia => this.nuevaAlergia = nuevaAlergia} />
                                        </FormGroup>
                                    </Col>
                                    <Col sm="3">
                                        <br />
                                        <Button color="success" onClick={this.toggleSuccess} className="mr-1">Guardar</Button>
                                    </Col>
                                </Row>



                            </Form>
                        </CardBody>
                    </Card>
                </Col>

                <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                    className={'modal-success ' + this.props.className}>
                    <ModalHeader toggle={this.toggleSuccess}>Confirmación</ModalHeader>
                    <ModalBody>
                        ¿Agregar esta nueva alergia, a nuestros registros?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={this.handleOnSubmit}>Aceptar</Button>{' '}
                        <Button color="secondary" onClick={this.toggleSuccess}>Cancelar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
                    className={'modal-warning ' + this.props.className}>
                    <ModalHeader toggle={this.toggleWarning}>Campos Vaisos</ModalHeader>
                    <ModalBody>
                        Campos vacios, debe verficar que no haya campos vacios.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleWarning}>Cerra</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}
/* <td> <Button outline color="warning" href={'#/PrimeraConsulta/Alergia/Editar/'+ item.id +'/'+ item.alergia.descripcion+ '/'+item.alergia.id} >Editar</Button></td> */
export default NuevaAlergia;
