import React, { Component } from 'react';
import { Button, DropdownItem, Card, CardBody, CardHeader, Col, Row, Form, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import Search from 'react-search-box';
import ListaAntecedente from './listaAntecedente';
import Alergias from '../Alergia/Alergias';
import Tolerancias from '../Tolerancia/Tolerancias';
import ExamenClinico from '../ExamenClinico/ExamenClinico.js';
import MotivoConsulta from '../MotivoConsulta/MotivoConsulta'

var url = process.env.REACT_APP_API_URL + 'odontologia/antecedentes/v1/antecedentes/';
//var diag;
class AntecedentesG extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fecha: '',
      enfermedad: 0,
      medicacion: '',
      tipo: '',

      medico: '',
      afiliado: '',
      fecha_atencion: '',

      pr_consulta: '',
      primary: false,
      warning: false,

      now: '',
      valueE: '',
      valueM: '',
      diagnosticos_v: [],
      list_diagnostico: [],
      dangerNoti: false,
      infoNoti: false,

      idA: this.props.match.params.id,
      nombres: this.props.match.params.nombres,
      apellido_pat: this.props.match.params.apellido_pat,
      apellido_mat: this.props.match.params.apellido_mat,
      idMedico:localStorage.getItem("idMedico"),

    }

    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleChangeLetter = this.handleChangeLetter.bind(this);
    this.handleChangeLetter2 = this.handleChangeLetter2.bind(this);

    this.togglePrimary = this.togglePrimary.bind(this);
    this.toggleWarning = this.toggleWarning.bind(this);
    this.toggleDangerNoti = this.toggleDangerNoti.bind(this);
    this.toggleInfoNoti = this.toggleInfoNoti.bind(this);
    //console.log('==========>',this.state.url2);

  }

  toggleDangerNoti() {
    this.setState({
      danger: !this.state.danger,
    });
    window.location.reload()
  }

  toggleInfoNoti() {
    this.setState({
      info: !this.state.info,
    });
    window.location.reload()
  }

  async componentDidMount() { //Lista de diagnosticos
    try {
      const respuesta = await fetch(process.env.REACT_APP_API_URL + 'odontologia/diagnostico/v1/cie10/lista/'); //Para el diagnostico
      const diagnosticos_v = await respuesta.json();
      this.setState({
        diagnosticos_v,
      });

    } catch (e) {
      console.log(e);
    }
  }


  handleChangeLetter(event) {
    this.setState({ valueE: event.target.value.toUpperCase() });
  }

  handleChangeLetter2(event) {
    this.setState({ valueM: event.target.value.toUpperCase() });
  }

  handleOnSubmit(e) {
    if (this.fecha.value !== '' && this.state.enfermedad.value !== 0 && this.medicacion.value !== '') {
      console.log('DATOS NO VACIOS')
      var options = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
      };
      var d = new Date();
      var time = d.toLocaleString('es-bo', options);
      var date = time.split(' ')[0].split('/');
      time = time.split(' ')[1].split(':');
      var dateS = date[2] + '-' + date[1] + '-' + date[0];
      var timeS = time[0] + ':' + time[1] + ':' + time[2];
      //var dateFull = new Date(date[2], date[1], date[0], time[0], time[1], time[2]);   
      //var dateS = dateFull.getFullYear() + '-' + dateFull.getMonth() + '-' + dateFull.getDate();  /*Date to send */
      //var timeS = dateFull.getHours() + ':' + dateFull.getMinutes() + ':' + dateFull.getSeconds();  /**Time to send */

      //console.log('dateS: ', dateS)
      //console.log('timeS: ', timeS)
      e.preventDefault()
      let data = {
        fecha: this.fecha.value,
        enfermedad: this.state.enfermedad,
        medicacion: this.medicacion.value,
        medico: this.state.idMedico, //this.state.idA
        afiliado: this.state.idA,
        fecha_atencion: dateS,
        hora: timeS,
        tipo: this.tipo.value,
      }
      //console.log('data: ', data)

      try {
        fetch(url, {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(data), // data can be string or {object}!
          headers: {
            'Content-Type': 'application/json'
          }
        })
          .then(resp => {
            if (resp.status === 201) {
              this.setState({
                infoNoti: true
              })
              //console.log('resp.status', resp.status)
            } else {
              //console.log('resp.status ERROR', resp.status)
              this.setState({
                dangerNoti: true
              })
            }
          })

        /*.then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response));*/
        //window.location.reload();
      } catch (e) {
        console.log(e);
      }

      this.setState({
        fecha: '',
        enfermedad: '',
        medicacion: '',
        medico: '',
        afiliado: '',
        fecha_atencion: '',
        tipo: '',
        idA: '',
      });

      this.setState({
        primary: !this.state.primary,
      });

      this.setState({ now: new Date() });

    } else {
      alert('Rellene campos faltantes')
      //  console.log('DATOS VACIOS')
      //  console.log('data--',this.fecha.value);
      //  console.log('data---',this.enfermedad.value);
      //  console.log('data----',this.medicacion.value);
    }

  }

  togglePrimary() {
    if (this.fecha.value !== '' && this.state.enfermedad.value !== 0 && this.medicacion.value !== '') {
      this.setState({ primary: !this.state.primary, });
    } else {
      this.setState({ warning: !this.state.warning, });

    }
  }

  toggleWarning() {
    this.setState({
      warning: !this.state.warning,
    });
  }



  handleChangeDiagnostico(value) {
    console.log(value);

    var idDiag = 0;


    for (let i = 0; i < this.state.diagnosticos_v.length; i++) {
      if (this.state.diagnosticos_v[i].descripcion === value) {
        //console.log('* ', i, ' - ', this.state.diagnosticos_v[i].descripcion, ' - ', this.state.diagnosticos_v[i].id);
        idDiag = this.state.diagnosticos_v[i].id;
        break;
      } else {
        //console.log('**: ', i)
      }
    }

    if (idDiag !== 0) {
      console.log('idDiag: ', idDiag)
      this.setState({
        enfermedad: idDiag
      })
    } else {
      alert('Error en asignar el id de DIAGNOSTICO')
    }
  }


  render() {
    //console.log("props=",this.props)
    //console.log("props.idA=",this.props.idA)
    //console.log("props,match.idA=",this.state.idA)
    //console.log('value= ', this.state.value);
    //console.log('*** idDiagnostico: ', this.state.idDiagnostico)
    return (
      <div className="animated fadeIn">
        <MotivoConsulta idA={this.state.idA} apellido_pat={this.state.apellido_pat} apellido_mat={this.state.apellido_mat} nombres={this.state.nombres} />
        <Row>
          <Col xs="12">
            <h3><p align="center">ANTECEDENTES GENERALES</p></h3>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Antecedentes generales</strong>

                <div className="card-header-actions">
                  <strong className="text-muted">Paciente: {this.state.nombres + ' ' + this.state.apellido_pat + ' ' + this.state.apellido_mat}</strong>
                </div>


              </CardHeader>
              <CardBody>
                <Form>
                  {/*console.log('dateeee= ', this.state.now)*/}
                  <Row>
                    <Col sm="5">
                      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label for="fecha" className="mr-sm-2">Fecha Diagnostico</Label>
                        <input type="date" className="form-control" ref={fecha => this.fecha = fecha} required />
                      </FormGroup>
                    </Col>
                    <Col sm="7">
                      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label for="enfermedad" className="mr-sm-2">Enfermedad</Label>
                        <br />
                        <Search
                          data={this.state.diagnosticos_v}
                          onChange={this.handleChangeDiagnostico.bind(this)}
                          placeholder="Diagnostico CIE10"
                          name="buscaDiagnostico"
                          class="search-class"
                          searchKey="descripcion"

                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm="5">
                      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label for="medicacion" className="mr-sm-2">Medicación</Label>
                        <input type="text" className="form-control" placeholder="Medicación recibida" value={this.state.valueM} onChange={this.handleChangeLetter2} ref={medicacion => this.medicacion = medicacion} />
                      </FormGroup>
                    </Col>
                    <Col sm="5">
                      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label htmlFor="ccnumber" className="mr-sm-2">Tipo</Label>
                        <select type="select" ref={tipo => this.tipo = tipo} className="form-control" required>
                          <option value="">Seleccionar</option>
                          <option value="ALTA">Alta</option>
                          <option value="EN TRATAMIENTO">En tratamiento</option>
                          <option value="ABANDONO">Abandono</option>
                        </select>
                      </FormGroup>
                    </Col>
                    <Col sm="2">
                      <Button color="success" style={{ 'marginTop': '30px' }} onClick={this.togglePrimary} >Guardar</Button>
                    </Col>
                  </Row>


                </Form>


                <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-primary ' + this.props.className}>
                  <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
                  <ModalBody>
                    ¿Está seguro de guardar, los datos introducidos?
                  </ModalBody>
                  <ModalFooter>
                    <Button onClick={this.handleOnSubmit} type="submit" color="primary">Aceptar</Button>{' '}
                    <Button color="secondary" onClick={this.togglePrimary}>Cancelar</Button>
                  </ModalFooter>
                </Modal>



                <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
                  className={'modal-warning ' + this.props.className}>
                  <ModalHeader toggle={this.toggleWarning}>Campos vacios</ModalHeader>
                  <ModalBody>
                    Llene los campos faltantes, para guardar el registro de antecedentes del paciente.
                  </ModalBody>
                  <ModalFooter>
                    <Button color="warning" onClick={this.toggleWarning}>Volver</Button>{' '}

                  </ModalFooter>
                </Modal>
                <br />

                <DropdownItem divider />
                <ListaAntecedente idA={this.state.idA} />

              </CardBody>
            </Card>
          </Col>
        </Row>
        {/*MODALES DE NOTIFICACION SI SE REGISTRO CORRECTAMENTE LOS DATOS*/}
        <Modal isOpen={this.state.dangerNoti} toggle={this.toggleDangerNoti}
          className={'modal-danger ' + this.props.className}>
          <ModalHeader toggle={this.toggleDangerNoti}><strong>Advertencia</strong></ModalHeader>
          <ModalBody>
            Los datos NO fueron registrados correctamente, verifique e intente nuevamente.
                    </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleDangerNoti}>Cerrar</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.infoNoti} toggle={this.toggleInfoNoti}
          className={'modal-info ' + this.props.className}>
          <ModalHeader toggle={this.toggleInfoNoti}><strong>Correcto</strong></ModalHeader>
          <ModalBody>
            Los datos fueron registrados correctamente.
                    </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleInfoNoti}>Cerrar</Button>
          </ModalFooter>
        </Modal>

        <div className="animated fadeIn">
          <Row>



            <Alergias idA={this.state.idA} apellido_pat={this.state.apellido_pat} apellido_mat={this.state.apellido_mat} nombres={this.state.nombres} />
            <Tolerancias idA={this.state.idA} apellido_pat={this.state.apellido_pat} apellido_mat={this.state.apellido_mat} nombres={this.state.nombres} />
          </Row>
        </div>

        <ExamenClinico idA={this.state.idA} apellido_pat={this.state.apellido_pat} apellido_mat={this.state.apellido_mat} nombres={this.state.nombres} />
      </div >
    );
  }
}
export default AntecedentesG;
/*
  <input type="text" value={this.state.valueE} onChange={this.handleChangeLetter} className="form-control" placeholder="Enfermedad" ref={enfermedad => this.enfermedad = enfermedad} />
*/