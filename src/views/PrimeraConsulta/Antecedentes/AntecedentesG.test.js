import React from 'react';
import ReactDOM from 'react-dom';
import AntecedentesG from './AntecedentesG';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AntecedentesG />, div);
  ReactDOM.unmountComponentAtNode(div);
});
