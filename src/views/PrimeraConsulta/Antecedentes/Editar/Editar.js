import React, { Component } from 'react';
//import { Redirect, Route, Switch } from 'react-router-dom';
import Search from 'react-search-box';
import { Table, Button, Card, CardBody, CardHeader, Col, Row, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Error from '../../../Mensaje/Error';
import Exito from '../../../Mensaje/Exito';


class DatosPersonales_F extends Component {

  constructor(props) {
    super(props)
    this.state = {
      ante: [],
      enfermedad: '',
      enf: '',
      medicacion: '',
      primary: false,
      warning: false,
      visible: false,
      swT: false,
      swF: false,
      valueE: '',
      valueM: '',
      diagnosticos_v: [],

    }
    this.toggleWarning = this.toggleWarning.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.togglePrimary = this.togglePrimary.bind(this);
    this.onDismiss = this.onDismiss.bind(this);

    this.handleChangeLetter = this.handleChangeLetter.bind(this);
    this.handleChangeLetter2 = this.handleChangeLetter2.bind(this);

  }
  toggleWarning() {
    this.setState({
      warning: !this.state.warning,
    });
  }
  handleChangeLetter(event) {
    this.setState({ valueE: event.target.value.toUpperCase() });
  }

  handleChangeLetter2(event) {
    this.setState({ valueM: event.target.value.toUpperCase() });
  }

  async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/antecedentesJoin/' + this.props.id);
      const ante = await respuesta.json();

      const respuesta2 = await fetch(process.env.REACT_APP_API_URL+'odontologia/diagnostico/v1/cie10/lista/'); //Para el diagnostico
      const diagnosticos_v = await respuesta2.json();

      this.setState({
        enfermedad: ante.enfermedad.id,
        enf: ante.enfermedad.descripcion,
        ante,
        diagnosticos_v,
      });

    } catch (e) {
      console.log(e);
    }
  }
  handleOnSubmit(e) {
    if (this.state.enfermedad !== '' && this.medicacion.value !== '' && this.tipo.value !== '' &&
      this.state.ante.medico !== '' && this.state.ante.afiliado !== '' && this.state.ante.fecha_atencion) {
        var fecha_enf;
        if(this.fecha.value !== ''){
          fecha_enf=this.fecha.value;
        }else{
          fecha_enf=this.state.ante.fecha;
        }
      e.preventDefault()
      let url = ''
      let data = {}
      url = process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/antecedentes/' + this.props.id + '/'
      data = {
        //        enfermedad: this.enfermedad.value,
        enfermedad: this.state.enfermedad,
        medicacion: this.medicacion.value,
        fecha: fecha_enf,
        tipo: this.tipo.value,
        
        hora:this.state.ante.hora,
        medico: this.state.ante.medico,
        afiliado: this.state.ante.afiliado,
        fecha_atencion: this.state.ante.fecha_atencion,

      }
      console.log('data:', data)
      try {
        fetch(url, {
          method: 'PUT',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error) /*,this.setState({ swF: true, })*/)
          .then(response => console.log('Success:', response)/*, this.setState({ swT: true, })*/);
        window.location.reload();
      } catch (e) {
        console.log('errrorrrr e= ', e);
      }

      this.setState({
        fecha: '',
        enfermedad: '',
        medicacion: '',
        medico: '',
        afiliado: '',
        fecha_atencion: '',
        tipo: '',
        swT: true,
        swF: false,
      });

      this.setState({
        primary: !this.state.primary,
      });


    } else {
      this.setState({ swF: true, swT: false, primary: !this.state.primary, });

    }

  }


  handleChangeDiagnostico(value) {
    console.log(value);

    var idDiag = 0;

    for (let i = 0; i < this.state.diagnosticos_v.length; i++) {
      if (this.state.diagnosticos_v[i].descripcion === value) {
        console.log('* ', i, ' - ', this.state.diagnosticos_v[i].descripcion, ' - ', this.state.diagnosticos_v[i].id);
        idDiag = this.state.diagnosticos_v[i].id;
        break;
      }
    }

    if (idDiag !== 0) {
      console.log('idDiag: ', idDiag)
      this.setState({
        enfermedad: idDiag
      })
    } else {
      alert('Error en asignar el id de DIAGNOSTICO')
    }
  }



  togglePrimary() {
    if (this.state.enfermedad !== '' && this.medicacion.value !== '' && this.tipo.value !== '' &&
      this.state.ante.medico !== '' && this.state.ante.afiliado !== '' && this.state.ante.fecha_atencion) {
      this.setState({
        primary: !this.state.primary,
      });
    } else {
      this.setState({
        warning: !this.state.warning,
      });
    }

  }


  onDismiss() {
    this.setState({ visible: !this.state.visible, });
  }


  render() {
    //console.log('idEDITAR= ', this.props.id);
    //console.log('visible= ', this.state.visible)

    //console.log('this.state.enfermedad: ', this.state.enfermedad)


    return (


      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>

              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Editar Antecedente</strong>
              </CardHeader>

              <CardBody>


                <Table responsive hover>

                  <tbody>
                    <tr>
                      <td><strong>Fecha Diagnostico</strong></td>
                      <td>{this.state.ante.fecha}
                        <hr />
                        <input type="date" className="form-control" ref={fecha => this.fecha = fecha} required />
                      </td>
                    </tr>
                    <tr>
                      <td><strong>Enfermedad:</strong></td>
                      <td>
                        <Search
                          data={this.state.diagnosticos_v}
                          onChange={this.handleChangeDiagnostico.bind(this)}
                          placeholder={this.state.enf}
                          name="buscaDiagnostico"
                          class="search-class"
                          searchKey="descripcion"
                          defaultValue={this.state.enfermedad}
                        />
                      </td>
                    </tr>
                    <tr>
                      <td><strong>Medicación</strong></td>
                      <td><input className="form-control" defaultValue={this.state.ante.medicacion} ref={medicacion => this.medicacion = medicacion}></input></td>
                    </tr>
                    <tr>
                      <td><strong>Tipo</strong></td>
                      <td>
                        <select type="select" ref={tipo => this.tipo = tipo} className="form-control" required>
                          <option value={this.state.ante.tipo}>{this.state.ante.tipo}</option>
                          <option value="ALTA">ALTA</option>
                          <option value="EN TRATAMIENTO">EN TRATAMIENTO</option>
                          <option value="ABANDONO">ABANDONO</option>
                        </select>
                      </td>
                    </tr>

                  </tbody>
                </Table>

                <Button color="success" onClick={this.togglePrimary} className="mr-1">Editar</Button>

                <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-primary ' + this.props.className}>
                  <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
                  <ModalBody>
                    ¿Está seguro que quiere editar?
                </ModalBody>
                  <ModalFooter>
                    <Button onClick={this.handleOnSubmit} type="submit" color="primary">Aceptar</Button>{' '}
                    <Button color="secondary" onClick={this.togglePrimary}>Cancelar</Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
                  className={'modal-warning ' + this.props.className}>
                  <ModalHeader toggle={this.toggleWarning}>Campos Vacios</ModalHeader>
                  <ModalBody>
                    Verfique que no haya campos vacios.
                  </ModalBody>
                  <ModalFooter>
                    <Button color="secondary" onClick={this.toggleWarning}>Cerrar</Button>
                  </ModalFooter>
                </Modal>


                {(this.state.swT === true) ? <Exito /> : (this.state.swF === true) ? <Error /> : null}

                {/*console.log('***swT: ', this.state.swT, ' *swF: ', this.state.swF)*/}





              </CardBody>
            </Card>
          </Col>
        </Row>

      </div>
    );
  }
}


/*
<input className="form-control" defaultValue={this.state.enf} ref={enfermedad => this.enfermedad = enfermedad}></input>


  const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/antecedentes/' + this.props.id);
  <Container fluid>
              <Switch>
                <Redirect from="/" to="/dashboard" />
              </Switch>
  </Container>
*/
export default DatosPersonales_F;
