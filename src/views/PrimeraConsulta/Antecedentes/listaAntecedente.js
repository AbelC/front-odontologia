import React, { Component } from 'react';
import { Table, Button, CardBody, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Editar from './Editar/Editar';


class ListaAntecedente extends Component {

  //es para sacar de la tabla de antecedentes_g
  constructor(props) {
    super(props);

    this.state = {
      antecedentes: [],
      modal: false,
      id: '',

    }
    this.toggle = this.toggle.bind(this);
    this.changeID = this.changeID.bind(this);

  }

  async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/lista/antecedentes/afiliado/' + this.props.idA + '/');
      const antecedentes = await respuesta.json();
      this.setState({
        antecedentes
      });
    } catch (e) {
      console.log(e);
    }
  }


  toggle() {

    this.setState({
      modal: !this.state.modal,

    });
  }
  changeID(a) {
    //console.log('a== ',a);
    this.setState({
      id: a
    });

  }


  render() {


    const listAn = this.state.antecedentes.map(item => {
      return (
        <tr key={item.id} >
          <td>{item.fecha}</td>
          <td><strong>{item.enfermedad.codigo + ': '}</strong> {item.enfermedad.descripcion}</td>
          <td>{item.medicacion}</td>
          <td>{item.tipo}</td>
          <td> <Button outline color="warning" onClick={() => { this.toggle(); this.changeID(item.id); }} >Editar</Button></td>
        </tr>
      )

    })
    return (
      <div>
        <CardBody>
          <Table responsive hover bordered size="sm">
            <thead>
              <tr>
                <th scope="col">Fecha Diagnostico</th>
                <th scope="col">Enfermedad</th>
                <th scope="col">Medicación</th>
                <th scope="col">Tipo</th>
                <th scope="col">Acción</th>
              </tr>
            </thead>
            <tbody>
              {listAn}
            </tbody>
          </Table>
        </CardBody>


        <Modal isOpen={this.state.modal} toggle={this.toggle} className={'modal-info ' + this.props.className}>
          <ModalHeader toggle={this.toggle}>Editar Antecedente</ModalHeader>
          <ModalBody>
            {/*console.log('id-Modal=',this.state.id)*/}
            <Editar id={this.state.id} />
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggle}>Cerrar</Button>
          </ModalFooter>
        </Modal>


      </div>
    );
  }
}
/* <td> <Button  outline color="warning" href={'#/PrimeraConsulta/Antecedentes/Editar/'+item.id}>Editar</Button></td>
 <td> <Button  outline  color="warning" onClick={ () => { this.toggle(); this.changeID(item.id);}  } >Editar</Button></td> 
*/
export default ListaAntecedente;
