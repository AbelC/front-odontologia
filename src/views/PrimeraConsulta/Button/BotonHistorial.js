import React, { Component } from 'react';
import { Button, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import FichaOdontologica2 from '../../FichaAtencionOdontologica/FichaOdontologica2';

class BotonHistorial extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dropdownOpen: false,
            collapse: false,
            modal: false
        };

        this.toggle = this.toggle.bind(this);
        this.toggleModal = this.toggleModal.bind(this);

    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen,

        });
    }


    toggleModal() {
        this.setState({
            modal: !this.state.modal
        });
    }
    render() {
        return (
            <div>
                {/*console.log("id_a_Hostorial===", this.props.idA)*/}


                <ButtonDropdown direction="right" isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                    <DropdownToggle caret className="btn btn-outline-primary" color="primary">
                        Ficha Odotologica
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem onClick={this.toggleModal}>Ver Aqui</DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem href={'#/FichaAtencionOdontologica/FichaOdontologica/' + this.props.idA}  >Otra Ventana</DropdownItem>
                    </DropdownMenu>
                </ButtonDropdown>






                <Modal isOpen={this.state.modal} toggle={this.toggleModal} className="modal-dialog modal-lg">
                    <ModalHeader toggle={this.toggleModal}>Ficha de Atencion Odontologica </ModalHeader>
                    <ModalBody>
                        <FichaOdontologica2 idAf={this.props.idA} />
                    </ModalBody>
                    <ModalFooter>

                        <Button color="secondary" onClick={this.toggleModal}>Cerrar</Button>
                    </ModalFooter>
                </Modal>




            </div>


        );
    }
}
export default BotonHistorial;