import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import BotonHistorial from './Button/BotonHistorial';
import BotonPrimeraConsulta from './Button/BotonPrimeraConsulta';
import BotonTratamiento from './Button/BotonTratamiento';




class DatosPersonales extends Component {
  constructor(props) {

    super(props);
    this.state = {
      collapse: false,
      collapse2: false,

      fadeIn: true,
      timeout: 300,
      estudiante: [],

      //muestraBotonPrimeraConsulta: false,
      //muestraBotonTratamiento: false,
      //muestraBotonHistorial: false      
    };

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);


  }




  toggle() {
    /*  this.setState({ 
        collapse: !this.state.collapse,
        collapse2:!this.state.collapse2,    
      });*/

    if (this.state.collapse === false) {
      this.setState({
        collapse: true,
        collapse2: false,
      });

    } else if (this.state.collapse2 === false) {
      this.setState({
        collapse: false,
        collapse2: true,
      });
    }

  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState } });
  }

  /*
  async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/persona/v1/estudiantes/'+4914509+'/');
      const estudiante = await respuesta.json();
      this.setState({
        estudiante
      });
    } catch (e) {
        console.log(e);
    }
  }*/





  render() {
    //  console.log('LOS PROPS DEL BUSCAR', this.props)
    // console.log(this.props.list);
    const lista = this.props.list.map((item, indice) => {
      return (
        <tr key={item.id}>
          <td>{item.ci}</td>
          <td>{item.registro_universitario}</td>
          <td>{item.cod_promes}</td>
          <td>{item.apellido_pat}</td>
          <td>{item.apellido_mat}</td>
          <td>{item.nombres}</td>
          <td>{item.fecha_nacimiento}</td>
        </tr>

      )

    }

    )

    //console.log('Apellido pat=',this.props.list[0].apellido_pat, ' Apellido mat= ', this.props.list[0].apellido_mat,'  nombres=', this.props.list[0].nombres, '  id== ', this.props.list[0].id);
    //console.log("ID_A=",lista[0].key);



    //console.log('Apellido pat=',this.props.list[0].apellido_pat, ' Apellido mat= ', this.props.list[0].apellido_mat,'  nombres=', this.props.list[0].nombres, '  id== ', this.props.list[0].id);
    //console.log("ID_A=",lista[0].key);


    return (

      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <Card>

              <CardHeader>
                <i className="fa fa-vcard-o (alias)"></i><strong>Datos Personales</strong>
              </CardHeader>

              <CardBody>

                <div>
                  <Table responsive hover >
                    <thead>
                      <tr>
                        <th>Cedula de identidad</th>
                        <th>Registro Universitario</th>
                        <th>Matricula Promes</th>
                        <th>Apellido paterno:</th>
                        <th>Apellido materno:</th>
                        <th>Nombres:</th>
                        <th>Fecha Nacimiento:</th>
                      </tr>
                    </thead>
                    <tbody>
                      {lista}
                    </tbody>
                  </Table>


                </div>
                {this.props.muestraBotonPrimeraConsulta ? < BotonPrimeraConsulta idA={this.props.list[0].id} apellido_pat={this.props.list[0].apellido_pat} apellido_mat={this.props.list[0].apellido_mat} nombres={this.props.list[0].nombres} /> : null}
                <Row>
                  <Col xs="6"></Col>
                  <Col xs="6"></Col>
                  <Col xs="6"> {this.props.muestraBotonTratamiento ? <BotonTratamiento idA={this.props.list[0].id} apellido_pat={this.props.list[0].apellido_pat} apellido_mat={this.props.list[0].apellido_mat} nombres={this.props.list[0].nombres} /> : null} </Col>
                  <Col xs="6">{this.props.muestraBotonHistorial ? <BotonHistorial idA={lista[0].key} /> : null}</Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DatosPersonales;