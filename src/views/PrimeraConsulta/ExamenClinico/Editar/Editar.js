
import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Modal, ModalBody, ModalFooter, ModalHeader, Button } from 'reactstrap';



class Editar extends Component {

  constructor(props) {
    super(props)
    this.state = {
      examen_clinico_v: [],
      success: false,
    }
    this.toggleSuccess = this.toggleSuccess.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    //console.log('myID==', this.props.id)

  }
  //http://localhost:8000/odontologia/examenclinico/v1/examenclinico/92/

  async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/examenclinico/v1/examenclinico/' + this.props.id + '/');
      const examen_clinico_v = await respuesta.json();
      this.setState({
        examen_clinico_v
      });
    } catch (e) {
      console.log(e);
    }
  }
  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });
  }

  handleOnSubmit(e) {
    e.preventDefault()

    if (this.higieneBucal.value !== '' && this.oclusion.value !== '' && this.mucosa_gingival.value !== '' && this.enfermedad_periodontal.value !== '' && this.mucosa_bucal.value !== '') {
      let url = ''
      let data = {}

      url = process.env.REACT_APP_API_URL+'odontologia/examenclinico/v1/examenclinico/' + this.props.id + '/';

      data = {
        higieneBucal: this.higieneBucal.value,
        oclusion: this.oclusion.value,
        mucosa_gingival: this.mucosa_gingival.value,
        enfermedad_periodontal: this.enfermedad_periodontal.value,
        mucosa_bucal: this.mucosa_bucal.value,

        medico: this.state.examen_clinico_v.medico,
        afiliado: this.state.examen_clinico_v.afiliado,
        fecha_atencion: this.state.examen_clinico_v.fecha_atencion,
        hora: this.state.examen_clinico_v.hora,
      }
      console.log('data: ', data)
      try {
        fetch(url, {
          method: 'PUT',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response));
        window.location.reload();
      } catch (e) {
        console.log(e);
      }

      this.setState({
        success: !this.state.success,
      });
    }
  }


  render() {
    /*document.write(this.props.match.params.id)*/
    return (
      <div className="animated fadeIn">
        <Row>
          <Col sm="12">
            <Card>

              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Editar Examen Clinico</strong>
              </CardHeader>
              <CardBody>
                edicion del examen clinico -- > {this.props.id}

                <Table responsive hover bordered size="sm">
                  <tbody>
                    <tr>
                      <td>Higiene Bucal:</td>
                      <td>
                        <select type="select" name="higieneBucal" id="higieneBucal" className="form-control" ref={higieneBucal => this.higieneBucal = higieneBucal}>
                          <option value={this.state.examen_clinico_v.higieneBucal}>{'* ' + this.state.examen_clinico_v.higieneBucal + ' *'}</option>
                          <option value="BUENA">BUENA</option>
                          <option value="REGULAR">REGULAR</option>
                          <option value="MALA">MALA</option>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td>Oclusión:</td>
                      <td>
                        <select type="select" name="oclusion" id="oclusion" className="form-control" ref={oclusion => this.oclusion = oclusion}>
                          <option value={this.state.examen_clinico_v.oclusion}>{'* ' + this.state.examen_clinico_v.oclusion + ' *'}</option>
                          <option value="NORMAL">NORMAL</option>
                          <option value="ALTERADA">ALTERADA</option>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td>Mucosa Gingival:</td>
                      <td>
                        <select type="select" name="mucosaGingival" id="mucosaGingival" className="form-control" ref={mucosa_gingival => this.mucosa_gingival = mucosa_gingival}>
                          <option value={this.state.examen_clinico_v.mucosa_gingival}>{'* ' + this.state.examen_clinico_v.mucosa_gingival + ' *'}</option>
                          <option value="NORMAL">NORMAL</option>
                          <option value="ALTERADA">ALTERADA</option>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td>Enfermedad Periodontal:</td>
                      <td>
                        <select type="select" name="enfermedadPeriodontal" id="enfermedadPeriodontal" className="form-control" ref={enfermedad_periodontal => this.enfermedad_periodontal = enfermedad_periodontal}>
                          <option value={this.state.examen_clinico_v.enfermedad_periodontal}>{'* ' + this.state.examen_clinico_v.enfermedad_periodontal + ' *'}</option>
                          <option value="SI">SI</option>
                          <option value="NO">NO</option>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td>Mucosa Bucal:</td>
                      <td>
                        <select type="select" name="mucosaBucal" id="mucosaBucal" className="form-control" ref={mucosa_bucal => this.mucosa_bucal = mucosa_bucal}>
                          <option value={this.state.examen_clinico_v.mucosa_bucal}>{'* ' + this.state.examen_clinico_v.mucosa_bucal + ' *'}</option>
                          <option value="NORMAL">NORMAL</option>
                          <option value="ALTERADA">ALTERADA</option>
                        </select>
                      </td>
                    </tr>
                  </tbody>
                </Table>

                <Button color="success" onClick={this.toggleSuccess} className="mr-1">Editar</Button>

                <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                  className={'modal-success ' + this.props.className}>
                  <ModalHeader toggle={this.toggleSuccess}>Confirmación</ModalHeader>
                  <ModalBody>
                    ¿Guardar los datos Editados?
                  </ModalBody>
                  <ModalFooter>
                    <Button color="success" onClick={this.handleOnSubmit}>Si</Button>{' '}
                    <Button color="secondary" onClick={this.toggleSuccess}>No</Button>
                  </ModalFooter>
                </Modal>

              </CardBody>
            </Card>
          </Col>
        </Row>
















      </div>
    );
  }
}

export default Editar;
