import React, { Component } from 'react';
import { Alert, Button, Card, CardBody, DropdownItem, CardHeader, Col, Row, Form, Label, FormGroup, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
//import { AppSwitch } from '@coreui/react'
import ListaExamen from './ListaExamen';
import Odontograma from '../Odontograma/Odontograma.js';
var url = process.env.REACT_APP_API_URL + 'odontologia/examenclinico/v1/examenclinico/';
class ExamenClinico extends Component {

  constructor(props) {
    super(props)
    this.state = {
      higieneBucal: '',
      oclusion: '',
      mucosa_gingival: '',
      enfermedad_periodontal: '',
      mucosa_bucal: '',
      medico: '',
      afiliado: this.props.idA,
      fecha_atencion: '',
      primary: false,
      warning: false,
      estado: null,
      idEXSent: 0,
      swRender: false,
      ocultaBottonEnviar: false,
      muestraOdontograma: false,
      visibleDanger: false,
      visibleInfo: false,
      idMedico:localStorage.getItem("idMedico"),
      url2: process.env.REACT_APP_API_URL + 'odontologia/persona/v1/afiliado/' + this.props.idA + '/'
    }

    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.handleNewOnSubmit = this.handleNewOnSubmit.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.toggleWarning = this.toggleWarning.bind(this);
    this.onDismissDanger = this.onDismissDanger.bind(this);
    this.onDismissInfo = this.onDismissInfo.bind(this);
  }
  onDismissDanger() {
    this.setState({ visibleDanger: !this.state.visibleDanger });
    window.location.reload()
  }
  onDismissInfo() {
    this.setState({ visibleInfo: !this.state.visibleInfo });
  }


  handleOnSubmit() {
    //e.preventDefault()
    if (this.higieneBucal.value !== '' && this.oclusion.value !== '' && this.mucosa_gingival.value !== '' && this.enfermedad_periodontal.value !== '' && this.mucosa_bucal.value !== '') {
      var options = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
      };
      var d = new Date();
      var time = d.toLocaleString('es-bo', options);
      var date = time.split(' ')[0].split('/');
      time = time.split(' ')[1].split(':');
      var dateS = date[2] + '-' + date[1] + '-' + date[0];
      var timeS = time[0] + ':' + time[1] + ':' + time[2];
      console.log('dateS: ', dateS)
      console.log('timeS: ', timeS)
      //var dateFull = new Date(date[2], date[1], date[0], time[0], time[1], time[2]);
      //var dateS = dateFull.getFullYear() + '-' + dateFull.getMonth() + '-' + dateFull.getDate();  /*Date to send */
      //var timeS = dateFull.getHours() + ':' + dateFull.getMinutes() + ':' + dateFull.getSeconds();  /**Time to send */

      let data = {
        higieneBucal: this.higieneBucal.value,
        oclusion: this.oclusion.value,
        mucosa_gingival: this.mucosa_gingival.value,
        enfermedad_periodontal: this.enfermedad_periodontal.value,
        mucosa_bucal: this.mucosa_bucal.value,
        medico: this.state.idMedico,
        afiliado: this.props.idA,
        fecha_atencion: dateS,
        hora: timeS,
      }

      try {
        fetch(url, {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(data), // data can be string or {object}!
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(res => {
          var op = {
            estado: res.status,
            respo: res.json()
          }
          return op;
          //console.log('OP: ',op);
        }).then(response => {
          //console.log('op.stado: ', response.estado);
          if (response.estado === 201) {
            response.respo.then(function (result) {
              console.log('Success', result);
              this.setState({ swRender: true, idEXSent: result.id, ocultaBottonEnviar: true, muestraOdontograma: true, visibleInfo: true })
            }.bind(this))
          } else {
            this.setState({ visibleDanger: true })
            //alert(response.estado)
          }

        });

        //console.log('OP: ', op)

        //window.location.reload();
        this.setState({
          higieneBucal: '',
          oclusion: '',
          mucosa_gingival: '',
          enfermedad_periodontal: '',
          mucosa_bucal: '',
          medico: '',
          afiliado: '',
          fecha_atencion: '',
          tableShow: [],
          muestraTable: false,
        });

      } catch (e) {
        console.log(e);
      }

      this.setState({
        primary: !this.state.primary,
      });


    }


  }


  handleNewOnSubmit() {
    //e.preventDefault()
    if (this.higieneBucal.value !== '' && this.oclusion.value !== '' && this.mucosa_gingival.value !== '' && this.enfermedad_periodontal.value !== '' && this.mucosa_bucal.value !== '') {
      let data2 = {
        pr_consulta: 1,
      }

      try {
        fetch(this.state.url2, {
          method: 'PUT', // or 'PUT'
          body: JSON.stringify(data2), // data can be string or {object}!
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:',response));
        this.setState({
          pr_consulta: '',
        });

      } catch (e) {
        console.log(e);
      }
    }
  }


  togglePrimary() {
    if (this.higieneBucal.value !== '' && this.oclusion.value !== '' && this.mucosa_gingival.value !== '' && this.enfermedad_periodontal.value !== '' && this.mucosa_bucal.value !== '') {
      this.setState({ primary: !this.state.primary, });
    } else {
      this.setState({ warning: !this.state.warning, });

    }
  }

  toggleWarning() {
    this.setState({
      warning: !this.state.warning,
    });
  }


  render() {
    //console.log('props EX: ', this.props)
    //console.log('idEXSent: ', this.state.idEXSent);
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <h3> <p align="center">EXAMEN CLINICO INTRAORAL</p></h3>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Examen Clinico</strong>
                <div className="card-header-actions">
                  <strong className="text-muted">Paciente: {this.props.nombres + ' ' + this.props.apellido_pat + ' ' + this.props.apellido_mat}</strong>
                </div>
              </CardHeader>
              <CardBody>
                <Form inline method="POST">
                  <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Label for="fecha" className="mr-sm-2">Higiene Bucal</Label>
                    <select type="select" name="higieneBucal" id="higieneBucal" className="form-control" ref={higieneBucal => this.higieneBucal = higieneBucal}>
                      <option value="">---</option>
                      <option value="BUENA">BUENA</option>
                      <option value="REGULAR">REGULAR</option>
                      <option value="MALA">MALA</option>
                    </select>
                  </FormGroup>

                  <FormGroup className="mb-2 mr-sm-2 mb-sm-0" >
                    <Label for="fecha" className="mr-sm-2">Oclusión</Label>
                    <select type="select" name="oclusion" id="oclusion" className="form-control" ref={oclusion => this.oclusion = oclusion}>
                      <option value="">---</option>
                      <option value="NORMAL">NORMAL</option>
                      <option value="ALTERADA">ALTERADA</option>
                    </select>
                  </FormGroup>

                  <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Label for="fecha" className="mr-sm-2">Mucosa Gingival</Label>
                    <select type="select" name="mucosaGingival" id="mucosaGingival" className="form-control" ref={mucosa_gingival => this.mucosa_gingival = mucosa_gingival}>
                      <option value="">---</option>
                      <option value="NORMAL">NORMAL</option>
                      <option value="ALTERADA">ALTERADA</option>
                    </select>
                  </FormGroup>

                  <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Label for="fecha" className="mr-sm-2">Enfermedad Periodontal</Label>
                    <select type="select" name="enfermedadPeriodontal" id="enfermedadPeriodontal" className="form-control" ref={enfermedad_periodontal => this.enfermedad_periodontal = enfermedad_periodontal}>
                      <option value="">---</option>
                      <option value="SI">SI</option>
                      <option value="NO">NO</option>
                    </select>
                  </FormGroup>

                  <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Label for="fecha" className="mr-sm-2">Mucosa Bucal</Label>
                    <select type="select"  name="mucosaBucal" id="mucosaBucal" className="form-control" ref={mucosa_bucal => this.mucosa_bucal = mucosa_bucal}>
                      <option value="">---</option>
                      <option value="NORMAL">NORMAL</option>
                      <option value="ALTERADA">ALTERADA</option>
                    </select>
                  </FormGroup>


<br />
                  {(this.state.ocultaBottonEnviar === false) ? <Button onClick={this.togglePrimary} >Guardar</Button> : null}

                  <br />
                </Form>
                <br />
                <Row>
                  <Col sm="12" md={{ size: 6, offset: 3 }}>
                    <Alert color="danger" isOpen={this.state.visibleDanger} toggle={this.onDismissDanger}>
                      Los datos NO fueron registrados correctamente, verifique e intente nuevamente.
                  </Alert>

                    <Alert color="info" isOpen={this.state.visibleInfo} toggle={this.onDismissInfo}>
                      Los datos fueron registrados correctamente.
                  </Alert>
                  </Col>
                </Row>
                <DropdownItem divider />
                <ListaExamen swRender={this.state.swRender} idA={this.props.idA} idEXSent={this.state.idEXSent} />
              </CardBody>
            </Card>
          </Col>
        </Row>
        {
          (this.state.muestraOdontograma === true) ?
            <Odontograma idEXSent={this.state.idEXSent} idA={this.props.idA} apellido_pat={this.props.apellido_pat} apellido_mat={this.props.apellido_mat} nombres={this.props.nombres} /> : null
        }



        <Modal isOpen={this.state.primary} toggle={this.togglePrimary}
          className={'modal-primary ' + this.props.className}>
          <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
          <ModalBody>
            ¿Está seguro de guardar, los datos?
                        </ModalBody>
          <ModalFooter>

            <Button onClick={() => { this.handleOnSubmit(); this.handleNewOnSubmit(); }} type="submit" color="primary">Aceptar</Button>{' '}

            <Button color="secondary" onClick={this.togglePrimary}>Cancelar</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
          className={'modal-warning ' + this.props.className}>
          <ModalHeader toggle={this.toggleWarning}>Campos vacios</ModalHeader>
          <ModalBody>
            Llene los campos faltantes, para guardar el registro del examen clinico del paciente.
                  </ModalBody>
          <ModalFooter>
            <Button color="warning" onClick={this.toggleWarning}>Volver</Button>{' '}

          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
/* idEXSent={this.state.idEXSent}
<Button onClick={this.handleOnSubmit} type="submit" color="primary">Aceptar</Button>{' '}
       <Table responsive hover bordered size="sm">
                  <thead>
                    <tr>
                      <th scope="col">Higiene Bucal</th>
                      <th scope="col">Oclusión</th>
                      <th scope="col">Mucosa Gingival</th>
                      <th scope="col">Enfermedad Periodontal</th>
                      <th scope="col">Mucosa Bucal</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.tableShow.map(item => (
                      <tr key={item.id}>
                        <td>{item.higieneBucal}</td>
                        <td>{item.oclusion}</td>
                        <td>{item.mucosa_gingival}</td>
                        <td>{item.enfermedad_periodontal}</td>
                        <td>{item.mucosa_bucal}</td>

                      </tr>

                    ))}


                  </tbody>
                </Table>


*/
export default ExamenClinico;
