import React, { Component } from 'react';
import { Table, CardBody, Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import Editar from './Editar/Editar'

class ListaExamen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            examen: [],
            swCut: false,
            id: '',
        }
        this.toggle = this.toggle.bind(this);
        this.changeID = this.changeID.bind(this);
    }
    toggle() {

        this.setState({
            modal: !this.state.modal,

        });
    }
    changeID(a) {
        //console.log('a== ',a);
        this.setState({
            id: a
        });

    }


    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/tolerancia/v1/lista/examenclinico/afiliado/' + this.props.idA + '/');
            const examen = await respuesta.json();
            this.setState({
                examen
            });
            if (this.props.idEXSent === 0) {
                this.setState({ swCut: false })
            } else if (this.props.idEXSent !== 0) {
                this.setState({ swCut: true })
            }
        } catch (e) {
            console.log(e);
        }
        //this.setState({swRender:false})
    }

    render() {
        (this.props.swRender === true && this.state.swCut === false) ? this.componentDidMount() : console.log();
        // console.log('swRender: ', this.props.swRender,'swCut: ', this.state.swCut);
        return (
            <div>
                <CardBody>
                    {/*'swRender ~ ' + this.props.swRender*/}
                    <Table responsive hover bordered size="sm">
                        <thead>
                            <tr>
                                
                                <th scope="col">Higiene Bucal</th>
                                <th scope="col">Oclusión</th>
                                <th scope="col">Mucosa Gingival</th>
                                <th scope="col">Enfermedad Periodontal</th>
                                <th scope="col">Mucosa Bucal</th>
                                <th scope="col">Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.examen.map(item => (
                                <tr key={item.id}>
                                    
                                    <td>{item.higieneBucal}</td>
                                    <td>{item.oclusion}</td>
                                    <td>{item.mucosa_gingival}</td>
                                    <td>{item.enfermedad_periodontal}</td>
                                    <td>{item.mucosa_bucal}</td>
                                    <td> <Button outline color="warning" onClick={() => { this.toggle(); this.changeID(item.id); }} >Editar</Button></td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </CardBody>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={'modal-info ' + this.props.className}>
                    <ModalHeader toggle={this.toggle}>Editar Tolerancia</ModalHeader>
                    <ModalBody>
                        {/*console.log('id-Modal=',this.state.id)*/}
                        <Editar id={this.state.id} />
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle}>Volver</Button>
                        <Button color="secondary" onClick={this.toggle}>Cerrar</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}
/*
   <td> <Button  outline color="warning"  href={'#/PrimeraConsulta/ExamenClinico/Editar/'+item.id}>Editar</Button></td> 
*/
export default ListaExamen;
