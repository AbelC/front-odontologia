import React, { Component } from 'react';
import { Table, Card, CardBody, CardHeader, Col, Row,  Button, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';
//, Alert, Modal, ModalBody, ModalFooter, ModalHeader , Button
import Search from 'react-search-box';
class Editar extends Component {

    constructor(props) {
        super(props)
        this.state = {
            motivoConsulta: [],
            list_tieneMotivoConsulta: [],
            idMotivoC: 0,
            success: false,
            warning: false, 
        }
        this.handleOnSubmit = this.handleOnSubmit.bind(this)
        this.toggleSuccess = this.toggleSuccess.bind(this);
        this.toggleWarning = this.toggleWarning.bind(this);
    }
   /*async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/tieneMotivoConsulta/' + this.props.idTieneMotivo + '/');
            const list_tieneMotivoConsulta = await respuesta.json();
            this.setState({
                list_tieneMotivoConsulta
            });
        } catch (e) {
            console.log(e);
        }
    }*/
    toggleSuccess() {
        if (this.state.idMotivoC !== 0) {
            this.setState({
                success: !this.state.success,
            });
        } else {
            this.setState({
                warning: !this.state.warning,
            });
        }
    }
    toggleWarning() {
        this.setState({
            warning: !this.state.warning,
        });
    }
    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {//http://localhost:8000/odontologia/antecedentes/v1/tieneMotivoConsulta/

            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/motivoConsulta/');
            const motivoConsulta = await respuesta.json();

            const respuesta2 = await fetch(process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/tieneMotivoConsulta/'+this.props.idTieneMotivo+'/');
            const list_tieneMotivoConsulta = await respuesta2.json();

            this.setState({
                motivoConsulta,
                list_tieneMotivoConsulta,
            });
        } catch (e) {
            console.log(e);
        }
    }
    handleChange(value) {
        //console.log('value: ', value)
        var idMotivoConsulta = 0;
        for (let i = 0; i < this.state.motivoConsulta.length; i++) {
            if (this.state.motivoConsulta[i].detalle === value) {
                idMotivoConsulta = this.state.motivoConsulta[i].id;
                break;
            }
        }
        if (idMotivoConsulta !== 0) {
            this.setState({ idMotivoC: idMotivoConsulta })
        } else {
            console.log('no')
        }

    }
    handleOnSubmit(e) {
        console.log('DATOS NO VACIOS')
        let url = ''
        let data = {}
        url = process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/tieneMotivoConsulta/' + this.props.idTieneMotivo + '/'

        data = {
            afiliado: this.state.list_tieneMotivoConsulta.afiliado,
            motivoConsulta_id: this.state.idMotivoC,
            fecha_motivoConsulta: this.state.list_tieneMotivoConsulta.fecha_motivoConsulta,
            hora:  this.state.list_tieneMotivoConsulta.hora,
        }

        try {
            fetch(url, {
                method: 'PUT', // or 'PUT'
                body: JSON.stringify(data), // data can be string or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => console.log('Success:', response));
            window.location.reload();
        } catch (e) {
            console.log(e);
        }

        this.setState({
            success: !this.state.success,
        });
    }

    render() {
        //console.log('props: ', this.props)
        //console.log('state: ', this.state.idMotivoC)
        //</CardBody>console.log('list_tieneMotivoConsulta: ', this.state.list_tieneMotivoConsulta)
        //</CardBody>console.log('fecha: ', this.state.list_tieneMotivoConsulta.fecha_motivoConsulta)
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify"></i><strong>Editar Motivo de Consulta</strong>
                            </CardHeader>
                            <CardBody>
                                <Table>
                                    <tbody>
                                        <tr>
                                            <td><strong>Motivo de Consulta </strong></td>
                                            <td>                                            
                                                <Search
                                                    data={this.state.motivoConsulta}
                                                    onChange={this.handleChange.bind(this)}
                                                    placeholder={this.props.detalleMotivoConsulta}
                                                    class="search-class"
                                                    searchKey="detalle"
                                                />
                                            </td>
                                        </tr>
                                    </tbody>
                                </Table>
                                <Button color="success" onClick={this.toggleSuccess} className="mr-1">Editar</Button>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                    className={'modal-success ' + this.props.className}>
                    <ModalHeader toggle={this.toggleSuccess}>Confirmación</ModalHeader>
                    <ModalBody>
                        ¿Desea guardar este registro?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={this.handleOnSubmit}>Aceptar</Button>{' '}
                        <Button color="secondary" onClick={this.toggleSuccess}>Cancelar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
                    className={'modal-warning ' + this.props.className}>
                    <ModalHeader toggle={this.toggleWarning}>Campo vacio</ModalHeader>
                    <ModalBody>
                        Verifique que no haya campos vacios
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleWarning}>Cerrar</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}
/*
<hr /> {' idMotivoC: ' + this.props.idMotivoC + ' idTieneMotivo: ' + this.props.idTieneMotivo }
*/
export default Editar;
