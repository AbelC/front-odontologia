import React, { Component } from 'react';
import { Table, CardBody, Row, CardHeader, Card, Col, Pagination, PaginationItem, PaginationLink } from 'reactstrap';

class ListaMotivoConsultaSE extends Component {
    //, Modal, ModalBody, ModalHeader, ModalFooter
    constructor(props) {
        super(props);
        this.state = {
            list_tieneMotivoConsulta: [],
            page: 1,
            eachPage: 5,
        }
        this.handleClick = this.handleClick.bind(this)
    }

    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/tieneMotivoConsultJoin/' + this.props.idA + '/');
            const list_tieneMotivoConsulta = await respuesta.json();
            this.setState({
                list_tieneMotivoConsulta
            });
        } catch (e) {
            console.log(e);
        }
    }
    handleClick(event) {
        this.setState({
            page: Number(event.target.id)
        });
    }
    render() {
        /*const motivoConsultaList = this.state.list_tieneMotivoConsulta.map(item => {
            return (
                <tr key={item.id}>
                    <td>{item.motivoConsulta_id.detalle}</td>
                    <td>{item.fecha_motivoConsulta}</td>
                </tr>
            )
        })*/
        /**************BEGIN PAGINACION (LISTA MOTIVO CONSULTA)***********/
        const { page, eachPage } = this.state;

        const last = page * eachPage;
        const first = last - eachPage;
        const listaMed = this.state.list_tieneMotivoConsulta.slice(first, last);

        //lista
        const motivoConsultaListPagination = listaMed.map((item, i) => {
            return (
                <tr key={item.id}>
                    <td>{item.motivoConsulta_id.detalle}</td>
                    <td>{item.fecha_motivoConsulta}</td>
                </tr>
            )
        });

        //para los numeros
        const pages = [];
        for (let i = 1; i <= Math.ceil(this.state.list_tieneMotivoConsulta.length / eachPage); i++) {
            pages.push(i);
        }

        const renderpages = pages.map(number => {
            if (number % 2 === 1) {
                return (
                    <PaginationItem key={number} active>
                        <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                    </PaginationItem>
                );
            }
            else {
                return (
                    <PaginationItem key={number}>
                        <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                    </PaginationItem>
                );
            }
        });
        /************END PAGINACION ((LISTA MOTIVO CONSULTA))***********/

        return (
            <div className="animated fadeIn">
                <Row>
                    <Col xs="12">
                        <h3><p align="center">MOTIVO DE CONSULTA</p></h3>
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify"></i><strong>MOTIVO DE CONSULTA</strong>

                                <div className="card-header-actions">
                                    <strong className="text-muted">Paciente: {this.props.nombres + ' ' + this.props.apellido_pat + ' ' + this.props.apellido_mat}</strong>
                                </div>
                            </CardHeader>
                            <CardBody>
                                <Row>
                                    <Col sm="12" md={{ size: 8, offset: 2 }}>
                                        <Table responsive hover bordered size="sm" style={{ 'textAlign': 'center' }}>
                                            <thead>
                                                <tr>
                                                    <th scope="col">Motivo de Consulta</th>
                                                    <th scope="col">Fechas de Motivos de consulta</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {motivoConsultaListPagination}
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md="12">
                                        <Pagination>
                                            {renderpages}
                                        </Pagination>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}
/* <td> <Button outline color="warning" href={'#/PrimeraConsulta/Alergia/Editar/' + item.id + '/' + item.alergia.descripcion + '/' + item.alergia.id} >Editar</Button></td> */
export default ListaMotivoConsultaSE;
