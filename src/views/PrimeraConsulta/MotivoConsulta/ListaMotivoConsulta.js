import React, { Component } from 'react';
import { Row, Col, Table, Button, CardBody, Modal, ModalBody, ModalFooter, ModalHeader, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import Editar from './Editar'

class ListaMotivoConsulta extends Component {
    //, Modal, ModalBody, ModalHeader, ModalFooter
    constructor(props) {
        super(props);
        this.state = {
            list_tieneMotivoConsulta: [],
            info: false,
            idTieneMotivo: 0,
            idMotivoC: 0,
            detalleMotivoConsulta: '',
            page: 1,
            eachPage: 5,
        }
        this.toggleInfo = this.toggleInfo.bind(this);
        this.changeID = this.changeID.bind(this);
        this.handleClick = this.handleClick.bind(this)
    }
    toggleInfo() {
        this.setState({
            info: !this.state.info,
        });
    }
    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/tieneMotivoConsultJoin/' + this.props.idA + '/');
            const list_tieneMotivoConsulta = await respuesta.json();
            this.setState({
                list_tieneMotivoConsulta
            });
        } catch (e) {
            console.log(e);
        }
    }
    changeID(idM, detalle, idTieneM) {
        //console.log('a== ', idM);
        //console.log('b== ', detalle);
        //console.log('c== ',c);
        this.setState({
            idTieneMotivo: idTieneM,
            idMotivoC: idM,
            detalleMotivoConsulta: detalle,
        });

    }
    handleClick(event) {
        this.setState({
            page: Number(event.target.id)
        });
    }
    render() {
        /**************** ESTA LISTA ES PARA EL IMPRIMIR ******************** */
        const motivoConsultaList = this.state.list_tieneMotivoConsulta.map((item, i) => {
            return (
                <tr key={item.id}>
                    <td style={{ 'width': '2px' }}>{i + 1}</td>
                    <td style={{ 'width': '100px' }}>{item.fecha_motivoConsulta}</td>
                    <td>{item.motivoConsulta_id.detalle}</td>
                    <td style={{ 'width': '40px' }}>
                        <Button outline color="warning" onClick={() => { this.toggleInfo(); this.changeID(item.motivoConsulta_id.id, item.motivoConsulta_id.detalle, item.id); }}>Editar</Button>
                    </td>
                </tr>
            )
        });

        /**************BEGIN PAGINACION (LISTA MOTIVO CONSULTA)***********/
        const { page, eachPage } = this.state;

        const last = page * eachPage;
        const first = last - eachPage;
        const listaMed = this.state.list_tieneMotivoConsulta.slice(first, last);

        //lista
        const motivoConsultaListPagination = listaMed.map((item, i) => {
            return (
                <tr key={item.id}>
                    <td style={{ 'width': '2px' }}>{i + 1}</td>
                    <td style={{ 'width': '100px' }}>{item.fecha_motivoConsulta}</td>
                    <td>{item.motivoConsulta_id.detalle}</td>
                    <td style={{ 'width': '40px' }}>
                        <Button outline color="warning" onClick={() => { this.toggleInfo(); this.changeID(item.motivoConsulta_id.id, item.motivoConsulta_id.detalle, item.id); }}>Editar</Button>
                    </td>
                </tr>
            )
        });

        //para los numeros
        const pages = [];
        for (let i = 1; i <= Math.ceil(this.state.list_tieneMotivoConsulta.length / eachPage); i++) {
            pages.push(i);
        }

        const renderpages = pages.map(number => {
            if (number % 2 === 1) {
                return (
                    <PaginationItem key={number} active>
                        <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                    </PaginationItem>
                );
            }
            else {
                return (
                    <PaginationItem key={number}>
                        <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                    </PaginationItem>
                );
            }
        });
        /************END PAGINACION ((LISTA MOTIVO CONSULTA))***********/

        return (
            <div>
                <CardBody>
                    <Row>
                        <Col sm="12" md={{ size: 8, offset: 2 }}>
                            <Table responsive hover bordered size="sm" style={{}}>
                                <thead>
                                    <tr>
                                        <th>Nro</th>
                                        <th scope="col">Fecha Motivo de Consulta</th>
                                        <th scope="col">Motivo de Consulta</th>
                                        <th scope="col">Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {motivoConsultaListPagination}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>


                    <Row>
                        <Col md="12">
                            <Pagination>
                                {renderpages}
                            </Pagination>
                        </Col>
                    </Row>
                    {/** Esta tabla es para el imprimir */}
                    <Table responsive hover bordered size="sm" style={{  }} hidden>
                        <thead>
                            <tr>
                                <th>Nro</th>
                                <th scope="col">Fecha Motivo de Consulta</th>
                                <th scope="col">Motivo de Consulta</th>
                                <th scope="col">Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            {motivoConsultaList}
                        </tbody>
                    </Table>
                </CardBody>

                <Modal isOpen={this.state.info} toggle={this.toggleInfo} className={'modal-info' + this.props.className}>
                    <ModalHeader toggle={this.toggleInfo}>Editar Motivo de Consulta</ModalHeader>
                    <ModalBody>
                        <Editar idMotivoC={this.state.idMotivoC} detalleMotivoConsulta={this.state.detalleMotivoConsulta} idTieneMotivo={this.state.idTieneMotivo} />
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggleInfo}>Cerrar</Button>{' '}
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}
/* 

<td> <Button outline color="warning" href={'#/PrimeraConsulta/Alergia/Editar/'+ item.id +'/'+ item.alergia.descripcion+ '/'+item.alergia.id} >Editar</Button></td> */
export default ListaMotivoConsulta;
