import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Form, FormGroup, Label, Button, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';
import Search from 'react-search-box';
import NuevoMotivoConsulta from './NuevoMotivoConsulta'
import ListaMotivoConsulta from './ListaMotivoConsulta'
var url = process.env.REACT_APP_API_URL + 'odontologia/antecedentes/v1/tieneMotivoConsulta/';
class MotivoConsulta extends Component {
    constructor(props) {
        super(props);
        this.state = {
            motivoConsulta: [],
            idMotivoC: 0,
            success: false,
            primary: false,
            warning: false,
            dangerNoti: false,
            infoNoti: false,
            swBottonSuccess: false,
            swBottonNuevoMC: false,
        };
        this.toggleSuccess = this.toggleSuccess.bind(this);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.togglePrimary = this.togglePrimary.bind(this);
        this.toggleWarning = this.toggleWarning.bind(this);
        this.toggleDangerNoti = this.toggleDangerNoti.bind(this);
        this.toggleInfoNoti = this.toggleInfoNoti.bind(this);

    }

    toggleDangerNoti() {
        this.setState({
            danger: !this.state.danger,
        });
        window.location.reload()
    }

    toggleInfoNoti() {
        this.setState({
            info: !this.state.info,
        });
        window.location.reload()
    }

    togglePrimary() {
        this.setState({
            primary: !this.state.primary,
        });
    }
    toggleWarning() {
        this.setState({
            warning: !this.state.warning,
        });
    }

    toggleSuccess() {
        if (this.state.idMotivoC !== 0) {
            this.setState({
                success: !this.state.success,
            });
        } else {
            this.setState({
                warning: !this.state.warning,
            });
        }

    }
    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL + 'odontologia/antecedentes/v1/motivoConsulta/');
            const motivoConsulta = await respuesta.json();
            this.setState({
                motivoConsulta,
            });
        } catch (e) {
            console.log(e);
        }
    }
    handleChange(value) {
        var idMotivoConsulta = 0;
        for (let i = 0; i < this.state.motivoConsulta.length; i++) {
            if (this.state.motivoConsulta[i].detalle === value) {
                idMotivoConsulta = this.state.motivoConsulta[i].id;
                break;
            }
        }
        if (idMotivoConsulta !== 0) {
            this.setState({ idMotivoC: idMotivoConsulta })
        } else {
            console.log('no')
        }

    }

    handleOnSubmit(e) {
        console.log('DATOS NO VACIOS')
        e.preventDefault()
        var options = {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit'
        };
        var d = new Date();
        var time = d.toLocaleString('es-bo', options);
        var date = time.split(' ')[0].split('/');
        time = time.split(' ')[1].split(':');
        var dateS = date[2] + '-' + date[1] + '-' + date[0];
        var timeS = time[0] + ':' + time[1] + ':' + time[2];
        //console.log('dateS: ', dateS)
        //console.log('timeS: ', timeS)
        //var dateFull = new Date(date[2], date[1], date[0], time[0], time[1], time[2]);
        //var dateS = dateFull.getFullYear() + '-' + dateFull.getMonth() + '-' + dateFull.getDate();  /*Date to send */
        //var timeS = dateFull.getHours() + ':' + dateFull.getMinutes() + ':' + dateFull.getSeconds();  /**Time to send */

        let data = {
            afiliado: this.props.idA,
            motivoConsulta_id: this.state.idMotivoC,
            fecha_motivoConsulta: dateS,
            hora: timeS,
        }
        try {
            fetch(url, {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(data), // data can be string or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(resp => {
                    if (resp.status === 201) {
                        this.setState({
                            infoNoti: true
                        })
                        //console.log('resp.status', resp.status)
                    } else {
                        //console.log('resp.status ERROR', resp.status)
                        this.setState({
                            dangerNoti: true
                        })
                    }
                })
            /*.then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => console.log('Success:', response));*/
            //window.location.reload();
        } catch (e) {
            console.log(e);
        }

        this.setState({
            success: !this.state.success,
        });
    }

    render() {
        //console.log('propsMC: ', this.props.idA)
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col xs="12">
                        <h3><p align="center">MOTIVO DE CONSULTA</p></h3>
                        <Card>
                            <CardHeader>
                                <i className="fa fa-plus-square"></i><strong>Motivo de Consulta</strong>
                                <div className="card-header-actions">
                                    <strong className="text-muted">Paciente: {this.props.nombres + ' ' + this.props.apellido_pat + ' ' + this.props.apellido_mat}</strong>
                                </div>
                            </CardHeader>
                            <CardBody>
                                <Form>
                                    <Row>
                                        <Col sm="8">
                                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                                <Label for="fecha" className="mr-sm-2">Descripción</Label>
                                                <Search
                                                    data={this.state.motivoConsulta}
                                                    onChange={this.handleChange.bind(this)}
                                                    placeholder="Buscar Motivos de Consulta"
                                                    class="search-class"
                                                    searchKey="detalle"
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col sm="4">
                                            <br />
                                            <Button color="link" onClick={this.togglePrimary}>Añadir nuevo</Button>
                                        </Col>
                                    </Row>
                                    <br />
                                    <Row>
                                        <Col></Col>
                                        <Col></Col>
                                        <Col></Col>
                                        <Col>
                                            <Button color="success" onClick={this.toggleSuccess} className="mr-1">Guardar</Button>
                                        </Col>

                                    </Row>
                                </Form>
                                <br />
                                <ListaMotivoConsulta idA={this.props.idA} />
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                    className={'modal-success ' + this.props.className}>
                    <ModalHeader toggle={this.toggleSuccess}>Confirmación</ModalHeader>
                    <ModalBody>
                        ¿Desea guardar este registro?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={this.handleOnSubmit}>Aceptar</Button>{' '}
                        <Button color="secondary" onClick={this.toggleSuccess}>Cancelar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
                    className={'modal-warning ' + this.props.className}>
                    <ModalHeader toggle={this.toggleWarning}>Campo vacio</ModalHeader>
                    <ModalBody>
                        Verifique que no haya campos vacios
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleWarning}>Cerrar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.primary} toggle={this.togglePrimary}
                    className={'modal-primary ' + this.props.className}>
                    <ModalHeader toggle={this.togglePrimary}>Nuevo Motivo de Consulta</ModalHeader>
                    <ModalBody>
                        <NuevoMotivoConsulta />
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.togglePrimary}>Cerrar</Button>
                    </ModalFooter>
                </Modal>

        {/*MODALES DE NOTIFICACION SI SE REGISTRO CORRECTAMENTE LOS DATOS*/}
                <Modal isOpen={this.state.dangerNoti} toggle={this.toggleDangerNoti}
                    className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDangerNoti}><strong>Advertencia</strong></ModalHeader>
                    <ModalBody>
                        Los datos NO fueron registrados correctamente, verifique e intente nuevamente.
                    </ModalBody>
                    <ModalFooter>                        
                        <Button color="secondary" onClick={this.toggleDangerNoti}>Cerrar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.infoNoti} toggle={this.toggleInfoNoti}
                    className={'modal-info ' + this.props.className}>
                    <ModalHeader toggle={this.toggleInfoNoti}><strong>Correcto</strong></ModalHeader>
                    <ModalBody>
                        Los datos fueron registrados correctamente.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleInfoNoti}>Cerrar</Button>
                    </ModalFooter>
                </Modal>




            </div>
        );
    }
}

export default MotivoConsulta;