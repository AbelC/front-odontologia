import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Form, FormGroup, Label, Button, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';

var url = process.env.REACT_APP_API_URL+'odontologia/antecedentes/v1/motivoConsulta/';
class NuevoMotivoConsulta extends Component {
    constructor(props) {
        super(props);
        this.state = {
            success: false,
            warning: false,
        };
        this.toggleSuccess = this.toggleSuccess.bind(this);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.toggleWarning = this.toggleWarning.bind(this);
    }
    toggleWarning() {
        this.setState({
            warning: !this.state.warning,
        });
    }

    toggleSuccess() {
        if(this.detalle.value !== ''){
            this.setState({
                success: !this.state.success,
            });
        }else{
            this.setState({
                warning: !this.state.warning,
            });
        }
    }
handleOnSubmit(e) {
        console.log('DATOS NO VACIOS')
        e.preventDefault()
        let data = {
            detalle: this.detalle.value,
        }

        try {
            fetch(url, {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(data), // data can be string or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => console.log('Success:', response));
            window.location.reload();
        } catch (e) {
            console.log(e);
        }

        this.setState({
            success: !this.state.success,
        });
    }

    render() {
        //console.log('propsMC: ', this.props.idA)
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col xs="12">
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify"></i><strong>Agregar Nuevo Motivo de Consulta</strong>
                            </CardHeader>
                            <CardBody>
                                <Form>
                                    <Row>
                                        <Col sm="12">
                                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                                <Label for="medicacion" className="mr-sm-2">Descripción</Label>
                                                <input type="text" className="form-control" placeholder="Descripcion"  ref={detalle => this.detalle = detalle}/>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <br />
                                    <Row>
                                        <Col></Col>
                                        <Col></Col>
                                        <Col></Col>
                                        <Col>
                                            <Button color="success" onClick={this.toggleSuccess} className="mr-1">Guardar</Button>
                                        </Col>
                                        
                                    </Row>
                                </Form>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                    className={'modal-success ' + this.props.className}>
                    <ModalHeader toggle={this.toggleSuccess}>Confirmación</ModalHeader>
                    <ModalBody>
                        ¿Desea guardar este registro?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={this.handleOnSubmit}>Si</Button>{' '}
                        <Button color="secondary" onClick={this.toggleSuccess}>No</Button>
                    </ModalFooter>
                </Modal>
                <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
                    className={'modal-warning ' + this.props.className}>
                    <ModalHeader toggle={this.toggleWarning}>Campo vacio</ModalHeader>
                    <ModalBody>
                        Verifique que no haya campos vacios
                    </ModalBody>
                    <ModalFooter>                       
                        <Button color="secondary" onClick={this.toggleWarning}>Cerrar</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default NuevoMotivoConsulta;