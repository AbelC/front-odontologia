import React, { Component } from 'react';
import {Alert, Table, Col, Row, Button, Card, CardBody, CardHeader, Modal, ModalBody, ModalFooter, ModalHeader, Popover, PopoverHeader, PopoverBody, FormGroup, Label } from 'reactstrap';
//import { AppSwitch } from '@coreui/react'

var optionesSVG = {
    stroke: 'navy',
    strokeWidth: '0.5',
    opacity: '1',
};
const points0 = { points: [[0, 0], [20, 0], [15, 5], [5, 5]], }
const points1 = { points: [[5, 15], [15, 15], [20, 20], [0, 20]], }
const points2 = { points: [[15, 5], [20, 0], [20, 20], [15, 15]], }
const points3 = { points: [[0, 0], [5, 5], [5, 15], [0, 20]], }
const points4 = { points: [[5, 5], [15, 5], [15, 15], [5, 15]] }
var optionsTEXT = {
    x: '6',
    y: '30',
    stroke: 'navy',
    fill: 'navi',
    strokeWidth: '0.1',
}
//style={{ ...cssTEXT, 'cursor': 'pointer' }}
var cssTEXT = {
    'fontSize': '6pt',
    'fontWeight': 'normal',
    'cursor': 'pointer'
}
//onContextMenu={this.handleOnContextMenu.bind(this)} 
//onClick={this.handleClick.bind(this)}
//{...optionsTEXT} style={{ ...cssTEXT, 'cursor': 'pointer' }}


//pieza 1.
const pieza11 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '1.1',
    patologia: 0,
    //idExamenClinico: 3,
};

const pieza12 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '1.2',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza13 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '1.3',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza14 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '1.4',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza15 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '1.5',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza16 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '1.6',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza17 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '1.7',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza18 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '1.8',
    patologia: 0,
    //idExamenClinico: 3,
};
//pieza 2.
const pieza21 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '2.1',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza22 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '2.2',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza23 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '2.3',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza24 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '2.4',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza25 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '2.5',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza26 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '2.6',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza27 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '2.7',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza28 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '2.8',
    patologia: 0,
    //idExamenClinico: 3,
};
//piezas 3.
const pieza31 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '3.1',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza32 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '3.2',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza33 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '3.3',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza34 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '3.4',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza35 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '3.5',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza36 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '3.6',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza37 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '3.7',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza38 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '3.8',
    patologia: 0,
    //idExamenClinico: 3,
};
//piezas 4.
const pieza41 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '4.1',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza42 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '4.2',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza43 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '4.3',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza44 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '4.4',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza45 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '4.5',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza46 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '4.6',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza47 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '4.7',
    patologia: 0,
    //idExamenClinico: 3,
};
const pieza48 = {
    vestubular: 'white',
    distal: 'white',
    palatina: 'white',
    mesial: 'white',
    oclusal: 'white',
    pieza_dental: '4.8',
    patologia: 0,
    //idExamenClinico: 3,
};

var allPieces = [pieza11, pieza12, pieza13, pieza14, pieza15, pieza16, pieza17, pieza18,
    pieza21, pieza22, pieza23, pieza24, pieza25, pieza26, pieza27, pieza28,
    pieza31, pieza32, pieza33, pieza34, pieza35, pieza36, pieza37, pieza38,
    pieza41, pieza42, pieza43, pieza44, pieza45, pieza46, pieza47, pieza48,];
var tableShow = [];
var url = process.env.REACT_APP_API_URL + 'odontologia/examenclinico/v1/odontograma/';

class Odontograma extends Component {
    constructor(props) {
        super(props);
        this.asignaPatologia = [];
        this.state = {
            clicked: false,
            rightClicked: false,
            success: false,
            popoverOpen: false,
            tar: "P-18",
            idOp: null,


            tableShow1: [],
            tableShow2: [],
            tableShow3: [],
            tableShow4: [],

            idEx: 0,
         
            contSendOdon: 0,
            contDontSend: 0,
        };
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.toggleSuccess = this.toggleSuccess.bind(this);
        this.toggle = this.toggle.bind(this);

        this.handlePatologia = this.handlePatologia.bind(this);
        
        //this.handleMouseEnter = this.handleMouseEnter.bind(this)
        //this.handleMouseLeave = this.handleMouseLeave.bind(this)
        //this.handleClick = this.handleClick.bind(this) //azul
        //this.handleOnContextMenu = this.handleOnContextMenu.bind(this) //rojo
    }

    toggleSuccess() {
        this.setState({
            success: !this.state.success,
        });
    }
    toggle() {
        this.setState({
            popoverOpen: !this.state.popoverOpen
        });
    }

    fEliminar1(i) {
        const nuevo = this.state.tableShow1.filter(tableShow1 => {
            return tableShow1 !== i
        })

        if (this.state.tableShow1.length !== 1) {
            this.setState({
                tableShow1: [...nuevo],
            })
        }
        else {
            this.setState({
                tableShow1: [...nuevo],
            })
        }
    }

    fEliminar2(i) {
        const nuevo = this.state.tableShow2.filter(tableShow2 => {
            return tableShow2 !== i
        })

        if (this.state.tableShow2.length !== 1) {
            this.setState({
                tableShow2: [...nuevo],
            })
        }
        else {
            this.setState({
                tableShow2: [...nuevo],
            })
        }
    }
    fEliminar3(i) {
        const nuevo = this.state.tableShow3.filter(tableShow3 => {
            return tableShow3 !== i
        })

        if (this.state.tableShow3.length !== 1) {
            this.setState({
                tableShow3: [...nuevo],
            })
        }
        else {
            this.setState({
                tableShow3: [...nuevo],
            })
        }
    }
    fEliminar4(i) {
        const nuevo = this.state.tableShow4.filter(tableShow4 => {
            return tableShow4 !== i
        })

        if (this.state.tableShow4.length !== 1) {
            this.setState({
                tableShow4: [...nuevo],
            })
        }
        else {
            this.setState({
                tableShow4: [...nuevo],
            })
        }
    }
    handlePatologia(e) {
        var idX = e.target.id;
        var patol;
        for (let i = 0; i < this.asignaPatologia.length; i++) {
            var check = this.asignaPatologia[i].checked;

            if (check) {
                //console.log('[]: ', this.asignaPatologia[i].value);
                switch (idX) {
                    //pieza 1.
                    case 'P-11':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow1: this.state.tableShow1.concat([patol]) })
                        break;
                    case 'P-12':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow1: this.state.tableShow1.concat([patol]) })
                        break;
                    case 'P-13':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow1: this.state.tableShow1.concat([patol]) })
                        break;
                    case 'P-14':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow1: this.state.tableShow1.concat([patol]) })
                        break;
                    case 'P-15':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow1: this.state.tableShow1.concat([patol]) })
                        break;
                    case 'P-16':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow1: this.state.tableShow1.concat([patol]) })
                        break;
                    case 'P-17':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow1: this.state.tableShow1.concat([patol]) })
                        break;
                    case 'P-18':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow1: this.state.tableShow1.concat([patol]) })
                        break;
                    //pieza 2.
                    case 'P-21':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow2: this.state.tableShow2.concat([patol]) })
                        break;
                    case 'P-22':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow2: this.state.tableShow2.concat([patol]) })
                        break;
                    case 'P-23':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow2: this.state.tableShow2.concat([patol]) })
                        break;
                    case 'P-24':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow2: this.state.tableShow2.concat([patol]) })
                        break;
                    case 'P-25':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow2: this.state.tableShow2.concat([patol]) })
                        break;
                    case 'P-26':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow2: this.state.tableShow2.concat([patol]) })
                        break;
                    case 'P-27':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow2: this.state.tableShow2.concat([patol]) })
                        break;
                    case 'P-28':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow2: this.state.tableShow2.concat([patol]) })
                        break;
                    //pieza 3.
                    case 'P-31':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow3: this.state.tableShow3.concat([patol]) })
                        break;
                    case 'P-32':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow3: this.state.tableShow3.concat([patol]) })
                        break;
                    case 'P-33':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow3: this.state.tableShow3.concat([patol]) })
                        break;
                    case 'P-34':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow3: this.state.tableShow3.concat([patol]) })
                        break;
                    case 'P-35':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow3: this.state.tableShow3.concat([patol]) })
                        break;
                    case 'P-36':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow3: this.state.tableShow3.concat([patol]) })
                        break;
                    case 'P-37':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow3: this.state.tableShow3.concat([patol]) })
                        break;
                    case 'P-38':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow3: this.state.tableShow3.concat([patol]) })
                        break;
                    //pieza 4.
                    case 'P-41':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow4: this.state.tableShow4.concat([patol]) })
                        break;
                    case 'P-42':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow4: this.state.tableShow4.concat([patol]) })
                        break;
                    case 'P-43':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow4: this.state.tableShow4.concat([patol]) })
                        break;
                    case 'P-44':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow4: this.state.tableShow4.concat([patol]) })
                        break;
                    case 'P-45':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow4: this.state.tableShow4.concat([patol]) })
                        break;
                    case 'P-46':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow4: this.state.tableShow4.concat([patol]) })
                        break;
                    case 'P-47':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow4: this.state.tableShow4.concat([patol]) })
                        break;
                    case 'P-48':
                        patol = { pieza: idX, patologia: this.asignaPatologia[i].value, };
                        this.setState({ tableShow4: this.state.tableShow4.concat([patol]) })
                        break;
                    default:
                        console.log('error de PATOLOGIA')
                }
            }
        }
        this.setState({ popoverOpen: !this.state.popoverOpen });
    }

    handleOnSubmit() {
        //e.preventDefault() 
        //console.log('lenght: ', allPieces.length)

        /*console.log('show1: ', this.state.tableShow1)
        console.log('show2: ', this.state.tableShow2)
        console.log('show3: ', this.state.tableShow3)
        console.log('show4: ', this.state.tableShow4)*/
        tableShow = tableShow.concat(this.state.tableShow1);
        tableShow = tableShow.concat(this.state.tableShow2);
        tableShow = tableShow.concat(this.state.tableShow3);
        tableShow = tableShow.concat(this.state.tableShow4);
        console.log('show: ', tableShow)
        /*console.log('tableShow.lenght: ', tableShow.length);
        for (let j = 0; j < tableShow.length; j++) {
            console.log('tableShow: ', tableShow[j]);
        }*/

        for (let j = 0; j < tableShow.length; j++) {
            switch (tableShow[j].pieza) {
                //pieza 1.
                case 'P-11':
                    pieza11.patologia = tableShow[j].patologia;
                    break;
                case 'P-12':
                    pieza12.patologia = tableShow[j].patologia;
                    break;
                case 'P-13':
                    pieza13.patologia = tableShow[j].patologia;
                    break;
                case 'P-14':
                    pieza14.patologia = tableShow[j].patologia;
                    break;
                case 'P-15':
                    pieza15.patologia = tableShow[j].patologia;
                    break;
                case 'P-16':
                    pieza16.patologia = tableShow[j].patologia;
                    break;
                case 'P-17':
                    pieza17.patologia = tableShow[j].patologia;
                    break;
                case 'P-18':
                    pieza18.patologia = tableShow[j].patologia;
                    break;
                //pieza 2.
                case 'P-21':
                    pieza21.patologia = tableShow[j].patologia;
                    break;
                case 'P-22':
                    pieza22.patologia = tableShow[j].patologia;
                    break;
                case 'P-23':
                    pieza23.patologia = tableShow[j].patologia;
                    break;
                case 'P-24':
                    pieza24.patologia = tableShow[j].patologia;
                    break;
                case 'P-25':
                    pieza25.patologia = tableShow[j].patologia;
                    break;
                case 'P-26':
                    pieza26.patologia = tableShow[j].patologia;
                    break;
                case 'P-27':
                    pieza27.patologia = tableShow[j].patologia;
                    break;
                case 'P-28':
                    pieza28.patologia = tableShow[j].patologia;
                    break;

                //pieza 3.
                case 'P-31':
                    pieza31.patologia = tableShow[j].patologia;
                    break;
                case 'P-32':
                    pieza32.patologia = tableShow[j].patologia;
                    break;
                case 'P-33':
                    pieza33.patologia = tableShow[j].patologia;
                    break;
                case 'P-34':
                    pieza34.patologia = tableShow[j].patologia;
                    break;
                case 'P-35':
                    pieza35.patologia = tableShow[j].patologia;
                    break;
                case 'P-36':
                    pieza36.patologia = tableShow[j].patologia;
                    break;
                case 'P-37':
                    pieza37.patologia = tableShow[j].patologia;
                    break;
                case 'P-38':
                    pieza38.patologia = tableShow[j].patologia;
                    break;

                //pieza 4.
                case 'P-41':
                    pieza41.patologia = tableShow[j].patologia;
                    break;
                case 'P-42':
                    pieza42.patologia = tableShow[j].patologia;
                    break;
                case 'P-43':
                    pieza43.patologia = tableShow[j].patologia;
                    break;
                case 'P-44':
                    pieza44.patologia = tableShow[j].patologia;
                    break;
                case 'P-45':
                    pieza45.patologia = tableShow[j].patologia;
                    break;
                case 'P-46':
                    pieza46.patologia = tableShow[j].patologia;
                    break;
                case 'P-47':
                    pieza47.patologia = tableShow[j].patologia;
                    break;
                case 'P-48':
                    pieza48.patologia = tableShow[j].patologia;
                    break;
                default:
                    console.log('error al asignar PATOLOGIA')
            }
        }


        console.log('lenght.allPieces: ', allPieces.length)
        for (let i = 0; i < allPieces.length; i++) {
            let data = {
                vestibular: allPieces[i].vestubular,
                distal: allPieces[i].distal,
                palatina: allPieces[i].palatina,
                mesial: allPieces[i].mesial,
                oclusal: allPieces[i].oclusal,
                patologia: allPieces[i].patologia,
                pieza_dental: allPieces[i].pieza_dental,
                //idExamenClinico: allPieces[i].idExamenClinico,
                idExamenClinico: this.props.idEXSent,
            }
            try {
                fetch(url, {
                    method: 'POST', // or 'PUT'
                    body: JSON.stringify(data), // data can be string or {object}!
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(resp => {
                    if (resp.status === 201) {
                        console.log('resp.status', resp.status, 'pieza: ', data.pieza_dental)
                        this.setState({
                            contSendOdon : this.state.contSendOdon + 1
                        })                        
                    } else {
                        console.log('resp.status ERROR', resp.status, 'pieza: ', data.pieza_dental)                     
                        this.setState({
                            contDontSend : this.state.contDontSend + 1
                        })
                    }                 
                })
                /*.then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(response => console.log('Success:', response))*/
                //window.location.reload();
            } catch (e) {
                console.log(e);
            }
        }


        /*for (let i = 0; i < allPieces.length; i++) {
           let data = {
               vestibular: allPieces[i].vestubular,
               distal: allPieces[i].distal,
               palatina: allPieces[i].palatina,
               mesial: allPieces[i].mesial,
               oclusal: allPieces[i].oclusal,
               patologia: allPieces[i].patologia,
               pieza_dental: allPieces[i].pieza_dental,
               idExamenClinico: allPieces[i].idExamenClinico,
           } 
           console.log('data: ', data)
       }*/
        
        this.setState({
            success: !this.state.success,
        });
    }



    handleClick(e) {
        e.preventDefault()

        var pieza = e.target.id;
        var strArray = pieza.split('-');
        var opColor = '';
        var clicked2 = !this.state.clicked

        if (clicked2) {
            e.target.setAttribute('fill', 'blue');
            opColor = 'blue';
        } else {
            e.target.setAttribute('fill', 'white');
            opColor = 'white';
        }

        switch (strArray[0]) {
            //pieza 1.
            case '1.1':
                if (strArray[1] === 'V') { console.log('opColor11 V: ', opColor); pieza11.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor11 P: ', opColor); pieza11.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor11 M: ', opColor); pieza11.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor11 D: ', opColor); pieza11.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor11 O: ', opColor); pieza11.oclusal = opColor; }

                break;
            case '1.2':
                if (strArray[1] === 'V') { console.log('opColor12 V: ', opColor); pieza12.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor12 P: ', opColor); pieza12.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor12 M: ', opColor); pieza12.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor12 D: ', opColor); pieza12.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor12 O: ', opColor); pieza12.oclusal = opColor; }

                break;
            case '1.3':
                if (strArray[1] === 'V') { console.log('opColor13 V: ', opColor); pieza13.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor13 P: ', opColor); pieza13.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor13 M: ', opColor); pieza13.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor13 D: ', opColor); pieza13.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor13 O: ', opColor); pieza13.oclusal = opColor; }

                break;
            case '1.4':
                if (strArray[1] === 'V') { console.log('opColor14 V: ', opColor); pieza14.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor14 P: ', opColor); pieza14.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor14 M: ', opColor); pieza14.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor14 D: ', opColor); pieza14.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor14 O: ', opColor); pieza14.oclusal = opColor; }

                break;
            case '1.5':
                if (strArray[1] === 'V') { console.log('opColor15 V: ', opColor); pieza15.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor15 P: ', opColor); pieza15.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor15 M: ', opColor); pieza15.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor15 D: ', opColor); pieza15.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor15 O: ', opColor); pieza15.oclusal = opColor; }
                break;
            case '1.6':
                if (strArray[1] === 'V') { console.log('opColor16 V: ', opColor); pieza16.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor16 P: ', opColor); pieza16.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor16 M: ', opColor); pieza16.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor16 D: ', opColor); pieza16.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor16 O: ', opColor); pieza16.oclusal = opColor; }

                break;
            case '1.7':
                if (strArray[1] === 'V') { console.log('opColor17 V: ', opColor); pieza17.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor17 P: ', opColor); pieza17.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor17 M: ', opColor); pieza17.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor17 D: ', opColor); pieza17.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor17 O: ', opColor); pieza17.oclusal = opColor; }

                break;
            case '1.8':
                if (strArray[1] === 'V') { console.log('opColor18 V: ', opColor); pieza18.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor18 P: ', opColor); pieza18.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor18 M: ', opColor); pieza18.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor18 D: ', opColor); pieza18.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor18 O: ', opColor); pieza18.oclusal = opColor; }
                break;

            //pieza 2.
            case '2.1':
                if (strArray[1] === 'V') { console.log('opColor21 V: ', opColor); pieza21.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor21 P: ', opColor); pieza21.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor21 M: ', opColor); pieza21.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor21 D: ', opColor); pieza21.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor21 O: ', opColor); pieza21.oclusal = opColor; }

                break;
            case '2.2':
                if (strArray[1] === 'V') { console.log('opColor22 V: ', opColor); pieza22.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor22 P: ', opColor); pieza22.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor22 M: ', opColor); pieza22.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor22 D: ', opColor); pieza22.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor22 O: ', opColor); pieza22.oclusal = opColor; }

                break;
            case '2.3':
                if (strArray[1] === 'V') { console.log('opColor23 V: ', opColor); pieza23.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor23 P: ', opColor); pieza23.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor23 M: ', opColor); pieza23.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor23 D: ', opColor); pieza23.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor23 O: ', opColor); pieza23.oclusal = opColor; }

                break;
            case '2.4':
                if (strArray[1] === 'V') { console.log('opColor24 V: ', opColor); pieza24.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor24 P: ', opColor); pieza24.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor24 M: ', opColor); pieza24.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor24 D: ', opColor); pieza24.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor24 O: ', opColor); pieza24.oclusal = opColor; }

                break;
            case '2.5':
                if (strArray[1] === 'V') { console.log('opColor25 V: ', opColor); pieza25.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor25 P: ', opColor); pieza25.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor25 M: ', opColor); pieza25.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor25 D: ', opColor); pieza25.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor25 O: ', opColor); pieza25.oclusal = opColor; }
                break;
            case '2.6':
                if (strArray[1] === 'V') { console.log('opColor26 V: ', opColor); pieza26.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor26 P: ', opColor); pieza26.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor26 M: ', opColor); pieza26.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor26 D: ', opColor); pieza26.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor26 O: ', opColor); pieza26.oclusal = opColor; }

                break;
            case '2.7':
                if (strArray[1] === 'V') { console.log('opColor27 V: ', opColor); pieza27.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor27 P: ', opColor); pieza27.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor27 M: ', opColor); pieza27.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor27 D: ', opColor); pieza27.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor27 O: ', opColor); pieza27.oclusal = opColor; }

                break;
            case '2.8':
                if (strArray[1] === 'V') { console.log('opColor28 V: ', opColor); pieza28.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor28 P: ', opColor); pieza28.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor28 M: ', opColor); pieza28.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor28 D: ', opColor); pieza28.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor28 O: ', opColor); pieza28.oclusal = opColor; }
                break;

            //pieza 3.
            case '3.1':
                if (strArray[1] === 'V') { console.log('opColor31 V: ', opColor); pieza31.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor31 P: ', opColor); pieza31.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor31 M: ', opColor); pieza31.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor31 D: ', opColor); pieza31.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor31 O: ', opColor); pieza31.oclusal = opColor; }

                break;
            case '3.2':
                if (strArray[1] === 'V') { console.log('opColor32 V: ', opColor); pieza32.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor32 P: ', opColor); pieza32.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor32 M: ', opColor); pieza32.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor32 D: ', opColor); pieza32.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor32 O: ', opColor); pieza32.oclusal = opColor; }

                break;
            case '3.3':
                if (strArray[1] === 'V') { console.log('opColor33 V: ', opColor); pieza33.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor33 P: ', opColor); pieza33.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor33 M: ', opColor); pieza33.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor33 D: ', opColor); pieza33.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor33 O: ', opColor); pieza33.oclusal = opColor; }

                break;
            case '3.4':
                if (strArray[1] === 'V') { console.log('opColor34 V: ', opColor); pieza34.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor34 P: ', opColor); pieza34.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor34 M: ', opColor); pieza34.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor34 D: ', opColor); pieza34.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor34 O: ', opColor); pieza34.oclusal = opColor; }

                break;
            case '3.5':
                if (strArray[1] === 'V') { console.log('opColor35 V: ', opColor); pieza35.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor35 P: ', opColor); pieza35.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor35 M: ', opColor); pieza35.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor35 D: ', opColor); pieza35.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor35 O: ', opColor); pieza35.oclusal = opColor; }
                break;
            case '3.6':
                if (strArray[1] === 'V') { console.log('opColor36 V: ', opColor); pieza36.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor36 P: ', opColor); pieza36.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor36 M: ', opColor); pieza36.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor36 D: ', opColor); pieza36.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor36 O: ', opColor); pieza36.oclusal = opColor; }

                break;
            case '3.7':
                if (strArray[1] === 'V') { console.log('opColor37 V: ', opColor); pieza37.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor37 P: ', opColor); pieza37.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor37 M: ', opColor); pieza37.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor37 D: ', opColor); pieza37.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor37 O: ', opColor); pieza37.oclusal = opColor; }

                break;
            case '3.8':
                if (strArray[1] === 'V') { console.log('opColor38 V: ', opColor); pieza38.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor38 P: ', opColor); pieza38.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor38 M: ', opColor); pieza38.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor38 D: ', opColor); pieza38.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor38 O: ', opColor); pieza38.oclusal = opColor; }
                break;

            //pieza 4.
            case '4.1':
                if (strArray[1] === 'V') { console.log('opColor41 V: ', opColor); pieza41.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor41 P: ', opColor); pieza41.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor41 M: ', opColor); pieza41.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor41 D: ', opColor); pieza41.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor41 O: ', opColor); pieza41.oclusal = opColor; }

                break;
            case '4.2':
                if (strArray[1] === 'V') { console.log('opColor42 V: ', opColor); pieza42.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor42 P: ', opColor); pieza42.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor42 M: ', opColor); pieza42.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor42 D: ', opColor); pieza42.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor42 O: ', opColor); pieza42.oclusal = opColor; }

                break;
            case '4.3':
                if (strArray[1] === 'V') { console.log('opColor43 V: ', opColor); pieza43.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor43 P: ', opColor); pieza43.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor43 M: ', opColor); pieza43.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor43 D: ', opColor); pieza43.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor43 O: ', opColor); pieza43.oclusal = opColor; }

                break;
            case '4.4':
                if (strArray[1] === 'V') { console.log('opColor44 V: ', opColor); pieza44.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor44 P: ', opColor); pieza44.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor44 M: ', opColor); pieza44.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor44 D: ', opColor); pieza44.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor44 O: ', opColor); pieza44.oclusal = opColor; }

                break;
            case '4.5':
                if (strArray[1] === 'V') { console.log('opColor45 V: ', opColor); pieza45.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor45 P: ', opColor); pieza45.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor45 M: ', opColor); pieza45.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor45 D: ', opColor); pieza45.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor45 O: ', opColor); pieza45.oclusal = opColor; }
                break;
            case '4.6':
                if (strArray[1] === 'V') { console.log('opColor46 V: ', opColor); pieza46.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor46 P: ', opColor); pieza46.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor46 M: ', opColor); pieza46.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor46 D: ', opColor); pieza46.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor46 O: ', opColor); pieza46.oclusal = opColor; }
                break;
            case '4.7':
                if (strArray[1] === 'V') { console.log('opColor47 V: ', opColor); pieza47.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor47 P: ', opColor); pieza47.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor47 M: ', opColor); pieza47.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor47 D: ', opColor); pieza47.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor47 O: ', opColor); pieza47.oclusal = opColor; }
                break;
            case '4.8':
                if (strArray[1] === 'V') { console.log('opColor48 V: ', opColor); pieza48.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor48 P: ', opColor); pieza48.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor48 M: ', opColor); pieza48.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor48 D: ', opColor); pieza48.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor48 O: ', opColor); pieza48.oclusal = opColor; }
                break;
            default:
                console.log('error de odontograma AZUL')
        }


        this.setState({
            clicked: clicked2
        })
        //console.log('after set click: ',this.state.clicked)
    }

    // right click
    handleOnContextMenu(e) {
        e.preventDefault()

        /* var rightClicked = null
        if (this.state.rightClicked === false) {
        rightClicked = !this.state.rightClicked
        } else {
        rightClicked = this.state.rightClicked
         }*/


        var pieza = e.target.id;
        var strArray = pieza.split('-');
        var opColor = '';

        var rightClicked = !this.state.rightClicked

        if (rightClicked) {
            e.target.setAttribute('fill', 'red')
            opColor = 'red';
        } else {
            e.target.setAttribute('fill', 'white')
            opColor = 'white';
        }
        switch (strArray[0]) {
            //pieza 1.
            case '1.1':
                if (strArray[1] === 'V') { console.log('opColor11 V: ', opColor); pieza11.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor11 P: ', opColor); pieza11.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor11 M: ', opColor); pieza11.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor11 D: ', opColor); pieza11.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor11 O: ', opColor); pieza11.oclusal = opColor; }

                break;
            case '1.2':
                if (strArray[1] === 'V') { console.log('opColor12 V: ', opColor); pieza12.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor12 P: ', opColor); pieza12.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor12 M: ', opColor); pieza12.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor12 D: ', opColor); pieza12.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor12 O: ', opColor); pieza12.oclusal = opColor; }

                break;
            case '1.3':
                if (strArray[1] === 'V') { console.log('opColor13 V: ', opColor); pieza13.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor13 P: ', opColor); pieza13.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor13 M: ', opColor); pieza13.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor13 D: ', opColor); pieza13.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor13 O: ', opColor); pieza13.oclusal = opColor; }

                break;
            case '1.4':
                if (strArray[1] === 'V') { console.log('opColor14 V: ', opColor); pieza14.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor14 P: ', opColor); pieza14.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor14 M: ', opColor); pieza14.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor14 D: ', opColor); pieza14.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor14 O: ', opColor); pieza14.oclusal = opColor; }

                break;
            case '1.5':
                if (strArray[1] === 'V') { console.log('opColor15 V: ', opColor); pieza15.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor15 P: ', opColor); pieza15.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor15 M: ', opColor); pieza15.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor15 D: ', opColor); pieza15.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor15 O: ', opColor); pieza15.oclusal = opColor; }
                break;
            case '1.6':
                if (strArray[1] === 'V') { console.log('opColor16 V: ', opColor); pieza16.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor16 P: ', opColor); pieza16.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor16 M: ', opColor); pieza16.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor16 D: ', opColor); pieza16.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor16 O: ', opColor); pieza16.oclusal = opColor; }

                break;
            case '1.7':
                if (strArray[1] === 'V') { console.log('opColor17 V: ', opColor); pieza17.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor17 P: ', opColor); pieza17.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor17 M: ', opColor); pieza17.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor17 D: ', opColor); pieza17.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor17 O: ', opColor); pieza17.oclusal = opColor; }

                break;
            case '1.8':
                if (strArray[1] === 'V') { console.log('opColor18 V: ', opColor); pieza18.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor18 P: ', opColor); pieza18.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor18 M: ', opColor); pieza18.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor18 D: ', opColor); pieza18.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor18 O: ', opColor); pieza18.oclusal = opColor; }
                break;

            //pieza 2.
            case '2.1':
                if (strArray[1] === 'V') { console.log('opColor21 V: ', opColor); pieza21.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor21 P: ', opColor); pieza21.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor21 M: ', opColor); pieza21.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor21 D: ', opColor); pieza21.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor21 O: ', opColor); pieza21.oclusal = opColor; }

                break;
            case '2.2':
                if (strArray[1] === 'V') { console.log('opColor22 V: ', opColor); pieza22.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor22 P: ', opColor); pieza22.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor22 M: ', opColor); pieza22.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor22 D: ', opColor); pieza22.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor22 O: ', opColor); pieza22.oclusal = opColor; }

                break;
            case '2.3':
                if (strArray[1] === 'V') { console.log('opColor23 V: ', opColor); pieza23.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor23 P: ', opColor); pieza23.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor23 M: ', opColor); pieza23.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor23 D: ', opColor); pieza23.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor23 O: ', opColor); pieza23.oclusal = opColor; }

                break;
            case '2.4':
                if (strArray[1] === 'V') { console.log('opColor24 V: ', opColor); pieza24.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor24 P: ', opColor); pieza24.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor24 M: ', opColor); pieza24.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor24 D: ', opColor); pieza24.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor24 O: ', opColor); pieza24.oclusal = opColor; }

                break;
            case '2.5':
                if (strArray[1] === 'V') { console.log('opColor25 V: ', opColor); pieza25.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor25 P: ', opColor); pieza25.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor25 M: ', opColor); pieza25.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor25 D: ', opColor); pieza25.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor25 O: ', opColor); pieza25.oclusal = opColor; }
                break;
            case '2.6':
                if (strArray[1] === 'V') { console.log('opColor26 V: ', opColor); pieza26.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor26 P: ', opColor); pieza26.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor26 M: ', opColor); pieza26.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor26 D: ', opColor); pieza26.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor26 O: ', opColor); pieza26.oclusal = opColor; }

                break;
            case '2.7':
                if (strArray[1] === 'V') { console.log('opColor27 V: ', opColor); pieza27.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor27 P: ', opColor); pieza27.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor27 M: ', opColor); pieza27.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor27 D: ', opColor); pieza27.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor27 O: ', opColor); pieza27.oclusal = opColor; }

                break;
            case '2.8':
                if (strArray[1] === 'V') { console.log('opColor28 V: ', opColor); pieza28.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor28 P: ', opColor); pieza28.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor28 M: ', opColor); pieza28.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor28 D: ', opColor); pieza28.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor28 O: ', opColor); pieza28.oclusal = opColor; }
                break;

            //pieza 3.
            case '3.1':
                if (strArray[1] === 'V') { console.log('opColor31 V: ', opColor); pieza31.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor31 P: ', opColor); pieza31.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor31 M: ', opColor); pieza31.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor31 D: ', opColor); pieza31.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor31 O: ', opColor); pieza31.oclusal = opColor; }

                break;
            case '3.2':
                if (strArray[1] === 'V') { console.log('opColor32 V: ', opColor); pieza32.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor32 P: ', opColor); pieza32.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor32 M: ', opColor); pieza32.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor32 D: ', opColor); pieza32.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor32 O: ', opColor); pieza32.oclusal = opColor; }

                break;
            case '3.3':
                if (strArray[1] === 'V') { console.log('opColor33 V: ', opColor); pieza33.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor33 P: ', opColor); pieza33.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor33 M: ', opColor); pieza33.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor33 D: ', opColor); pieza33.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor33 O: ', opColor); pieza33.oclusal = opColor; }

                break;
            case '3.4':
                if (strArray[1] === 'V') { console.log('opColor34 V: ', opColor); pieza34.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor34 P: ', opColor); pieza34.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor34 M: ', opColor); pieza34.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor34 D: ', opColor); pieza34.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor34 O: ', opColor); pieza34.oclusal = opColor; }

                break;
            case '3.5':
                if (strArray[1] === 'V') { console.log('opColor35 V: ', opColor); pieza35.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor35 P: ', opColor); pieza35.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor35 M: ', opColor); pieza35.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor35 D: ', opColor); pieza35.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor35 O: ', opColor); pieza35.oclusal = opColor; }
                break;
            case '3.6':
                if (strArray[1] === 'V') { console.log('opColor36 V: ', opColor); pieza36.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor36 P: ', opColor); pieza36.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor36 M: ', opColor); pieza36.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor36 D: ', opColor); pieza36.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor36 O: ', opColor); pieza36.oclusal = opColor; }

                break;
            case '3.7':
                if (strArray[1] === 'V') { console.log('opColor37 V: ', opColor); pieza37.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor37 P: ', opColor); pieza37.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor37 M: ', opColor); pieza37.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor37 D: ', opColor); pieza37.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor37 O: ', opColor); pieza37.oclusal = opColor; }

                break;
            case '3.8':
                if (strArray[1] === 'V') { console.log('opColor38 V: ', opColor); pieza38.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor38 P: ', opColor); pieza38.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor38 M: ', opColor); pieza38.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor38 D: ', opColor); pieza38.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor38 O: ', opColor); pieza38.oclusal = opColor; }
                break;

            //pieza 4.
            case '4.1':
                if (strArray[1] === 'V') { console.log('opColor41 V: ', opColor); pieza41.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor41 P: ', opColor); pieza41.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor41 M: ', opColor); pieza41.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor41 D: ', opColor); pieza41.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor41 O: ', opColor); pieza41.oclusal = opColor; }

                break;
            case '4.2':
                if (strArray[1] === 'V') { console.log('opColor42 V: ', opColor); pieza42.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor42 P: ', opColor); pieza42.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor42 M: ', opColor); pieza42.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor42 D: ', opColor); pieza42.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor42 O: ', opColor); pieza42.oclusal = opColor; }

                break;
            case '4.3':
                if (strArray[1] === 'V') { console.log('opColor43 V: ', opColor); pieza43.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor43 P: ', opColor); pieza43.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor43 M: ', opColor); pieza43.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor43 D: ', opColor); pieza43.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor43 O: ', opColor); pieza43.oclusal = opColor; }

                break;
            case '4.4':
                if (strArray[1] === 'V') { console.log('opColor44 V: ', opColor); pieza44.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor44 P: ', opColor); pieza44.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor44 M: ', opColor); pieza44.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor44 D: ', opColor); pieza44.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor44 O: ', opColor); pieza44.oclusal = opColor; }

                break;
            case '4.5':
                if (strArray[1] === 'V') { console.log('opColor45 V: ', opColor); pieza45.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor45 P: ', opColor); pieza45.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor45 M: ', opColor); pieza45.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor45 D: ', opColor); pieza45.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor45 O: ', opColor); pieza45.oclusal = opColor; }
                break;
            case '4.6':
                if (strArray[1] === 'V') { console.log('opColor46 V: ', opColor); pieza46.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor46 P: ', opColor); pieza46.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor46 M: ', opColor); pieza46.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor46 D: ', opColor); pieza46.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor46 O: ', opColor); pieza46.oclusal = opColor; }

                break;
            case '4.7':
                if (strArray[1] === 'V') { console.log('opColor47 V: ', opColor); pieza47.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor47 P: ', opColor); pieza47.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor47 M: ', opColor); pieza47.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor47 D: ', opColor); pieza47.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor47 O: ', opColor); pieza47.oclusal = opColor; }

                break;
            case '4.8':
                if (strArray[1] === 'V') { console.log('opColor48 V: ', opColor); pieza48.vestubular = opColor; }
                if (strArray[1] === 'P') { console.log('opColor48 P: ', opColor); pieza48.palatina = opColor; }
                if (strArray[1] === 'M') { console.log('opColor48 M: ', opColor); pieza48.mesial = opColor; }
                if (strArray[1] === 'D') { console.log('opColor48 D: ', opColor); pieza48.distal = opColor; }
                if (strArray[1] === 'O') { console.log('opColor48 O: ', opColor); pieza48.oclusal = opColor; }
                break;
            default:
                console.log('error de odontograma rogo')
        }

        this.setState({
            rightClicked: rightClicked
        })
    };
    render() {

        //console.log('this.state.vestubular18 : ', this.state.vestubular18);
        //console.log('pieza18: ', pieza18pieza18)
        //console.log('allPieces: ', allPieces)
        // {this.setState({idEx:this.props.idEXSent})}


        const pat = <FormGroup check>
            <Row>
                <Col>
                    <Label check>
                        <input type="radio" name="op" value={0} ref={ref => this.asignaPatologia[0] = ref} />{' '}
                        SANO
                    </Label>
                </Col>
                <Col>
                    <Label check>
                        <input type="radio" name="op" value={4} ref={ref => this.asignaPatologia[4] = ref} />{' '}
                        RESTO RADICULAR
                    </Label>
                </Col>
            </Row>


            <Row>
                <Col>
                    <Label check>
                        <input type="radio" name="op" value={1} ref={ref => this.asignaPatologia[1] = ref} />{' '}
                        CARIES
                    </Label>
                </Col>
                <Col>
                    <Label check>
                        <input type="radio" name="op" value={5} ref={ref => this.asignaPatologia[5] = ref} />{' '}
                        FRACTURA
                    </Label>
                </Col>
            </Row>

            <Row>
                <Col>
                    <Label check>
                        <input type="radio" name="op" value={2} ref={ref => this.asignaPatologia[2] = ref} />{' '}
                        OBTURADO
                    </Label>
                </Col>
                <Col>
                    <Label check>
                        <input type="radio" name="op" value={6} ref={ref => this.asignaPatologia[6] = ref} />{' '}
                        CORONA
                    </Label>
                </Col>
            </Row>

            <Row>
                <Col>
                    <Label check>
                        <input type="radio" name="op" value={3} ref={ref => this.asignaPatologia[3] = ref} />{' '}
                        AUSENTE
                    </Label>
                </Col>
                <Col>
                    <Label check>
                        <input type="radio" name="op" value={7} ref={ref => this.asignaPatologia[7] = ref} />{' '}
                        NO CLASIFICABLE
                    </Label>
                </Col>
            </Row>
            <Row>
                <Col></Col>
                <Col></Col>
                <Col><Button color="link" id={this.state.idOp} onClick={this.handlePatologia}>añadir</Button></Col>
            </Row>

        </FormGroup>;

        return (
            <div className="animated fadeIn">
                <Row>
                    <Col xs="12">

                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify"></i><strong>Odontograma {/*' - ' + this.props.idEXSent*/}  </strong>
                                <div className="card-header-actions">
                                    <strong className="text-muted">Paciente: {this.props.nombres + ' ' + this.props.apellido_pat + ' ' + this.props.apellido_mat}</strong>
                                </div>
                            </CardHeader>
                            <CardBody>
                                <Row>
                                    <Popover placement="bottom-end" isOpen={this.state.popoverOpen} target={this.state.tar} toggle={this.toggle}>
                                        <PopoverHeader>Patologias {' ' + this.state.idOp}</PopoverHeader>
                                        <PopoverBody>
                                            {pat}
                                        </PopoverBody>
                                    </Popover>

                                    <Col sm="8">
                                        <svg style={{ width: '100%', height: '250px' }}>
                                            <g>
                                                {'Dientes de adulto'}
                                                <g transform="scale(1.5)">
                                                    <g transform="translate(0,0)">
                                                        <polygon {...points0} id="1.8-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="1.8-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="1.8-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="1.8-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="1.8-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-18" onClick={() => { this.toggle(); this.setState({ idOp: 'P-18', tar: 'P-18' }); }}>18</a></text>

                                                    </g>
                                                    <g transform="translate(25,0)">
                                                        <polygon {...points0} id="1.7-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="1.7-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="1.7-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="1.7-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="1.7-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-17" onClick={() => { this.toggle(); this.setState({ idOp: 'P-17', tar: 'P-17' }); }}>17</a></text>
                                                    </g>
                                                    <g transform="translate(50,0)">
                                                        <polygon {...points0} id="1.6-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="1.6-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="1.6-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="1.6-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="1.6-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-16" onClick={() => { this.toggle(); this.setState({ idOp: 'P-16', tar: 'P-16' }); }}>16</a></text>
                                                    </g>
                                                    <g transform="translate(75,0)">
                                                        <polygon {...points0} id="1.5-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="1.5-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="1.5-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="1.5-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="1.5-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-15" onClick={() => { this.toggle(); this.setState({ idOp: 'P-15', tar: 'P-15' }); }}>15</a></text>
                                                    </g>
                                                    <g transform="translate(100,0)">
                                                        <polygon {...points0} id="1.4-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="1.4-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="1.4-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="1.4-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="1.4-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-14" onClick={() => { this.toggle(); this.setState({ idOp: 'P-14', tar: 'P-14' }); }}>14</a></text>
                                                    </g>
                                                    <g transform="translate(125,0)">
                                                        <polygon {...points0} id="1.3-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="1.3-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="1.3-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="1.3-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="1.3-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-13" onClick={() => { this.toggle(); this.setState({ idOp: 'P-13', tar: 'P-13' }); }}>13</a></text>
                                                    </g>
                                                    <g transform="translate(150,0)">
                                                        <polygon {...points0} id="1.2-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="1.2-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="1.2-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="1.2-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="1.2-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-12" onClick={() => { this.toggle(); this.setState({ idOp: 'P-12', tar: 'P-12' }); }}>12</a></text>
                                                    </g>
                                                    <g transform="translate(175,0)">
                                                        <polygon {...points0} id="1.1-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="1.1-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="1.1-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="1.1-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="1.1-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-11" onClick={() => { this.toggle(); this.setState({ idOp: 'P-11', tar: 'P-11' }); }}>11</a></text>
                                                    </g>
                                                    <g transform="scale(1)">
                                                        <g transform="translate(210,0)">
                                                            <polygon {...points0} id="2.1-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points1} id="2.1-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points2} id="2.1-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points3} id="2.1-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points4} id="2.1-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-21" onClick={() => { this.toggle(); this.setState({ idOp: 'P-21', tar: 'P-21' }); }}>21</a></text>
                                                        </g>
                                                        <g transform="translate(235,0)">
                                                            <polygon {...points0} id="2.2-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points1} id="2.2-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points2} id="2.2-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points3} id="2.2-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points4} id="2.2-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-22" onClick={() => { this.toggle(); this.setState({ idOp: 'P-22', tar: 'P-22' }); }}>22</a></text>
                                                        </g>
                                                        <g transform="translate(260,0)">
                                                            <polygon {...points0} id="2.3-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points1} id="2.3-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points2} id="2.3-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points3} id="2.3-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points4} id="2.3-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-23" onClick={() => { this.toggle(); this.setState({ idOp: 'P-23', tar: 'P-23' }); }}>23</a></text>
                                                        </g>
                                                        <g transform="translate(285,0)">
                                                            <polygon {...points0} id="2.4-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points1} id="2.4-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points2} id="2.4-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points3} id="2.4-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points4} id="2.4-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-24" onClick={() => { this.toggle(); this.setState({ idOp: 'P-24', tar: 'P-24' }); }}>24</a></text>
                                                        </g>
                                                        <g transform="translate(310,0)">
                                                            <polygon {...points0} id="2.5-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points1} id="2.5-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points2} id="2.5-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points3} id="2.5-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points4} id="2.5-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-25" onClick={() => { this.toggle(); this.setState({ idOp: 'P-25', tar: 'P-25' }); }}>25</a></text>
                                                        </g>
                                                        <g transform="translate(335,0)">
                                                            <polygon {...points0} id="2.6-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points1} id="2.6-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points2} id="2.6-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points3} id="2.6-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points4} id="2.6-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-26" onClick={() => { this.toggle(); this.setState({ idOp: 'P-26', tar: 'P-26' }); }}>26</a></text>
                                                        </g>
                                                        <g transform="translate(360,0)">
                                                            <polygon {...points0} id="2.7-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points1} id="2.7-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points2} id="2.7-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points3} id="2.7-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points4} id="2.7-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-27" onClick={() => { this.toggle(); this.setState({ idOp: 'P-27', tar: 'P-27' }); }}>27</a></text>
                                                        </g>
                                                        <g transform="translate(385,0)">
                                                            <polygon {...points0} id="2.8-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points1} id="2.8-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points2} id="2.8-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points3} id="2.8-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <polygon {...points4} id="2.8-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                            <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-28" onClick={() => { this.toggle(); this.setState({ idOp: 'P-28', tar: 'P-28' }); }}>28</a></text>
                                                        </g>
                                                    </g>
                                                </g>
                                                <g transform="scale(1.5)">
                                                    <g transform="translate(210,40)">
                                                        <polygon {...points0} id="3.1-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="3.1-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="3.1-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="3.1-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="3.1-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-31" onClick={() => { this.toggle(); this.setState({ idOp: 'P-31', tar: 'P-31' }); }}>31</a></text>
                                                    </g>
                                                    <g transform="translate(235,40)">
                                                        <polygon {...points0} id="3.2-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="3.2-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="3.2-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="3.2-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="3.2-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-32" onClick={() => { this.toggle(); this.setState({ idOp: 'P-32', tar: 'P-32' }); }}>32</a></text>
                                                    </g>
                                                    <g transform="translate(260,40)">
                                                        <polygon {...points0} id="3.3-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="3.3-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="3.3-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="3.3-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="3.3-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-33" onClick={() => { this.toggle(); this.setState({ idOp: 'P-33', tar: 'P-33' }); }}>33</a></text>
                                                    </g>
                                                    <g transform="translate(285,40)">
                                                        <polygon {...points0} id="3.4-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="3.4-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="3.4-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="3.4-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="3.4-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-34" onClick={() => { this.toggle(); this.setState({ idOp: 'P-34', tar: 'P-34' }); }}>34</a></text>
                                                    </g>
                                                    <g transform="translate(310,40)">
                                                        <polygon {...points0} id="3.5-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="3.5-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="3.5-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="3.5-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="3.5-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-35" onClick={() => { this.toggle(); this.setState({ idOp: 'P-35', tar: 'P-35' }); }}>35</a></text>
                                                    </g>
                                                    <g transform="translate(335,40)">
                                                        <polygon {...points0} id="3.6-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="3.6-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="3.6-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="3.6-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="3.6-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-36" onClick={() => { this.toggle(); this.setState({ idOp: 'P-36', tar: 'P-36' }); }}>36</a></text>
                                                    </g>
                                                    <g transform="translate(360,40)">
                                                        <polygon {...points0} id="3.7-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="3.7-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="3.7-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="3.7-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="3.7-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-37" onClick={() => { this.toggle(); this.setState({ idOp: 'P-37', tar: 'P-37' }); }}>37</a></text>
                                                    </g>
                                                    <g transform="translate(385,40)">
                                                        <polygon {...points0} id="3.8-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="3.8-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="3.8-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="3.8-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="3.8-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-38" onClick={() => { this.toggle(); this.setState({ idOp: 'P-38', tar: 'P-38' }); }}>38</a></text>
                                                    </g>
                                                </g>
                                                <g transform="scale(1.5)">
                                                    <g transform="translate(0,40)">
                                                        <polygon {...points0} id="4.8-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="4.8-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="4.8-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="4.8-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="4.8-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-48" onClick={() => { this.toggle(); this.setState({ idOp: 'P-48', tar: 'P-48' }); }}>48</a></text>
                                                    </g>
                                                    <g transform="translate(25,40)">
                                                        <polygon {...points0} id="4.7-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="4.7-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="4.7-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="4.7-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="4.7-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-47" onClick={() => { this.toggle(); this.setState({ idOp: 'P-47', tar: 'P-47' }); }}>47</a></text>
                                                    </g>
                                                    <g transform="translate(50,40)">
                                                        <polygon {...points0} id="4.6-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="4.6-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="4.6-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="4.6-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="4.6-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-46" onClick={() => { this.toggle(); this.setState({ idOp: 'P-46', tar: 'P-46' }); }}>46</a></text>
                                                    </g>
                                                    <g transform="translate(75,40)">
                                                        <polygon {...points0} id="4.5-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="4.5-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="4.5-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="4.5-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="4.5-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-45" onClick={() => { this.toggle(); this.setState({ idOp: 'P-45', tar: 'P-45' }); }}>45</a></text>
                                                    </g>
                                                    <g transform="translate(100,40)">
                                                        <polygon {...points0} id="4.4-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="4.4-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="4.4-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="4.4-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="4.4-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-44" onClick={() => { this.toggle(); this.setState({ idOp: 'P-44', tar: 'P-44' }); }}>44</a></text>
                                                    </g>
                                                    <g transform="translate(125,40)">
                                                        <polygon {...points0} id="4.3-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="4.3-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="4.3-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="4.3-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="4.3-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-43" onClick={() => { this.toggle(); this.setState({ idOp: 'P-43', tar: 'P-43' }); }}>43</a></text>
                                                    </g>
                                                    <g transform="translate(150,40)">
                                                        <polygon {...points0} id="4.2-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="4.2-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="4.2-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="4.2-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="4.2-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-42" onClick={() => { this.toggle(); this.setState({ idOp: 'P-42', tar: 'P-42' }); }}>42</a></text>
                                                    </g>
                                                    <g transform="translate(175,40)">
                                                        <polygon {...points0} id="4.1-V" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points1} id="4.1-P" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points2} id="4.1-M" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points3} id="4.1-D" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <polygon {...points4} id="4.1-O" fill="white" {...optionesSVG} onContextMenu={this.handleOnContextMenu.bind(this)} onClick={this.handleClick.bind(this)} />
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}><a id="P-41" onClick={() => { this.toggle(); this.setState({ idOp: 'P-41', tar: 'P-41' }); }}>41</a></text>
                                                    </g>
                                                </g>
                                                {'Dientes de niño'}
                                                <g transform="scale(1.5)">
                                                    <g transform="translate(210,80)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>61</text>
                                                    </g>
                                                    <g transform="translate(235,80)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>62</text>
                                                    </g>
                                                    <g transform="translate(260,80)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>63</text>
                                                    </g>
                                                    <g transform="translate(285,80)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>64</text>
                                                    </g>
                                                    <g transform="translate(310,80)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>65</text>
                                                    </g>
                                                </g>
                                                <g transform="scale(1.5)">
                                                    <g transform="translate(75,80)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>55</text>
                                                    </g>
                                                    <g transform="translate(100,80)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>54</text>
                                                    </g>
                                                    <g transform="translate(125,80)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>53</text>
                                                    </g>
                                                    <g transform="translate(150,80)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>52</text>
                                                    </g>
                                                    <g transform="translate(175,80)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>51</text>
                                                    </g>
                                                </g>
                                                <g transform="scale(1.5)">
                                                    <g transform="translate(210,120)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>71</text>
                                                    </g>
                                                    <g transform="translate(235,120)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>72</text>
                                                    </g>
                                                    <g transform="translate(260,120)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>73</text>
                                                    </g>
                                                    <g transform="translate(285,120)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>74</text>
                                                    </g>
                                                    <g transform="translate(310,120)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>75</text>
                                                    </g>
                                                </g>
                                                <g transform="scale(1.5)">
                                                    <g transform="translate(75,120)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>85</text>
                                                    </g>
                                                    <g transform="translate(100,120)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>84</text>
                                                    </g>
                                                    <g transform="translate(125,120)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>83</text>
                                                    </g>
                                                    <g transform="translate(150,120)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>82</text>
                                                    </g>
                                                    <g transform="translate(175,120)">
                                                        <polygon points="0,0 20,0 15,5 5,5" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,15 15,15 20,20 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="15,5 20,0 20,20 15,15" fill="#fff" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="0,0 5,5 5,15 0,20" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <polygon points="5,5 15,5 15,15 5,15" fill="white" stroke="navy" strokeWidth="0.5" opacity="1"></polygon>
                                                        <text {...optionsTEXT} style={{ ...cssTEXT }}>81</text>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>

                                    </Col>
                                    <Col sm="4">
                                        <Table responsive hover bordered size="sm" style={{ width: '100%' }}>
                                            <tbody>
                                                <tr><td>0</td><td>SANO</td><td>4</td><td>RESTO RADICULAR</td></tr>
                                                <tr><td>1</td><td>CARIES</td><td>5</td><td>FRACTURA</td></tr>
                                                <tr><td>2</td><td>OBTURADO</td><td>6</td><td>CORONA</td></tr>
                                                <tr><td>3</td><td>AUSENTE</td><td>7</td><td>NO CLASIFICABLE</td></tr>
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>
                                <hr />
                                <Row>
                                    <Col>
                                        <center><p>Pieza 1.1 - 1.8</p></center>
                                        <Table responsive hover bordered size="sm" style={{ 'width': '100%', 'textAlign': 'center', 'fontSize': '12px' }}>
                                            <thead>
                                                <tr>
                                                    <th >Nro</th>
                                                    <th>Pieza</th>
                                                    <th>Patologia</th>
                                                    <th>Acción</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.tableShow1.map((u, i) => {
                                                    return (
                                                        <tr key={i}>
                                                            <td style={{ 'width': '1px' }}>{i + 1}</td>
                                                            <td style={{ 'width': '15px' }}>{u.pieza}</td>
                                                            <td style={{ 'width': '30px' }}>{
                                                                (u.patologia === '0') ? 'SANO' :
                                                                    (u.patologia === '1') ? 'CARIES' :
                                                                        (u.patologia === '2') ? 'OBTURADO' :
                                                                            (u.patologia === '3') ? 'AUSENTE' :
                                                                                (u.patologia === '4') ? 'RESTO RADICULAR' :
                                                                                    (u.patologia === '5') ? 'FRACTURA' :
                                                                                        (u.patologia === '6') ? 'CORONA' :
                                                                                            (u.patologia === '7') ? 'NO CLASIFICABLE' : null
                                                            }</td>
                                                            <td style={{ 'width': '15px' }}>
                                                                {/* <Button onClick={(e)=>this.fEditar(i)} type="button">Editar</Button> */}
                                                                <Button onClick={(e) => this.fEliminar1(u)} color="link" style={{ 'color': 'red', 'fontSize': '12px' }}>Eliminar</Button>
                                                            </td>
                                                        </tr>
                                                    )
                                                })}

                                            </tbody>
                                        </Table>
                                    </Col>

                                    <Col>
                                        <center><p>Pieza 2.1 - 2.8</p></center>
                                        <Table responsive hover bordered size="sm" style={{ 'width': '100%', 'textAlign': 'center', 'fontSize': '12px' }}>
                                            <thead>
                                                <tr>
                                                    <th >Nro</th>
                                                    <th>Pieza</th>
                                                    <th>Patologia</th>
                                                    <th>Acción</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                {this.state.tableShow2.map((u, i) => {
                                                    return (
                                                        <tr key={i}>
                                                            <td style={{ 'width': '1px' }}>{i + 1}</td>
                                                            <td style={{ 'width': '15px' }}>{u.pieza}</td>
                                                            <td style={{ 'width': '30px' }}>{
                                                                (u.patologia === '0') ? 'SANO' :
                                                                    (u.patologia === '1') ? 'CARIES' :
                                                                        (u.patologia === '2') ? 'OBTURADO' :
                                                                            (u.patologia === '3') ? 'AUSENTE' :
                                                                                (u.patologia === '4') ? 'RESTO RADICULAR' :
                                                                                    (u.patologia === '5') ? 'FRACTURA' :
                                                                                        (u.patologia === '6') ? 'CORONA' :
                                                                                            (u.patologia === '7') ? 'NO CLASIFICABLE' : null
                                                            }</td>
                                                            <td style={{ 'maxWidth': '25px' }}>
                                                                {/* <Button onClick={(e)=>this.fEditar(i)} type="button">Editar</Button> */}
                                                                <Button onClick={(e) => this.fEliminar2(u)} color="link" style={{ 'color': 'red', 'fontSize': '12px' }}>Eliminar</Button>
                                                            </td>
                                                        </tr>
                                                    )
                                                })}

                                            </tbody>
                                        </Table>
                                    </Col>

                                    <Col>
                                        <center><p>Pieza 3.1 - 3.8</p></center>
                                        <Table responsive hover bordered size="sm" style={{ 'width': '100%', 'textAlign': 'center', 'fontSize': '12px' }}>
                                            <thead>
                                                <tr>
                                                    <th >Nro</th>
                                                    <th>Pieza</th>
                                                    <th>Patologia</th>
                                                    <th>Acción</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                {this.state.tableShow3.map((u, i) => {
                                                    return (
                                                        <tr key={i}>
                                                            <td style={{ 'width': '1px' }}>{i + 1}</td>
                                                            <td style={{ 'width': '15px' }}>{u.pieza}</td>
                                                            <td style={{ 'width': '30px' }}>{
                                                                (u.patologia === '0') ? 'SANO' :
                                                                    (u.patologia === '1') ? 'CARIES' :
                                                                        (u.patologia === '2') ? 'OBTURADO' :
                                                                            (u.patologia === '3') ? 'AUSENTE' :
                                                                                (u.patologia === '4') ? 'RESTO RADICULAR' :
                                                                                    (u.patologia === '5') ? 'FRACTURA' :
                                                                                        (u.patologia === '6') ? 'CORONA' :
                                                                                            (u.patologia === '7') ? 'NO CLASIFICABLE' : null
                                                            }</td>
                                                            <td style={{ 'maxWidth': '25px' }}>
                                                                {/* <Button onClick={(e)=>this.fEditar(i)} type="button">Editar</Button> */}
                                                                <Button onClick={(e) => this.fEliminar3(u)} color="link" style={{ 'color': 'red', 'fontSize': '12px' }}>Eliminar</Button>
                                                            </td>
                                                        </tr>
                                                    )
                                                })}

                                            </tbody>
                                        </Table>
                                    </Col>

                                    <Col>
                                        <center><p>Pieza 4.1 - 4.8</p></center>
                                        <Table responsive hover bordered size="sm" style={{ 'width': '100%', 'textAlign': 'center', 'fontSize': '12px' }}>
                                            <thead>
                                                <tr>
                                                    <th>Nro</th>
                                                    <th>Pieza</th>
                                                    <th>Patologia</th>
                                                    <th>Acción</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.tableShow4.map((u, i) => {
                                                    return (
                                                        <tr key={i}>
                                                            <td style={{ 'width': '1px' }}>{i + 1}</td>
                                                            <td style={{ 'width': '15px' }}>{u.pieza}</td>
                                                            <td style={{ 'width': '30px' }}>{
                                                                (u.patologia === '0') ? 'SANO' :
                                                                    (u.patologia === '1') ? 'CARIES' :
                                                                        (u.patologia === '2') ? 'OBTURADO' :
                                                                            (u.patologia === '3') ? 'AUSENTE' :
                                                                                (u.patologia === '4') ? 'RESTO RADICULAR' :
                                                                                    (u.patologia === '5') ? 'FRACTURA' :
                                                                                        (u.patologia === '6') ? 'CORONA' :
                                                                                            (u.patologia === '7') ? 'NO CLASIFICABLE' : null
                                                            }</td>
                                                            <td style={{ 'maxWidth': '25px' }}>
                                                                {/* <Button onClick={(e)=>this.fEditar(i)} type="button">Editar</Button> */}
                                                                <Button onClick={(e) => this.fEliminar4(u)} color="link" style={{ 'color': 'red', 'fontSize': '12px' }}>Eliminar</Button>
                                                            </td>
                                                        </tr>
                                                    )
                                                })}
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm="12" md={{ size: 8, offset: 2 }}>
                                    {this.state.contSendOdon === 32 ? 
                                    <Alert style={{'textAlign': 'center'}} color="success">ODONTOGRAMA registrado con éxito.
                                        <Button color="link" href={'#/Tratamiento/RegistraTratamiento/' + this.props.idA + '/' + this.props.apellido_pat + '/' + this.props.apellido_mat + '/' + this.props.nombres}>Ir a Tratamiento</Button>
                                    </Alert>: <Button color="primary" block onClick={this.toggleSuccess}>Guardar odontograma</Button>
                                    }
                                    {this.state.contDontSend > 0 ? <Alert color="success">ODONTOGRAMA NO registrado con exito. </Alert> : null }
                                        
                                    </Col>
                                </Row>
                            </CardBody>
                            

                        </Card>
                    </Col>
                </Row>

                <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                    className={'modal-success ' + this.props.className}>
                    <ModalHeader toggle={this.toggleSuccess}>Confirmación</ModalHeader>
                    <ModalBody>
                        ¿Esta seguro de guardar el ODONTOGRAMA?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={this.handleOnSubmit} >Aceptar</Button>{' '}
                        <Button color="secondary" onClick={this.toggleSuccess}>Cancelar</Button>
                    </ModalFooter>
                </Modal>

                {/*MODALES DE NOTIFICACION SI SE REGISTRO CORRECTAMENTE LOS DATOS*/}

            {/*console.log('contSendOdon: ', this.state.contSendOdon, 'contDontSend: ', this.state.contDontSend)*/}
                
            </div >
        );
    }
    /*
    <Button color="link" href={'#/Tratamiento/RegistraTratamiento/' + this.props.idA + '/' + this.props.apellido_pat + '/' + this.props.apellido_mat + '/' + this.props.nombres}>Ir a Tratamiento</Button>
                            
    */
}
export default Odontograma;
