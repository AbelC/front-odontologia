import React from 'react'
import Polygon from './Polygon'
import Text from './Text'
//vestibular
//palatina
//mesial
//distal
//oclusal
const faces = {
  face0: [[0, 0], [20, 0], [15, 5], [5, 5]], 
  face1: [[5, 15], [15, 15], [20, 20], [0, 20]],
  face2: [[15, 5], [20, 0], [20, 20], [15, 15]],
  face3: [[0, 0], [5, 5], [5, 15], [0, 20]],
  face4: [[5, 5], [15, 5], [15, 15], [5, 15]]
}

class Piece extends React.Component {
  getPolygonProps (index) {
    return {
      key: index,
      points: this.getPoints(faces['face' + index]),
      fill: 'white',
      stroke: 'navy',
      strokeWidth: '0.5',
      opacity: '1'
    }
  }

  getPoints (pointsMatrix) {
    return pointsMatrix.join(' ')
  }

  renderPiece () {
    const polygon = []
    //const text=[]

    for (let i = 0; i < 5; i++) {
      polygon.push(<Polygon {...this.getPolygonProps(i)} /> )
      //text.push(<Text/>)
      //console.log('polygon: ', polygon)
      
    }
    //console.log('lenght: ', polygon.length)

    /*for (let j = 0; j < polygon.length; j++) {      
      console.log('polygon[j]: ', polygon[j])
    }
    console.log('=====================================')*/
    return polygon
  }

  render () {
    //console.log('props Pieces ', this.props.text)
    return (
      <g transform={this.props.translateTransform}>
        {this.renderPiece()}   
        <Text children={1}/>  
      </g>
    )
  }
}

export default Piece
