import React from 'react'
import Piece from './Piece'
//import Text from './Text'
//FOORMA LAS CASILLAS DE 8 

const T_ROW_PIECES = 8
const B_ROW_PIECES = 5
class PieceRow extends React.Component {
  getPieceProps(index) {
    return {
      key: index,
      translateTransform: `translate(${this.xAxisTranslate},${this.yAxisTranslate})`,
      text: `<text x="6" y="30" fill="navy" stroke="navy" strokeWidth="0.1" style={{ 'fontSize': '6pt', 'fontWeight': 'normal' }}>${index}</text>`
    }
  }

  renderPieceRow() {
    const pieces = []
    //const text=[]
    let piecesQuantity


    //esto es para el translate
    switch (this.props.rowPosition) {
      case 'L':
        this.xAxisTranslate = 0
        break
      case 'R':
        this.xAxisTranslate = 210
        break
      default:
        this.xAxisTranslate = null;
    }

    switch (this.props.rowId) {
      case '1':
        this.yAxisTranslate = 0
        break
      case '2':
        this.yAxisTranslate = 40
        break
      case '3':
        this.yAxisTranslate = 80
        break
      case '4':
        this.yAxisTranslate = 120
        break
      default:
        this.yAxisTranslate = null;
    }



    // esto es para definir el numero de piezas (adulto o niño)
    switch (this.props.rowType) {
      case 'T':
        piecesQuantity = T_ROW_PIECES
        break
      case 'B':
        piecesQuantity = B_ROW_PIECES
        break
      default:
        piecesQuantity = null;
    }
    //esto es para el TRASLATE de los dientes de niños
    if (this.props.rowType === 'B' && this.props.rowPosition === 'R') {
      this.xAxisTranslate = 210
    } else if (this.props.rowType === 'B' && this.props.rowPosition === 'L') {
      this.xAxisTranslate = 75
    }

    for (let i = 0; i < piecesQuantity; i++) {
      if (i > 0) this.xAxisTranslate += 25

      pieces.push(<Piece {...this.getPieceProps(i)} />)
      //text.push(<Text children={i}/>)
      //console.log('key: ', key)
    }
    //console.log('lenght: ', pieces.length)

    /*for (let j = 0; j < pieces.length; j++) {      
      console.log('pieces[j]: ', pieces[j])
    }
    console.log('=====================================')*/
    return pieces
  }


  render() {


    //  console.log('props PieceRow: ', this.props)
    return (
      <g transform='scale(1.5)'>
        {this.renderPieceRow()}   
    
      </g>

    )
  }
}
/**
  
 */
export default PieceRow
