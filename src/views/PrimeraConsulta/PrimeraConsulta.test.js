import React from 'react';
import ReactDOM from 'react-dom';
import PrimeraConsulta from './PrimeraConsulta';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PrimeraConsulta />, div);
  ReactDOM.unmountComponentAtNode(div);
});
