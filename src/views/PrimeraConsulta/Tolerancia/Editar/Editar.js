import React, { Component } from 'react';
import { Table, Button, Card, CardBody, CardHeader, Col, Row, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';



class Editar extends Component {

  constructor(props) {
    super(props)
    this.state = {
      tolerancia_v: [],
      hemorragia: '',
      tolerancia_antibioticos: '',
      hemo: '',
      tol: '',
      medico: '',
      afiliado: '',
      fecha_atencion: '',
      primary: false,
    }
    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)

    console.log('myID==', this.props)

  }
  async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/tolerancia/v1/tolerancia/' + this.props.id + '/');
      const tolerancia_v = await respuesta.json();
      this.setState({
        tolerancia_v
      });
    } catch (e) {
      console.log(e);
    }
  }


  handleOnSubmit(e) {
    e.preventDefault()
    let url = ''
    let data = {}
    url = process.env.REACT_APP_API_URL+'odontologia/tolerancia/v1/tolerancia/' + this.props.id + '/'
    data = {

      hemorragia: this.hemorragia.value,
      tolerancia_antibioticos: this.tolerancia_antibioticos.value,

      hora: this.state.tolerancia_v.hora,
      medico: this.state.tolerancia_v.medico,
      afiliado: this.state.tolerancia_v.afiliado,
      fecha_atencion: this.state.tolerancia_v.fecha_atencion
    }
    try {
      fetch(url, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response));
      window.location.reload();
    } catch (e) {
      console.log(e);
    }

    this.setState({
      primary: !this.state.primary,
    });

  }


  togglePrimary() {
    this.setState({
      primary: !this.state.primary,
    });
  }



  render() {
    /*document.write(this.props.match.params.id)*/
    return (




      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>

              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Editar Tolerancia</strong>
              </CardHeader>

              <CardBody>


                <Table responsive hover>
                  <tbody>
                    <tr>
                      <td><strong>Sufre hemorragia despues de una exodoncia</strong></td>

                      <td>
                        <select type="select" name="hemorragia" id="hemorragia" className="form-control" ref={hemorragia => this.hemorragia = hemorragia}>
                          <option value={this.state.tolerancia_v.hemorragia}>{this.state.tolerancia_v.hemorragia}</option>
                          <option value="SI">Si</option>
                          <option value="NO">No</option>
                          <option value="A VECES">A Veces</option>
                        </select>

                      </td>
                    </tr>
                    <tr>
                      <td><strong>Tolerancias antibioticos</strong></td>
                      <td>

                        <select type="select" className="form-control" name="tolerancia" id="tolerancia" ref={tolerancia_antibioticos => this.tolerancia_antibioticos = tolerancia_antibioticos}>
                          <option value={this.state.tolerancia_v.tolerancia_antibioticos}>{this.state.tolerancia_v.tolerancia_antibioticos}</option>
                          <option value="SI">Si</option>
                          <option value="NO">No</option>
                        </select>
                      </td>
                    </tr>

                  </tbody>
                </Table>

                <Button color="success" onClick={this.togglePrimary} className="mr-1">Editar</Button>

                <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-primary ' + this.props.className}>
                  <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
                  <ModalBody>
                    ¿Está seguro que quiere editar?
                </ModalBody>
                  <ModalFooter>
                    <Button onClick={this.handleOnSubmit} type="submit" color="primary">Aceptar</Button>{' '}
                    <Button color="secondary" onClick={this.togglePrimary}>Cancelar</Button>
                  </ModalFooter>
                </Modal>













              </CardBody>
            </Card>
          </Col>
        </Row>
















      </div>
    );
  }
}

export default Editar;
