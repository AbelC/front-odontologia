import React, { Component } from 'react';
import { Button, CardBody, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Editar from './Editar/Editar';

class ListaTolerancia extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listTolerancia: [],
            modal: false,
            id: '',
        }
        this.toggle = this.toggle.bind(this);
        this.changeID = this.changeID.bind(this);
    }

    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/tolerancia/v1/lista/tolerancia/afiliado/' + this.props.idA + '/');
            const listTolerancia = await respuesta.json();
            this.setState({
                listTolerancia
            });
        } catch (e) {
            console.log(e);
        }
    }


    toggle() {

        this.setState({
            modal: !this.state.modal,

        });
    }
    changeID(a) {
        //console.log('a== ',a);
        this.setState({
            id: a
        });

    }



    render() {
        return (


            <CardBody>

                <Table responsive hover bordered size="sm">
                    <thead>
                        <tr>
                            <th scope="col">Tolerancias antibioticos</th>
                            <th scope="col">Sufre hemorragia despues de una exodoncia</th>
                            <th scope="col">Acción</th>

                        </tr>
                    </thead>
                    <tbody>
                        {this.state.listTolerancia.map(item => (
                            <tr key={item.id}>
                                <td>{item.tolerancia_antibioticos}</td>
                                <td>{item.hemorragia}</td>
                                <td> <Button outline color="warning" onClick={() => { this.toggle(); this.changeID(item.id); }} >Editar</Button></td>

                            </tr>

                        ))
                        }


                    </tbody>
                </Table>

                <Modal isOpen={this.state.modal} toggle={this.toggle} className={'modal-info ' + this.props.className}>
                    <ModalHeader toggle={this.toggle}>Editar Tolerancia</ModalHeader>
                    <ModalBody>
                        {/*console.log('id-Modal=',this.state.id)*/}
                        <Editar id={this.state.id} />
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle}>Volver</Button>
                        <Button color="secondary" onClick={this.toggle}>Cerrar</Button>
                    </ModalFooter>
                </Modal>
            </CardBody>





        );
    }
} /* <td> <Button outline color="warning" href={'#/PrimeraConsulta/Tolerancia/Editar/' + item.id} >Editar</Button></td>
                                 */
export default ListaTolerancia;
