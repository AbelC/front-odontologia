import React, { Component } from 'react';
import { Button, DropdownItem, Card, CardBody, CardHeader, Col, Form, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import ListaTolerancia from './ListaTolerancia'

var url = process.env.REACT_APP_API_URL+'odontologia/tolerancia/v1/tolerancia/';

class Tolerancias extends Component {

  constructor(props) {
    super(props)

    this.state = {
      hemorragia: '',
      tolerancia_antibioticos: '',
      medico: '',
      afiliado: '',
      fecha_atencion: '',
      warning: false,
      primary: false,
      dangerNoti: false,
      infoNoti: false,
      idMedico:localStorage.getItem("idMedico"),
    }


    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.toggleWarning = this.toggleWarning.bind(this);
    this.toggleDangerNoti = this.toggleDangerNoti.bind(this);
    this.toggleInfoNoti = this.toggleInfoNoti.bind(this);
  }
  toggleDangerNoti() {
    this.setState({
      danger: !this.state.danger,
    });
    window.location.reload()
  }

  toggleInfoNoti() {
    this.setState({
      info: !this.state.info,
    });
    window.location.reload()
  }

  handleOnSubmit(e) {
    if (this.hemorragia.value !== '' && this.tolerancia_antibioticos.value !== '') {
      e.preventDefault()
      var options = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
    };
    var d = new Date();
    var time = d.toLocaleString('es-bo', options);
    var date = time.split(' ')[0].split('/');
    time = time.split(' ')[1].split(':');
    var dateS = date[2] + '-' + date[1] + '-' + date[0];
    var timeS = time[0] + ':' + time[1] + ':' + time[2];
    //console.log('dateS: ', dateS)
    //console.log('timeS: ', timeS)
    //var dateFull = new Date(date[2], date[1], date[0], time[0], time[1], time[2]);
    //var dateS = dateFull.getFullYear() + '-' + dateFull.getMonth() + '-' + dateFull.getDate();  /*Date to send */
    //var timeS = dateFull.getHours() + ':' + dateFull.getMinutes() + ':' + dateFull.getSeconds();  /**Time to send */

      let data = {
        hemorragia: this.hemorragia.value,
        tolerancia_antibioticos: this.tolerancia_antibioticos.value,
        medico: this.state.idMedico,
        afiliado: this.props.idA,
        fecha_atencion: dateS,
        hora:timeS,
      }

      try {
        fetch(url, {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(data), // data can be string or {object}!
          headers: {
            'Content-Type': 'application/json'
          }
        })
        .then(resp => {
          if (resp.status === 201) {
            this.setState({
              infoNoti: true
            })
            console.log('resp.status', resp.status)
          } else {
            //console.log('resp.status ERROR', resp.status)
            this.setState({
              dangerNoti: true
            })
          }
        })
        /*.then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response));*/
        //window.location.reload();
        this.setState({
          hemorragia: '',
          tolerancia_antibioticos: '',
          medico: '',
          afiliado: '',
          fecha_atencion: ''
        });



      } catch (e) {
        console.log(e);
      }
      this.setState({
        primary: !this.state.primary,
      });

    }
  }

  togglePrimary() {
    if (this.hemorragia.value !== '' && this.tolerancia_antibioticos.value !== '') {
      this.setState({ primary: !this.state.primary, });
    } else {
      this.setState({ warning: !this.state.warning, });

    }
  }

  toggleWarning() {
    this.setState({
      warning: !this.state.warning,
    });
  }
  render() {
    return (
      <Col xs="12" sm="6">
        <Card>
          <CardHeader>
            <i className="fa fa-align-justify"></i><strong>Tolerancias</strong>
            <div className="card-header-actions">
              <strong className="text-muted">Paciente: {this.props.nombres + ' ' + this.props.apellido_pat + ' ' + this.props.apellido_mat}</strong>
            </div>

          </CardHeader>
          <CardBody>
            <Form>

              <Row>
                <Col xs="6">
                  <FormGroup>
                    <Label htmlFor="ccnumber" className="mr-sm-2">Sufre hemorragia despues de una exodoncia</Label>
                    <select type="select" className="form-control" name="hemorragia" id="hemorragia" ref={hemorragia => this.hemorragia = hemorragia}>
                      <option value="">---</option>
                      <option value="SI">SI</option>
                      <option value="NO">NO</option>
                      <option value="A VECES">A VECES</option>
                      <option value="NO SABE">NO SABE</option>
                    </select>
                  </FormGroup></Col>
                <Col xs="6">
                <br/>
                  <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Label htmlFor="name" className="mr-sm-2">Tolerancias antibioticos</Label>
                    <select type="select" className="form-control" name="tolerancia" id="tolerancia" ref={tolerancia_antibioticos => this.tolerancia_antibioticos = tolerancia_antibioticos}>
                      <option value="">--</option>
                      <option value="SI">SI</option>
                      <option value="NO">NO</option>
                      <option value="NO SABE">NO SABE</option>
                    </select>
                  </FormGroup>
                </Col>
              </Row>
              
              <Button onClick={this.togglePrimary}>Guardar</Button>


              <Modal isOpen={this.state.primary} toggle={this.togglePrimary}
                className={'modal-primary ' + this.props.className}>
                <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
                <ModalBody>
                  ¿Está seguro de guardar, los datos introducidos?
                                </ModalBody>
                <ModalFooter>
                  <Button onClick={this.handleOnSubmit} type="submit" color="primary">Aceptar</Button>{' '}
                  <Button color="secondary" onClick={this.togglePrimary}>Cancelar</Button>
                </ModalFooter>
              </Modal>





            </Form>
            <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
              className={'modal-warning ' + this.props.className}>
              <ModalHeader toggle={this.toggleWarning}>Campos vacios</ModalHeader>
              <ModalBody>
                Llene los campos faltantes, para guardar el registro de Tolerancias del paciente.
                  </ModalBody>
              <ModalFooter>
                <Button color="warning" onClick={this.toggleWarning}>Volver</Button>{' '}

              </ModalFooter>
            </Modal>
              {/*MODALES DE NOTIFICACION SI SE REGISTRO CORRECTAMENTE LOS DATOS*/}
              <Modal isOpen={this.state.dangerNoti} toggle={this.toggleDangerNoti}
              className={'modal-danger ' + this.props.className}>
              <ModalHeader toggle={this.toggleDangerNoti}><strong>Advertencia</strong></ModalHeader>
              <ModalBody>
                Los datos NO fueron registrados correctamente, verifique e intente nuevamente.
                    </ModalBody>
              <ModalFooter>
                <Button color="secondary" onClick={this.toggleDangerNoti}>Cerrar</Button>
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.infoNoti} toggle={this.toggleInfoNoti}
              className={'modal-info ' + this.props.className}>
              <ModalHeader toggle={this.toggleInfoNoti}><strong>Correcto</strong></ModalHeader>
              <ModalBody>
                Los datos fueron registrados correctamente.
                    </ModalBody>
              <ModalFooter>
                <Button color="secondary" onClick={this.toggleInfoNoti}>Cerrar</Button>
              </ModalFooter>
            </Modal>
            <br />

            <DropdownItem divider />
            <ListaTolerancia idA={this.props.idA} />


          </CardBody>
        </Card>
      </Col>







    );
  }
}
export default Tolerancias;
