
import React, { Component } from 'react';
import { CardBody, Table, Row, Col, Pagination, PaginationItem, PaginationLink} from 'reactstrap';
//import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

class ListaTratamiento extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listTratamiento: [],
            modal: false,
            page: 1,
            eachPage: 15,
        }
        this.handleClick = this.handleClick.bind(this)

    }


    async componentDidMount() { //ciclo de vida de react de forma asincrona
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'odontologia/tratamiento/v1/listado/afiliado/' + this.props.idA + '/');
            const listTratamiento = await respuesta.json();
            this.setState({
                listTratamiento
            });
        } catch (e) {
            console.log(e);
        }
    }
    handleClick(event) {
        this.setState({
            page: Number(event.target.id)
        });
    }
    render() {
        
        /**************BEGIN PAGINACION (TRATAMIENTO LIST)***********/
        const { page, eachPage } = this.state;

        const last = page * eachPage;
        const first = last - eachPage;
        const listaMed = this.state.listTratamiento.slice(first, last);

        //lista
        const listT = listaMed.map(item => (
            <tr key={item.id}>

                <td style={{ textAlign: 'center', width: '100px' }}>{item.fecha_atencion}</td>
                <td style={{ width: '10px', textAlign: 'center' }}>{item.pieza_tratada}</td>
                <td style={{ width: '300px', height: '50px', maxWidth: '301px', maxHeight: '100px' }}>{
                    item.diagnostico.map(d => (
                        <ul key={d.codigo}>
                            <li>{d.codigo + '-' + d.descripcion}</li>
                        </ul>
                    ))
                }</td>
                <td style={{ width: '200px', height: '50px', maxWidth: '301px', maxHeight: '100px' }}>{item.tratamiento_realizado}</td>
                <td style={{ width: '200px', height: '50px', maxWidth: '301px', maxHeight: '100px' }}>{item.observaciones}</td>
                <td style={{ textAlign: 'center' }}>{item.medico.nombres + ' ' + item.medico.apellido_paterno}</td>
            </tr>
        ));
        //para los numeros
        const pages = [];
        for (let i = 1; i <= Math.ceil(this.state.listTratamiento.length / eachPage); i++) {
            pages.push(i);
        }

        const renderpages = pages.map(number => {
            if (number % 2 === 1) {
                return (
                    <PaginationItem key={number} active>
                        <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                    </PaginationItem>
                );
            }
            else {
                return (
                    <PaginationItem key={number}>
                        <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                    </PaginationItem>
                );
            }
        });
        /************END PAGINACION (TRATAMIENTO LIST)**********/

        return (
            <CardBody>
                <h4 style={{ textAlign: 'center' }}><strong>TRATAMIENTO ODONTOLÓGICO</strong></h4>
                <Table responsive hover bordered size="sm" style={{ width: '100%' }}>
                    <thead>
                        <tr>
                            <th scope="col" style={{ textAlign: 'center', width: '100px' }}>Fecha de atención</th>
                            <th scope="col" style={{ width: '10px' }}>Pieza Dentaria</th>
                            <th scope="col" style={{ textAlign: 'center', width: '300px', maxWidth: '301px' }}>Diagnostico</th>
                            <th scope="col" style={{ textAlign: 'center', width: '200px', maxWidth: '301px' }}>Tratamiento realizado</th>
                            <th scope="col" style={{ textAlign: 'center' }}>Observaciones</th>
                            <th scope="col">Doctor</th>

                        </tr>
                    </thead>
                    <tbody>
                        {listT}
                    </tbody>
                </Table>
                <Row>
                    <Col md="10">
                        <Pagination>
                            {renderpages}
                        </Pagination>
                    </Col>             
                </Row>
            </CardBody>
        );
    }
}
/**
  <td style={{ textAlign: 'center', width: '100px' }}>{item.id}</td>
  <BootstrapTable
                    data={listT}
                    pagination>
                    <TableHeaderColumn>ID</TableHeaderColumn>
                    <TableHeaderColumn>Fecha de atención</TableHeaderColumn>
                    <TableHeaderColumn>Pieza Dentaria</TableHeaderColumn>
                    <TableHeaderColumn>Diagnostico</TableHeaderColumn>
                    <TableHeaderColumn>Tratamiento realizado</TableHeaderColumn>
                    <TableHeaderColumn>Observaciones</TableHeaderColumn>
                </BootstrapTable>
 *   <td> <Button outline color="warning" >Editar</Button></td>
 * <th scope="col">Acción</th>
 */
export default ListaTratamiento;