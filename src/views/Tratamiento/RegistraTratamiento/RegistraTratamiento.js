import React, { Component } from 'react';
import Search from 'react-search-box';
import { Collapse, Card, CardBody, Container, CardHeader, Col, Row, Table, Form, FormGroup, Label, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
//import { AppSwitch } from '@coreui/react'
import ListaTratamiento from './ListaTratamiento';
import ListaMotivoConsultaSE from '../../PrimeraConsulta/MotivoConsulta/ForFirstYear/ListarMotivoConsultaSinEditar';
import MotivoConsultaSinLista from '../../PrimeraConsulta/MotivoConsulta/ForFirstYear/MotivoConsultaSinLista';
//import Ficha from '../FichaAtencionOdontologica/Ficha';

var url = process.env.REACT_APP_API_URL + 'odontologia/tratamiento/v1/lista/';
var diag;
var options = {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
};
var d = new Date();
var time = d.toLocaleString('es-bo', options);
var date = time.split(' ')[0].split('/');
time = time.split(' ')[1].split(':');

var fecha_en = date[2] + '-' + date[1] + '-' + date[0];
var fecha_show = date[0] + '-' + date[1] + '-' + date[2];
var yearServer = date[2];



//var dateFull = new Date(date[2], date[1], date[0], time[0], time[1], time[2]);
//var fecha_en = dateFull.getFullYear() + '-' + dateFull.getMonth() + '-' + dateFull.getDate();  /*Date to send */
//var fecha_show = dateFull.getDate() + '-' + dateFull.getMonth() + '-' + dateFull.getFullYear();  /*Date to send */
//var yearServer = dateFull.getFullYear();
//var timeS = dateFull.getHours() + ':' + dateFull.getMinutes() + ':' + dateFull.getSeconds();  /**Time to send */



/*console.log('--> d:  ', d)
console.log('--> time:  ', time)
console.log('--> date:  ', date)
console.log('--> date --- > year :  ', date[2])
console.log('--> fecha_en:  ', fecha_en)
console.log('--> fecha_show:  ', fecha_show)
console.log('--> yearServer:  ', yearServer)*/

class Tratamiento extends Component {
    constructor(props) {
        super(props);

        this.state = {
            diagnosticos_v: [],


            pieza_dentaria_v: [],
            list_diagnostico: [],
            list_tieneMotivoConsulta: [],

            //para el let
            idD: '',
            codigo: '',
            diagnostico_list: '',
            diagnostico_table: [],

            //nombre de los campos de la BD
            diagnostico: [],
            tratamiento_realizado: '',
            pieza_tratada: '',
            observaciones: '',
            medico: '',
            afiliado: '',
            fecha_atencion: '',
            swAdiciona: false,

            primary: false,
            warning: false,
            valueT: '',
            valueO: '',
            dangerNoti: false,
            infoNoti: false,
            idA: this.props.match.params.id,
            nombres: this.props.match.params.nombres,
            apellido_pat: this.props.match.params.apellido_pat,
            apellido_mat: this.props.match.params.apellido_mat,

            collapse: true,
            swMuestraFormMotivoConsulta: false,
            idMedico:localStorage.getItem("idMedico"),
        }

        this.handleAdiciona = this.handleAdiciona.bind(this);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.handleChangePiezaDentaria = this.handleChangePiezaDentaria.bind(this);
        this.togglePrimary = this.togglePrimary.bind(this);
        this.toggleWarning = this.toggleWarning.bind(this);
        this.toggle = this.toggle.bind(this);

        this.handleChangeLetter = this.handleChangeLetter.bind(this);
        this.handleChangeLetter2 = this.handleChangeLetter2.bind(this);
        this.toggleDangerNoti = this.toggleDangerNoti.bind(this);
        this.toggleInfoNoti = this.toggleInfoNoti.bind(this);

    }
    toggleDangerNoti() {
        this.setState({
            danger: !this.state.danger,
        });
        window.location.reload()
    }

    toggleInfoNoti() {
        this.setState({
            info: !this.state.info,
        });
        window.location.reload()
    }


    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }
    handleChangeLetter(event) {
        this.setState({ valueT: event.target.value.toUpperCase() });
    }

    handleChangeLetter2(event) {
        this.setState({ valueO: event.target.value.toUpperCase() });
    }
    togglePrimary() {
        if (this.state.diagnostico !== [] && this.tratamiento_realizado.value !== '' && this.state.pieza_tratada !== '' &&
            this.state.idA !== '' && fecha_en !== '') {
            this.setState({ primary: !this.state.primary, });
        } else {
            this.setState({ warning: !this.state.warning, });

        }
    }

    toggleWarning() {
        this.setState({
            warning: !this.state.warning,
        });
    }

    /**************************************************************DIAGNOSTICO******************************************************** */

    async componentDidMount() { //Lista de diagnosticos
        try {
            const respuesta = await fetch(process.env.REACT_APP_API_URL + 'odontologia/diagnostico/v1/cie10/lista/'); //Para el diagnostico
            const diagnosticos_v = await respuesta.json();

            const respuestaPiezaDentaria = await fetch(process.env.REACT_APP_API_URL + 'odontologia/examenclinico/v1/pieza_dentaria/'); //Para la pieza dentaria
            const pieza_dentaria_v = await respuestaPiezaDentaria.json();

            const respuestaMC = await fetch(process.env.REACT_APP_API_URL + 'odontologia/antecedentes/v1/tieneMotivoConsultJoin/' + this.state.idA + '/');
            const list_tieneMotivoConsulta = await respuestaMC.json();
            var fechaMC = list_tieneMotivoConsulta[0].fecha_motivoConsulta;
            var strArray = fechaMC.split('-');
            var yearLastMotivoConsulta = strArray[0];
            if (yearServer > yearLastMotivoConsulta) {
                this.setState({ swMuestraFormMotivoConsulta: true })
            }

            /*console.log('fechaMC: ', fechaMC)
            console.log('strArray [].lenght: ', strArray.length)
            console.log('strArray []: ', strArray)
            console.log('yearLastMotivoConsulta: ', yearLastMotivoConsulta)
            console.log('--------------------------')
            console.log('yearLastMotivoConsulta: ', yearLastMotivoConsulta)
            console.log('yearServer: ', yearServer)*/


            this.setState({
                diagnosticos_v,
                pieza_dentaria_v,
                list_tieneMotivoConsulta,
            });

        } catch (e) {
            console.log(e);
        }
    }

    handleChangePiezaDentaria(value) {
        console.log(value);

        this.setState({
            pieza_tratada: value
        });

    }

    handleChangeDiagnostico(value) {
        //console.log(value);
        //console.log("data=",this.state.data)

        this.state.diagnosticos_v.map(item => {

            if (value === item.descripcion) {
                //console.log(item.id); 
                fetch(process.env.REACT_APP_API_URL + 'odontologia/diagnostico/v1/busca/cie10_id/' + item.id + '/')
                    .then((Response) => Response.json())
                    .then((findresponse) => {
                        //console.log("FINDRESPONSE[]=",findresponse[0].pr_consulta)
                        // console.log("FINDRESPONSE=", findresponse)

                        this.setState({
                            list_diagnostico: findresponse,
                        })
                        // console.log('LIST=', this.state.list_diagnostico.length)
                        if (this.state.list_diagnostico.length === 0) {
                            this.setState({
                                //hacer un alerta danger
                            })
                            console.log('vacio')
                        }
                        else {

                            diag = {
                                idD: item.id,
                                codigo: item.codigo,
                                diagnostico_list: item.descripcion,
                            };

                            this.setState({
                                swAdiciona: true,

                                //diagnostico: this.state.diagnostico.concat([item.id]),
                            })
                            //console.log('Lleno: this.state.diagnostico***** VAR : ', this.state.diagnostico_table)
                        }

                    });

            }
            return 0;
        }


        )




    }
    /**********************************************************Pieza Dentaria************************************************************ */
    handleAdiciona() {
        this.setState({
            diagnostico_table: this.state.diagnostico_table.concat([diag]),
            diagnostico: this.state.diagnostico.concat([diag.idD]),
            swAdiciona: false,
        })
        // console.log('adicionaaaaa: ', diag.idD)

    }

    fEliminar(i, idD) {
        const nuevo = this.state.diagnostico_table.filter(diagnostico_table => {
            return diagnostico_table !== i
        })
        const nuevo2 = this.state.diagnostico.filter(diagnostico => {
            return diagnostico !== idD
        })



        if (this.state.diagnostico_table.length !== 1) {
            this.setState({
                diagnostico_table: [...nuevo],
            })
        }
        else {
            this.setState({
                diagnostico_table: [...nuevo],
            })
        }




        if (this.state.diagnostico.length !== 1) {
            this.setState({
                diagnostico: [...nuevo2]
            })
        }
        else {
            this.setState({
                diagnostico: [...nuevo2]

            })
        }
    }
    /*********************************************************PARA GUARDAR EM LA BD************************************************************* */

    handleOnSubmit(e) {

        if (this.state.diagnostico !== [] && this.tratamiento_realizado.value !== '' && this.state.pieza_tratada !== '' && this.state.idA !== '' && fecha_en !== '') {
            e.preventDefault()
            // console.log('***this.state.diagnostico[]: ', this.state.diagnostico, ' -this.tratamiento_realizado.value:', this.tratamiento_realizado.value, ' -this.state.pieza_tratada: ', this.state.pieza_tratada, ' - this.observaciones.value: ', this.observaciones.value, ' -this.state.idA', ' -this.state.fecha_atencion.value:', this.state.fecha_atencion.value)
            var options = {
                day: '2-digit',
                month: '2-digit',
                year: 'numeric',
                hour: '2-digit',
                minute: '2-digit',
                second: '2-digit'
            };
            var d = new Date();
            var time = d.toLocaleString('es-bo', options);
            //var date = time.split(' ')[0].split('/');
            time = time.split(' ')[1].split(':');
            var timeS = time[0] + ':' + time[1] + ':' + time[2];

            //var dateFull = new Date(date[2], date[1], date[0], time[0], time[1], time[2]);
            //var timeS = dateFull.getHours() + ':' + dateFull.getMinutes() + ':' + dateFull.getSeconds();  /**Time to send */


            let data = {
                diagnostico: this.state.diagnostico,
                tratamiento_realizado: this.tratamiento_realizado.value,
                pieza_tratada: this.state.pieza_tratada,
                observaciones: this.observaciones.value,
                medico: this.state.idMedico,
                afiliado: this.state.idA,
                fecha_atencion: fecha_en,
                hora: timeS,

            }
            console.log('dataaa: ', data)
            try {
                fetch(url, {
                    method: 'POST', // or 'PUT'
                    body: JSON.stringify(data), // data can be string or {object}!
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                    .then(resp => {
                        if (resp.status === 201) {
                            this.setState({
                                infoNoti: true
                            })
                            //console.log('resp.status', resp.status)
                        } else {
                            //console.log('resp.status ERROR', resp.status)
                            this.setState({
                                dangerNoti: true
                            })
                        }
                    })
                /*.then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(response => console.log('Success:', response));*/
                //window.location.reload();
                this.setState({
                    diagnostico: '',
                    tratamiento_realizado: '',
                    pieza_tratada: '',
                    observaciones: '',
                    medico: '',
                    afiliado: '',
                    fecha_atencion: '',
                });



            } catch (e) {
                console.log(e);
            }
            this.setState({
                primary: !this.state.primary,
            });

        }
    }



    render() {
        //console.log('The PROPS', this.props)
        //console.log('The PROPS', this.state.idA)
        //console.log('listDiG=', this.state.diagnosticos_v)
        //console.log('DIENTES=', this.state.pieza_dentaria_v)
        // console.log('------------>', this.state.pieza_tratada);

        //console.log(' ********this.state.pieza_tratada: ', this.state.pieza_tratada)
        //console.log('diagnostico_table[] : ', this.state.diagnostico_table);

        //console.log('diagnostico[]: ', this.state.diagnostico)


        /*console.log('fecha_server:', fecha_server);
        console.log('dia:', dia)
        console.log('mes:', mes)
        console.log('año:', year)
        console.log('fecha enviar:', fecha_en)

*/        //console.log('list_tieneMotivoConsulta: ', this.state.list_tieneMotivoConsulta)

        return (
            <div className="animated fadeIn">
                {(this.state.swMuestraFormMotivoConsulta === true) ?
                    <MotivoConsultaSinLista idA={this.state.idA} apellido_pat={this.state.apellido_pat} apellido_mat={this.state.apellido_mat} nombres={this.state.nombres} /> :
                    <div>
                        <Button color="link" onClick={this.toggle} style={{ 'marginBottom': '1rem' }}>{(this.state.collapse) ? 'Ocultar motivo de consulta' : 'Ver motivo de consulta'}   </Button>
                        <Collapse isOpen={this.state.collapse}>
                            <ListaMotivoConsultaSE idA={this.state.idA} apellido_pat={this.state.apellido_pat} apellido_mat={this.state.apellido_mat} nombres={this.state.nombres} />
                        </Collapse>
                    </div>

                }

                {
                    (this.state.swMuestraFormMotivoConsulta === false) ?
                        <Row>
                            <Col xs="12">
                                <h3><p align="center">TRATAMIENTO</p></h3>
                                <Card>
                                    <CardHeader>
                                        <i className="fa fa-align-justify"></i><strong>Registro de Tratamiento del Paciente</strong>

                                        <div className="card-header-actions">
                                            <strong className="text-muted">Paciente: {this.state.nombres + ' ' + this.state.apellido_pat + ' ' + this.state.apellido_mat}</strong>
                                        </div>

                                    </CardHeader>
                                    <CardBody>
                                        <Container>
                                            <Row>

                                                <Col sm="12" md={{ size: 8, offset: 2 }}>
                                                    <Label for="Diagnostico" className="mr-sm-2">Diagnostico</Label>
                                                    <Search
                                                        data={this.state.diagnosticos_v}
                                                        onChange={this.handleChangeDiagnostico.bind(this)}
                                                        placeholder="Diagnostico CIE10"
                                                        name="buscaDiagnostico"
                                                        class="search-class"
                                                        searchKey="descripcion"
                                                    />

                                                </Col>
                                                {(this.state.swAdiciona === true) ? <Button color="link" onClick={this.handleAdiciona} >Añadir</Button> : null}

                                            </Row>
                                            <br />
                                            <Table responsive hover bordered size="sm" style={{ width: '100%', textAlign: 'center' }}>
                                                <thead>
                                                    <tr>
                                                        <th >nro</th>
                                                        <th>codigo</th>
                                                        <th>Descripción</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    {this.state.diagnostico_table.map((u, i) => {
                                                        return (
                                                            <tr key={i}>
                                                                <td style={{ maxWidth: '5px' }}>{i + 1}</td>
                                                                <td style={{ maxWidth: '50px' }}>{u.codigo}</td>
                                                                <td style={{ maxWidth: '50px' }}>{u.diagnostico_list}</td>
                                                                <td style={{ maxWidth: '50px' }}>
                                                                    {/* <Button onClick={(e)=>this.fEditar(i)} type="button">Editar</Button> */}
                                                                    <Button onClick={(e) => this.fEliminar(u, u.idD)} color="danger" type="button">Eliminar</Button></td>
                                                            </tr>
                                                        )
                                                    })}

                                                </tbody>
                                            </Table>
                                        </Container>
                                        <br />
                                        <hr />
                                        <br />
                                        <Form encType="multipart/form-data" className="form-horizontal">
                                            <FormGroup row>
                                                <Col xs="12" md="6">
                                                    <Row>
                                                        <Col md="2">
                                                            <Label for="fecha" className="mr-sm-2">Fecha</Label>
                                                        </Col>
                                                        <Col xs="12" md="7">
                                                            <input type="text" className="form-control" value={fecha_show} style={{ width: '150px' }} disabled />
                                                        </Col>
                                                    </Row>
                                                </Col>

                                                <Col xs="12" md="6">
                                                    <Row>
                                                        <Col md="3">
                                                            <Label for="Pieza Dentaria" className="mr-sm-2">Pieza Dentaria</Label>
                                                        </Col>
                                                        <Col xs="12" md="3">


                                                            <Search
                                                                data={this.state.pieza_dentaria_v}
                                                                onChange={this.handleChangePiezaDentaria.bind(this)}
                                                                maxLength="3"
                                                                placeholder="#.#"
                                                                className="form-control"
                                                                searchKey="nro_pieza"
                                                            />

                                                        </Col>
                                                    </Row>
                                                </Col>




                                                <Col xs="12" md="6">
                                                    <Row>
                                                        <Col md="3">
                                                            <Label for="exampleText" className="mr-sm-2">Tratamiento Realizado</Label>
                                                        </Col>
                                                        <Col xs="12" md="3">
                                                            <textarea type="textarea" style={{ width: '20rem', height: '10rem', marginTop: '1000erm' }} placeholder="Tratamiento Realizado" value={this.state.valueT} onChange={this.handleChangeLetter} name="tratamientoRealizado" id="tratamientoRealizado" ref={tratamiento_realizado => this.tratamiento_realizado = tratamiento_realizado} />
                                                        </Col>
                                                    </Row>
                                                </Col>
                                                <Col xs="12" md="6">
                                                    <Row>
                                                        <Col md="3">
                                                            <Label for="exampleText" className="mr-sm-2">Observación</Label>
                                                        </Col>
                                                        <Col xs="12" md="3">
                                                            <textarea type="textarea" style={{ width: '20rem', height: '10rem' }} placeholder="Observaciones" name="observacion" id="observacion" value={this.state.valueO} onChange={this.handleChangeLetter2} ref={observaciones => this.observaciones = observaciones} />
                                                        </Col>
                                                    </Row>
                                                </Col>



                                            </FormGroup>




                                            <Row>
                                                <Col sm="12" md={{ size: 8, offset: 10 }}>
                                                    <Button color="success" onClick={this.togglePrimary} className="mr-1">Guardar</Button>
                                                </Col>
                                            </Row>

                                        </Form>
                                        <hr />
                                        <br />
                                        <ListaTratamiento idA={this.state.idA} />

                                    </CardBody>
                                </Card>
                            </Col>
                        </Row> : null
                }

                <Modal isOpen={this.state.primary} toggle={this.togglePrimary}
                    className={'modal-primary ' + this.props.className}>
                    <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
                    <ModalBody>
                        ¿Está seguro de guardar, los datos introducidos?
                                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={this.handleOnSubmit}>Aceptar</Button>{' '}
                        <Button color="secondary" onClick={this.togglePrimary}>Cancelar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
                    className={'modal-warning ' + this.props.className}>
                    <ModalHeader toggle={this.toggleWarning}>Campos vacios</ModalHeader>
                    <ModalBody>
                        Llene los campos faltantes, para registrar el tratamiento realizado al paciente.
                                    </ModalBody>
                    <ModalFooter>
                        <Button color="warning" onClick={this.toggleWarning}>Volver</Button>{' '}

                    </ModalFooter>
                </Modal>

                {/*MODALES DE NOTIFICACION SI SE REGISTRO CORRECTAMENTE LOS DATOS*/}
                <Modal isOpen={this.state.dangerNoti} toggle={this.toggleDangerNoti}
                    className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDangerNoti}><strong>Advertencia</strong></ModalHeader>
                    <ModalBody>
                        Los datos NO fueron registrados correctamente, verifique e intente nuevamente.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleDangerNoti}>Cerrar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.infoNoti} toggle={this.toggleInfoNoti}
                    className={'modal-info ' + this.props.className}>
                    <ModalHeader toggle={this.toggleInfoNoti}><strong>Correcto</strong></ModalHeader>
                    <ModalBody>
                        Los datos fueron registrados correctamente.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleInfoNoti}>Cerrar</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}
/**
 * ref={/*fecha_atencion => this.state.fecha_atencion = fecha_atencion}
 */
export default Tratamiento;
