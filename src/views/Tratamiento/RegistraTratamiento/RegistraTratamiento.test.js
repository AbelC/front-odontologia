import React from 'react';
import ReactDOM from 'react-dom';
import RegistraTratamiento from './RegistraTratamiento';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RegistraTratamiento />, div);
  ReactDOM.unmountComponentAtNode(div);
});
