import React from 'react';
import ReactDOM from 'react-dom';
import Tratamiento from './Tratamiento';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Tratamiento />, div);
  ReactDOM.unmountComponentAtNode(div);
});
